---
author: SK
tags: 2017, foss
timestamp: 22:45:00
title: Pune GNU/Linux Users Group ClipArt Hackathon 2017
---

I had a chance to attend the <a href="http://www.plug.org.in/">Pune
GNU/Linux Users Group (PLUG)</a> ClipArt Hackathon on Sunday, March
12, 2017 at <a href="https://www.bprim.org/">Bhaskaracharya
Pratishthana</a>. The hackathon was an initiative to start organizing
different kinds of F/OSS events.

<a href="http://www.opensourcecook.in/trainers">Gaurav ("dexter")
Pant</a> started the day's proceedings with a quick demo of <a
href="https://inkscape.org/en/">Inkscape</a>. He also provided the
participants with an Inkscape quick reference sheet. The advantage of
creating Scalable Vector Graphics (SVG) is that it can be used on a
web page as well as enlarged for printing on posters and banners,
without any distortion.

I took this time to create some illustrations. The first image was a
logo with the letter "S", whose shape was modified to fit into a
boundary. The <a
href="https://openclipart.org/detail/202673/stick-figure-pushing">stick-figure-push</a>
SVG is already available in <a
href="https://openclipart.org/">Openclipart</a>. I modified it for a
stick-figure-pull image. Few icons were then created for use in web
pages. Finally, I completed an SVG of <a
href="https://en.wikipedia.org/wiki/The_Common_Man">"The Common
Man"</a>.

<img alt="S logo" src="http://shakthimaan.com/gallery/2017/plug-clipart-hackathon-apr-5-2017/S-logo.svg" /><br /><br />

<img alt="Stick figure pull" src="http://shakthimaan.com/gallery/2017/plug-clipart-hackathon-apr-5-2017/Stick-figure-pull.svg" /><br /><br />

<img alt="Envelope icon" width="384" src="http://shakthimaan.com/gallery/2017/plug-clipart-hackathon-apr-5-2017/Envelope-icon.svg" /><br /><br />

<img alt="Monitor icon" width="384" src="http://shakthimaan.com/gallery/2017/plug-clipart-hackathon-apr-5-2017/Monitor-icon.svg" /><br /><br />

<img alt="Telephone icon" width="384" src="http://shakthimaan.com/gallery/2017/plug-clipart-hackathon-apr-5-2017/Telephone-icon.svg" /><br /><br />

<img alt="The Common Man" width="384" src="http://shakthimaan.com/gallery/2017/plug-clipart-hackathon-apr-5-2017/Common-man.svg" /><br /><br />

All the images are available in Creative Commons Attribution-Share
Alike 4.0 International license at Wikimedia ( <a
href="https://commons.wikimedia.org/w/index.php?title=Special:ListFiles/Shakthimaan">https://commons.wikimedia.org/w/index.php?title=Special:ListFiles/Shakthimaan</a>
).

The hackathon was a good initiative to encourage creative
contributions to F/OSS. I was happy to see a number of excellent
designers who were creating beautiful SVGs. Participants used both
GIMP and Inkscape for their work. It was also good to have met the
PLUG members.

<img alt="Group photo" src="http://shakthimaan.com/gallery/2017/plug-clipart-hackathon-apr-5-2017/plug-clipart-hackathon-march-12-2017.png" /><br />
PC: Siddharth Subramaniam

Such events help grow the activities in a region, and also showcases
the diversity in a F/OSS group.
