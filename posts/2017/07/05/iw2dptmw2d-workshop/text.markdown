---
author: SK
tags: 2017, travel, FOSS
timestamp: 13:45:00
title: "i want 2 do project. tell me wat 2 do" workshop, reserved-bit, Pune
---

I had organized a one-day workshop based on my
book
[i want 2 do project. tell me wat 2 do](http://www.shakthimaan.com/what-to-do.html) at
[reserved-bit](https://reserved-bit.com), Pune on
Saturday, June 3, 2017. Thanks
to [Nisha Poyarekar](https://twitter.com/Nisha_Poyarekar)
and [Siddhesh Poyarekar](https://twitter.com/siddhesh_p) for providing
their makerspace as venue for the workshop.

<img alt="Workshop in progress" width="400" src="http://shakthimaan.com/images/2017/book-workshop-reserved-bit/workshop-in-progress.png"></img>

The objective of the workshop is to share the methodology and best
practices on working with Free and Open Source Software projects, and
also to guide the participants on career options. Although there is
plenty of free documentation (including my own) available on the
subject, some people prefer a formal, systematic approach to learning
in a private coaching environment. Hence, this workshop is tailored
for the same and is made a paid workshop, similar to personal
tutoring.

The book has been in circulation for many years. So, I had to give two
options - pay only for the workshop, or pay for both the workshop and
the book (if they have not already bought the book). I have also kept
additional copies of my book at the reserved-bit makerspace if people are interested. I had covered the following topics:

1.  Careers with F/OSS
2.  "i want 2 do project. tell me wat 2 do" best practices
3.  Introduction to Git
4.  Hands-on problem solving exercises
5.  Real-world project example
6.  Goal-driven development

The feedback has been positive and I am sharing the same below:

<img alt="Feedback 1" width="400" src="http://shakthimaan.com/images/2017/book-workshop-reserved-bit/1-feedback.png"></img><br />

<img alt="Feedback 2" width="400" src="http://shakthimaan.com/images/2017/book-workshop-reserved-bit/2-feedback.png"></img><br />

<img alt="Feedback 3" width="400" src="http://shakthimaan.com/images/2017/book-workshop-reserved-bit/3-feedback.png"></img><br />

<img alt="Feedback 4" width="400" src="http://shakthimaan.com/images/2017/book-workshop-reserved-bit/4-feedback.png"></img><br />

<img alt="Feedback 5" width="400" src="http://shakthimaan.com/images/2017/book-workshop-reserved-bit/5-feedback.png"></img><br />

If you are interested in attending such a workshop, do write to me:
author at shakthimaan dot com.

