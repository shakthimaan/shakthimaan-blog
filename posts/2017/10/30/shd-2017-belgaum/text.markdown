---
author: SK
tags: 2017, travel
timestamp: 16:45:00
title: Science Hack Day India 2017, Belgaum
---

# Day 0

I arrived at [Belgaum](https://en.wikipedia.org/wiki/Belgaum) on
Friday, October 13, 2017 morning by train, and headed to the [Sankalp
Bhumi](http://sankalphospitality.in/) resort. I was given the [Rock
Cottage
Dhyan](http://sankalphospitality.in/lens_portfolio/cottages-tapas-dhyan)
("meditation") for my stay. After freshening up and having breakfast,
I started to explore the resort. The place was an abandoned stone
quarry that has been converted into a beautiful resort spread over an
area of eight acres. It is a very quiet, peaceful place with trees,
lawns, and water bodies. A number of fellow hackers and mentors also
started arriving at the venue, and we were just getting ready for the
day's events.

<img alt="Banner"
src="https://www.shakthimaan.in/gallery/upload/2017/10/30/20171030125247-b8bee464.jpg"></img>

After lunch, we headed to the large function hall where volunteers
were to be trained on soldering. [Jithin B. P.](http://jithinbp.in/)
had prepared a simple PCB layout with LEDs and potentiometers for
volunteers to practice soldering. These volunteers will help school
students in the soldering workshops in the coming two days at the
event.

<img alt="Training volunteers for soldering"
src="https://www.shakthimaan.in/gallery/upload/2017/10/30/20171030125315-12487dbe.jpg"></img>

In the evening, I had suggested to [Praveen
Patil](https://www.gnovi.in/) and [Hitesh
Dharmdasani](http://mason.gmu.edu/~hdharmda/) to have an informal BoF
session. So, everyone got together under one roof, and Hitesh
explained how and why they started SHD. There were a round of
introductions followed by open discussions. After dinner, some of our
fellow hackers headed to the hackerspace ("cafeteria") to test the
Wi-Fi, and prepare for the next day's hacks.

# Day 1

The volunteers arrived in the morning to ready the registration desk
and prepare the name cards. Science Hack Day banners were placed at
various locations in the premises. After breakfast, everyone gathered
at the function hall for an informal inauguration. This was followed
by a 30-minute introduction on Aeromodelling by Mr. Arjun Bilawar
(retired trainer, NCC Karnataka and Goa Directorate). It had rained
heavily last evening and hence a nearby playground was not suitable to
demo the aeroplane. Mr. Arjun then took us to a nearby open area, and
with a short runway, he was able to lift off the model plane, and flew
it for few minutes to the loud cheers from the students.

<img alt="Aeromodelling session"
src="https://www.shakthimaan.in/gallery/upload/2017/10/30/20171030125332-3759fc1e.jpg"></img>

We then headed to the hackerspace to start work on our
projects. [Siddhesh](https://siddhesh.in/) and other hackers began to
build a 3D printer. Shreyas K was building an image detector to be
used in his college to detect students jumping off the wall in his
college premises. This was covered in the news, [Tarun
Bharat](http://epaper.tarunbharat.com/m5/1395867/Tarun-Bharat/BELGAUM#page/7/1).

<img alt="Ganesh Kumar and Shreyas K"
src="https://www.shakthimaan.in/gallery/upload/2017/10/30/20171030125344-f0fbe7a5.jpg"></img>

Students from Kerala were working on a Bat detector. Few students were
creating 3D artefacts using the 3D pen from
[reserved-bit](https://reserved-bit.com/). Hitesh Dharmdasani had
brought his company's ([Informant
Networks](https://informantnetworks.com/)) Banana Pi based-router for
hacking. [Vaishali Thakkar](http://vaishalithakkar.in/) was working on
the [Tessel](https://tessel.io/) board and
[Rust](https://www.rust-lang.org/en-US/). I was setting up the
[Icoboard](http://icoboard.org/) with [Raspberry Pi 3 Model
B](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/) to
demonstrate chip design on free and open source hardware. 

The school students were divided into two groups. One group attended
the soldering workshop in the function hall, and the other group
participated in the science toys workshop in an open arena. The groups
will swap the workshops the following day. In another sit out area,
biology students had brought specimens from the labs to showcase
them. A team of architects were also creating a geodesic dome that can
be used as a makerspace.

In the evening at 1830 IST, we had lightning talks, and I had
presented the latest version of ["Nursery
Rhymes"](http://shakthimaan.com/downloads.html#nursery-rhymes). There
were other interesting talks on embedded hardware, security, product
design, mechanics of a bicycle, biogas etc. It was a long day!

<img alt="Gautam Samant"
src="https://www.shakthimaan.in/gallery/upload/2017/10/30/20171030125405-619f7e62.jpg"></img>

# Day 2

The second day of the event began with a video conferencing
session with [Science Hack Day, San
Francisco](http://sf.sciencehackday.org/). Their event was scheduled
on October 14-15, 2017 at the GitHub office. [Ariel
Waldman](http://arielwaldman.com/) used the webcam to show around
their hackerspace. The students then headed to their respective
soldering and science toys workshops. Some of the toys and experiments
were from [Arvind
Gupta](http://www.arvindguptatoys.com/toys.html). The hackers at the
makerspace continued working on their projects. All the hacks had to
be completed by noon, as they have to be demoed and voted for in the
evening.

<img alt="Science toys workshop"
src="https://www.shakthimaan.in/gallery/upload/2017/10/30/20171030125428-b24e02c2.jpg"></img>

After lunch, there was a rocket session where students launched
rockets prepared by experts. We also had a group photo session, and
headed to the function hall. All the hacks were presented to the
students, and parents were also invited to attend the evening
session. There was also an audience voting poll to choose their
favourite science hack. After demonstrating the Icoboard, I had to
rush to catch the train back to Bengaluru, so I did miss the closing
ceremony.

<img alt="Group photo" src="https://www.shakthimaan.in/gallery/upload/2017/10/30/20171030125449-15c704c9.jpg"></img>

My hearty congratulations to the organizers of Science Hack Day India
2017, Belgaum for a wonderful event that motivates students to learn
and experiment with science. 

I have uploaded over 80 photos during the trip to my new
[gallery](https://www.shakthimaan.in/gallery/index.php?/category/24). Do
browse through them leisurely!
