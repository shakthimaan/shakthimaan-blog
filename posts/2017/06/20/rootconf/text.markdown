---
author: SK
tags: 2017, documentation, foss
timestamp: 14:30:00
title: Rootconf 2017
---

# Introduction

I attended [Rootconf 2017](https://rootconf.in/2017/) on May 11-12,
2017 at [MLR Convention Centre, JP Nagar](http://mlr.in/mlr-convention-centre-jp-nagar/mlr-jpnagar.html), Bengaluru.

<img alt="Sponsors" width="400" src="http://shakthimaan.com/images/2017/rootconf/2-sponsors.jpg"></img>

The event had two parallel tracks, the Rootconf track at the main
conference hall, and the DevConf.in track sponsored by Red Hat. A
number of infrastructure hosting companies had sponsored the
event. The selected talks had good diversity with topics ranging from
monitoring, databases, networking, security etc.

# Day I

The day's proceedings began with the talk
on the ["State of open source monitoring landscape"](https://rootconf.talkfunnel.com/2017/63-state-of-the-open-source-monitoring-landscape) by
[Bernd Erk](https://twitter.com/gethash). He gave a good overview of
the available monitoring tools. He is involved in
the [Icinga2](https://www.icinga.com/products/icinga-2/) project,
which is re-written in C++ for performance. It is an alternative for
the Nagios tool.

[Aditya Patawari](http://adityapatawari.com/) then provided an
introduction to Kubernetes with a demo, as Spencer Krum could not make
it to his talk on the "Anatomy of an alert". After a short break, I
attended
["A little bot for big cause"](https://rootconf.talkfunnel.com/2017/7-a-little-bot-for-big-cause) session by
[Pooja Shah](https://p00j4.github.io/). The talk basically
demonstrated how to create a bot by integrating various APIs.

I attended [Manan Bharara's](https://twitter.com/mananbharara) talk
on
["Monitoring performance critical applications"](https://speakerdeck.com/manan/necessary-tooling-and-monitoring-for-performance-critical-applications-rootconf-2017) where
he had explained how they had used Clojure for monitoring at Otto in Germany. The presentation was very good, and
the [Oscillator](https://github.com/otto-de/oscillator) Clojure code
is available. He had provided good illustrations and code snippets in
his presentation.

<img alt="Manan Bharara' oscillator visualization" src="http://shakthimaan.com/images/2017/rootconf/1-manan-visualization.png"></img>

A number of stalls were available in the hallway, and I spent
considerable time talking with the sponsors. At the Red Hat stall, I
learnt some of the features and use cases for the [Foreman](https://www.theforeman.org/) provisioning
tool. I was pictured with the Foreman helmet:

<img alt="Shakthimaan with Foreman helmet" height="400" src="http://shakthimaan.com/images/2017/rootconf/4-shakthimaan-foreman.png"></img>

After lunch, I
attended
["SELinux: A deep dive"](https://rootconf.talkfunnel.com/devconf-2017/82-selinux-a-deep-dive) by
[Rejy M Cyriac](https://fedoraproject.org/wiki/User:Rejymc). He gave
an excellent introduction to SELinux, explaining the underlying
concepts and rationale for the same. We also did basic hands-on
exercises, but, I could not attend the last section as I had to move
to the main conference hall for the flash talks. I sang the Nursery
Rhymes version for DevOps. The <a href="https://www.youtube.com/watch?v=87_1FCiHkK4#t=27m15s">video</a> is available.

We then headed to the Rootconf party sponsored by Go-Jek!

# Day II

The second day started with the talk
by [Kunal Grover](https://crondev.wordpress.com/)
on
["Failure resilient architecture with microservice dependencies"](https://rootconf.talkfunnel.com/2017/29-failure-resilient-architecture-with-microservice-d). He
presented the ways by which disruptions are performed in cloud
infrastructure and how recovery mechanisms are tested. [Ramya A](https://twitter.com/atramya) then gave an
interesting talk
on
["Asynchronous integration tests for microservices"](https://rootconf.talkfunnel.com/2017/64-asynchronous-integration-tests-for-microservices). She
had explained the [pact.io](https://docs.pact.io/) family of
frameworks to support Consumer Driven Contracts testing. Laxmi
Nagarajan then gave a high level overview
of
["Capacity planning for AWS and configuring AWS autoscaling policies"](https://rootconf.talkfunnel.com/2017/34-a-quick-how-to-on-capacity-planning-for-an-applica) sharing
her Industry experience. After a short break, [Colin Charles](http://www.bytebot.net/)
presented his story on ["Capacity planning for your data stores"](https://rootconf.talkfunnel.com/2017/65-capacity-planning-for-your-data-stores) citing
real world examples.

I then moved to the DevConf.in track to
attend [Ruchi Singh's](https://github.com/ruchi15) talk
on
["Migration of 300 microservices from AWS cloud to Snapdeal cloud"](https://rootconf.talkfunnel.com/devconf-2017/88-migration-of-300-microservices-from-aws-cloud-to-s). The
time was too short for such a talk, and it was an overview. I would
have liked to see more details, given that they use Chef for
provisioning and handle more than 16 TB of data as mentioned by Ruchi
Singh.

After a quick lunch, I
attended
["Adventures in Postgres management"](https://rootconf.talkfunnel.com/2017/1-adventures-in-postgres-management) by
[Ramanan Balakrishnan](https://github.com/ramananbalakrishnan). It was
a good technical talk going over the failures and experiences
learnt. After the talk I went to the hallway to listen to the
Off-The-Record (OTR) session on mistakes to avoid when planning your
infrastructure.

<img alt="OTR session" width="400" src="http://shakthimaan.com/images/2017/rootconf/3-OTR-session.jpg"></img>

I returned to the main conference hall for doing some stretching
exercises. In my opinion, all conferences should make this session
mandatory, especially after you have been sitting for a long period of time. This session was then followed by
the ["Working with Secrets"](https://rootconf.talkfunnel.com/2017/58-working-with-secrets) talk
by [Shrey Agarwal](https://github.com/shreyagarwal), who gave an
overview of using Hashicorp's Vault for managing passwords. It was a
very short introduction on the topic.

After a short beverage
break, [Toshaan Bharvani](http://www.toshaan.com/)
presented
["Living with SELinux"](https://rootconf.talkfunnel.com/2017/50-living-with-selinux),
which was an excellent presentation on the subject. The initial slides had
some material that Rejy M Cyriac had introduced the previous day, but,
the content and presentation were good. With proper SELinux policies,
he said that root in his production servers cannot do much. Rejy and
Toshaan both asked people to use permissive mode instead of disabling
SELinux altogether, so that you at least know what is being audited.

The last talk of the day was
by [Anurag Bhatia](https://anuragbhatia.com/)
on
["Understanding eyeball routing via RIPE Atlas"](https://rootconf.talkfunnel.com/2017/76-understanding-eyeball-routing-via-ripe-atlas). He
gave an overview of the [RIPE Atlas](https://atlas.ripe.net/) project
and how network metrics are measured and monitored wherever the RIPE device
is installed. It is a TP-Link hardware whose firmware can be flashed
using Libre software. Unfortunately, at this point the source of the
firmware is not released as Free and Open Source Software. I was told
that there is still an ongoing discussion on the same.

# Conclusion

The talk by Manan Bharara and Rejy's SELinux workshop were the
highlights for day one for me. The content on the second day was much
better and had greater depth in my opinion. Overall, it was a useful
technical conference, and a good place to meet like-minded people.

I would like to thank [Aerospike](http://www.aerospike.com/) for
sponsoring me to attend the conference.
