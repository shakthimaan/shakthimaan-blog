---
author: SK
tags: 2017, devops
timestamp: 19:30:00
title: Ansible deployment of Cacti for Monitoring
---
*[Published in Open Source For You (OSFY) magazine, May 2017 edition.]*

# Introduction

In this third article in the DevOps series, we will install and set up
Cacti, a free and open source Web-based network monitoring and
graphing tool, using Ansible. Cacti is written in PHP and uses the
MySQL database as a backend. It uses the RRDtool (Round-Robin Database
tool) to handle time series data and has built-in SNMP support. Cacti
has been released under the GNU General Public License.

# Setting up Cacti

We will use a CentOS 6.8 virtual machine (VM) running on KVM to setup
Cacti. Just for this demonstration, we will disable SELinux. You will
need to set the following in */etc/selinux/config* and reboot the VM.

    SELINUX=disabled

When using in production, it is essential that you enable SELinux. You
should then test for Internet connectivity from within the VM.

The Ansible version used on the host Parabola GNU/Linux-libre x86_64
is 2.2.1.0. The *ansible/inventory/kvm/* directory structure is shown
below:

    ansible/inventory/kvm/inventory
    ansible/inventory/kvm/group_vars/all/all.yml

The IP address of the guest CentOS 6.8 VM is provided in the inventory
file as shown below:

    centos ansible_host=192.168.122.98 ansible_connection=ssh ansible_user=root ansible_password=password

Add an entry for 'centos' in the /etc/hosts file as indicated below:

    192.168.122.98 centos

The contents of the *all.yml* for use with the playbook are as follows:

    ---
    mysql_cacti_password_hash: "{{ vault_mysql_cacti_password_hash }}"
    
    mysql_username: "{{ vault_mysql_user }}"
    mysql_password: "{{ vault_mysql_password }}"

The cacti.yml playbook is located in the
*ansible/playbooks/configuration* folder.

# Vault

Ansible provides the Vault feature, which allows you to store
sensitive information like passwords in encrypted files. You can set
the EDITOR environment variable to the text editor of your choice, as
shown below:

    $ export EDITOR=nano

In order to store our MySQL database credentials, we will create a
*vault.yml* file as indicated below:

    $ ansible-vault create inventory/kvm/group_vars/all/vault.yml

Provide a password when prompted, following which, the Nano text
editor will open. You can enter the following credentials and save the
file.

    ---
    vault_mysql_cacti_password_hash: "*528573A4E6FE4F3E8B455F2F060EB6F63ECBECAA"
    
    vault_mysql_user: "cacti"
    vault_mysql_password: "cacti123"

You can edit the same file, if you wish, using the following command:

    $ ansible-vault edit inventory/kvm/group_vars/all/vault.yml

It will prompt you for a password, and on successful authentication,
your text editor will open with the decrypted file contents for
editing.

# Apache

Cacti has many dependency packages, and the first software that we
will install is the Apache HTTP server.

    ---
    - name: Install web server
      hosts: centos
      gather_facts: true
      tags: [httpd]
    
      tasks:
        - name: Update the software package repository
          yum:
    	name: '*'
    	update_cache: yes
    
        - name: Install HTTP packages
          package:
    	name: "{{ item }}"
    	state: latest
          with_items:
    	- wget
    	- nano
    	- httpd
    	- httpd-devel
    
        - name: Start the httpd server
          service:
    	name: httpd
    	state: started
    
        - wait_for:
    	port: 80

A 'yum update' is first performed to sync with the package
repositories. The *httpd* Web server and a few other packages are then
installed. The server is started, and the Ansible playbook waits for
the server to listen on port 80.

# MySQL and PHP

The MySQL, PHP and RRDTool packages are then installed, following
which the SNMP and MySQL servers are started as shown below:

    - name: Install MySQL, PHP packages
      hosts: centos
      become: yes
      become_method: sudo
      gather_facts: true
      tags: [database-web]
    
      tasks:
        - name: Install database/web packages
          package:
    	name: "{{ item }}"
    	state: latest
          with_items:
    	- mysql
    	- mysql-server
    	- MySQL-python
    	- php-mysql
    	- php-pear
    	- php-common
    	- php-gd
    	- php-devel
    	- php
    	- php-mbstring
    	- php-cli
    	- php-process
    	- php-snmp
    	- net-snmp-utils
    	- net-snmp-libs
    	- rrdtool
    
        - name: Start snmpd server
          service:
    	name: snmpd
    	state: started
    
        - name: Start mysqld server
          service:
    	name: mysqld
    	state: started
    
        - wait_for:
    	port: 3306

# Cacti

Cacti is available in the EPEL repository for CentOS. The GPG key for
the CentOS repositories is enabled before installing the EPEL
repository. A 'yum update' is performed and the Cacti package is
installed. A 'cacti' user is then created in the MySQL database.

    - name: Install Cacti
      hosts: centos
      become: yes
      become_method: sudo
      gather_facts: true
      tags: [cacti]
    
      tasks:
        - name: Import EPEL GPG key
          rpm_key:
    	key: http://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-6
    	state: present
    
        - name: Add YUM repo
          yum_repository:
    	name: epel
    	description: EPEL YUM repo
    	baseurl: https://dl.fedoraproject.org/pub/epel/$releasever/$basearch/
    	gpgcheck: yes
    
        - name: Update the software package repository
          yum:
    	name: '*'
    	update_cache: yes
    
        - name: Install cacti
          package:
    	name: "{{ item }}"
    	state: latest
          with_items:
    	- cacti
    
        - name: Create cacti database user
          mysql_user:
    	name: cacti
    	password: "{{ mysql_cacti_password_hash }}"
    	encrypted: yes
    	priv: '*.*:ALL,GRANT'
    	state: present

# Fixing a bug

The time zone data is missing in this MySQL version (5.1.73-8). In
order to resolve this bug, the *mysql_test_data_timezone.sql* file
needs to be imported and the 'cacti' user needs to be given the SELECT
privilege to do this.

    - name: For bug https://github.com/Cacti/cacti/issues/242
      hosts: centos
      become: yes
      become_method: sudo
      gather_facts: true
      tags: [bug]
    
      tasks:
        - name: Import mysql_test_data_timezone.sql
          mysql_db:
    	state: import
    	name: mysql
    	target: /usr/share/mysql/mysql_test_data_timezone.sql
    
        - name: Grant privileges
          mysql_user:
    	name: cacti
    	append_privs: true
    	priv: 'mysql.time_zone_name:SELECT'
    	state: present

It is a good practice to have a separate playbook for such exceptional
cases. In future, when you upgrade to newer versions that have bug
fixes, you can simply skip this step.

# Configuration

The last step involves configuring Cacti.

    - name: Configuration
      hosts: centos
      become: yes
      become_method: sudo
      gather_facts: true
      tags: [config]
    
      tasks:
        - name: Create a database for cacti
          mysql_db:
    	name: cacti
    	state: present
    
        - name: Import cacti.sql
          mysql_db:
    	state: import
    	name: cacti
    	target: /usr/share/doc/cacti-1.0.4/cacti.sql
    
        - name: Update database credentials in config file
          lineinfile:
    	dest: /etc/cacti/db.php
    	regexp: "{{ item.regexp }}"
    	line: "{{ item.line }}"
          with_items:
    	- { regexp: '^\$database_username', line: "$database_username = '{{ mysql_username }}';" }
    	- { regexp: '^\$database_password', line: "$database_password = '{{ mysql_password }}';" }
    
        - name: Allow port 80
          shell: iptables -I INPUT 5 -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
    
        - name: Update access in cacti.conf for httpd
          replace:
    	dest: /etc/httpd/conf.d/cacti.conf
    	regexp: "{{ item.regexp }}"
    	replace: "{{ item.replace }}"
          with_items:
    	- { regexp: 'Require host localhost', replace: 'Require all granted' }
    	- { regexp: 'Allow from localhost', replace: 'Allow from all' }
    
        - lineinfile:
    	dest: /etc/cron.d/cacti
    	regexp: '^#(.*)$'
    	line: '\1'
    	backrefs: yes    
    
        - name: Start mysqld server
          service:
    	name: mysqld
    	state: restarted
    
        - wait_for:
    	port: 3306
    
        - name: Start the httpd server
          service:
    	name: httpd
    	state: restarted
    
        - wait_for:
    	port: 80

A database called 'cacti' is created for the application, and the
*cacti.sql* file is imported into it. The database credentials are
updated for the Cacti application. The firewall rules are then updated
to allow incoming HTTP requests for port 80. The periodic cron poller
is then enabled in */etc/cron.d/cacti*:

    */5 * * * *     cacti   /usr/bin/php /usr/share/cacti/poller.php > /dev/null 2>&1

The MySQL and HTTP servers are then restarted.

# The result

The entire playbook can now be invoked as follows:

    $ ansible-playbook -i inventory/kvm/inventory playbooks/configuration/cacti.yml --ask-vault-pass

It will prompt you for the Vault password, following which all the
playbooks will be completed. You can then open
<http://192.168.122.98/cacti> to accept the GNU General Public License
agreement. After you agree to the terms of the license, click
'Next'. The Cacti installation wizard shows the pre-installation
checks, which should not have any errors. This is followed by the
selection of the installation type, binary location, version, and the
directory permission checks. You can then decide on the templates you
would like to set up, following which a user login is provided. The
default user name and password is 'admin:admin', and you will be
immediately prompted to change the password after logging in. You can
then proceed to log in to the Cacti dashboard. Figures 1 to 8 give the
screenshots of the Cacti Web UI installation for reference. 

<img width="800" alt="License Agreement" src="http://www.shakthimaan.com/images/2017/osfy-devops/3-cacti/1-License-agreement.png"><br />

<img width="800" alt="Pre-installation checks" src="http://www.shakthimaan.com/images/2017/osfy-devops/3-cacti/2-Pre-installation-checks.png"><br />

<img width="800" alt="Installation type" src="http://www.shakthimaan.com/images/2017/osfy-devops/3-cacti/3-Installation-type.png"><br />

<img width="800" alt="Binary location and version" src="http://www.shakthimaan.com/images/2017/osfy-devops/3-cacti/4-Binary-location-and-version.png"><br />

<img width="800" alt="Directory permission checks" src="http://www.shakthimaan.com/images/2017/osfy-devops/3-cacti/5-Directory-permission-checks.png"><br />

<img width="800" alt="Template setup" src="http://www.shakthimaan.com/images/2017/osfy-devops/3-cacti/6-Template-setup.png"><br />

<img width="800" alt="User login" src="http://www.shakthimaan.com/images/2017/osfy-devops/3-cacti/7-User-login.png"><br />

<img width="800" alt="Change password" src="http://www.shakthimaan.com/images/2017/osfy-devops/3-cacti/8-Change-password.png"><br />

A screenshot of Cacti graphing for memory usage is shown in Figure 9.

<img width="800" alt="Cacti Web UI" src="http://www.shakthimaan.com/images/2017/osfy-devops/3-cacti/9-Cacti.png">

