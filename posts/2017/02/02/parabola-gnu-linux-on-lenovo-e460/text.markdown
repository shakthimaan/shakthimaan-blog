---
author: SK
tags: 2017, laptop, install
timestamp: 16:15:00
title: Parabola GNU/Linux Libre on Lenovo E460
---

I wanted to buy a laptop for my personal use, and exclusively for Free
Software. I have mostly used Lenovo Thinkpads because of their good
support for GNU/Linux. I neither wanted an expensive one nor did I
want to pay for Windows. The use of SSD would help speed up the boot
time. A wide screen laptop will be helpful to split the Emacs frame. I
looked at various Netbooks in the market, but, the processing power
was not good enough. I really need computing power, with at least four
CPUs. 8 GB of RAM with room for expansion will be helpful in future. I
prefer to use anti-glare matte displays as they are easy on the
eyes. Also, I wanted a laptop that is less than 2kg, and is easy to
carry around.

After reviewing many models and configurations, I purchased
the
[Lenovo E460](http://www3.lenovo.com/in/en/laptops/thinkpad/thinkpad-e-series/E460/p/22TP2TEE460) model
that comes with [FreeDOS](http://www.freedos.org/), and swapped the
default HDD for SSD (< 500 GB).

<img alt="Lenovo E460 screen" src="http://shakthimaan.com/images/2017/lenovo-e460/lenovo-e460-screen.png"></img>

# Specification

* Intel(R) Core(TM) i5-6200 CPU @ 2.30 GHz (4 processors).
* 14" display
* 437 GB SSD disk
* 8 GB RAM
* Intel Corporation HD Graphics 520
* Intel Dual Band Wireless-AC 3165 Plus Bluetooth
* Intel I219-V Ethernet Controller
* 3 USB ports
* 1 HDMI port
* 4-in-1 Card Reader
* FreeDOS
* 1.81 kg

# Parabola GNU/Linux Libre

I tried [Trisquel GNU/Linux](https://trisquel.info/) first on on this
laptop. It is a derivative of Ubuntu without non-free software. I
experimented with [Qubes OS](https://www.qubes-os.org/), but, its dom0
has proprietary blobs. [GNU Guix](https://www.gnu.org/software/guix/)
is an interesting project, but, it does not have all the packages that
I need (yet). I liked rolling distributions, and hence decided to
try [Parabola GNU/Linux Libre](https://www.parabola.nu/), a derivative
of [Arch](https://www.archlinux.org/), without the binary blobs.

There is no CD/DVD drive on this laptop, but, you can boot from USB. I
first checked if all the software that I need are available in the
Parabola GNU/Linux Libre repository, and then proceeded to install the
same. I always encrypt the disk during installation. I have
the [Mate desktop environment](https://mate-desktop.org/)
with [XMonad](http://xmonad.org/) setup as a tiling window manager.

<img alt="Lenovo E460 screen" src="http://shakthimaan.com/images/2017/lenovo-e460/lenovo-e460-screenshot.png"></img>

Audio works out of the box. I do not use the web cam. I had to use the
package scripts to install [Grisbi](http://grisbi.org/) as it was not
available in the base repository. Virtualization support exists on
this hardware, and hence I
use
[Virtual Machine Manager](https://virt-manager.org/),
[QEMU](http://wiki.qemu.org/Main_Page)
and [libvirt](https://libvirt.org/).

# Command Output

All the hardware worked out of the box, except for the wireless which
requires a binary blob. So, I purchased
a
[ThinkPenguin Wireless N USB Adapter for GNU/Linux](https://www.thinkpenguin.com/gnu-linux/penguin-wireless-n-usb-adapter-gnu-linux-tpe-n150usb) which
uses the free ath9k Atheros wireless driver.

As mandatory, I am providing some command outputs.

~~~~{.shell}
$ lspci

00:00.0 Host bridge: Intel Corporation Skylake Host Bridge/DRAM Registers (rev 08)
00:02.0 VGA compatible controller: Intel Corporation HD Graphics 520 (rev 07)
00:14.0 USB controller: Intel Corporation Sunrise Point-LP USB 3.0 xHCI Controller (rev 21)
00:14.2 Signal processing controller: Intel Corporation Sunrise Point-LP Thermal subsystem (rev 21)
00:16.0 Communication controller: Intel Corporation Sunrise Point-LP CSME HECI #1 (rev 21)
00:17.0 SATA controller: Intel Corporation Sunrise Point-LP SATA Controller [AHCI mode] (rev 21)
00:1c.0 PCI bridge: Intel Corporation Device 9d12 (rev f1)
00:1c.5 PCI bridge: Intel Corporation Sunrise Point-LP PCI Express Root Port #6 (rev f1)
00:1f.0 ISA bridge: Intel Corporation Sunrise Point-LP LPC Controller (rev 21)
00:1f.2 Memory controller: Intel Corporation Sunrise Point-LP PMC (rev 21)
00:1f.3 Audio device: Intel Corporation Sunrise Point-LP HD Audio (rev 21)
00:1f.4 SMBus: Intel Corporation Sunrise Point-LP SMBus (rev 21)
00:1f.6 Ethernet controller: Intel Corporation Ethernet Connection I219-V (rev 21)
01:00.0 Network controller: Intel Corporation Intel Dual Band Wireless-AC 3165 Plus Bluetooth (rev 99)
02:00.0 Unassigned class [ff00]: Realtek Semiconductor Co., Ltd. RTS522A PCI Express Card Reader (rev 01)

$ uname -a

Linux aether 4.8.17-gnu-1 #1 SMP PREEMPT Wed Jan 18 05:04:13 UYT 2017 x86_64 GNU/Linux

$ df -h

Filesystem             Size  Used Avail Use% Mounted on
dev                    3.7G     0  3.7G   0% /dev
run                    3.7G  920K  3.7G   1% /run
/dev/mapper/cryptroot  437G   95G  321G  23% /
tmpfs                  3.7G   26M  3.7G   1% /dev/shm
tmpfs                  3.7G     0  3.7G   0% /sys/fs/cgroup
tmpfs                  3.7G  196K  3.7G   1% /tmp
/dev/sda1              976M   45M  865M   5% /boot
tmpfs                  745M   28K  745M   1% /run/user/1000

$ free -h
              total        used        free      shared  buff/cache   available
Mem:           7.3G        2.4G        3.2G         83M        1.7G        4.6G
Swap:          2.1G          0B        2.1G

$ lscpu

Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                4
On-line CPU(s) list:   0-3
Thread(s) per core:    2
Core(s) per socket:    2
Socket(s):             1
NUMA node(s):          1
Vendor ID:             GenuineIntel
CPU family:            6
Model:                 78
Model name:            Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz
Stepping:              3
CPU MHz:               499.951
CPU max MHz:           2800.0000
CPU min MHz:           400.0000
BogoMIPS:              4801.00
Virtualization:        VT-x
L1d cache:             32K
L1i cache:             32K
L2 cache:              256K
L3 cache:              3072K
NUMA node0 CPU(s):     0-3
Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch epb intel_pt tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp
~~~~

# Conclusion

<img alt="Lenovo E460 screen" src="http://shakthimaan.com/images/2017/lenovo-e460/lenovo-e460-sticker.png"></img>

I have been using the laptop for more than three months, and it has
really been a smooth experience. It costed less than ₹ 55,000. The
battery life is decent. I printed couple of Free Software stickers to
identify my
laptop. The
["Inside GNU/Linux"](https://static.fsf.org/nosvn/stickers/GNU-Linux.png) sticker
covers the web cam, and
the
["Free Software Foundation"](http://static.fsf.org/nosvn/stickers/fsf.svg) sticker is
pasted behind the screen. The folks at #parabola IRC channel on
irc.freenode.net are quite helpful. The [Parabola GNU/Linux Libre Wiki](https://wiki.parabola.nu/)
has excellent documentation for your reference.
