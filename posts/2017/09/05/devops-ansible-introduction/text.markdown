---
author: SK
tags: 2017, devops
timestamp: 13:00:00
title: Introduction to Ansible
---
*[Published in Open Source For You (OSFY) magazine, March 2017 edition.]*

With this article, we begin a new series on DevOps, starting out with
Ansible, which helps you to build a strong foundation. As the Ansible
website proclaims, proudly, "Deploy apps. Manage systems. Crush
complexity."

# Introduction

Ansible is an IT automation tool that is used for provisioning,
configuration, deployment and managing infrastructure. The project was
first released in 2012, and is written in Python. The main objective
of the tool is to be simple and easy to use. It is based on an
agent-less (push-based) architecture, and the playbooks are written in
plain English. Today, it also supports pull-based deployments. Ansible
uses SSH to execute commands on remote machines. It is available under
the GNU General Public License.

# Installation

You can install Ansible using your GNU/Linux distribution package
manager.

On Fedora, you can use Yum to install Ansible, as follows:

    $ sudo yum install ansible

If you are using RHEL or CentOS, install the epel-release, and then
use the Yum command to install Ansible.

On Ubuntu, you need to add the *ppa* repository before installing the
tool, as shown below:

    $ sudo apt-get install software-properties-common
    $ sudo apt-add-repository ppa:ansible/ansible
    
    $ sudo apt-get update
    $ sudo apt-get install ansible

The Ansible documentation encourages Debian users to access the Ubuntu
repository to obtain Ansible. You need to add the following line to
*/etc/apt/sources.list*:

    deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main

You can then install the software using the following commands:

    $ sudo apt-get update
    $ sudo apt-get install ansible

The Parabola GNU/Linux-libre distribution is a derivative of Arch
Linux, without the binary blobs. You can install Ansible using the
*pacman* utility:

    $ pacman -S ansible

The latest Ansible version 2.2 (as of date) is what we will use in
this article. Ansible is also available for BSD variants, Mac OS X,
and Windows. You are encouraged to refer to the Ansible documentation
for more information.

# Virtualisation

Ansible can be used to provision new machines and also configure
them. Instead of using bare metal machines, you can create multiple
Virtual Machines (VMs) on your system. Lots of Free and Open Source
Software (F/OSS) virtualization software is available.

QEMU is a machine emulator and virtualiser. It can also use host CPU
support to run guest VMs for better performance. It is written by
Fabrice Bellard, and released under the GNU General Public License
(GPL). You can install it on Parabola GNU/Linux-libre, using the
following command:

    $ sudo pacman -S qemu

KVM or Kernel-based Virtual Machine (KVM) has direct support in the
Linux kernel. It requires hardware support to be able to run guest
operating systems. It is written in C, and is released under the GNU
General Public License.

You need to check if your hardware first supports KVM. The 'lscpu'
command will show an entry for 'Virtualization' if there is hardware
support. For example:

    $ lscpu
    
    Architecture:          x86_64
    CPU op-mode(s):        32-bit, 64-bit
    Byte Order:            Little Endian
    CPU(s):                4
    On-line CPU(s) list:   0-3
    Thread(s) per core:    2
    Core(s) per socket:    2
    Socket(s):             1
    NUMA node(s):          1
    Vendor ID:             GenuineIntel
    CPU family:            6
    Model:                 78
    Model name:            Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz
    Stepping:              3
    CPU MHz:               2275.341
    CPU max MHz:           2800.0000
    CPU min MHz:           400.0000
    BogoMIPS:              4801.00
    Virtualization:        VT-x
    L1d cache:             32K
    L1i cache:             32K
    L2 cache:              256K
    L3 cache:              3072K
    NUMA node0 CPU(s):     0-3
    Flags:                 fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch epb intel_pt tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp

You can also check the /proc/cpuinfo output as shown below:

    $ grep -E "(vmx|svm)" --color=always /proc/cpuinfo
    
    flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm constant_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 sdbg fma cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm abm 3dnowprefetch epb intel_pt tpr_shadow vnmi flexpriority ept vpid fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid mpx rdseed adx smap clflushopt xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts hwp hwp_notify hwp_act_window hwp_epp

The Libvirt project provides APIs to manage guest machines on KVM,
QEMU and other virtualisation software. It is written in C, and is
released under the GNU Lesser GPL. The Virtual Machine Manager (VMM)
provides a graphical user interface for managing the guest VMs and is
written in Python.

You can install all this software on Parabola GNU/Linux-libre using
the following command:

    $ sudo pacman -S libvirt virt-manager

A screenshot of Virtual Machine Manager is provided below:

<img alt="Virtual Machine Manager" width="800" src="http://shakthimaan.com/images/2017/osfy-devops/vmm-screenshot.png"></img>

Check your distribution documentation to install the appropriate
virtualisation software packages.

You can use the VMM to create a new virtual machine, and install a
GNU/Linux distribution using an *.iso* image. You can specify RAM,
disk size and follow the installation steps for your particular
distro. You can also import an existing *.qcow2* disk image to use it
as a virtual machine.

# Ansible with libvirt-VM

The version of Ansible used for this article is given below:

    $ ansible --version
    
    ansible 2.2.1.0
      config file = /etc/ansible/ansible.cfg
      configured module search path = Default w/o overrides

If you have the *sshd* daemon running on your local machine, you can
use Ansible to test it. For example, a ping test on the localhost is
shown below:

    $ ansible localhost -m ping
    
    localhost | SUCCESS => {
        "changed": false, 
        "ping": "pong"
    }

You can also check how long the system has been up and running using
the following command:

    $ ansible localhost -a uptime
    
    localhost | SUCCESS | rc=0 >>
     11:00:20 up  4:09,  0 users,  load average: 0.18, 0.14, 0.11

You can execute a shell command on the remote machine (localhost, in
this case) as illustrated below:

    $  ansible localhost -a "date"
    
    localhost | SUCCESS | rc=0 >>
    Sun Feb  5 11:24:53 IST 2017

The 'setup' command provides details of the remote target machine. A
snippet output is provided below:

    $  ansible localhost -m setup
    
    localhost | SUCCESS => {
        "ansible_facts": {
            "ansible_all_ipv4_addresses": [
                "192.168.10.1", 
                "192.168.5.6"
            ], 
            "ansible_all_ipv6_addresses": [
                "fe90::fc24:ff:feb9:cb61", 
                "ff80::5846:fac1:6afc:2e30"
            ], 
            "ansible_architecture": "x86_64", 
            "ansible_bios_date": "06/12/2016", 
            "ansible_bios_version": "R00ET45W (1.20 )", 
            "ansible_cmdline": {
                "BOOT_IMAGE": "/vmlinuz-linux-libre", 
                "cryptdevice": "/dev/sda1:cryptroot", 
                "quiet": true, 
                "root": "/dev/mapper/cryptroot", 
                "rw": true
            }, 
            ....

An Ubuntu 15.04 instance with VMM is used in the following examples
with Ansible. The IP address of the instance is added to */etc/hosts*:

    192.168.122.250 ubuntu

The */etc/ansible/hosts* file contains the following:

    ubuntu

You can now do a ping test from the host to the Ubuntu VM using the
following command sequence for the user 'xetex':

    $  ansible ubuntu -m ping -u xetex --ask-pass
    SSH password: 
    ubuntu | SUCCESS => {
        "changed": false, 
        "ping": "pong"
    }

To avoid prompting for the password, you can add the localhost public
SSH key to the VM, as follows:

    $  ssh-copy-id -i ~/.ssh/id_rsa.pub xetex@ubuntu
    
    /usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/user/.ssh/id_rsa.pub"
    /usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
    /usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
    xetex@ubuntu's password: 
    
    Number of key(s) added: 1
    
Now try logging into the machine, with *ssh xetex@ubuntu* and check to
make sure that only the key you wanted was added.

You can now issue the following command to get the same result:

    $ ansible ubuntu -m ping -u xetex
    
    ubuntu | SUCCESS => {
        "changed": false, 
        "ping": "pong"
    }

For the Ubuntu system, you can also add the defined user in the
*/etc/ansible/hosts* file as follows:

    ubuntu ansible_ssh_host=ubuntu ansible_ssh_user=xetex

The ping command is now simplified to:

    $  ansible ubuntu -m ping
    
    ubuntu | SUCCESS => {
        "changed": false, 
        "ping": "pong"
    }

You can now try the earlier Ansible commands on the target Ubuntu VM
as illustrated below:

    $ ansible ubuntu -a uptime
    
    ubuntu | SUCCESS | rc=0 >>
     12:32:14 up 25 min,  3 users,  load average: 0.02, 0.07, 0.06

    $ ansible ubuntu -a date
    
    ubuntu | SUCCESS | rc=0 >>
    Sun Feb  5 12:32:45 IST 2017

    $  ansible ubuntu -m setup
    ubuntu | SUCCESS => {
        "ansible_facts": {
            "ansible_all_ipv4_addresses": [
                "192.168.122.250"
            ], 
            "ansible_all_ipv6_addresses": [
                "ff20::5034:ff:fa9f:6123"
            ], 
            "ansible_architecture": "x86_64", 
            "ansible_bios_date": "04/01/2014", 
            "ansible_bios_version": "1.10.1-20151022_124906-anatol", 
            "ansible_cmdline": {
                "BOOT_IMAGE": "/boot/vmlinuz-3.19.0-15-generic", 
                "quiet": true, 
                "ro": true, 
                "root": "UUID=f43c2c72-5bc7-4a97-9a43-12e634ae232af", 
                "splash": true, 
                "vt.handoff": "7"
            }, 
            ...

