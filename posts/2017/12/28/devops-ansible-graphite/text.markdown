---
author: SK
tags: 2017, devops
timestamp: 16:00:00
title: Ansible deployment of Graphite
---
*[Published in Open Source For You (OSFY) magazine, July 2017 edition.]*

# Introduction

In this fifth article in the DevOps series we will learn to install
and set up Graphite using Ansible. Graphite is a monitoring tool that
was written by Chris Davis in 2006. It has been released under the
Apache 2.0 license and comprises three components:

1.  Graphite-Web
2.  Carbon
3.  Whisper

Graphite-Web is a Django application and provides a dashboard for
monitoring. Carbon is a server that listens to time-series data, while
Whisper is a database library for storing the data.

# Setting it up

A CentOS 6.8 Virtual Machine (VM) running on KVM is used for the
installation. Please make sure that the VM has access to the
Internet. The Ansible version used on the host (Parabola
GNU/Linux-libre x86\_64) is 2.2.1.0. The *ansible/* folder contains the
following files:

    ansible/inventory/kvm/inventory
    ansible/playbooks/configuration/graphite.yml
    ansible/playbooks/admin/uninstall-graphite.yml

The IP address of the guest CentOS 6.8 VM is added to the *inventory*
file as shown below:

    graphite ansible_host=192.168.122.120 ansible_connection=ssh ansible_user=root ansible_password=password

Also, add an entry for the *graphite* host in */etc/hosts* file as
indicated below:

    192.168.122.120 graphite

# Graphite

The playbook to install the Graphite server is given below:

    ---
    - name: Install Graphite software
      hosts: graphite
      gather_facts: true
      tags: [graphite]
    
      tasks:
        - name: Import EPEL GPG key
          rpm_key:
            key: http://dl.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-6
            state: present
    
        - name: Add YUM repo
          yum_repository:
            name: epel
            description: EPEL YUM repo
            baseurl: https://dl.fedoraproject.org/pub/epel/$releasever/$basearch/
            gpgcheck: yes
    
        - name: Update the software package repository
          yum:
            name: '*'
            update_cache: yes
    
        - name: Install Graphite server
          package:
            name: "{{ item }}"
            state: latest
          with_items:
            - graphite-web

We first import the keys for the Extra Packages for Enterprise Linux
(EPEL) repository and update the software package list. The
'graphite-web' package is then installed using Yum. The above playbook
can be invoked using the following command:

    $ ansible-playbook -i inventory/kvm/inventory playbooks/configuration/graphite.yml --tags "graphite"

# MySQL

A backend database is required by Graphite. By default, the SQLite3
database is used, but we will install and use MySQL as shown below:

    - name: Install MySQL
      hosts: graphite
      become: yes
      become_method: sudo
      gather_facts: true
      tags: [database]
    
      tasks:
        - name: Install database
          package:
            name: "{{ item }}"
            state: latest
          with_items:
            - mysql
            - mysql-server
            - MySQL-python
            - libselinux-python
    
        - name: Start mysqld server
          service:
            name: mysqld
            state: started
    
        - wait_for:
            port: 3306
    
        - name: Create graphite database user
          mysql_user:
            name: graphite
            password: graphite123
            priv: '*.*:ALL,GRANT'
            state: present
    
        - name: Create a database
          mysql_db:
            name: graphite
            state: present
    
        - name: Update database configuration
          blockinfile:
            path: /etc/graphite-web/local_settings.py
            block: |
              DATABASES = {
                'default': {
                'NAME': 'graphite',
                'ENGINE': 'django.db.backends.mysql',
                'USER': 'graphite',
                'PASSWORD': 'graphite123',
               }
              }
    
        - name: syncdb
          shell: /usr/lib/python2.6/site-packages/graphite/manage.py syncdb --noinput
    
        - name: Allow port 80
          shell: iptables -I INPUT -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
    
        - name:
          lineinfile:
            path: /etc/httpd/conf.d/graphite-web.conf
            insertafter: '           # Apache 2.2'
            line: '           Allow from all'
    
        - name: Start httpd server
          service:
            name: httpd
            state: started

As a first step, let's install the required MySQL dependency packages
and the server itself. We then start the server and wait for it to
listen on port 3306. A graphite user and database is created for use
with the Graphite Web application. For this example, the password is
provided as plain text. In production, use an encrypted Ansible Vault
password.

The database configuration file is then updated to use the MySQL
credentials. Since Graphite is a Django application, the *manage.py*
script with *syncdb* needs to be executed to create the necessary
tables. We then allow port 80 through the firewall in order to view
the Graphite dashboard. The *graphite-web.conf* file is updated to
allow read access, and the Apache web server is started.

The above playbook can be invoked as follows:

    $ ansible-playbook -i inventory/kvm/inventory playbooks/configuration/graphite.yml --tags "database"

# Carbon and Whisper

The Carbon and Whisper Python bindings need to be installed before
starting the *carbon-cache* script.

    - name: Install Carbon and Whisper
      hosts: graphite
      become: yes
      become_method: sudo
      gather_facts: true
      tags: [carbon]
    
      tasks:
        - name: Install carbon and whisper
          package:
            name: "{{ item }}"
            state: latest
          with_items:
            - python-carbon
            - python-whisper
    
        - name: Start carbon-cache
          shell: /etc/init.d/carbon-cache start

The above playbook is invoked as follows:

    $ ansible-playbook -i inventory/kvm/inventory playbooks/configuration/graphite.yml --tags "carbon"

# Dashboard

You can open <http://192.168.122.120> in the browser on the host to
view the Graphite dashboard. A screenshot of the Graphite web
application is shown below:

<img width="800" alt="Graphite dashboard" src="http://shakthimaan.com/images/2017/osfy-devops/5-graphite/graphite-screenshot.png">

# Uninstall

An uninstall script to remove the Graphite server and its dependency
packages is required for administration. The Ansible playbook for the
same is available in *playbooks/admin* folder and is given below:

    ---
    - name: Uninstall Graphite and dependencies
      hosts: graphite
      gather_facts: true
      tags: [remove]
    
      tasks:
        - name: Stop the carbon-cache server
          shell: /etc/init.d/carbon-cache stop
    
        - name: Uninstall carbon and whisper
          package:
            name: "{{ item }}"
            state: absent
          with_items:
            - python-whisper
            - python-carbon
    
        - name: Stop httpd server
          service:
            name: httpd
            state: stopped
    
        - name: Stop mysqld server
          service:
            name: mysqld
            state: stopped
    
        - name: Uninstall database packages
          package:
            name: "{{ item }}"
            state: absent
          with_items:
            - libselinux-python
            - MySQL-python
            - mysql-server
            - mysql
            - graphite-web

The script can be invoked as follows:

    $ ansible-playbook -i inventory/kvm/inventory playbooks/admin/uninstall-graphite.yml

# References

1.  Graphite documentation. <https://graphite.readthedocs.io/en/latest/>

2.  Carbon. <https://github.com/graphite-project/carbon>

3.  Whisper database. <http://graphite.readthedocs.io/en/latest/whisper.html>

