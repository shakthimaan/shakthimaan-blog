---
author: SK
tags: 2017, laptop, lisp
timestamp: 21:00:00
title: StumpWM on Parabola GNU/Linux-libre
---

I have installed and configured [StumpWM](https://stumpwm.github.io/)
(v1.0.0)
on [Parabola GNU/Linux-libre](https://www.parabola.nu/). StumpWM is a
keyboard driven, tiling Window Manager written in Common Lisp (CL). It
is thus very hackable, and you can customize its configuration by
writing Lisp code and reload it dynamically. The installation and
configuration steps are provided below for reference.

Firstly, the [Steel Bank Common Lisp](http://sbcl.org/) (SBCL) compiler needs to be installed on the system using Pacman.

    $ sudo pacman -S sbcl 
    $ sudo pacman -S acpi xtrlock emacs mate-terminal iceweasel

The *acpi* utility is installed to show the battery status in the
modeline. The *xtrlock* utility is used to lock the screen. When the
screen is locked, you will need to enter your user password to unlock
the same. GNU Emacs, Iceweasel and Mate-terminal software packages are
also installed. [Quicklisp](https://www.quicklisp.org/beta/) is a
package manager for Common Lisp and is used to install the StumpWM dependencies with SBCL.

An installation directory is created at ~/downloads/quicklisp, and the
quicklisplisp file is fetched as follows:

    $ mkdir ~/downloads/quicklisp
    $ cd ~/downloads/quicklisp
    $ curl -O https://beta.quicklisp.org/quicklisp.lisp

The quicklisp.lisp file is loaded using the SBCL interpreter. The
installation steps and output are shown below:

    ~/downloads/quicklisp $ sbcl --load quicklisp.lisp 
    
        This is SBCL 1.3.17, an implementation of ANSI Common Lisp.
        More information about SBCL is available at <http://www.sbcl.org/>.

        SBCL is free software, provided as is, with absolutely no warranty.
        It is mostly in the public domain; some portions are provided under
        BSD-style licenses.  See the CREDITS and COPYING files in the
        distribution for more information.

          ==== quicklisp quickstart 2015-01-28 loaded ====

            To continue with installation, evaluate: (quicklisp-quickstart:install)

            For installation options, evaluate: (quicklisp-quickstart:help)
            
    * (quicklisp-quickstart:install)
    
        ; Fetching #<URL "http://beta.quicklisp.org/client/quicklisp.sexp">
        ; 0.82KB
        ==================================================
        838 bytes in 0.00 seconds (0.00KB/sec)
        ; Fetching #<URL "http://beta.quicklisp.org/client/2017-03-06/quicklisp.tar">
        ; 250.00KB
        ==================================================
        256,000 bytes in 0.16 seconds (1602.56KB/sec)
        ; Fetching #<URL "http://beta.quicklisp.org/client/2015-09-24/setup.lisp">
        ; 4.94KB
        ==================================================
        5,054 bytes in 0.00 seconds (0.00KB/sec)
        ; Fetching #<URL "http://beta.quicklisp.org/asdf/2.26/asdf.lisp">
        ; 194.07KB
        ==================================================
        198,729 bytes in 0.09 seconds (2086.79KB/sec)
        ; Fetching #<URL "http://beta.quicklisp.org/dist/quicklisp.txt">
        ; 0.40KB
        ==================================================
        408 bytes in 0.00 seconds (0.00KB/sec)
        Installing dist "quicklisp" version "2017-07-25".
        ; Fetching #<URL "http://beta.quicklisp.org/dist/quicklisp/2017-07-25/releases.txt">
        ; 372.80KB
        ==================================================
        381,744 bytes in 0.15 seconds (2485.31KB/sec)
        ; Fetching #<URL "http://beta.quicklisp.org/dist/quicklisp/2017-07-25/systems.txt">
        ; 241.23KB
        ==================================================
        247,022 bytes in 0.10 seconds (2297.45KB/sec)

          ==== quicklisp installed ====

            To load a system, use: (ql:quickload "system-name")

            To find systems, use: (ql:system-apropos "term")

            To load Quicklisp every time you start Lisp, use: (ql:add-to-init-file)

            For more information, see http://www.quicklisp.org/beta/

        NIL
      
    * (ql:add-to-init-file)
    
        I will append the following lines to #P"/home/guest/.sbclrc":

          ;;; The following lines added by ql:add-to-init-file:
          #-quicklisp
          (let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                                 (user-homedir-pathname))))
            (when (probe-file quicklisp-init)
              (load quicklisp-init)))

        Press Enter to continue.

        #P"/home/guest/.sbclrc"
        
    * (ql:quickload "clx")
    
        To load "clx":
          Install 1 Quicklisp release:
            clx
        ; Fetching #<URL "http://beta.quicklisp.org/archive/clx/2017-06-30/clx-20170630-git.tgz">
        ; 452.92KB
        ==================================================
        463,786 bytes in 0.12 seconds (3806.02KB/sec)
        ; Loading "clx"
        [package xlib]....................................
        ..................................................
        ..................................................
        ..................................................
        ..................................................
        ..................................................
        ..................................................
        ..................................................
        [package xlib/glx]................................
        [package xlib/gl].................................
        [package xlib/dpms]...............................
        [package xlib/xtest]..............................
        [package xlib/xinerama]...........................
        [package xlib-demo/clclock].......................
        [package xlib-demo/clipboard].....................
        [package xlib-demo/demos].........................
        [package xlib-demo/gl-test].......................
        [package xlib-demo/mandel].............
        ("clx")
      
    * (ql:quickload "cl-ppcre")
    
        To load "cl-ppcre":
          Install 1 Quicklisp release:
            cl-ppcre
        ; Fetching #<URL "http://beta.quicklisp.org/archive/cl-ppcre/2015-09-23/cl-ppcre-2.0.11.tgz">
        ; 156.08KB
        ==================================================
        159,829 bytes in 0.08 seconds (1903.45KB/sec)
        ; Loading "cl-ppcre"
        [package cl-ppcre]................................
        ................
        ("cl-ppcre")
      
    * (ql:quickload "alexandria")
    
        To load "alexandria":
          Install 1 Quicklisp release:
            alexandria
        ; Fetching #<URL "http://beta.quicklisp.org/archive/alexandria/2017-06-30/alexandria-20170630-git.tgz">
        ; 49.97KB
        ==================================================
        51,168 bytes in 0.04 seconds (1135.65KB/sec)
        ; Loading "alexandria"
        [package alexandria.0.dev].......................
        ("alexandria")
      
    * (exit)

    ~/downloads/quicklisp $ 

The StumpWM v1.0.0 sources are fetched to ~/downloads/stumpwm
directory, extracted and compiled using the following steps:

    $ mkdir ~/downloads/stumpwm
    $ cd ~/downloads/stumpwm
    $ wget https://github.com/stumpwm/stumpwm/archive/1.0.0.tar.gz
    $ tar xzvf 1.0.0.tar.gz
    $ cd stumpwm-1.0.0/
    $ ./autogen.sh 
    $ ./configure 
    $ make
    
You can then install the built *stumpwm* binary, which will get copied
to /usr/local/bin/stumpwm as shown below:
    
    $ sudo make install
    
    $ which stumpwm
    /usr/local/bin/stumpwm

I use the Lightweight X11 Display Manager (lxdm), and thus created a
/usr/share/xsessions/stumpwm.desktop file to login to StumpWM from the
login manager:

    [Desktop Entry]
    Encoding=UTF-8
    Name=StumpWM
    Comment=StumpWM
    Exec=/usr/local/bin/stumpwm
    Type=Application

A sample ~/.stumpwmrc configuration file is given below (should be self-explanatory):

    ;; -*-lisp-*-

    (in-package :stumpwm)

    ;; Startup message: display the machine's name
    (setf *startup-message* (machine-instance))

    ;; Turn on the modeline
    (if (not (head-mode-line (current-head)))
        (toggle-mode-line (current-screen) (current-head)))

    ;; Lock screen
    (define-key *root-map* (kbd "L") "exec xtrlock")

    ;; I like messages to be centered on the screen.
    (setf *message-window-gravity* :center)
    (setf *input-window-gravity* :center)

    ;; I thought that this mode-line was fabulous!
    (defvar *battery-status-command*
      "acpi -b | awk -F '[ ,]' '{printf \"%s%s\", $3, $5}' | sed s/Discharging/\-/ | sed s/Unknown// | sed s/Full// | sed s/Charging/+/")

    (defvar *vol-status-command*
      "amixer get Master | grep \"[[:digit:]]\\+%\" -o | tr -d \"\\n\"")

    (setf *screen-mode-line-format*
          (list "%w [^B%n^b] ^>"
          '(:eval (run-shell-command *battery-status-command* t))
          " | Vol. "
          '(:eval (run-shell-command *vol-status-command* t))
          " | %d"))

    ;; urxvt
    (define-key *root-map* (kbd "c") 
      "exec urxvt +sb -fn \"xft:Ubuntu Mono:pixelsize=15,style=regular\"")

    ;; Window information format
    (setf *window-info-format* "%wx%h %n (%t - %c)")

    ;; Window format
    (setf *window-format* "%m%n%s%10t")

    ;; Emacs
    (defvar *emacs-command* nil
      "Start an emacs client frame. Starts an emacs daemon if necessary.")
    (setf *emacs-command* "bash -c -i 'emacsclient -c -a \"\"'")

    (define-key *root-map* (kbd "e") "run-emacs")

    (defcommand run-emacs () ()
        (run-shell-command (concat "exec " *emacs-command*)))

    ;; iceweasel
    (defcommand iceweasel-browser () ()
      "run iceweasel"
      (run-or-raise "iceweasel" '(:instance "iceweasel")))
    (define-key *root-map* (kbd "b") "iceweasel-browser")

    ;; mate-terminal
    (defcommand mate-terminal () ()
      (run-or-raise "mate-terminal --hide-menubar" '(:class "mate-terminal")))

    (define-key *root-map* (kbd "C") "mate-terminal")

    ;; Clear rules
    (clear-window-placement-rules)

    (define-frame-preference "Default"
      ;; frame raise lock (lock AND raise == jumpto)
      (0 t   t :instance "emacs")
      (1 t   t :instance "iceweasel-browser"))

    ;; dvorak and தமிழ்
    (stumpwm:run-shell-command "sh -c 'setxkbmap us,in dvorak,tam_unicode grp:ctrls_toggle'")

    ;; Start default applications
    (run-emacs)
    (iceweasel-browser)

On logging into StumpWM, GNU Emacs and Iceweasel browser are opened
automatically. I also switch between English and Tamil keyboard
layouts when required, and the two Control keys are used to toggle
between them. A StumpWM screenshot is shown below:

<img alt="StumpWM screenshot" width="800" src="http://shakthimaan.com/images/2017/misc/stumpwm-screenshot.png"></img>

You are encouraged to read
the
[StumpWM manual](https://stumpwm.github.io/1.0.0/stumpwm-1.0.0.pdf) to
know more about its usage and configuration.
