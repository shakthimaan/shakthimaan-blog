---
author: SK
tags: 2017, emacs
timestamp: 22:00:00
title: GNU Emacs - Org Mode
---
*[Published in Open Source For You (OSFY) magazine, May 2016 edition.]*

Keeping notes, maintaining to-do lists, planning projects and author
documents with a fast and effective plain-text system - that's what
Emacs Org mode helps you do. There's even a mobile version available
as FOSS so that your favourite note-taking tool remains with you
always.

In this next article in the GNU Emacs series, let's learn about Org mode.

# Introduction

Org-mode is an outline, major mode in GNU Emacs that can be used for
taking notes, planning and documentation. It was written by Carsten
Dominik in 2003. The Org file is written in plain text and markdown,
and it can be exported into multiple output formats (TeX, HTML, PDF,
etc.). Org-mode ships with GNU Emacs and this tutorial uses GNU Emacs
24.4.1 (*M-x version*) and Org mode version 8.2.10 (*M-x org-version*).

# Structure

An Org file ends with the .org filename extension. The top-level
headlines are marked using an asterisk ('*'). For example, a college
student may have the following high-level topics:

    * College
    * Home
    * Recreation

You can create topics in the sub-section with more asterisks. For example:

    * College
    ** Assignments
    ** Lab
    ** Exams
    * Home
    * Recreation

If you wish to hide the multiple asterisks at the sub-section level,
you can add the following to your ~/.emacs:

    (setq org-hide-leading-stars t)

The resultant Org file will look like what follows:

    * College
     * Assignments
     * Lab
     * Exams
    * Home
    * Recreation

You can add unnumbered (use minus or plus symbols) or numbered lists
as shown below:

    * College
     * Assignments
     * Lab
       1. Compiler Design
       2. Programming Languages
     * Exams
       - Mathematics
       - Operating Systems
       - Databases
       - Compilers
    * Home
    * Recreation

You can cycle through the various levels in the Org file using the *Tab*
and *Shift-Tab* keys.

A checkbox with a '/' or a '%' symbol can be used next to a topic name
to indicate the completion status of the task. The lists under a topic
can each have a checkbox too. Using *C-c C-c* will mark a checkbox for
completion ('X') and will also update the statistics in the top-level
checkbox. An example is shown below:

    * College...
    * Home [2/2]
      - [X] Read book
      - [X] Take print-out
    * Recreation [50%]
      - [X] Read newspaper
      - [ ] Meditate

If a task is completed, you can move it out of the Org file using *C-c
C-x C-a*. This will create an archive file with the completed items.

# Planner

An Org mode file can be used as a planner. Each task can be marked
with any of the following states - {TODO, DONE} using *C-c C-t* key
combination. For example:

    * TODO College...
    * DONE Home [2/2]...
    * Recreation [50%]...

You can also customize the TODO states depending on your workflow by
setting *org-todo-keywords* in your Emacs startup file. For example:

    (setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT(n)" "STARTED(s)" "WAITING(w)" "|" "DONE(d)" "CANCELED(c)")))

The tasks can be scheduled using *C-c C-s*. A date is prompted for
using the *Calendar* and is placed below the list entry. For example:

    * TODO College
     * Assignments
     * Lab
       SCHEDULED: <2016-04-06 Wed>
       1. Compiler Design
       2. Programming Languages
     * Exams...

You can also add the time interval during which you intend to complete
the task. The above example with a scheduled time is shown below:

    * TODO College
     * Assignments
     * Lab
       SCHEDULED: <2016-04-06 Wed 14:00-16:00>
       1. Compiler Design
       2. Programming Languages
     * Exams...

A deadline can be added to a task using *C-c C-d* shortcut. An example
is given below:

    * TODO College
     * Assignments
     * Lab...
     * Exams
       DEADLINE: <2016-04-08 Fri>
       - Mathematics
       - Operating Systems
       - Databases
       - Compilers
    * DONE Home [2/2]...
    * Recreation [50%]...

You can have multiple Org files stored in your system, and you can
instruct GNU Emacs where to find them by setting *org-agenda-files* in
your Emacs start-up file as shown below:

    (setq org-agenda-files (quote ("/tmp")))

Additionally, if you want an agenda view to see the scheduled items
and deadlines, add the following to your GNU Emacs startup init file:

    (define-key global-map "\C-ca" 'org-agenda)

In the Org file, when you press *C-c a*, the following agenda will
show up in a new buffer:

    Week-agenda (W14):
    Monday      4 April 2016 W14
      test:       In   4 d.:  Exams
    Tuesday     5 April 2016
    Wednesday   6 April 2016
      test:       14:00-16:00 Scheduled:  Lab
    Thursday    7 April 2016
    Friday      8 April 2016
      test:       Deadline:   Exams
    Saturday    9 April 2016
    Sunday     10 April 2016

# Tables

Org-mode has a built-in table editor which neatly aligns the column data. For example:

    * TODO College...
    * DONE Home [2/2]...
    * Recreation [50%]
      - [X] Read newspaper
      - [ ] Meditate
      | Day       | Time | Status |
      |-----------+------+--------|
      | Monday    | 1.25 | Done   |
      | Tuesday   | 1.50 | Done   |
      | Wednesday |      |        |
      | Thursday  |      |        |
      | Friday    |      |        |
      | Saturday  |      |        |
      | Sunday    |      |        |

You can also use spreadsheet formula on these tables to perform calculations. For example:

    * TODO College...
    * DONE Home [2/2]...
    * Recreation [50%]
      - [X] Read newspaper
      - [ ] Meditate
      | Day       | Time | Status |
      |-----------+------+--------|
      | Monday    | 1.25 | Done   |
      | Tuesday   | 1.50 | Done   |
      | Wednesday |      |        |
      | Thursday  |      |        |
      | Friday    |      |        |
      | Saturday  |      |        |
      | Sunday    |      |        |
      |-----------+------+--------|
      | Total     | 2.75 |        |
      #+TBLFM: @9$2=vsum(@2$2..@8$2)

# Exporting

The Org file can be exported to multiple output formats (TeX, HTML,
ASCII, PDF, etc.). Using *C-c C-e* will produce a buffer with the 'Org
Export Dispatcher' menu to select an exporter. This is shown in the
following figure:

<img alt="Org Export Dispatcher" src="http://www.shakthimaan.com/gallery/2017/osfy-emacs-mode-mar-31-2017/000-org-export-dispatch.png"></img>

You can also write your own backend customisations to suit your needs.

# Literate programming

Donald Knuth coined the term 'Literate Programming' in 1984. To quote
him:

> "I believe that the time is ripe for significantly better documentation
> of programs, and that we can best achieve this by considering programs
> to be works of literature. Hence, my title: 'Literate Programming'.
> 
> Let us change our traditional attitude to the construction of
> programs: Instead of imagining that our main task is to instruct a
> computer what to do, let us concentrate rather on explaining to human
> beings what we want a computer to do."

Org mode supports this style of programming using Babel. You need to
activate the support for the programming languages in your GNU Emacs
startup file. For example, the following code snippet helps to execute
Bash shell scripts.

    (org-babel-do-load-languages
     'org-babel-load-languages
     '((sh . t)
       ))

Consider a shell command to find the disk usage. You can create an Org
file, and enclose the command in a Babel code block as shown below:

    #+BEGIN_SRC sh
      df -h
    #+END_SRC

When you press *C-c C-c* on this code block, you will be prompted with
the string "Evaluate this sh code block on your system? (yes/no)." If
you input "yes", the output is produced in a *Results* section as
shown below:

    #+RESULTS:
    | Filesystem | Size | Used | Avail | Use% | Mounted        | on |
    | udev       | 1.9G | 0    | 1.9G  |   0% | /dev           |    |
    | tmpfs      | 384M | 6.0M | 378M  |   2% | /run           |    |
    | /dev/sda1  | 913G | 75G  | 792G  |   9% | /              |    |
    | tmpfs      | 1.9G | 57M  | 1.9G  |   3% | /dev/shm       |    |
    | tmpfs      | 5.0M | 4.0K | 5.0M  |   1% | /run/lock      |    |
    | tmpfs      | 1.9G | 0    | 1.9G  |   0% | /sys/fs/cgroup |    |
    | tmpfs      | 384M | 64K  | 384M  |   1% | /run/user/1000 |    |

You can learn more on Babel from their web page <a
href="http://orgmode.org/worg/org-contrib/babel/">http://orgmode.org/worg/org-contrib/babel/</a>.

A mobile version of Org mode is also available as Free and Open Source
Software. You can use a third party service to sync your Org files
between your mobile and system. Since the files are plain text, they
can also be revision controlled using Git or any version control
software.

Please refer to the Org reference manual at <a
href="http://orgmode.org/#docs">http://orgmode.org/#docs</a> for more
tips, customisation options and documentation.
