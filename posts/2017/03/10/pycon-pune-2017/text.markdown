---
author: SK
tags: 2017, travel
timestamp: 07:30:00
title: PyCon Pune 2017
---

I attended [PyCon Pune 2017](https://pune.pycon.org/) conference
between February 16-17 (Thursday-Friday), 2017 at [Amanora - The Ferns
Hotels and
Club](http://amanora.com/township-project-in-pune-about-parktown),
Pune.

<img alt="Ferns Hotel" src="http://shakthimaan.com/images/2017/pycon-pune-2017/1-ferns-hotel.jpg" />

# Day I

I reached the venue early in the morning, to be of any help to the
volunteers. The projector had to be checked, and I used my laptop to
test the same. After changing couple of cables and adapters, the
clarity on the screen was good.

This event had a single track where everyone sat in one big hall. I
welcome this change!

[Honza Král](https://github.com/HonzaKral) started the conference with
his keynote, titled "(My) OSS Life". He shared his experiences and
learning in the Free and Open Source Software (F/OSS) world. At present, he
maintains the Python drivers for [Elasticsearch](https://www.elastic.co/).

<img alt="Honza Král" src="http://shakthimaan.com/images/2017/pycon-pune-2017/2-honza-key-note.jpg" />

The keynote was followed by [Anand
Chitipotu's](http://anandology.com/) talk on "Writing Beautiful
Code". He illustrated code examples on how to write simple, elegant,
readable Python code. The use of meaningful variable names, comments
were emphasized a lot. It was a short list of collated points on basic
mistakes that newbie programmers make, and how to effectively write
beautiful code.

[Florian Fuchs](https://goldmag.de/) then spoke on "Hacking Mailing
Lists - The Mailman 3 API Ecosystem". He explained the new
architecture and API with code examples. He has been hacking on the
new Mailman 3 web UI and the Python API bindings.

After attending these talks, I made a visit to the three booths at the
conference - [Red Hat](https://www.redhat.com), [Python Software Foundation](https://www.python.org/psf/) and
[reserved-bit](https://reserved-bit.com/). I also signed copies of [my book](http://shakthimaan.com/what-to-do.html) that people had
brought.

After lunch, I attended the "i18n-ise Django Apps" talk by [Sundeep
Anand](http://sundeep.co.in/), where he showed the
internationalization processes for a Django
application. All the relevant file modifications and commands involved
were demonstrated.

[John ‘warthog9’ Hawley](https://github.com/warthog9) is a Perl guy
and gave an interesting keynote on building your own hardware, and why
you should do that. He explained the various challenges he had faced,
the process involved in the same. He had exclusively designed and
produced a "Battle Bunny" embedded micro-Python kit for the conference
and development sprints.

The "Building Trust in Releases" talk by [Nigel
Babu](https://nigelb.me/) was very informative. He explained four
important aspects in release management - cadence, documentation,
testing, and empathy. This was also an experience report on DevOps
practices, and was quite detailed and useful.

The last keynote of the day was by a Physics teacher, [Praveen
Patil](http://www.gnovi.in/). He shared his exploration on using
Python to teach Physics to high school students. He is actively
involved in [ExpEYES Project](http://expeyes.in), teacher training
programs and also contributes content to National Repository of Open
Educational Resources (NROER).

<img alt="Praveen Patil system setup" src="http://shakthimaan.com/images/2017/pycon-pune-2017/3-praveen-demo.jpg">

# Day II

The morning keynote was by [Katie
Cunningham](http://therealkatie.net/). She was initially testing the
microphone and laptop by singing nursery rhymes. While the organizers
decided to wait for people to arrive and settle down, there was time
for lightning talks. So, I volunteered to start the day with the
resounding ["Nursery
Rhymes"](http://shakthimaan.com/downloads.html#nursery-rhymes). After
a couple of other lightning talks, Katie started her keynote on
accessibility guidelines. It was quite an informative session.

"You can help develop Python - and you should!" talk by [Stephen
Turnbull](http://turnbull.sk.tsukuba.ac.jp/Blog/) was on the history
of Python, PEP guidelines and the functioning of the community. I also
had a chance to talk with him personally on the story of
[XEmacs](https://www.xemacs.org/).

[Farhaan Bukhsh](https://farhaanbukhsh.wordpress.com/) and [Vivek
Anand](http://www.vivekanand.xyz/) presented their Google Summer of
Code (GSoC) work on the project [Pagure](https://pagure.io/), which is
an alternative to GitHub and GitLab. They shared the past, present and
future roadmap for the project. In the "Testing native binaries using
CFFI" talk, [Noufal Ibrahim](http://nibrahim.net.in/) demonstrated how
to write Python bindings using CFFI.

After lunch, there was time for lightning talks. Different Python user
group communities (PyDelhi, HydPy, PythonPune, PyLadies Pune) pitched
about their work . I had prepared the sequel to ["The Yet Another
Lightning
Talk"](http://shakthimaan.com/downloads.html#yet-another-lightning-talk)
and requested the audience to sing on my behalf. The feedback was
positive, as usual. The latest addition to the nursery rhyme is as
follows:

    Twinkle, Twinkle, unit tests,
    How I wonder, where you exist!
    I will write unit tests,
    Until the project is laid to rest.

The afternoon keynote was by [Nick
Coghlan](http://www.curiousefficiency.org/). He also shared his
know-how on Free and Open Source Software and community best
practices. "Django on Steroids - Lessons from Scale" by [Sanket
Saurav](https://sanketsaurav.com/) was a good technical,
intermediate-level talk on Django customization, nginx settings,
scaling and deployment.

The last keynote of the day and the conference was by [Terri
Oda](http://terri.zone12.com/) on "Is OSS more secure?". She presented the various dimensions on which one needs to answer the question. She
concluded by saying that there is definitely more scope in F/OSS to be
more secure given the number of people involved, and the transparency
in the process.

# Conclusion

The number of participants at the conference was more than five
hundred, and they all came on a week day to attend! For a first-time
event, that is quite impressive. This clearly shows that there is a
demand for such events across India.

<img alt="PyCon Pune 2017 group photo" src="http://shakthimaan.com/images/2017/pycon-pune-2017/4-group-photo.jpg" /><br />
PC: [Kushal Das](kushaldas.in)

Initially, there was a lot of
[resistance](https://mail.python.org/pipermail/inpycon/2016-October/010755.html) (follow the thread)
to this event, including the name of the event. Communities are meant
for innovation, and stifling is futile.  You can learn a lot in a
community, and there are guidelines and best practices that are
followed.

It was a four day event with development sprints, and hence it had to be a PyCon. Legally, the
Python Software Foundation allows using the name "PyCon" for regional
events too. Given the context and the necessity, I am happy that the
Python Software Foundation (PSF) got the message right, and understood
the need for the conference and supported it in a big way!

The development sprints had a limited seating capacity, and the
registration got over early. I was informed that there were
hundred more requests for the development sprints, which again
emphasizes the need for such events! I also had a chance to meet some
of the #dgplug (irc.freenode.net) folks with whom I have been
interacting online on IRC.

It does take a lot of effort to organize a conference, and I
congratulate the PyCon Pune team on their first event.
