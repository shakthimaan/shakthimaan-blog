---
author: SK
tags: 2008, FOSS
timestamp: 10:00:00
title: mukt.in, Osmania University, Hyderabad
---

<a href="http://mukt.in">mukt.in</a> was organized in <a
href="http://www.osmania.ac.in/Engineering%20College/Comp-Science-Eng1.htm">CSE
department, Osmania University, Hyderabad</a>, between August 1-3,
2008. Thanks to all the volunteers, speakers, organizers, and sponsors
for their support and contribution.

<img alt="group photo"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album49/11_day_two_group_photo.jpg"></img><br
/>

Couple of my presentations are available: <a
href="http://shakthimaan.com/downloads.html#inky-pinky-poky">Inky
Pinky Poky</a>, and <a
href="http://shakthimaan.com/downloads.html#openmoko-free-your-phone">Openmoko
- Free Your Phone</a>.

Some pictures taken by <a
href="http://picasaweb.google.com/jigish.gohil/MuktIn">Jigish
Gohil</a>, and few at my <a
href="http://www.shakthimaan.com/Mambo/gallery/album49">/gallery</a>.
