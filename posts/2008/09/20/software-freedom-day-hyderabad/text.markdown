---
author: SK
tags: 2008, FOSS
timestamp: 10:00:00
title: Software Freedom Day, Primora, Hyderabad
---

I gave a brief session on installation concepts, and methods, FOSS -
history, and communication tools at the Software Freedom Day
celebrations at Primora, Hyderabad. Krish gave a demo of LiveCDs, and
Manish gave a demo on Compiz.

<img alt="cake cutting"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album50/1_cake_cutting.jpg"></img><br
/>

Thanks to Primora for offering us their office space for this meet-up.

Few photos taken during the meet are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album50">/gallery</a>.
