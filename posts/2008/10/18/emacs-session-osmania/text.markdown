---
author: SK
tags: 2008, FOSS, emacs
timestamp: 10:00:00
title: Emacs a day keeps the vi-zing away, Osmania University, Hyderabad
---

Thanks to all those who attended the FOSS session on "Emacs a day
keeps the vi-zing away" on Saturday, October 18, 2008 at <a
href="http://www.osmania.ac.in/Science%20College/Astronomy1.htm">Department
of Astronomy, Osmania University</a>.

The key bindings used in the session are available at <a
href="http://shakthimaan.com/downloads/glv/hyd/emacs-a-day-keeps-the-vi-zing-away.txt">emacs-a-day-keeps-the-vi-zing-away.txt</a>

Eighteen people attended the session. We explained few key bindings,
and then the participants tried it out on their systems. If they got
stuck, then we helped them out. We waited for everyone to get the
commands working on their system, before moving to the next set of key
bindings. No-one is left behind :)

Those who didn't know keyboard typing, had to search for the keys,
most of the time. When they finally did, they were not looking at the
monitor to see the output, because they were keen on typing the keys
correctly! It will take them some time and practice to get used to it.

Most of them were able to understand the commands and try it out by
themselves though. It was good to see some hands-on experience in the
labs.

Nikhil suggested on doing a vim session, and I welcome the same. I'd
encourage anyone to come forward to take sessions, or make requests to
the group.

Thanks to Prof. Hasan for arranging the labs for the session. He
suggested inviting the adjacent Mathematics Department students in
future sessions. They are in the process of setting up another lab
too.
