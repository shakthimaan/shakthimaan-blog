---
author: SK
tags: 2008, travel
timestamp: 10:00:00
title: Göteborg, Sweden
---

<img alt="Stora Nygatan"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album52/9_stora_nygatan.jpg"></img><br
/>

<img alt="Lindholmen"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album52/8_lindholmen.jpg"></img><br
/>

<img alt="Sanatoriegatan"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album52/13_sanatoriegatan.jpg"></img><br
/>

More photos available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album52">/gallery</a>.
