---
author: SK
tags: 2008, FOSS, travel
timestamp: 10:00:00
title: LinuxTag, Berlin, Germany
---

<img alt="LinuxTag entrance"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album46/68_myself_at_lt2008_entrance.jpg"></img><br
/>

<img alt="Reichstag"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album46/18_reichstag_building.jpg"></img><br
/>

<img alt="Meet at Maredo"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album46/52_gang_at_maredo.jpg"></img><br
/>

More photos available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album46">/gallery</a>.

