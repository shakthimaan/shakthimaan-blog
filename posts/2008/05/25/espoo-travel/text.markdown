---
author: SK
tags: 2008, travel
timestamp: 10:00:00
title: Espoo, Helsinki, Finland
---

<img alt="Helsinki cathedral"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album48/24_distant_view_helsinki_cathedral.jpg"></img><br
/>

<img alt="Leppävaara station"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album48/5_lepp_vaara_station.jpg"></img><br
/>

<img alt="Helsingin rautatieasema"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album48/17_helsinki_central_railway_station.jpg"></img><br
/>

More photos available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album48">/gallery</a>.
