---
author: SK
tags: 2008, FOSS
timestamp: 10:00:00
title: Debian installation, SLC, Andhra Pradesh
---

I conducted a "Debian GNU/Linux Installation" session as part of the
first day of the 15-day Industry oriented summer school program,
conducted by FSF-AP chapter, today, May 16, 2008, at <a href="http://www.slc.edu.in/">Satyam Learning
Centre Institute of Engineering and Technology, Piglipur village,
Hayath Nagar, Andhra Pradesh</a> (few kilometers past Ramoji Film City).

<img alt="group photo"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album44/8_labs_to_debian.jpg"></img><br
/>

I would like to thank FSF-AP for the arrangements. Due to official
work, I was able to spend only one day. This program is conducted in
parallel in three other colleges outside of Hyderabad. In the evening,
they provide training on team-building, communication skills,
personality development, and other soft skills. Please check <a
href="http://swecha.org/"></a> for more details.

Students were mostly from Andhra Pradesh, and few from Tamil Nadu. The
INR 1500 cost for the program is inclusive of stay, food, and training
(lectures, and lab-usage) for the 15 days.

Most of organizers are volunteers - students, academicians or people
working in the Industry in Hyderabad. They also meet during the
weekends at:

FSF India A.P Chapter,
Flat No.201, Karan Apartments,
(exactly opposite to petrol bunk)
Paradise,
Secunderabad.
Landmark: Beside Sandarshini hotel.

<a href="http://www.hyderabadcityinfo.com/city-bus.html">Hyderabad bus
routes</a> for reference.

Few photos are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album44">/gallery</a>.
