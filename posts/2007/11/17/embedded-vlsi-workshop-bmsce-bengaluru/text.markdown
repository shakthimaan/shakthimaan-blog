---
author: SK
tags: 2007, FOSS, vlsi, vhdl, verilog
timestamp: 10:00:00
title: Embedded/VLSI workshop, BMSCE, Bengaluru
---

A one-day embedded/VLSI Free/Open Source workshop at <a
href="http://www.bmsce.in/">BMS College of Engineering, Bengaluru
(Bangalore), Karnataka</a> was organized today, Saturday, November 17,
2007.

I handled embedded, system architecture concepts, Linux device
drivers, and an introduction to OpenMoko. Aanjhan addressed FOSS EDA
tools, Verilog, embedded GNU/Linux labs HOWTO et. al.

I would like to thank Shashank Bharadwaj for working with me during
the past few weeks in coordinating/organizing the event.

Students were mostly from EEE/ECE background. According to the few
interested students, the institution management are quite willing to
help/invest for the students, but, very few takers for it.

There are few permanent faculty, who teach different courses every
semester. But, most of them move to the Industry.

During the workshop, I did emphasize on the need to use FOSS tools to
enhance their skills, and get exposure to the Industry, FOSS
community, and the real world.

Relevant links:

* <a href="http://fci.wikia.com/wiki/Bangalore/BMSCE">fci wikia on
  BMSCE</a>

* <a
  href="http://www.shakthimaan.com/downloads.html#i-want-2-do-project-tell-me-wat-2-do">i-want-2-do-project-tell-me-wat-2-do</a>

* <a
  href="http://www.shakthimaan.com/downloads.html#mailing-list-guidelines">Mailing
  list guidelines</a>

* <a
  href="http://www.shakthimaan.com/downloads.html#openmoko-free-your-phone">OpenMoko
  introduction</a>

* <a
  href="http://www.shakthimaan.com/downloads.html#linux-device-driver-programming-code-examples">Linux
  device driver code examples</a>
