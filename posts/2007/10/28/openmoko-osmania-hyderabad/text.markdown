---
author: SK
tags: 2007, FOSS
timestamp: 10:00:00
title: OpenMoko talk, Osmania University, Hyderabad
---

I gave an introductory talk on the <a
href="http://www.openmoko.org">OpenMoko</a> project at the <a
href="http://www.osmania.ac.in/Science%20College/Astronomy1.htm">Department
of Astronomy, Osmania University, Hyderabad</a>, Andhra Pradesh on
Sunday, October 28, 2007.

The audience were introduced to the Neo1973 device and its
accessories. The <a href="http://openmoko.org">openmoko.org</a>
project documentation website was shown. The recommended flavor for
working on OpenMoko is Debian or Debian-based distributions like
Ubuntu.

The different methods of installation - online and through <a
href="http://openembedded.org">openembedded.org</a> were
mentioned. SSH into the device, and exporting the user interface were
demonstrated. Software emulation through QEMU was also shown. Building
a simple "Hello World" application was explained using autotools,
Makefile and the ipk, bitbake packaging system. Audio and video were
tested on the device. The next version of Neo and Openmoko is expected
to have better hardware and more features.

The feeds of Neo and OpenMoko are at <a
href="http://scap.linuxtogo.org">scap.linuxtogo.org</a>.
