---
author: SK
tags: 2007, FOSS
timestamp: 10:00:00
title: Free Software workshop, University of Hyderabad, Gachibowli
---

I conducted a one-day Free(/Open Source) Software workshop for <a
href="http://dcis.uohyd.ernet.in/">Department of Computer and
Information Sciences, University of Hyderabad, Gachibowli,
Hyderabad</a> in association with IEEE Student Chapter, Hyderabad, on
Saturday, October 27, 2007.

<img alt="IEEE poster"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album37/3_ieee_poster.jpg
"></img><br />

Students were mostly from the Masters' programme. The forenoon
sessions included technical discussions/presentations - system,
embedded, device driver concepts; Linux device drivers; introduction
to OpenMoko: build, development for OpenMoko; demo of Neo1973.

The afternoon sessions were non-technical discussions - <a
href="http://www.shakthimaan.com/downloads.html#i-want-2-do-project-tell-me-wat-2-do">i-want-2-do-project.tell-me-wat-2-do</a>,
<a
href="http://www.shakthimaan.com/downloads.html#mailing-list-guidelines">mailing
list guidelines</a>, <a
href="http://www.shakthimaan.com/downloads.html#free-software-communication-guidelines">Free
Software communication guidelines</a>, and brainstorming sessions.

More than half of the faculty from the University have done their
MS/PhDs abroad. The university and the faculty have the freedom to
implement whatever they like, which is cool.

The labs use Fedora/Red Hat/SuSE distributions. They run their own
mail+web servers, and use NFS. There are few professors who give
assignments with Makefiles, gdb, Linux kernel compilation, but, they
are just starting, and experimenting with these. It was announced that
INR 20,000 will be alloted each year for Free/Open Source events.

If anyone is interested in working with this team, contact me offline,
and I shall provide the contacts.

Thanks to Dr. Vineet, Prof. Anupama, Dr. Madhav Negi (IEEE), Dr. Atul
Negi for their help, coordination in planning this workshop.

Few photos are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album37">/gallery</a>.

Feedback/criticisms/suggestions on the presentations welcome, as always.
