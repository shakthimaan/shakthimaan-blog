---
author: SK
tags: 2007, FOSS, vlsi
timestamp: 10:00:00
title: FOSS VLSI talk, NIT, Calicut
---

I gave a couple of talks: <a
href="http://www.shakthimaan.com/downloads.html#alliance">"Alliance
VLSI CAD Tools"</a>, and <a
href="http://www.shakthimaan.com/downloads.html#careers-with-gnu-linux">"Careers
with GNU/Linux"</a> at <a href="http://www.nitc.ac.in/">National
Institute of Technology, Calicut, Kerala</a> on Saturday, March 3,
2007.

The code examples and presentation are available. I'd like to thank
Praveen for inviting me over (which he didn't have to do). The
interaction with the students was good.

The schedule was a bit tight, and I didn't take any pictures.