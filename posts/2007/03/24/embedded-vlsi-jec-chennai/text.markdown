---
author: SK
tags: 2007, FOSS, vlsi
timestamp: 10:00:00
title: Embedded/VLSI workshop, JEC, Chennai
---

I conducted couple of sessions for the ECE, EEE and EIE department
students of <a href="http://www.jec.ac.in/">Jaya Engineering College,
Thiruninravur, Tamil Nadu</a>, today, Saturday, March 24, 2007.

I started off with an introduction to Free/Libre/Open Source Software,
taught them examples of <a
href="http://www.shakthimaan.com/downloads.html#alliance">Alliance
VLSI CAD tools</a>, and <a
href="http://www.shakthimaan.com/downloads.html#embedded-gnu-linux-labs-howto">embedded
GNU/Linux HOWTO</a>. I gave a demo of an embedded development
kit. After the sessions, we had an informal discussion with the
faculty of EEE, EIE.

After lunch, we had an informal discussion with few
students in the FOSS lab about their final year
project work. Most of them knew only about Java, as
that was what was "prescribed" in the syllabus. But, I
gave them an insight on the different domains that
they could work on, and possibilities available in the
Industry. 

The FOSS lab is running Red Hat 9. The systems have
256 MB RAM, so, I asked them to upgrade to a recent
distro ASAP. They also are in the process of moving to
PHP, MySQL for their website.

Their servers have been obtained from our good friends at <a
href="http://www.deeproot.in/">DeepRoot</a>, so they can try to setup
an internal portal (PHP, mysql) for intra-department communication.

The system administrator in the lab wanted few tips on
networking with Red Hat. I showed him how to use scp
to transfer files between two Red Hat systems. He was
so happy! During summer holidays, Prof. Kumaran wants
to do some FOSS camp for the staff. Should be fun, if
you guys can help out, and if you are in town.

I would like to thank Prof. Kumaran for his continued support. Special
thanks also to Harish for coordinating and organizing the event.
