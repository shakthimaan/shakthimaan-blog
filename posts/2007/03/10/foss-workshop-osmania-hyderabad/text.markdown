---
author: SK
tags: 2007, FOSS, vlsi
timestamp: 10:00:00
title: F/OSS workshop, Osmania University, Hyderabad
---

I conducted a "Careers with GNU/Linux and Free Software" workshop on
Saturday, March 10, 2007 at <a
href="http://www.osmania.ac.in/">Osmania University, Hyderabad</a>.

This is the first time that I actually "flew" to a workshop. Indian
Airlines flight was delayed by two hours. The workshop was to start at
10:15, but, I reached Hyderabad only at 1200 noon. Hyderabad is a
Pretty Hot Place (PHP?). It was 30 degree celsius, but, felt like 50!
I presented the following: <a
href="http://www.shakthimaan.com/downloads.html#careers-with-gnu-linux">Careers
with GNU/Linux</a>, <a
href="http://www.shakthimaan.com/downloads.html#free-software-for-engineers">Free
Software</a>, GNU/Linux desktop, <a
href="http://www.shakthimaan.com/downloads.html#embedded-gnu-linux-labs-howto">Embedded
HOWTO</a>, <a
href="http://www.shakthimaan.com/downloads.html#alliance">Alliance
VLSI CAD Tools HOWTO</a>.

The posters made for the workshop are available: <a
href="http://shakthimaan.com/downloads/glv/posters/flyer_workshop.pdf">[Flyer
I]</a> <a
href="http://shakthimaan.com/downloads/glv/posters/flyer_workshop_2.pdf">[Flyer
II]</a>

There were about 180 students from in and around Hyderabad. There were
students who came all the way from Vijayawada (300 km) by bus,
overnight, and I was elated. Students from other colleges who
participated in the workshop were eager to have similar workshops for
their colleges. I have agreed for the same. 

I was able to meet folks from Free Software Foundation, Andhra Pradesh
Chapter. I shall provide the organizers' contacts to NRC-FOSS.

I would like to thank Dr. Atul Negi (Vice-Chairman, IEEE Hyderabad
Section), Dr. Madhav Negi (Chairman, IEEE Computer Society, Hyderabad
Section) for their support.

Thanks also to Santosh Arjun (Vice-Chairman, IEEE Student Chapter,
Osmania University) for working with me for more than a month in
organizing this workshop.
