---
author: SK
tags: 2007, travel
timestamp: 10:00:00
title: Grimstad, Norway
---

<img alt="Teknologisenter"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album40/2_teknologisenter.jpg"></img><br
/>

<img alt="View from Østereng & Benestad"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album40/13_from_ostereng.jpg"></img><br
/>

<img alt="Fields"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album40/22_fields.jpg"></img><br
/>

More photos available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album40">/gallery</a>.
