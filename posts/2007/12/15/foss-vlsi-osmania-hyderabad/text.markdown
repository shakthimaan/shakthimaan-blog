---
author: SK
tags: 2007, FOSS, vlsi, vhdl
timestamp: 10:00:00
title: FOSS VLSI tools, Osmania University, Hyderabad
---

I was able to get a 30-minute session during the "VLSI-Trends and
Applications" workshop to talk about FOSS VLSI tools, conducted by the
<a href="http://www.osmania.ac.in/physics/">Department of Physics,
Osmania University, Hyderabad</a>, today, Saturday, December 15,
2007. Thanks to Jagan Vemula (Qvantel), and Dr. Lingam Reddy
(Professor, Department of Physics, OU) for their help in this regard.

<img alt="Audience"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album38/1_audience.jpg
"></img><br />

I did a short 10-minute presentation (with screenshots): <a
href="http://shakthimaan.com/downloads/glv/presentations/vlsi-foss-1.0.odp
">[odp]</a> <a
href="http://shakthimaan.com/downloads/glv/presentations/vlsi-foss-1.0.pdf">[pdf]</a>

I then gave a demo of VHDL examples, and showed them FOSS project
websites (<a href="http://opencores.org">opencores.org</a>, and <a
href="http://opencollector.org">opencollector.org</a>, and <a
href="http://www.shakthimaan.com/downloads/glv/embedded-howto/
">Embedded HOWTO</a>).

As usual students didn't ask any questions during the session, but,
had a long offline Q&A, and discussion after the session.

Few photos are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album38">/gallery</a>.
