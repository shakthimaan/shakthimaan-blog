---
author: SK
tags: 2007, travel
timestamp: 10:00:00
title: Arendal, Norway
---

<img alt="Beautiful Arendal"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album41/17_beautiful_arendal.jpg"></img><br
/>

<img alt="Boats and buildings"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album41/25_boats_and_buildings.jpg"></img><br
/>

<img alt="bridge to Tromøy"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album41/37_connecting_bridge.jpg"></img><br
/>

More photos available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album41">/gallery</a>.
