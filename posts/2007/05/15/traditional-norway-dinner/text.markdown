---
author: SK
tags: 2007, travel
timestamp: 10:00:00
title: Traditional Norwegian style dinner
---

<img alt="Departure"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album42/1_departure.jpg"></img><br
/>

<img alt="Houses"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album42/9_beautiful_houses.jpg"></img><br
/>

<img alt="Dinner"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album42/7_with_company.jpg"></img><br
/>

More photos available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album42">/gallery</a>.
