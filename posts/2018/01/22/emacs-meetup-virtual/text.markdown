---
author: SK
tags: 2018, emacs
timestamp: 17:00:00
title: Emacs Meetup (virtual), January 15, 2018
---

I wanted to start the New Year (2018) by organizing an Emacs meetup
session in the APAC time zone. Since there are a number of users in
different cities, I thought a virtual session will be ideal. An online
Google Hangout session was scheduled for Monday, January 15, 2018 at
1000 IST.

<img alt="Hangout announcement" src="http://shakthimaan.com/images/2018/emacs-hangout-announcement/emacs-hangout-announcement.png"><br />

Although I announced the same only on Twitter and IRC (#emacs on
irc.freenode.net), we had a number of Emacsers join the session. The
chat log with the useful web links that were shared are provided below
for reference.

We started our discussion on organizing Org files and maintaining TODO
lists.

~~~~
9:45 AM Suraj Ghimire: it is clear and loud :) video quality is good too 
                       yes. is there any session today on hangouts ?
                       wow thats nice thanks you for introducing me to emacs :). I am happy emacs user
                       should we add bhavin and vharsh for more testing
                       oh you already shared :)
	
9:55 AM Suraj Ghimire:  working on some of my todos https://i.imgur.com/GBylmeQ.png
~~~~

For few Vim users who wanted to try Emacs Org mode, it was suggested
to get started with <a
href="http://spacemacs.org/">Spacemacs</a>. Other project management
and IRC tools with Emacs were also shared:

~~~~
  HARSH VARDHAN can now join this call.
  HARSH VARDHAN joined group chat.
  Google Apps can now join this call.
  Google Apps joined group chat.
	
10:05 AM Shakthi Kannan: http://spacemacs.org/
                         https://github.com/ianxm/emacs-scrum
                         https://github.com/ianxm/emacs-scrum/blob/master/example-report.txt
                         https://www.emacswiki.org/emacs/CategoryWebBrowser
                         ERC for IRC chat
                         https://www.emacswiki.org/emacs/InternetRelayChat

10:13 AM Shakthi Kannan: https://github.com/skeeto/elfeed
                         https://www.emacswiki.org/emacs/EmacsMailingLists
	
10:18 AM Suraj Ghimire: I started using emacs after your session on emacs, before that i used to 
                        get scared due to lot of shortcuts. I will work on improvements you told me.
	
10:19 AM Shakthi Kannan: M - Alt, C - Control
                         http://www.tldp.org/HOWTO/Emacs-Beginner-HOWTO-3.html

  Google Apps left group chat.
  Google Apps joined group chat.
  Sacha Chua can now join this call.
  Sacha Chua joined group chat.
~~~~

We then discussed on key bindings, available modes, and reading
material to learn and master Emacs:

~~~~
10:27 AM Shakthi Kannan: http://shop.oreilly.com/product/9780596006488.do
	
10:31 AM Shakthi Kannan: https://www.masteringemacs.org/
                         http://shakthimaan.com/tags/emacs.html
                         http://shakthimaan.com/posts/2016/04/04/introduction-to-gnu-emacs/news.html

  Dhavan Vaidya can now join this call.
  Dhavan Vaidya joined group chat.
  Sacha Chua left group chat.
	
10:42 AM Shakthi Kannan: https://www.finseth.com/craft/
                         http://shop.oreilly.com/product/9781565922617.do

  Rajesh Deo can now join this call.
  Rajesh Deo joined group chat.
~~~~

Users also wanted to know of language modes for Erlang:
    
~~~~    
10:52 AM Shakthi Kannan: http://www.lambdacat.com/post-modern-emacs-setup-for-erlang/

  HARSH VARDHAN left group chat.
  Aaron Hall can now join this call.
	
10:54 AM Shakthi Kannan: https://github.com/elixir-editors/emacs-elixir
~~~~

Aaron Hall joined the channel and had few interesting questions. After
an hour, we ended the call.

~~~~
  Aaron Hall joined group chat.
	
10:54 AM Aaron Hall: hi!
10:54 AM Dhavan Vaidya: hi!
	
10:55 AM Aaron Hall: This is really cool!

Maikel Yugcha can now join this call.
Maikel Yugcha joined group chat.
	
10:57 AM Aaron Hall: Anyone here using Emacs as their window manager?
	
10:57 AM Suraj Ghimire: not yet :)
	
10:57 AM Shakthi Kannan: I am "mbuf" on IRC. http://stumpwm.github.io/
	
10:58 AM Aaron Hall: What about on servers? I just played around, but I like tmux for persistence and emacs inside of tmux.
	
10:59 AM Shakthi Kannan: https://github.com/pashinin/workgroups2
	
11:00 AM Aaron Hall: Is anyone compiling emacs from source?

Zsolt Botykai can now join this call.
Zsolt Botykai joined group chat.
	
11:00 AM Aaron Hall: yay, me too!

Zsolt Botykai left group chat.
	
11:00 AM Aaron Hall: it wasn't easy to start, the config options are hard
                     I had trouble especially with my fonts until I got my configure right

Maikel Yugcha left group chat.
	
11:03 AM Shakthi Kannan: https://github.com/shakthimaan/cask-dot-emacs
	
11:04 AM Aaron Hall anyone using Haskell? With orgmode? I've been having a lot of trouble with that...
                    code blocks are hard to get working
                    ghci
                    inferior?
                    not really sure
                    it's been a while since I worked on it
                    I had a polyglot file I was working on, I got a lot of languages working
                    Python, Bash, R, Javascript,
                    I got C working too
	
11:06 AM Shakthi Kannan: Rajesh: http://company-mode.github.io/
	
11:07 AM Aaron Hall: cheers, this was fun!

Aaron Hall left group chat.
Dhavan Vaidya left group chat.
Rajesh Deo left group chat.
Google Apps left group chat.
Suraj Ghimire left group chat.
~~~~

A screenshot of the Google Hangout session is shown below:

<img width="800" alt="Google Hangout screenshot" src="http://shakthimaan.com/images/2018/emacs-hangout-announcement/emacs-jan-15-2018.png"><br />

We can try a different online platform for the next meetup (Monday,
February, 19, 2018). I would like to have the meetup on the third
Monday of every month. Special thanks to <a
href="http://sachachua.com/">Sacha Chua</a> for her valuable inputs in
organizing the online meetup session.
