---
author: SK
tags: 2018, workshop
timestamp: 13:00:00
title: Elixir Workshop: MVJ College of Engineering, Bengaluru
---

I had organized a hands-on scripting workshop using the <a href="https://elixir-lang.org/">Elixir</a>
programming language for the Computer Science and Engineering
department, <a href="https://www.mvjce.edu.in/">MVJ College of Engineering, Whitefield, Bengaluru</a> on May
5, 2018.

<img alt="Elixir scripting session" src="http://shakthimaan.com/images/2018/elixir-scripting-workshop-mvj/elixir-workshop-session.png"><br />

The department were interested in organizing a scripting workshop, and
I felt using a new programming language like Elixir with the power of
the <a href="https://www.erlang.org/">Erlang</a> Virtual Machine (VM) will be a good choice. The syntax and
semantics of the Elixir language were discussed along with the
following topics:

* Basic types
* Basic operators
* Pattern matching
* case, cond and if
* Binaries, strings and char lists
* Keywords and maps
* Modules and functions

Students had setup Erlang and Elixir on their laptops, and tried the
code snippets in the Elixir interpreter. The complete set of examples
are available in the following repo:

<a href="https://gitlab.com/shakthimaan/elixir-scripting-workshop">https://gitlab.com/shakthimaan/elixir-scripting-workshop</a>

A group photo was taken at the end of the workshop.

<img alt="Elixir scripting session" src="http://shakthimaan.com/images/2018/elixir-scripting-workshop-mvj/elixir-workshop-group.png"><br />

I would like to thank Prof. Karthik Myilvahanan J for working with me
in organizing this workshop.
