---
author: SK
tags: 2018, emacs
timestamp: 16:25:00
title: Emacs Meetup (virtual), February-March, 2018
---

I have been trying to have regular monthly Emacs meetups online, starting from 2018.

The following are the meeting minutes and notes from the Jitsi meetings held online in the months of February and March 2018.

# February 2018

The February 2018 meetup was primarily focussed on using Emacs for publishing.

Using Emacs with <a href="https://jaspervdj.be/hakyll/">Hakyll</a> to build websites and resumes were discussed. It is also possible to output in muliple formats (PDF, YAML, text) from the same source.

I shared my <a href="https://gitlab.com/shakthimaan/shakthimaan-blog">shakthimaan-blog</a> sources that uses Hakyll to generate the web site. We also discussed the advantanges of using static site generators, especially when you have large user traffic to your web site.

I had created the <a href="https://gitlab.com/shakthimaan/xetex-book-template">xetex-book-template</a>, for creating multilingual book PDFs. The required supported features in the same, and its usage were discussed in the meeting.

<a href="https://kushaldas.in/">Kushal Das</a> asked about keyboards use in Emacs, and in particular for Control and Alt, as he was using the Kinesis. The best Emacs keyboard options available at <a href="http://ergoemacs.org/emacs/emacs_best_keyboard.html">http://ergoemacs.org/emacs/emacs_best_keyboard.html</a> was shared. The advantage of using thumb keys for Control and Alt was obvious with the bowl shaped keyboard layout in Kinesis.

We also talked about the <a href="https://en.wikipedia.org/wiki/Eww_(web_browser)">Emacs Web Browser (eww)</a>, and suggested the use of <a href="https://www.emacswiki.org/emacs/mu4e">mu4e</a> for checking e-mails with Emacs.

# March 2018

At the beginning of the meetup, the participants asked if there was a live stream available, but, we are not doing so at this point in time with Jitsi.

For drawing inside Emacs, I had suggested ASCII art using <a href="https://www.emacswiki.org/emacs/ArtistMode">Artist Mode</a>.

Emacs has support for rendering PDFs inside, as the following old blog post shows <a href="http://www.idryman.org/blog/2013/05/20/emacs-and-pdf/">http://www.idryman.org/blog/2013/05/20/emacs-and-pdf/</a>. nnnick then had a question on "Why Emacs?":

~~~~
nnnick 19:23:00
Can you tell me briefly why emacs is preferred over other text editors
~~~~~

The discussion then moved to the customization features and extensibility of Emacs that makes it well suited for your needs.

For people who want to start with a basic configuration for Emacs, the following repository was suggested <a href="https://github.com/technomancy/emacs-starter-kit">https://github.com/technomancy/emacs-starter-kit</a>.

I had also shared links on using Org mode and scrum-mode for project management:

* <a href="https://github.com/ianxm/emacs-scrum">https://github.com/ianxm/emacs-scrum</a>

* <a href="https://gitlab.com/shakthimaan/operation-blue-moon">https://gitlab.com/shakthimaan/operation-blue-moon</a>

I shared my <a href="https://github.com/cask/cask">Cask</a> setup link <a href="https://gitlab.com/shakthimaan/cask-dot-emacs">https://gitlab.com/shakthimaan/cask-dot-emacs</a> and mentioned that with a rolling distribution like <a href="https://www.parabola.nu/">Parabola GNU/Linux-libre</a>, it was quite easy to re-run install.sh for newer Emacs versions, and get a consistent setup.

In order to SSH into local or remote systems (VMs), <a href="https://www.emacswiki.org/emacs/TrampMode">Tramp mode</a> was suggested.

I also shared my presentation on "Literate DevOps" inspired by Howardism <a href="https://gitlab.com/shakthimaan/literate-devops-using-gnu-emacs/blob/master/literate-devops.org">https://gitlab.com/shakthimaan/literate-devops-using-gnu-emacs/blob/master/literate-devops.org</a>.

Org entries can also be used to keep track of personal journal entries. The Date Trees are helpful in this context as shown in the following web page <a href="http://members.optusnet.com.au/~charles57/GTD/datetree.html">http://members.optusnet.com.au/~charles57/GTD/datetree.html</a>.

Tejas asked about using Org files for executing code in different programming languages. This can be done using Org Babel, and the same was discussed.

~~~~
Tejas 19:38:23
can org mode files be used to keep executable code in other languages apart from elisp?

mbuf 19:38:42
Yes

mbuf 19:39:15
https://orgmode.org/worg/org-contrib/babel/languages.html
~~~~

The other useful tools that were discussed for productivity are given below:

* Magit for VCS: <a href="https://github.com/magit/magit">https://github.com/magit/magit</a>.

* Workgroups for workspace management: <a href="https://github.com/pashinin/workgroups2">https://github.com/pashinin/workgroups2</a>.

* Winner mode to switch between sessions in windows: <a href="https://www.emacswiki.org/emacs/WinnerMode">https://www.emacswiki.org/emacs/WinnerMode</a>.

* Emacs Code Browser is like a full flown IDE-like environment: <a href="http://ecb.sourceforge.net/">http://ecb.sourceforge.net/</a>.

Tejas said that he uses <a href="https://github.com/nex3/perspective-el">perspective-el</a>, but it does not have the save option - just separate workspaces to switch between them - for different projects basically.

A screenshot of the session in progress is shown below:

<img width="800" alt="Emacs APAC March 2018 meetup" src="http://www.shakthimaan.com/images/emacs/emacs-apac-mar-19-2018-screenshot.png"></img><br />

Arun also suggested using <a href="https://github.com/larstvei/Try">Try</a> for trying out Emacs packages without installation, and <a href="https://github.com/pierre-lecocq/cycle-resize">cycle-resize</a> package for managing windows.

Tejas and Arun then shared their Emacs configuration files.

~~~~
Arun 19:51:37
https://github.com/aruntakkar/emacs.d

Tejas 19:51:56
https://github.com/tejasbubane/dotemacs
~~~~

We closed the session with few references on learning Emacs Lisp:

~~~~
Tejas 20:02:42
before closing off, can you guys quickly point me to some resources for learning elisp?

mbuf 20:03:59
Writing GNU Emacs Extensions.

mbuf 20:04:10
Tejas: Emacs Lisp manual

Tejas 20:04:35
Thanks 
~~~~
