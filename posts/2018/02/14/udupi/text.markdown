---
author: SK
tags: 2018, travel, photo
timestamp: 13:00:00
title: Travel - Udupi, Karnataka
---

<img alt="Resort" src="http://shakthimaan.com/images/2018/paradise-lagoon-udupi/1-resort.jpg"></img><br />

<img alt="Infinity pool" src="http://shakthimaan.com/images/2018/paradise-lagoon-udupi/2-infinity-pool.jpg"></img><br />

<img alt="Floating restaurant" src="http://shakthimaan.com/images/2018/paradise-lagoon-udupi/3-floating-restaurant.jpg"></img><br />

<img alt="Rich and poor boats" src="http://shakthimaan.com/images/2018/paradise-lagoon-udupi/4-rich-poor-boats.jpg"></img><br />

<img alt="Udupi temple" src="http://shakthimaan.com/images/2018/paradise-lagoon-udupi/5-udupi-temple.jpg"></img><br />

<img alt="Lagoon" src="http://shakthimaan.com/images/2018/paradise-lagoon-udupi/6-lagoon.jpg"></img><br />

<img alt="St. Mary's island" src="http://shakthimaan.com/images/2018/paradise-lagoon-udupi/7-st-marys-island.jpg"></img><br />

<img alt="Sunset" src="http://shakthimaan.com/images/2018/paradise-lagoon-udupi/8-sunset.jpg"></img><br />
