---
author: SK
tags: 2016, photo
timestamp: 12:45:00
title: Karaikudi and Chettinad trip photos
---

Few photos taken on a trip to <a
href="http://en.wikipedia.org/wiki/Karaikudi">Karaikudi</a> and <a href="https://en.wikipedia.org/wiki/Chettinad">Chettinad</a>, Tamil
Nadu, India. More photos in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album95">/gallery</a>.

<img width="320" alt="Courtyard"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album95/1_open_space.jpg"></img>

<br />

<img width="320" alt="Corridor"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album95/2_corridor.sized.jpg"></img>

<br />

<img width="320" alt="Hall"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album95/3_hall.jpg"></img>
