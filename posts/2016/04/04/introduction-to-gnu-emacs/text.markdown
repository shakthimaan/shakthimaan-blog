---
author: SK
tags: 2016, emacs
timestamp: 15:15:00
title: Introduction to GNU Emacs
---
*[Published in Open Source For You (OSFY) magazine, July 2015 edition.]*

# Introduction

GNU Emacs is a very popular text editor written in C and Emacs
Lisp. It can be run on many platforms, and can be easily customised
and extended for user needs. It was created by Richard Stallman, the
founder of the GNU project. This article is the first in a series on
how to use it. There are a number of tutorials available for GNU
Emacs; this series of articles provide one approach to understanding
and learning the software. You are encouraged to refer to the official
GNU Emacs reference manual for more information, and it supersedes
everything. Here are a few interesting quotes on Emacs.

> "I use Emacs, which might be thought of as a thermonuclear word
> processor" &#x2013; Neal Stephenson (In the Beginning&#x2026; Was the Command Line)

> "Emacs is undoubtedly the most powerful programmer's editor in
> existence. It's a big, feature-laden program with a great deal of
> flexibility and customizability. &#x2026; Emacs has an entire programming
> language inside it that can be used to write arbitrarily powerful
> editor functions." &#x2013; Eric S. Raymond (from 'The Art of UNIX Programming')

> "Personally, I feel inspired whenever I open Emacs. Like a craftsman
> entering his workshop, I feel a realm of possibility open before me. I
> feel the comfort of an environment that has evolved over time to fit
> me perfectly - an assortment of packages and keybindings which help me
> bring ideas to life day after day." &#x2013; Daniel Higginbotham (in 'Clojure for
> the Brave and True')

> "EMACS could not have been reached by a process of careful design,
> because such processes arrive only at goals which are visible at the
> outset, and whose desirability is established on the bottom line at
> the outset. Neither I nor anyone else visualized an extensible editor
> until I had made one, nor appreciated its value until he had
> experienced it. EMACS exists because I felt free to make individually
> useful small improvements on a path whose end was not in sight." &#x2013;
> Richard Stallman

# Installation

You can use your favourite GNU/Linux distribution package manager to
install GNU Emacs. On Debian/Ubuntu, you can install with the
following command:

~~~~ {.bash}
$ sudo apt-get install emacs
~~~~

On Fedora, you can use the Yum package manager as shown below:

~~~~ {.bash}
$ sudo yum install emacs
~~~~

The emerge tool can install GNU Emacs on Gentoo, as follows:

~~~~ {.bash}
# emerge --ask app-editors/emacs
~~~~
    
On Arch, the Pacman software can help you in installing GNU Emacs:

~~~~ {.bash}
$ sudo pacman -S emacs
~~~~

Use the Zypper package manager in SUSE as shown below:

~~~~ {.bash}
$ sudo zypper install emacs
~~~~

# Startup

If you use the Unity interface, you can search for emacs in the Dash,
and it will show the Emacs icon that you can click to open the
editor. This is illustrated in the following figure.

<img alt="Unity Emacs search image" src="http://www.shakthimaan.com/images/emacs/final-emacs-unity.png"></img><br />

On the Metacity interface or any other desktop environment with a
panel, you can open GNU Emacs from "Applications" -> "Accessories" ->
"Emacs" as shown below:

<img alt="Metacity Emacs" src="http://www.shakthimaan.com/images/emacs/final-emacs-metacity.png"></img><br />

You can also open the editor from the terminal by simply typing
'emacs' and hitting the return key.

~~~~ {.bash}
$ emacs
~~~~

The version that I have used for this article is GNU Emacs 24.3.1.

# Exit

To exit from GNU Emacs, you need to use C-c C-q, where 'C' stands for
the Control key. You can also use your mouse and close the editor by
clicking on the 'x', but, GNU Emacs was designed to be completely
usable with a keyboard, and I am going to encourage you to only use
the keyboard shortcuts.

# Concepts

While you can work using GUI editors, I am going to teach you to work
entirely on the keyboard to experience the power of shortcuts in GNU
Emacs. You can disconnect the mouse and your touchpad when working on
GNU Emacs. Just as an exercise, I'd encourage you to remove your mouse
completely and see how you can work with a computer for one day. You
will realise that a lot of user interfaces are heavily dependent and
designed for mouse interactions! By only using the keyboard, you can
be blazingly fast and productive.

The keyboard shortcuts in GNU Emacs may seem to involve many
keys. But, please bear with me on this, because the way the shortcuts
are designed, you will be able to remember them easily. As you
practice, you will gain insight into how consistently they have been
defined.

GNU Emacs is a 'stateless' editor for the most part. By 'stateless', I
mean that there are no specific state transitions that need to happen
before you can use the commands. There does exist the concept of
modes. When you open GNU Emacs, you will see menus, buffer and a mode
line as illustrated in the following figure:

<img alt="Emacs default screen" width="650" src="http://www.shakthimaan.com/images/emacs/final-emacs-default-text.png"></img><br />

As mentioned earlier, we will not be clicking on the menus or icons
with a mouse, but only use keyboard shortcuts. Everything is a buffer
in GNU Emacs. Each buffer can have one major mode and one or more
minor modes. The mode determines the keyboard shortcuts that are
applicable primarily on the buffer. Examples of major modes are given
below:

--------------------------------------------------
Mode            Description
----            ----------------------------------
Text mode       Writing text

HTML mode       Writing HTML

cc mode         Writing C, C++ and C-like programs

Dired mode      Handling files and directories

Shell mode      Working with shell

LaTeX mode      Formatting TeX and LaTeX files

Picture mode    Creating ASCII art

Outline mode    Writing outlines

SQL mode        Interacting with SQL databases

Lisp mode       Writing Lisp programs
--------------------------------------------------

The mode line exists below the buffer and it gives you a lot of
information on the status of the buffer, such as what modes are active
and other useful information. Below the mode line is the mini-buffer,
where any commands you issue are indicated and prompts for user input
are shown. This is the overall view of the default GNU Emacs
interface.

In today's user interface applications, an application is treated as a
window on a desktop. But, when you open GNU Emacs, you are actually
opening a frame. A frame can be split it into many windows. Everything
is a buffer in GNU Emacs. So, you can have many frames of GNU Emacs,
and inside each you can have one or more windows containing buffers
(or files).

# Features

Although GNU Emacs was designed primarily for text editing, it can do
a lot more. 'A lot' is probably a highly simplified term. It has got
support for syntax highlighting for a large number of programming
languages. You can also generate code snippets from templates using
the *yasnippet* package. You can also enter markup or markdown
text. There is support for indentation of text and programs depending
on the programming languages and modes. Internationalization support
is available, and you can use it to even enter text in Indian
languages.

A number of configurations are available for setting it up for your
development work, including automating tasks for compiling, executing,
testing and deployment. When you become familiar with Emacs Lisp, you
can implement your own modules. Since, it is Lisp, it is also easily
extensible. You can write your own macros to perform repeated
tasks. You can also query an inbuilt help system for information,
shortcuts, tutorials and other documentation. You are not at all
dependent on the Internet for information, and thus you can work
offline too. Version control support is available for many centralized
(cvs, svn) and decentralized systems (Git, Hg).

org-mode is a very popular mode for managing your notes. You can use
it for planning your day-to-day activities. GNU Emacs can be used as
publishing software. You can create wikis, blogs and publish books
using it. This article is written using org-mode. Spell-checking
modules are also available for your documentation needs. It is also
possible to export plain text into a number of formats (PDF, HTML
etc.).

A number of Emacs lisp packages are available for networking. You can
use Gnus for checking your e-mails, and reading and writing to
newsgroups. Emacs Relay Chat (ERC) can be used for connecting to
Internet Relay Chat (IRC) channels. There are modules that support the
Jabber protocol for communicating with chat servers. There is also
support for viewing Web pages inside Emacs. There are a large number
of Emacs modules available through package repositories such as MELPA
(melpa.org), and Marmalada (marmalade-repo.org).

# History

The first version of Emacs (macros) was written by Richard Stallman
and Guy L. Steele, Jr. in 1976 for the TECO editor in MIT. It was
written in a low-level language for the PDP-10 assembler. People were
able to freely hack on the code, make improvements and share their
changes. This was the original hacker culture that existed in
MIT. Unfortunately, business entities started to make software
proprietary and this hacker culture ceased to exist, especially in the
MIT AI labs. Richard Stallman wanted to revive the hacker culture and
started the GNU project. He wrote the second implementation of the
editor entirely in C in 1984, and released it as the first program of
the GNU project. Today, it is a very popular editor that is widely
used, and has more than 570 contributors. The official web site for
GNU Emacs is at <http://www.gnu.org/software/emacs/>.
