---
author: SK
tags: 2016, emacs
timestamp: 12:45:00
title: Dired Mode
---
*[Published in Open Source For You (OSFY) magazine, August 2015 edition.]*

# Introduction

Dired is a directory editor in GNU Emacs. It opens a buffer containing
a list of directories and files to operate on. It is a 'read-only'
mode and hence you cannot input any text. This article explores some
of the basic commands that can be used in Dired mode.

Let us first create a sample directory, sub-directories and files that
we can use for our demonstration. Open a terminal and execute the
following commands:

~~~~ {.bash}
$ mkdir /tmp/test/network -p
$ mkdir /tmp/test/kernel -p

$ date > /tmp/test/date.txt
$ locale > /tmp/test/locale.txt

$ cat /etc/resolv.conf > /tmp/test/network/resolv.conf
$ ifconfig > /tmp/test/network/ifconfig.output

$ dmesg > /tmp/test/kernel/dmesg.txt
~~~~

In general, filename extensions don't have any meaning on *nix
systems. Some applications do check the filename extension before
using them. The extension is only for the benefit of the user, and
hence you can have a filename without any extension on *nix.

# Invocation

After opening GNU Emacs, you can enter Dired mode using *M-x
dired*. It will prompt you for the directory to be opened in the
buffer where you can input '/tmp/test'. You will see a buffer with the
following contents:

~~~~ {.bash}
/tmp/test:
total used in directory 24 available 146721468
drwxrwxr-x  4 shakthi shakthi 4096 Jul  3 11:36 .
drwxrwxrwt 12 root    root    4096 Jul  3 11:38 ..
-rw-rw-r--  1 shakthi shakthi   29 Jul  3 11:36 date.txt
drwxrwxr-x  2 shakthi shakthi 4096 Jul  3 11:36 kernel
-rw-rw-r--  1 shakthi shakthi  270 Jul  3 11:36 locale.txt
drwxrwxr-x  2 shakthi shakthi 4096 Jul  3 11:36 network
~~~~
    
The contents of the buffer are similar to the *ls -al* output as
observed in the terminal. The files were created by user 'shakthi',
and this is indicated in the owner and group fields. The '.' entry
represents the current '/tmp/test' directory. The '..' listing
represents the parent directory, which is '/tmp' and owned by the
'root' user.

# Exit

You can exit from Dired mode by simply pressing *q* (quit-window) in
the Dired buffer.

# Navigation

You can move to a previous line or the next line in the Dired buffer
using the *p* and *n* keys, respectively. If you wish to move the
cursor to the previous and next directories, you can use the '<' and
'>' keys. If the cursor is at 'kernel' directory as shown below ...

~~~~ {.bash}
/tmp/test:
total used in directory 24 available 146721468
drwxrwxr-x  4 shakthi shakthi 4096 Jul  3 11:36 .
drwxrwxrwt 12 root    root    4096 Jul  3 11:38 ..
-rw-rw-r--  1 shakthi shakthi   29 Jul  3 11:36 date.txt
drwxrwxr-x  2 shakthi shakthi 4096 Jul  3 11:36 kernel      <-- CURSOR
-rw-rw-r--  1 shakthi shakthi  270 Jul  3 11:36 locale.txt
drwxrwxr-x  2 shakthi shakthi 4096 Jul  3 11:36 network
~~~~
    
... then, when you press '>', the cursor will move to 'network', which
is the next directory in the buffer.

~~~~ {.bash}
/tmp/test:
total used in directory 24 available 146721468
drwxrwxr-x  4 shakthi shakthi 4096 Jul  3 11:36 .
drwxrwxrwt 12 root    root    4096 Jul  3 11:38 ..
-rw-rw-r--  1 shakthi shakthi   29 Jul  3 11:36 date.txt
drwxrwxr-x  2 shakthi shakthi 4096 Jul  3 11:36 kernel      
-rw-rw-r--  1 shakthi shakthi  270 Jul  3 11:36 locale.txt
drwxrwxr-x  2 shakthi shakthi 4096 Jul  3 11:36 network     <-- CURSOR
~~~~
    
You can move to the parent directory using the '^' key. To enter into
a directory, move to its listing in the Dired buffer, and simply hit
the *return* key.

# Viewing files

To view a file, you can place the cursor on its entry and use the *f*
or *v* key, or simply hit the *return* key. This will open the file in
a new buffer (view). To return to the Dired buffer, you can type *C-x
b* and the minibuffer will prompt you with the message "Switch to
buffer (&#x2026;)". If you then press *TAB*, it will open a new
*Completions* buffer that will list the available buffers. You can
also type the entire 'test' buffer name, or type it partially and hit
TAB for auto-completion, and hit the return key to get to the 'test'
buffer. If you wish to close a buffer, you can use *C-k* to kill
it. This will only close the buffer, but, the file will still exist!
You can use the '+' key to create a new sub-directory.

# Marking and unmarking files

Dired mode allows you to operate on multiple files and directories. In
order to run commands on them, you need to first select the files. The
*m* key can be used to mark a file or directory for subsequent
operations. For example, pressing 'm' on the date.txt entry will mark
it, and this is indicated by an asterisk in front of the line, as
shown below:

~~~~ {.bash}
  /tmp/test:
  total used in directory 24 available 146933380
  drwxrwxr-x  4 shakthi shakthi 4096 Jul  5 09:53 .
  drwxrwxrwt 10 root    root    4096 Jul  5 09:46 ..
* -rw-rw-r--  1 shakthi shakthi   29 Jul  5 09:46 date.txt
  drwxrwxr-x  2 shakthi shakthi 4096 Jul  5 09:46 kernel
  -rw-rw-r--  1 shakthi shakthi  270 Jul  5 09:46 locale.txt
  drwxrwxr-x  2 shakthi shakthi 4096 Jul  5 09:46 network
~~~~
      
You can unmark the above selection using the *u* key. This is
applicable for both files and directories. To undo a selection, you
can also use *M-del* (*M* and the *Delete* key). Suppose, you wish to
mark all the directories in the Dired buffer, you can use '* /' key
combination. The resultant Dired buffer is shown below:

~~~~ {.bash}
  /tmp/test:
  total used in directory 24 available 146933432
  drwxrwxr-x  4 shakthi shakthi 4096 Jul  5 09:53 .
  drwxrwxrwt 10 root    root    4096 Jul  5 09:46 ..
  -rw-rw-r--  1 shakthi shakthi   29 Jul  5 09:46 date.txt
* drwxrwxr-x  2 shakthi shakthi 4096 Jul  5 09:46 kernel
  -rw-rw-r--  1 shakthi shakthi  270 Jul  5 09:46 locale.txt
* drwxrwxr-x  2 shakthi shakthi 4096 Jul  5 09:46 network
~~~~
    
If you want to invert the selection, use *t*. The buffer will look
like the following:

~~~~ {.bash}
  /tmp/test:
  total used in directory 24 available 146933432
  drwxrwxr-x  4 shakthi shakthi 4096 Jul  5 09:53 .
  drwxrwxrwt 10 root    root    4096 Jul  5 09:46 ..
* -rw-rw-r--  1 shakthi shakthi   29 Jul  5 09:46 date.txt
  drwxrwxr-x  2 shakthi shakthi 4096 Jul  5 09:46 kernel
* -rw-rw-r--  1 shakthi shakthi  270 Jul  5 09:46 locale.txt
  drwxrwxr-x  2 shakthi shakthi 4096 Jul  5 09:46 network
~~~~
      
To mark all the files, you can use '* s' key combination.

# Modifying the buffer

After you have selected files, you can remove them from the listing by
pressing the *k* key. This does not delete the files! You can always
press *g* to refresh the Dired buffer contents. You can change the
'ls' command line options used in the Dired listing using the *C-u s*
key combination.

# Actions

You can perform a number of operations on the marked files and
directories. To copy a file, you can press *C* on a file, and it will
prompt you in the minibuffer regarding where you would like to create
the new copy. You can use *R* to rename a file. If you would like to
delete a file, you can press *D*. You can change the mode of a file
with the *M* command. You can compress or uncompress a file with the
*Z* command.

A regular expression search can be done on selected files with the *A*
command. You can create a symbolic link file with the *S* key. You can
run a shell command on marked files using the '!'  command. For
example, if you want to perform a word count (wc) on the date.txt
file, you can press '!' on the date.txt entry listing, and it will
prompt you in the minibuffer with "! on date.txt: " message. If you
input 'wc', the results will again be shown in the minibuffer. On my
system, it returned '1 6 29 date.txt'. You can also run asynchronous
shell commands on files using the '&' command, and a new buffer will
be opened that will contain the results.

# Deletion

You can mark files for deletion using the *d* command. After selecting
the files or directories, if you press *x*, the respective actions
will be performed on the files. To demonstrate an example, let us use
'd' on the 'kernel' directory to mark it for deletion. The letter 'D'
will be added to its entry as shown below:

~~~~ {.bash}
  /tmp/test:
  total used in directory 24 available 146933432
  drwxrwxr-x  4 shakthi shakthi 4096 Jul  5 09:53 .
  drwxrwxrwt 10 root    root    4096 Jul  5 09:46 ..
  -rw-rw-r--  1 shakthi shakthi   29 Jul  5 09:46 date.txt
D drwxrwxr-x  2 shakthi shakthi 4096 Jul  5 09:46 kernel
  -rw-rw-r--  1 shakthi shakthi  270 Jul  5 09:46 locale.txt
  drwxrwxr-x  2 shakthi shakthi 4096 Jul  5 09:46 network
~~~~
      
When you press 'x', you will be prompted to confirm deletion with the
message "Delete kernel (yes or no)". When you input "yes" and hit the
return key, the directory contents will be removed. GNU Emacs will
perform auto-save on files, and also create backup files ending with
'~'. You can mark such files using the '~' command.

# Regex

You can use '% d' to select files based on a regular expression
(regex) to mark for deletion. To simply mark files based on regex, you
can use '% m' shortcut. To rename the marked files, you can use '% R'.

# Find

You can search for files matching a pattern using the *M-x
find-name-dired* command. Suppose you wish to find all the .txt files
in our original /tmp/test directory, you can use 'M-x find-name-dired'
in the Dired buffer. It will prompt you in the minibuffer with the
following message "Find-name (directory): /tmp/test". After you press
the return key, it will ask for the regex with the message "Find-name
(filename wildcard):". If you input '*.txt', it will return the
following contents:

~~~~ {.bash}
/tmp/test/:
find . \( -name \*.txt \) -ls
6826601    4 -rw-rw-r--   1 shakthi  shakthi        29 Jul  5 09:46 date.txt
6826616   64 -rw-rw-r--   1 shakthi  shakthi     63050 Jul  5 09:46 kernel/dmesg.txt
6826602    4 -rw-rw-r--   1 shakthi  shakthi       270 Jul  5 09:46 locale.txt

find finished at Sun Jul  5 10:27:02
~~~~
    
You can also find files based on regex patterns that exist in the file
contents using the *M-x find-grep-dired*. Suppose, you wish to find
the files that have the text 'eth' in them, you can use 'M-x
find-grep-dired', which will prompt you in the minibuffer with the
message "Find-grep (directory): /tmp/test". After you press the return
key, it will prompt you for the regex with the message "Find-grep
(grep regexp):", where you can input 'eth' and this will return the
following results:

~~~~ {.bash}
/tmp/test/:
find . \( -type f -exec grep -q -e eth \{\} \; \) -ls
6826605    4 -rw-rw-r--   1 shakthi  shakthi      1620 Jul  5 09:46 network/ifconfig.output
6826616   64 -rw-rw-r--   1 shakthi  shakthi     63050 Jul  5 09:46 kernel/dmesg.txt

find finished at Sun Jul  5 10:31:27
~~~~
    
You can also execute a find operation on marked files using *M-x
find-dired*.

# Help

To open the help menu for Dired, you can use the *h* shortcut key. To
see the Dired log messages, you can press the *?* key.

You may refer to a quick reference card at
<https://www.gnu.org/software/emacs/refcards/pdf/dired-ref.pdf>, and try
out more Dired commands.
