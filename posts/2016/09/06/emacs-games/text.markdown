---
author: SK
tags: 2016, emacs
timestamp: 16:30:00
title: GNU Emacs - Games
---
*[Published in Open Source For You (OSFY) magazine, December 2015 edition.]*

In this article in the GNU Emacs series, we shall learn how to
play games and try out some software for fun.

Ten games that are available and can be played easily in GNU Emacs are
listed and discussed in this article. We start with the Tetris game.

# Tetris

Tetris is available in GNU Emacs and you can launch it using *M-x
tetris*. The 'Up' arrow key can be used to spin the tile, while the
space key can quickly drop the tile. You can pause the game by
pressing the 'p' key. At the end of the game, a score is displayed in
a buffer. For example:

    112 shakthi shakthi <author@shakthimaan.com>  Mon Nov  2 21:12:04 2015
    52 shakthi Shakthi Kannan <author@shakthimaan.com>  Mon Nov  2 15:24:43 2015
    11 shakthi shakthi <author@shakthimaan.com>  Mon Nov  2 15:29:39 2015

A screenshot of Tetris in GNU Emacs:

<img alt="Tetris" src="http://www.shakthimaan.com/images/emacs/tetris.png"></img>

# Pong

You can play Ping Pong in GNU Emacs with two players using *M-x
pong*. One player can use the 'Up' and 'Down' arrow keys, and the
second player can use the number keys - '4' and '6'. Each paddle is
represented by three square tiles. If the ball hits the edge of the
paddle, it changes its direction. You can use the 'p' key to pause the
game. A live score is displayed during the game. A screenshot of Ping Pong:

<img alt="Pong" src="http://www.shakthimaan.com/images/emacs/pong.png"></img>

# Doctor

GNU Emacs comes with a psychotherapist with whom you can chat. Simply
type *M-x doctor* and it will start a session with the
therapist. After you type in your statement, you need to hit Enter
twice, and the therapist will reply. It will usually ask you another
question, and you can continue to chat with the doctor. An example of
a session is given below:

    I am the psychotherapist.  Please, describe your problems.  Each time
    you are finished talking, type RET twice.
    
    Hi
    
    How do you do?  What brings you to see me?
    
    I am trying to talk to the doctor.
    
    Why do you say you are trying to talk to the doctor?
    
    I want to share this transcript with my readers.
    
    What makes you believe you might want to share this transcript your
    readers?
    
    I can, from GNU Emacs, copy this text and paste it in my article.
    
    Emacs?  Hah!  I would appreciate it if you would continue.
    
    I am looking forward to watching today's cricket match that India is playing.
    
    Is it because india is playing that you came to me?
    
    Yes, who do you think will win?
    
    Earlier you said you would like to share this transcript your readers?
    
    Yes, I am still going to do that.
    
    Can you elaborate on that?
    
    I am writing a GNU Emacs series for the OSFY magazine.
    
    Are you sure?
    
    Yes, I have been writing for the past few months.
    
    Maybe your life has something to do with this.
    
    I want people to learn and use GNU Emacs to improve their productivity.
    
    Are you sure that that?
    
    Absolutely!
    
    Perhaps you could continue.

# Bubbles

The objective of the Bubbles game is to remove a series of connected
bubbles in the fewest number of moves. The game can be started using
*M-x bubbles*. As you move through the grid of bubbles, the connected
bubbles with the same colour get highlighted. You can press the space
bar to remove them. A screenshot of bubbles:

<img alt="Bubbles" src="http://www.shakthimaan.com/images/emacs/bubbles.png"></img>

At the end of the game, a summary of the score is shown.

    Selected:    9
    Score:    1932
    Game Over!

GNU Emacs also keeps a record of the scores:

    01959   Mon Nov  2 21:26:41 2015        shakthi <author@shakthimaan.com>
    01932   Mon Nov  2 21:20:38 2015        shakthi <author@shakthimaan.com>

# Conway's life

The shortcut *M-x life* will start the Conway's Game of Life in a new
GNU Emacs buffer. The population increases in course of time, and the
generation count is shown in the mode line. A screenshot of the
cellular automation running inside GNU Emacs:

<img alt="Conway's Life" src="http://www.shakthimaan.com/images/emacs/life.png"></img>

# Morse and NATO conversions

You can convert text to Morse code and vice versa by selecting the
text in a buffer and using *M-x morse-region*. For example, the text
'morse code' gets converted to the following:

    --/---/.-./.../. -.-./---/-../.

You can get back the text by selecting the Morse text and applying
*M-x unmorse-region*. Similarly, if you have a word that you would
like to spell using the NATO phonetic alphabet, you can use *M-x
nato-region*. To convert it back, you need to use *M-x denato-region*.
For example, the text 'abc' gets converted to:

    Alfa-Bravo-Charlie

# Snake

The Snake game can be started using *M-x snake*. You can use the arrow
keys to move the head. As you play, red boxes appear in the window. If
you go over them, the length of the snake increases along with the
score. At the end of the game, a summary of the scores is shown. A
screenshot of the Snake game:

<img alt="Snake" src="http://www.shakthimaan.com/images/emacs/snake.png"></img>

# Dunnet

Dunnet is a text based adventure game that needs to be started in
batch mode as shown below:

    $ emacs -batch -l dunnet
    
    Dead end
    You are at a dead end of a dirt road.  The road goes to the east.
    In the distance you can see that it will eventually fork off.  The
    trees here are very tall royal palms, and they are spaced equidistant
    from each other.
    There is a shovel here.

The help command gives you the context of the game:

    >help
    Welcome to dunnet (2.01), by Ron Schnell (ronnie@driver-aces.com).
    Here is some useful information (read carefully because there are one
    or more clues in here):
    - If you have a key that can open a door, you do not need to explicitly
      open it.  You may just use 'in' or walk in the direction of the door.
    
    - If you have a lamp, it is always lit.
    
    - You will not get any points until you manage to get treasures to a certain
      place.  Simply finding the treasures is not good enough.  There is more
      than one way to get a treasure to the special place.  It is also
      important that the objects get to the special place *unharmed* and
      *untarnished*.  You can tell if you have successfully transported the
      object by looking at your score, as it changes immediately.  Note that
      an object can become harmed even after you have received points for it.
      If this happens, your score will decrease, and in many cases you can never
      get credit for it again.
    
    - You can save your game with the 'save' command, and use restore it
      with the 'restore' command.
    
    - There are no limits on lengths of object names.
    
    - Directions are: north,south,east,west,northeast,southeast,northwest,
                      southwest,up,down,in,out.
    
    - These can be abbreviated: n,s,e,w,ne,se,nw,sw,u,d,in,out.
    
    - If you go down a hole in the floor without an aid such as a ladder,
      you probably won't be able to get back up the way you came, if at all.
    
    - To run this game in batch mode (no Emacs window), use:
         emacs -batch -l dunnet
    NOTE: This game *should* be run in batch mode!
    
    If you have questions or comments, please contact ronnie@driver-aces.com
    My home page is http://www.driver-aces.com/ronnie.html

You can then give directions and proceed with the game. An example of
a session is shown below:

    >n
    You can't go that way.
    >s
    You can't go that way.
    >e
    E/W Dirt road
    You are on the continuation of a dirt road.  There are more trees on
    both sides of you.  The road continues to the east and west.
    There is a large boulder here.
    >in
    You can't go that way.
    >w
    Dead end
    There is a shovel here.
    >d
    You can't go that way.
    >e
    E/W Dirt road
    There is a large boulder here.
    >n s e
    You can't go that way.
    >in
    You can't go that way.
    >e
    Fork
    You are at a fork of two passages, one to the northeast, and one to the
    southeast.  The ground here seems very soft. You can also go back west.
    >w
    E/W Dirt road
    There is a large boulder here.

You can exit the game by typing 'quit' at the prompt (>).

# Gomoku

Gomoku is a strategy board game where you need to get five pieces in a
row (any direction) to win. You and the computer will take turns to
play the game. You need to use the *Enter* key to mark your cross in a
position, and the computer will mark its position with a circle. The
game ends when either player gets five continuous pieces. A screenshot of Gomoku
is shown below:

<img alt="Gomoku" src="http://www.shakthimaan.com/images/emacs/gomoku.png"></img>

# Le Solitaire

Le Solitaire is a strategy game that consists of stones represented by
'o' and holes represented by '.' (a dot). The objective of the game is
to remove all the stones except the last one. You can jump over
another stone to create a hole. You can use the *Shift* key with the
arrow keys to move a stone. A screenshot of the game in progress:

<img alt="Le Solitaire" src="http://www.shakthimaan.com/images/emacs/le-solitaire.png"></img>

All the games and examples were tried on GNU Emacs 24.5.1.
