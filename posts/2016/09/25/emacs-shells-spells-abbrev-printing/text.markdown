---
author: SK
tags: 2016, emacs
timestamp: 17:00:00
title: GNU Emacs - Shells, spell-checkers, abbreviations and printing
---
*[Published in Open Source For You (OSFY) magazine, January 2016 edition.]*

In this next article in the GNU Emacs series, we shall learn how to
execute shell commands, run shells, use spell-checkers and
abbreviations, and print from GNU Emacs.

# Shell mode commands

You can run a shell command from GNU Emacs using the *M-!*
shortcut. For example, typing 'M-!' will prompt you with the message
'Shell command:' in the minibuffer. If you then type 'date', it will
produce the output 'Tue Dec 8 21:19:24 IST 2015' in the
minibuffer. But, if you want the output to be inserted in the current
buffer you can use the *C-u M-!* command sequence.

Consider the following poem that I wrote:

    "Water, water everywhere
    Little power and aid to spare
    Really tough for us to bear
    We will face it with Chennaites' flair.

    Lakes and rivers overflowing everywhere
    No road, rail or plane to go anywhere
    People really are in big despair
    But we will ride it with Chennaites' flair.

    More storms are forecast to be aware
    Nothing like this in 100 years to compare
    Stay indoors and please do take care
    And we will take it with Chennaites' flair."

Suppose I want to know the number of words used, I can mark the poem
as a region in the GNU Emacs buffer, and execute a shell command for
the region using *M-|* shortcut. It then prompts with the string
'Shell command on region', and when I type in 'wc -w' it returns '90'.

# Shells

There are three shells available in GNU Emacs - shell, ansi-term and
eshell. Using *M-x shell* will invoke a shell for you. This starts a
new buffer with the name '*shell*' in it. You can use all the GNU
Emacs buffer commands in this window. For example, *C-p* will move the
cursor up, and *C-n* will move the cursor down. In order to interrupt
a current job in the shell buffer, you can use *C-c C-c*. If you want
to suspend the current job, you can use *C-c C-z*. You can also clear
the output from the previous command using *C-c C-o*. For example, the
output of 'ifconfig' command is shown below:

    /tmp $ ifconfig docker0
    docker0   Link encap:Ethernet  HWaddr 56:84:7a:fe:97:99
              inet addr:172.17.42.1  Bcast:0.0.0.0  Mask:255.255.0.0
              UP BROADCAST MULTICAST  MTU:1500  Metric:1
              RX packets:0 errors:0 dropped:0 overruns:0 frame:0
              TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
              collisions:0 txqueuelen:0 
              RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
    /tmp $

After executing 'C-c C-o', the previous command output gets cleared as
shown below:

    /tmp $ ifconfig docker0
     *** output flushed ***
    /tmp $

You can move the cursor up to the previously executed commands using
*C-c C-p* and move down using *C-c C-n*. You can also cycle backwards
and forwards through the command history using the *M-p* and *M-n*
shortcuts respectively. The *C-c C-r* shortcut moves to the first line
of the output to the top of the window. You can move the cursor to the
bottom of the window using *C-c C-e*. The *C-r* key binding allows you
to search for a previously typed command from history.

The default shell is Bash, but, you can change it to use other shells
like ksh, csh or zsh. You can open another shell buffer using *C-u M-x
shell*. The new shell buffer will be called '*shell*<2>". You can, of
course, rename this buffer. You can thus open multiple shells to work
inside GNU Emacs.

GNU Emacs also has a terminal emulator that you can invoke using *M-x
ansi-term*. You can start a Bash session with this command to get the
actual colours that you see in a terminal session. This is like a
fallback shell if you do not want to use 'M-x shell'. Eshell is the
Emacs built-in shell written completely in Emacs Lisp (Elisp). You can
start it using *M-x eshell*. The advantage of using this is that you
can extend it, write your own customized Elisp functions and use it
similar to shell scripting.

Screenshots of shell, ansi-term and eshells in GNU Emacs are shown in
Figure 1, 2 and 3 respectively:

<img alt="shell" src="http://www.shakthimaan.com/images/emacs/emacs-shell.png"></img>

<img alt="ansi-term" src="http://www.shakthimaan.com/images/emacs/emacs-ansi-term.png"></img>

<img alt="eshell" src="http://www.shakthimaan.com/images/emacs/emacs-eshell.png"></img>

# Spell check commands

You can check the spelling of a word by placing the cursor on it and
using *M-$* shortcut. For example, for the word 'hte', GNU Emacs
provides the following spelling options:

    (0) hate (1) HT (2) ht (3) GTE (4) the (5) He (6) Te (7) he (8) Hts
    (9) hie (:) hoe (;) hue (<) Rte (=) Ste (>) Ute (@) ate (B) rte

It also lists some options in the minibuffer:

    C-h or ? for more options; SPC to leave unchangedo Character to
    replace word

On pressing the number 4, the word is replaced with the correct
spelling. If the spelling is already correct, then GNU Emacs will tell
you that the word is correct. You can also spell check a region using
*M-x ispell-region* and a buffer using *M-x ispell-buffer*. If you
would like to stop the spell checker while it is running, you can use
*M-x ispell-kill-ispell*. There is a flyspell mode that can check your
spelling as you type. You can enable it using *M-x flyspell-mode*. If
you want this mode only in a buffer, you can use *M-x
flyspell-buffer*.

# Word abbreviation

GNU Emacs can complete words for you. If you type 'ba' and then hit
*M-/*, then GNU Emacs will try to complete the word for you. If you
continue to use 'M-/', it will cycle through the various options such
as 'backwards', 'ball', 'bash', 'bar', 'back', 'based' etc. To enter
into the abbreviation mode, you need to use *M-x abbrev-mode* followed
by the Enter key.

The abbreviations can either be local or global. Suppose, you want to
define a local abbreviation for 'international', you can type in
'intl' and use *C-x a i l* to define the expansion. It will prompt you
with the message 'Mode expansion for "intl":'. You can then type the
word 'international'. The next time you enter 'intl' followed by the
space key, GNU Emacs will automatically expand the same for you. In
order to define a global expansion, you need to use *C-x a i g*
command sequence. If you would like to remove all abbreviations for
the current session, you can use *M-x kill-all-abbrevs* followed by
the Enter key.

You can save the abbreviations to a file, say ~/.emacs.d/.abbrev\_defs,
using *M-x write-abbrev-file* for future use. You can also edit the
stored abbreviations using *M-x edit-abbrevs* shortcut. If you would
like to view all the defined abbreviations, you can use *M-x
list-abbrevs*. The relevant contents of ~/.emacs.d/.abbrev_defs are
shown below:

    ;;-*-coding: utf-8;-*-
    (define-abbrev-table 'Buffer-menu-mode-abbrev-table '())
    
    (define-abbrev-table 'completion-list-mode-abbrev-table '())
    
    (define-abbrev-table 'edit-abbrevs-mode-abbrev-table '())
    
    (define-abbrev-table 'emacs-lisp-mode-abbrev-table
      '(
        ("intl" "international" nil 2)
       ))
    
    (define-abbrev-table 'fundamental-mode-abbrev-table '())
    
    (define-abbrev-table 'global-abbrev-table '())
    
    (define-abbrev-table 'lisp-mode-abbrev-table '())
    
    (define-abbrev-table 'sh-mode-abbrev-table '())
    
    (define-abbrev-table 'shell-mode-abbrev-table '())

# Printing

You can print the buffer contents from GNU Emacs using *M-x
print-buffer* command. If you would like to print a selected region,
you can use *M-x print-region* command. These will be printed with
page numbers and headers. You can also print a buffer without page
numbers using *M-x lpr-buffer*. Similarly, to print a region without
page numbers, use *M-x lpr-region*. If you are already using the Dired
mode, you can select the files that you want to print using the *P*
shortcut, and then execute in Dired mode to print them.
