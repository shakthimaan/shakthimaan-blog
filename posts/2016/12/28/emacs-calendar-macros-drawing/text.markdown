---
author: SK
tags: 2016, emacs
timestamp: 16:30:00
title: GNU Emacs - Calendar, Macros and Drawing
---
*[Published in Open Source For You (OSFY) magazine, March 2016 edition.]*

In this next article in the GNU Emacs series, you will learn how to
use its calendar, center text, macros and drawing tools.

# Calendar

You can use and display a calendar inside GNU Emacs using the
following command (also see figure below):

    M-x calendar

<img alt="Calendar" src="http://www.shakthimaan.com/images/emacs/calendar.png"></img>

## Day

You can move forward by a day using the *C-f* shortcut, and move back
a day using the *C-b* keys. You can move to the current date using the
'.' key.

To start the week on a Monday, set the following in your ~/.emacs.

    (setq calendar-week-start-day 1)

## Week

If you wish to move forward by a week, you can use the *C-n* shortcut,
and to move back by a week, use the *C-p* shortcut. The *C-a* shortcut
can be used to move to the beginning of the week, while the *C-e*
shortcut can be used to move to the end of the week.

## Month

You can move to the beginning of a month using the *M-a* shortcut. To
move to the end of the month, use *M-e*.

You can move forward and backward a month using the *M-}* and *M-{*
shortcuts, respectively.

If you wish to scroll forward three months, use the *C-v* shortcut. To
scroll backward three months, use the *M-v* shortcut.

## Year

In order to move forward a year, you can use the *C-x ]* shortcut, and
to move back a year, you can use the *C-x [* shortcut.

You can go to a specified date using the *g d* key combination. It
will then prompt you with the messages "Year (>0):", "Month name:"
and "Day (1-31):", and will take you to the specified date.

You can move to the beginning of the year using the *M-<* shortcut,
and to the end of the year using the *M-v* shortcut.

You are encouraged to read the 'Calendar' section in the GNU Emacs
manual at <a
href="https://www.gnu.org/software/emacs/manual/html_node/emacs/Calendar_002fDiary.html#Calendar_002fDiary">https://www.gnu.org/software/emacs/manual/html_node/emacs/Calendar_002fDiary.html#Calendar_002fDiary</a>,
to learn more.

# Centering

Consider the following poem that I wrote in 2015:

    Poem

    Project issues in the way, 
    In the way, in the way. 
    Project issues in the way, 
    My fair user. 

    Fixing bugs right way, 
    Right away, right away. 
    Fixing bugs right way, 
    My fair user. 

    Merging pull requests as I say, 
    As I say, as I say. 
    Merging pull requests as I say, 
    My fair user. 

    All the tests are passing, hey! 
    Passing, hey! Passing, hey! 
    All the tests are passing, hey! 
    My fair user. 

    As a client, you should pay, 
    You should pay, you should pay. 
    As a client, you should pay, 
    My fair user. 

    Python really saved the day, 
    Saved the day, saved the day. 
    Python really saved the day, 
    My fair user.

You can center the title "Poem" by placing the cursor on it, and
typing "M-x set-justification-center".

Marking and highlighting the poem, and using *M-x center-region* will
center the poem. The output is shown below:

             Poem

    Project issues in the way, 
     In the way, in the way. 
    Project issues in the way, 
          My fair user. 

     Fixing bugs right way, 
    Right away, right away. 
     Fixing bugs right way, 
         My fair user. 

    Merging pull requests as I say, 
          As I say, as I say. 
    Merging pull requests as I say, 
             My fair user. 

    All the tests are passing, hey! 
      Passing, hey! Passing, hey! 
    All the tests are passing, hey! 
             My fair user. 

      As a client, you should pay, 
    You should pay, you should pay. 
      As a client, you should pay, 
             My fair user. 

     Python really saved the day, 
    Saved the day, saved the day. 
     Python really saved the day, 
            My fair user. 

# Macros

Macros are recorded key strokes that can be stored and replayed. You
can start defining a keyboard macro using *C-x (* command or the *F3*
key. You can then type a series of keys that constitute the macro. To
finish defining the macro, you can use *C-x )* or the *F4* key. In
order to execute the previous defined macro, you can use *C-x e*
shortcut or *F4*.

Consider the following text in a buffer that contains a serial number,
date and an examination subject list:

    1,2015-03-02,English
    2,2015-03-03,Physics
    3,2015-03-05,Mathematics
    4,2015-03-08,Biology
    5,2015-03-10,Chemistry

Suppose you wish to add a space after each comma, you can define the
following macro (exclude the semi-colon followed by the text) for the
first line using the following key strokes:

    F3    ; Start macro definition
    C-s   ; Search for
    ,     ;   comma
    Enter ;
    Space ;
    C-s   ; Search again for
    ,     ;   comma
    Enter ;
    Space ;
    C-n   ; Move to next line
    C-a   ; Move to beginning of line
    F4    ; End macro definition

Using *C-x e* or *F4* repeatedly will turn the above input CSV text
into the following:

    1, 2015-03-02, English
    2, 2015-03-03, Physics
    3, 2015-03-05, Mathematics
    4, 2015-03-08, Biology
    5, 2015-03-10, Chemistry

You can give a name (say, 'comma') to the previously defined macro
using *C-x C-k n*. You can then execute the macro using *M-x
comma*. You can also insert the named macro into a file using *M-x
insert-kbd-macro* command. You can bind a macro to a key using *C-x
C-k b* shortcut.

If you wish to apply the macro to each line in a region, you can use
*C-x C-k r* keys. In order to cycle between the previous and next
macros in the macro ring, you can use *C-x C-k C-p* and *C-x C-k C-n*
shortcuts respectively. You can also delete a macro using *C-x C-k
C-d* key combination.

# Picture mode

You can draw diagrams inside Emacs using Picture mode. To start
Picture mode, use *M-x picture-mode* command, and to exit use the *C-c
C-c* shortcut.

The cursor movement keys in a buffer are also applicable in picture
mode. To move the cursor right, you can use the *C-f* keys, and to
move left by one character, you can use the *C-b* shortcut. To move
the cursor up and down by one character, use the *C-p* and *C-n*
shortcuts, respectively. The *C-d* shortcut is used to delete a
character.

Before you move the cursor to draw in the buffer, you need to set the
drawing direction. The following table summarizes the shortcut keys,
and their associated drawing direction.

-------- ---------
Shortcut Direction
-------- ---------
C-c ^    Up

C-c .    Down

C-c >    Right

C-c <    Left

C-c `    Northwest

C-c '    Northeast

C-c /    Southwest

C-c \    Southeast
-------- ---------

If you want to move the cursor forward in the drawing direction, you
can use the *C-c C-f* shortcut. To move the cursor backward, use the
*C-c C-b* key combination. If you want to delete a line, use the *C-k*
command. You can insert a new line using the *C-o* shortcut. You can
also draw a rectangle around a region using the *C-c C-r* shortcut. A
drawing done using Picture mode is shown in Figure 2.

<img alt="Diagram using Picture mode" src="http://shakthimaan.com/images/emacs/picture-mode.png"></img>

# Artist mode

Artist mode can also be used to draw diagrams in GNU Emacs. You can
enter this mode using *M-x artist-mode*, and exit the same using *C-c
C-c*.

You can draw pictures using the keyboard alone or also use the mouse
in Artist mode. In order to start and stop drawing, use the *Enter*
key. This is equivalent to putting the pen down when drawing, and
lifting it up when you want to perform a different action.

The buffer navigation commands to move right and left are the same as
*C-f* and *C-b* shortcuts respectively. You can move up a column using
the *C-p* shortcut, and move down a column using the *C-n* key.

You can draw geometric shapes using Artist mode. To select a shape or
operation you can use *C-c C-a C-o* key combination. This will provide
a list of shapes and actions you can perform. This list is shown in
Figure 3:

<img alt="Artist mode operations" src="http://shakthimaan.com/images/emacs/artist-select-operation.png"></img>

The shortcuts listed in the following table are available for drawing
specific shapes:

--------- ----------
Shortcut  Shape
--------- ----------
C-c C-a e Ellipse

C-c c-a p Polylines

C-c C-a r Rectangles

C-c C-a l Lines
--------- ---------

Figure 4 depicts an ellipse drawn using Artist mode:

<img alt="Ellipse" src="http://shakthimaan.com/images/emacs/artist-ellipse.png"></img>

Figure 5 is an example of polylines:

<img alt="Polylines" src="http://shakthimaan.com/images/emacs/artist-polyline.png"></img>

You can fill a shape using *C-c C-a f* key combination. The following
Figure 6 shows a circular representation filled with dots.

<img alt="Circle fill" src="http://shakthimaan.com/images/emacs/artist-circle-fill.png"></img>

You can also spray characters in the buffer using the *C-c C-a S*
shortcut keys. An example is shown in Figure 7:

<img alt="Spray" src="http://shakthimaan.com/images/emacs/artist-spray.png"></img>

The character to be used for drawing can be changed using *C-c C-a
C-l* shortcut. The character to fill shapes can be set using *C-c C-a
C-f* key combination.

If you want to cut an area, you can draw a rectangle around it using
*C-c C-a C-k* key combination. You can also copy the image area using
the *C-c C-a M-w* keys, and paste the same using *C-c C-a C-y* or *C-x
r y* shortcuts. To set the operation to erase text, you can use *C-c
C-a C-d* key combination.

You can refer to the Emacs Wiki Artist mode for more documentation and
help - <http://www.emacswiki.org/emacs/ArtistMode>.
