---
author: SK
tags: 2016, emacs
timestamp: 11:00:00
title: GNU Emacs - Search, Frames and Windows
---
*[Published in Open Source For You (OSFY) magazine, November 2015 edition.]*

In this next article in the GNU Emacs series, we shall learn how to
perform text search in a buffer, and introduce the concept of windows
and frames.

# Search

You can copy the following poem, which I wrote in 2013, in the
*scratch* buffer or a file inside GNU Emacs to try out the search
commands:

    Emacs is, an operating system 
    Which unlike many others, is truly, a gem 
    Its goodies can be installed, using RPM 
    Or you can use ELPA, which has already packaged them 
    
    You can customize it, to your needs 
    You can also check EmacsWiki, for more leads 
    Your changes work, as long as reload succeeds 
    And helps you with, your daily deeds 
    
    People say, it lacks a decent editor 
    But after using its features, they might want to differ 
    Using Magit’s shortcuts, you might infer 
    That it is something, you definitely prefer 
    
    Plan your life, with org-mode 
    You don’t necessarily need, to write code 
    TODO lists and agenda views, can easily be showed 
    Reading the documentation, can help you come aboard 
    
    Emacs is, a double-edged sword 
    Its powerful features, can never be ignored 
    Customization is possible, because of Free Software code 
    And this is, my simple ode.

You can search for a word in a buffer using *C-s* shortcut. You will
then be prompted with *I-Search:* in the minibuffer where you can
type any text, and GNU Emacs will try to find words matching it, in
the buffer. This is an incremental forward search and is case
insensitive. Thus, if you search for the word 'todo' in the poem, it
will match the string 'TODO'. You can exit from the incremental
search using the *Enter* key, or abort the search using the *C-g* key
combination. If you want to do an incremental search in the reverse
direction - from the cursor position to the top of the buffer -- you
can use the *C-r* shortcut.

If you place the cursor on the letter 'E' in 'Emacs' in the poem's
first line, and press *C-s C-w*, Emacs will try to find all
occurrences of the word 'Emacs' in the text. Suppose, you have cut or
copied text to the kill ring, you can search for this text by using
*C-s C-y* shortcut. You can repeat the previous forward search using
*C-s C-s*, and the previous backward search using *C-r C-r* shortcuts.

The first occurrence of a text can be looked up in the forward
direction using *C-s*. This will prompt you in the minibuffer with a
*Search:* string where you can type the text that you want to search
for, and then press the *Enter* key. It will then search for the text
and move the cursor to the matching word. This is a non-incremental
forward search. Similarily, you can perform a non-incremental backward
search using *C-r*. You can then input the search string to be
searched for, followed by the *Enter* key.

Regular expression searches are very useful too. In order to search
forward for an expression, you can use *C-M-s* followed by the *Enter*
key, which will prompt you in the minibuffer with the string 'Regexp
search:'. You can then enter a regular expression. This will only
match the first occurrence of the text and the search will then
terminate. You can perform a one-time backward regular expression
search using *C-M-r* shortcut. To perform an incremental forward
search, you need to use *C-M-s* and you will be prompted with the
string 'Regexp I-search:', where you can provide the pattern to
match. For example, '[a-z]+-[a-z]+' will match both the expressions
'org-mode' and 'double-edged' words in the poem. You can use *C-M-r*
for an incremental backward regex search.

A common use case is to find and replace text in a buffer. The
sequence to be used is *M-x query-replace* followed by the *Enter*
key. You will then be prompted with the string 'Query replace:' where
you will be asked which word or phrase is to be replaced. For example,
if you mention 'ode', it will again prompt you with 'Query replace ode
with:' and then you can enter the replacement string. You can also
search and replace text by matching a regular expression with the
*C-M-%* shortcut key combination.

# Frames

The outermost user interface boundary of GNU Emacs is called a
frame. In fact, when you split the GNU Emacs user interface, you are
actually creating windows. So, in GNU Emacs, you have windows inside a
frame. This is in contrast to today's user applications, where the
entire application is contained in a 'window'. This is an important
terminology to remember when using GNU Emacs.

You can create a new frame using *C-x 5 2* key combination. You can
move the cursor to the next frame using *C-x 5 o* (letter 'o'), and
delete the current frame using *C-x 5 0* (zero) shortcut. This will
not delete the existing buffers, but, only the view. In order to open
a file in a new frame, you can use *C-x 5 f*. You can also open a file
in a new frame in read-only mode using *C-x 5 r*. To switch to the
buffer in a new frame, use *C-x 5 b* key combination.

# Windows

You can split a frame vertically to create two windows using *C-x 2*
(Figure 1). 

<img width="800" alt="Split frame vertically" src="http://www.shakthimaan.com/images/emacs/c-x-2.png"></img>

To split horizontally, you can use *C-x 3* (Figure 2). 

<img width="800" alt="Split frame horizontally" src="http://www.shakthimaan.com/images/emacs/c-x-3.png"></img>

To move the cursor to the next window, use *C-x o* (the letter
'o'). You can delete the current window using *C-x 0* (zero). Note
that this does not delete the buffer, but, just the view. If you have
multiple windows and you want to retain the current window and remove
the rest of the windows from the display, you can use *C-x 1*.

You can open a file in a new window using *C-x 4 f*. You can also
select an existing buffer in another window using *C-x 4 b*. If you
have multiple windows that you would like to be balanced equally, you
can use *C-x +*. Figure 3 shows an Emacs screenshot with three windows
that are balanced.

<img width="800" alt="Balanced windows" src="http://www.shakthimaan.com/images/emacs/c-x-plus.png"></img>

You can scroll the contents in the other window using *C-M-v*. You can
scroll backwards using *C-M-Shift-v*.

You can also use the following shortcuts in your ~/.emacs to simplify
the shortcuts used to split and remove windows.

    (global-set-key (kbd "C-1") 'delete-other-windows)
    (global-set-key (kbd "C-2") 'split-window-below)
    (global-set-key (kbd "C-3") 'split-window-right)
    (global-set-key (kbd "C-0") 'delete-window)

If you would like to make a window wider, you can use *C-x }* shortcut
and to reduce it horizontally, you will need to use *C-x {*. You can
use a prefix count to perform the operation 'n' times. For example,
*C-u 5 C-x {* to shrink a window horizontally. To make a window
taller, you can use *C-x ^* shortcut; and to make it smaller, you have
to use a negative prefix. For example, *C-u -1 C-x ^*. A screenshot
of a custom Emacs frame with three windows is shown in Figure 4.

<img width="800" alt="Custom windows in a frame" src="http://www.shakthimaan.com/images/emacs/custom-windows.png"></img>

