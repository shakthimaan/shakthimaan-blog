---
author: SK
tags: 2016, emacs
timestamp: 16:00:00
title: GNU Emacs - News Reader
---
*[Published in Open Source For You (OSFY) magazine, February 2016 edition.]*

In this next article in the GNU Emacs series, we shall learn how to
use GNU Emacs as a news reader.

# Elfeed

Elfeed is an Emacs web feed reader that is extensible and supports
both Atom and RSS feeds. It has written by Christopher Wellons.

## Installation

We shall use Milkypostman's Experimental Lisp Package Archive (MELPA)
to install Elfeed. Create an initial GNU Emacs start-up file that
contains the following:

    (require 'package) ;; You might already have this line
    (add-to-list 'package-archives
                 '("melpa" . "https://melpa.org/packages/"))
    
    (when (< emacs-major-version 24)
      ;; For important compatibility libraries like cl-lib
      (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
    (package-initialize) ;; You might already have this line

The above code snippet has been taken from the MELPA project
documentation website <http://melpa.org/#/getting-started>, and has
been tested on GNU Emacs 24.5.2.

You can now start GNU Emacs using the following command:

    $ emacs -Q -l ~/elfeed-start.el

You can obtain the list of available packages using *M-x
list-packages*, which will search the melpa.org and elpa.gnu.org
repositories. You can search for 'elfeed' in this buffer, and select
the same for installation by pressing the 'i' key. To actually install
the package, press the 'x' (execute) key, and Elfeed will be installed
in ~/.emacs.d/elpa directory.

## Configuration

You can create a shortcut to start Elfeed using the following code
snippet in your ~/elfeed-start.el file.

    (global-set-key (kbd "C-x w") 'elfeed)

The list of feeds can be defined as shown below:

    (setq elfeed-feeds
          '(("http://www.shakthimaan.com/news.xml" people)
            ("http://arduino.cc/blog/feed/" projects)
            ("http://planet-india.randomink.org/rss10.xml" people planet)
            ))

Tags can be added at the end of the feed. The above feeds include
'people', 'projects' and 'planet' tags.

## Usage

You can use the *C-x w* shortcut to start Elfeed. If you press 'G', it
will fetch the latest news feeds from the servers, starting with the
message '3 feeds pending, 0 in process ...'. A screenshot of
Elfeed in GNU Emacs is shown below:

<img alt="Elfeed" width="800" src="http://www.shakthimaan.com/images/emacs/1-elfeed.png"></img>

The RSS entries are stored in ~/.elfeed directory on your system.

You can read a blog entry by pressing the 'Enter' key. If you would
like to open an entry in a browser, you can use the 'b' key. In
order to copy the selected URL entry, you can use the 'y' key. To mark
an entry as read, you can use the 'r' key, and to unmark an entry,
press the 'u' key. You can add and remove tags for an entry using the
'+' and '-' keys, respectively.

You can also filter the feeds based on search critera. Pressing 's'
will allow you to update the filter that you want to use. There are
many filter options available. You can use '+' to indicate that a tag
must be present, and '-' to indicate that the tag must be absent. For
example, "+projects -people".

The filter text starting with '@' represents a relative time. It can
contain plain English text combined with dashes -- for example,
'@1-month-ago +unread'. The '!' notation can be used to negate a
filter. To limit the number of entries to be displayed, you can use
the '#' pattern. For example, '+unread #5' will list five unread blog
articles. A screenshot of Elfeed with a filter applied is shown in the
following figure:

<img alt="Elfeed filter" width="800" src="http://www.shakthimaan.com/images/emacs/2-elfeed-search.png"></img>

You can also use regular expressions as part of your filter text. The
default search filter can be changed by modifying the value of
*elfeed-search-filter*. For example:

    (setq-default elfeed-search-filter "@1-month-ago +unread")

The search format date can be customized as shown below:

    (defun elfeed-search-format-date (date)
      (format-time-string "%Y-%m-%d %H:%M" (seconds-to-time date)))

Elfeed also has an export option to view the feeds in a browser. If
you install the *elfeed-web* package from the packages list, you can
then start it using *M-x elfeed-web-start*. You can then start a
browser, and open <http://localhost:8080/elfeed/> to view the feeds. A
screenshot is shown below:

<img alt="Elfeed web" width="800" src="http://www.shakthimaan.com/images/emacs/3-elfeed-web.png"></img>

The entire contents of the elfeed-start.el configuration file are
shown below:

    (require 'package) ;; You might already have this line
    (add-to-list 'package-archives
                 '("melpa" . "https://melpa.org/packages/"))
    
    (when (< emacs-major-version 24)
      ;; For important compatibility libraries like cl-lib
      (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
    (package-initialize) ;; You might already have this line
    
    (global-set-key (kbd "C-x w") 'elfeed)
    
    (defun elfeed-search-format-date (date)
      (format-time-string "%Y-%m-%d %H:%M" (seconds-to-time date)))
    
    (setq elfeed-feeds
          '(("http://www.shakthimaan.com/news.xml" people)
            ("http://arduino.cc/blog/feed/" projects)
            ("http://planet-india.randomink.org/rss10.xml" people planet)
            ))

# Gnus

## Configuration

Gnus is an Emacs package for reading e-mail and Usenet news. The nnrss
backend supports reading RSS feeds. Gnus is available by default in
GNU Emacs. After launching emacs using *emacs -Q* in the terminal, you
can start Gnus using *M-x gnus*. To add a new RSS entry, you can use
'G R'. It will prompt you with the message 'URL to Search for
RSS:'. You can then provide the feed, for example,
http://www.shakthimaan.com/news.xml. It will try to connect to the
server and will provide you the message 'Contacting host:
www.shakthimaan.com:80'. After a successful connect, it will prompt
for the title, 'Title: Shakthimaan's blog.' You can simply hit
Enter. You will then be prompted for a description, 'Description: RSS
feed for Shakthimaan's blog.' You can hit Enter to proceed. Now, the
blog entry has been added to Gnus. In this fashion, you can add the
other blog entries too. A screenshot of the main Gnus group buffer is
shown below:

<img alt="Gnus" width="800" src="http://www.shakthimaan.com/images/emacs/4-gnus.png"></img>

## Usage

You can press 'g' to refresh the buffer and ask Gnus to check for
latest blog entries. Using the 'Enter' key will open the feed, and the
list of blogs for a feed. A screenshot is shown in Figure 5.

<img alt="Gnus articles" width="800" src="http://www.shakthimaan.com/images/emacs/5-gnus-articles.png"></img>

You can press 'Enter' on a blog entry, and it will open the contents
in a new buffer. It will then be marked as read, indicated by 'R'. A
screenshot of a blog entry rendering text and image is shown in the
following figure:

<img alt="Gnus blog entry" width="800" src="http://www.shakthimaan.com/images/emacs/6-gnus-blog-entry.png"></img>

You can press 'q' to quit from any level inside Gnus. You are
encouraged to read the Gnus tutorial (
http://www.emacswiki.org/emacs/GnusTutorial ) and manual (
http://www.gnus.org/manual/big-gnus.html ) to learn more, and to
customize it for your needs.
