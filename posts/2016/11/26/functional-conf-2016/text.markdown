---
author: SK
tags: 2016, travel, documentation
timestamp: 16:45:00
title: Functional Conference 2016, Bengaluru
---

I attended [Functional Conf 2016](http://functionalconf.com/2016/) at
[Hotel Chancery Pavilion,
Bengaluru](http://chanceryhotels.com/accommodation/chancerypavilion/index.htm)
between October 13-16, 2016. The conference was on October 14-15, 2016
and there were pre- and post-conference workshops.

After arriving early on the day of the workshop, I checked-in to my
hotel accommodation. A view of the Kanteerva stadium from the hotel.

<img alt="Kanteerva Stadium"
src="http://www.shakthimaan.com/images/functionalconf-2016/1-kanteerva-stadium.png"
/>

Pre-Conference Workshop

I had registered for the ["Deep Dive into Erlang Ecosystem"
workshop](https://confengine.com/functional-conf-2016/proposal/3150/deep-dive-into-erlang-ecosystem)
by [Robert Virding](https://github.com/rvirding), one of the creators
of the [Erlang programming language](https://www.erlang.org/). He
started the day's proceedings with an introduction to Erlang basics
and covered both sequential and concurrent programming. He also gave
an overview of the [Open Telecom Platform
(OTP)](https://en.wikipedia.org/wiki/Open_Telecom_Platform) and
answered a number of questions from the participants. He, along with
Joe Armstrong and Mike Williams, designed the Erlang programming
language for telecommunication, keeping the system in mind and all the
way from the ground-up.

He also mentioned how WhatsApp was able to handle two million
concurrent connections on a single box, and they would peak at three
million at times. As another Emacs and Lisp user, he wrote [Lisp
Flavoured Erlang (LFE)](http://lfe.io/). He did not have much time to
talk about it during the workshop, but, he did share differences
between Erlang, Elixir and other languages that are being built around
the Erlang ecosystem.

Day I

<img alt="Robert Virding"
src="http://www.shakthimaan.com/images/functionalconf-2016/2-robert-erlang.png"
/>

The keynote of the day was from Robert Virding on ["The Erlang
Ecosystem"](https://confengine.com/functional-conf-2016/proposal/2964/the-erlang-ecosystem). He
gave a good overview and history of the Erlang programming language,
and the rationale for designing the same. He elaborated on the
challenges they faced in the early days of computing, and the first
principles that they had to adhere to. They did not intend the
language to be functional, but, it turned out to be so, and greatly
helped their use case. One of the beautiful expressions in Erlang to
represent bit-level protocol formats in an expressive format is shown
below:

~~~~ {.erlang}
<<?IP_VERSION:4, HLen:4, SrvcType:8, TotLen:16, 
      ID:16, Flgs:3, FragOff:13,
      TTL:8, Proto:8, HdrChkSum:16,
      SrcIP:32,
      DestIP:32, RestDgram/binary>>
~~~~

Robert's keynote was followed by another keynote by [Brian
McKenna](https://brianmckenna.org/blog/) on ["No Silver Bullets in
Functional
Programming"](https://confengine.com/functional-conf-2016/proposal/3137/no-silver-bullets-in-functional-programming). He
gave the pros and cons of using Functional and other programming
paradigms, and discussed the trade-offs. A number of code examples
were shown to illustrate the concepts.

The next talk that I attended was by [Aloïs
Cochard](https://github.com/aloiscochard) on ["Welcome to the
Machines"](https://confengine.com/functional-conf-2016/proposal/2957/welcome-to-the-machines). He
gave an overview on the history of various Haskell libraries for data
stream processing (pipes, conduit) and finally provided a tutorial on
[machines](https://hackage.haskell.org/package/machines).

[Abdulsattar Mohammed](https://bimorphic.com/) introduced the need for
dependent types using [Idris](http://www.idris-lang.org/) with simple
examples in his ["Dependently Typed Programming with
Idris"](https://confengine.com/functional-conf-2016/proposal/2205/dependently-typed-programming-with-idris)
talk. The concepts were well narrated with numerous code snippets.

The next talk by [Debasish Ghosh](https://twitter.com/debasishg) on
["An algebraic approach to functional domain
modeling"](https://confengine.com/functional-conf-2016/proposal/2761/an-algebraic-approach-to-functional-domain-modeling)
was a modelling exercise on how to map business logic into functional
algebra. He demonstrated a real world step-by-step process on the
transformation from a problem domain to the solution domain consisting
of algebraic data types, functions that operate on them, and business
rules.

Ravi Mohan started his talk titled, ["Equational Reasoning - From Code
To Math and Back
Again"](https://confengine.com/functional-conf-2016/proposal/2920/equational-reasoning-from-code-to-math-and-back-again),
with his learning in the Functional Programming (FP) world, and an
overview of how to go about reasoning from code to math. His laptop
had ran out of battery power, and he did not have his laptop
charger. Before his scheduled talk, he had re-created plain text notes
of his slides and walked us through the content.

["Implementing Spark like system in
Haskell"](https://confengine.com/functional-conf-2016/proposal/2365/implementing-spark-like-system-in-haskell)
was an interesting session by [Yogesh
Sajanikar](https://github.com/yogeshsajanikar) on his attempt to
create a DSL for map-reduce jobs. He did cover much of the internals
in his implementation and the challenges faced. The hspark code is
available at
[https://github.com/yogeshsajanikar/hspark](https://github.com/yogeshsajanikar/hspark).

Day II

The second day began with the keynote by [John Hughes]()
on
["Why Functional Programming Matters"](http://www.cse.chalmers.se/~rjmh/). This
was the best keynote of the conference, where John gave a very good
historical perspective of FP and the experiences learnt in the
process. His slide deck was excellent and covered all the necessary
points that were part of
his [famous paper](http://www.cse.chalmers.se/~rjmh/Papers/whyfp.pdf)
with the same title.

This was followed by a series of demos on cool features in Functional
Programming languages - Erlang, Idris, APL, F# and Julia.

["Using F# in production: A
retrospective"](https://confengine.com/functional-conf-2016/proposal/3168/using-f-in-production-a-retrospective)
was a talk by [Ankit Solanki](https://ankitsolanki.com/) on the
lessons learned in using a functional language in implementing a tax
e-filing application. They heavily use F# Type Providers to handle the
variation in input CSV files.

["Real world functional programming in Ads serving"](https://confengine.com/functional-conf-2016/proposal/3132/real-world-functional-programming-in-ads-serving) was
a talk by [Sathish Kumar](https://github.com/sathish316) from Flipkart
on how they used functional programming in Java 8 for their
product. They initially prototyped with Haskell, and used the
constructs in Java.

I skipped the next talks, and spent time with Robert Virding in the
Erlang booth.

[Rethinking "State Management."](https://confengine.com/functional-conf-2016/proposal/2724/rethinking-state-management) was
presented by Tamizhvendan S. He narrated examples on state management
for a cafe application using F#. He also gave a demo
of [Ionide](http://ionide.io/) text editor and its features.

Post-conference workshop

I attended [John
Hughes](https://en.wikipedia.org/wiki/John_Hughes_(computer_scientist))
workshop on [Property-based
Testing](https://confengine.com/functional-conf-2016/proposal/3152/property-based-testing). Initially,
I thought he would be using [Haskell
QuickCheck](https://hackage.haskell.org/package/QuickCheck), but, in
the workshop he used the Erlang implementation. John mentioned that
the Haskell and Erlang implementations are different, and their
interests have diverged.

<img alt="John Hughes" src="http://www.shakthimaan.com/images/functionalconf-2016/3-john-hughes.png" />

He started the workshop by taking an example of writing property tests
for encoded SMS messages using Erlang. He also demonstrated on how a
minimal test example is produced when a test fails. The choice of
deciding on what properties to test is still an active research
problem. He also demonstrated how to collect statistics from the test
results to analyse and improve them.

The property-based testing has been used by his
company, [QuviQ](http://www.quviq.com/), to test C protocols for the
automobile industry. They were able to generate tests to detect bugs
in the CAN bus implementation. Here is a summary of the statistics for
a project:

~~~~ {.sh}
3,000 pages of specification
20,000 lines of QuickCheck
1,000,000 LoC, 6 suppliers
200 problems
100 problems in the standard
~~~~

He also shared his experience in generating tests for Klarna - an
invoicing service web shop that uses
[Mnesia](http://erlang.org/doc/man/mnesia.html) - the distributed
Erlang database. He concluded by saying that we should not write
tests, but, they shoud be generated.

Overall, the workshops were quite useful. It was good to have met both
Robert Virding and John Hughes.
