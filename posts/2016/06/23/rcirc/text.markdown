---
author: SK
tags: 2016, emacs
timestamp: 16:00:00
title: GNU Emacs - rcirc IRC client
---
*[Published in Open Source For You (OSFY) magazine, October 2015 edition.]*

Internet Relay Chat (IRC) provides instant communication over the
Web. It is generally used for one-to-one communication or group
discussion forums, also known as channels. In this next article in the
GNU Emacs series, we shall learn how to use GNU Emacs as an Internet
Relay Chat (IRC) client.

# What is IRC?

Internet Relay Chat (IRC) is a messaging protocol that was created by
Jarkko Oikarinen in 1988. An IRC server has a number of channels (or
rooms, as how they are called in other chat software) where both
technical and non-technical discussions take place. Every user
requires a nickname to chat. You will need to register your nickname
with a password to identify yourself every time you log in to the IRC
server. IRC can also be used for file sharing among users.

You can connect to any one of a number of IRC servers that are
available worldwide, free. The most popular is Freenode
(irc.freenode.net). The other IRC servers, to name a few, are IRCNet,
OFTC, and EFNet. The Debian and Ubuntu projects have their own chat
servers -- irc.debian.org and irc.ubuntu.com. All IRC channel names
begin with '#'. Some channels begin with '##'. Examples of channels
are ##linux-india, #dgplug, #emacs, #ruby, #guile, #lisp, and
\#scheme. You can also create your own channels, or host your own IRC
server. Some examples of free and open source IRC server software are
IRCd, UnrealIRCd, and Synchronet. Each IRC channel has a topic that
can include useful links relevant to the channel. Sometimes
announcements, or news updates are also mentioned in the topic.

# Basic commands

A number of commands can be given to the IRC server. A few of them are
discussed below:

1.  */list* is used to provide a list of all the available channels in
    a server. For example: */list* in irc.freenode.net returned the
    following:

        ...
        #linod           1       
        ##welding        3       Welcome to ##Welding, We're a little bare at the moment, but will help if we can. Tutorials: https://www.youtube.com/channel/UCJAFY2kKKb5sg79yld7T3hA
        #drupal-ph       1       "Drupalista! Welcome to Philippine Drupal Users Group. Have a good time chatting. If you have a question, please don't ask to ask but fire up your question in very specific and constructive way! Please join #drupal or #drupal-support if no one is around"
        #orx-project     4       Orx: Portable Game Engine
        #tinkerforge     5       
        #osi             10      The Open Source Initiative
        #xampp           1       
        #guitar          8       
        #bitcoin-ar      3       Comunidad Bitcoin Argentina
        #LargeHadrosaurCollider 19      Welcome to the LHC, est. 2001 | http://www.largehadrosaurcollider.net | August Birthdays: Digby 08/21, Josh 08/31 | At night it is pitch black, often for months. | http://tinyurl.com/psgdagl
        * End of /LIST

2.  */msg NickServ REGISTER password e-mail* is used to register your
    nickname to the IRC server.
    */msg NickServ IDENTIFY password* is used to identify yourself to
    the server.

3.  */me message* displays the message for a user. For example:
    
        /me says "Hello, World!"
        
         * mbuf says "Hello, World!"

4.  */whois nickname* provides useful information for a user. For example:
    
         /whois mbuf
        
        * [mbuf] (~shakthi@123.123.123.123): Shakthi Kannan
        * [mbuf] #guile #scheme ##linux-india #stumpwm #guix #dgplug #lisp #emacs 
        * [mbuf] kornbluth.freenode.net :Frankfurt, Germany
        * [mbuf] is connecting from *@123.123.123.123 123.123.123.123
        * [mbuf] idle 00:41:52, signon: Thu Sep  3 20:36:52
        * [mbuf] is logged in as mbuf
        * [mbuf] End of WHOIS list.

5.  */msg nickname* is used to send a private message to a nickname and
    to start a private conversation.

6.  */help* provides useful help on basic IRC commands. These commands
    are:
    
          ADDBUTTON ALLCHAN   ALLCHANL  ALLSERV   AWAY
          BACK      BAN       CHANOPT   CHARSET   CLEAR
          CLOSE     COUNTRY   CTCP      CYCLE     DCC
          DEBUG     DEHOP     DELBUTTON DEOP      DEVOICE
          DISCON    DNS       ECHO      EXEC      EXECCONT
          EXECKILL  EXECSTOP  EXECWRITE FLUSHQ    GATE
          GETFILE   GETINT    GETSTR    GHOST     GUI
          HELP      HOP       ID        IGNORE    INVITE
          JOIN      KICK      KICKBAN   KILLALL   LAGCHECK
          LASTLOG   LIST      LOAD      MDEHOP    MDEOP
          ME        MENU      MKICK     MODE      MOP
          MSG       NAMES     NCTCP     NEWSERVER NICK
          NOTICE    NOTIFY    OP        PART      PING
          QUERY     QUIT      QUOTE     RECONNECT RECV
          SAY       SEND      SERVCHAN  SERVER    SET
          SETCURSOR SETTAB    SETTEXT   SPLAY     TOPIC
          TRAY      UNBAN     UNIGNORE  UNLOAD    URL
          USELECT   USERLIST  VOICE     WALLCHAN  WALLCHOP
        
        User defined commands:
        
          ACTION    AME       ANICK     AMSG      BANLIST
          CHAT      DIALOG    DMSG      EXIT      GREP
          J         KILL      LEAVE     M         ONOTICE
          RAW       SERVHELP  SPING     SQUERY    SSLSERVER
          SV        UMODE     UPTIME    VER       VERSION
          WALLOPS   WII       
        
        Plugin defined commands:
        
          UNLOAD    UNLOAD    LOAD      PY        LOAD
          RELOADALL SOURCE    TCL       RELOADALL UNLOADALL
          PL_RELOAD RELOAD    UNLOAD    LOAD      TIMER
        
        
        Type /HELP <command> for more information, or /HELP -l

7.  */quit* is used to disconnect and exit from IRC.

8.  */join #channel* - is used to join a channel. For example: /join #guix.

9.  */nick newnickname* changes your nick to newnickname. Suppose, you
    wish to move away from the computer, you can change your nick to
    *nick|away*, or *nick|phone*.

10. */part* is used to leave a channel.

# Using rcirc

If you are using a recent GNU/Linux distribution, you should already
have *rcirc* as part of GNU Emacs. You can simply start it by typing
*M-x rcirc* from inside Emacs. The 'M' key represents the Meta key,
which is usually mapped to the 'Alt' key. After *rcirc* connects to
the IRC server, you can use */nick* to change your nickname, register
(only the first time) your nick, identify yourself, join channels, and
start chatting! Since everything is a buffer in GNU Emacs, each
channel is a separate buffer. For example, *#emacs@irc.freenode.net*
is the #emacs IRC channel. All your basic buffer navigation commands
will work just like they would on a file!

# Some basic rcirc commands

The rcirc commands for the above mentioned IRC commands are given in
the following table:

------------- ------------------
IRC           rcirc
------------- ------------------
/list         rcirc-cmd-list

/msg NickServ rcirc-authenticate

/me           rcirc-cmd-me

/whois        rcirc-cmd-whois

/msg nick     rcirc-cmd-msg

/help         /help

/quit         rcirc-cmd-quit

/join         rcirc-cmd-join

/nick         rcirc-cmd-nick

/part         rcirc-cmd-part
------------- ------------------

# ~/.emacs

GNU Emacs is an extensible editor. There are a number of locations
where Emacs checks for custom configurations before launching the
editor. These are: *~/.emacs*, *~/.emacs.el*, and
*~/.emacs.d/init.el*. The start-up files can be customised, and their
locations can also be changed. There are a number of ways to organize,
and manage your Emacs configuration. Until we get to learn Emacs lisp,
and customize Emacs as a project, we shall use *~/.emacs* for all our
user-specific customizations.

# rcirc customization

Create a *~/.emacs.d/etc* folder in your $HOME directory and an Elisp
file called *init-rcirc.el* in it. It should contain the following
(change nick, user-name and full-name to suit your needs):

    ;; Default user.
    (setq rcirc-default-nick "shaks")
    (setq rcirc-default-user-name "shakthimaan")
    (setq rcirc-default-full-name "Shakthi Kannan")
    
    ;; Channels to join at startup.
    (setq rcirc-server-alist
          '(("irc.freenode.net" :channels ("##linux-india" "#dgplug" "#rcirc" "#emacs"))))

The above is an example of Emacs Lisp code. Comments begin with two
semi-colons. The *setq* construct sets the second argument value to
the first argument, which is a quoted symbol. For example, the symbol
'rcirc-default-nick is set to "shaks". The *rcirc-server-alist* defines
the initial list of channels to login at startup.

You can now start GNU Emacs from the GNOME terminal using the
following command:

    $ emacs -q -l ~/.emacs.d/etc/init-rcirc.el

You will then automatically connect to the four IRC channels.

# How to use IRC

People join IRC channels to have their doubts regarding free and open
source software clarified. Sometimes, off-topic discussions also
happen. It is like live technical support, but has a social context to
it. Whenever you are connected online, you must be logged in to
IRC. You can have discussions in the channel, or in private, if the
other party agrees. It is a good place to learn a lot about free and
open source software, and you are bound to make a lot of
friends. Since people from all over the world participate, which means
they are online in different time zones, some channels log the
discussions for future reference. As always, before asking a question,
it is important for you to do your homework first. Take sufficient
time and put in an effort to debug and identify the problem to the
best of your ability.

Some users in the channel may ask you for more information before
being able to provide you with any assistance. So, be prepared to
provide all the information necessary about the bug or error when you
seek help. Sometimes, people might be logged in the channel, but, they
might be away from the computer. So, even if you don't get a response,
be patient; come back later and ask again.

You should not paste more than four continuous lines of text in the
channel, as it will 'flood' the screen for everyone else. Instead, use
an external paste service like *gist.github.com*, or *fpaste.org*. These
services will provide a shortened URL that you can pass around in the
channel. Whoever is interested in helping you will view the contents
from the link. If you enter text in the channel, it means that it is
addressed to everyone in the channel. If you wish to say something to
a specific user, mention their nickname first, and then type in the
text.

Most IRC client software provide you with panels that list the
channels that you are logged in, and show the list of users. If
someone mentions your nickname in a channel, then the corresponding
channel will change colour or representation to indicate that there is
a message for you. A few users are channel operators (or moderators) and
they have special privileges. They are similar to 'root' users in a
system, and they exist to keep the signal-to-noise ratio to a
minimum, and keep a vigil on the channel.

An IRC bot is a client software that connects to the IRC server as a
user, but can respond to commands. It can thus be programmed to
provide many services in a channel. You can customize existing bots,
or write your own. Examples of IRC bots are Cerberus, Gambot and
irccd. Cinch is an example of an IRC bot building framework written in
Ruby. Bots can be used during an IRC meeting session to keep track of
user questions. They can evaluate programming language constructs and
return meaningful errors to newbies in the channel. They can be used
to send a notification to the channel if a project test build fails,
or when a new bug has been filed. The possibilities are endless.

# IRC meeting protocol

A free and open source software project will have a dedicated IRC
channel where the project members will meet to have
discussions. Meetings can be scheduled, and can happen in different
time zones depending on where the users are located. There is a
protocol and etiquette to be followed during such meetings. The
speaker or moderator should not be interrupted during the session.

If you wish to ask a question, type '?' and wait. When the speaker has
finished and feels that you can type in your text, you will be asked
to do so. After you have finished typing your content, end with
'EOF'. Similarly, if you need to speak during the session, type '!',
and wait. You can give your consent or dissent to statements made in
the channel using +1 or -1, respectively.

You are encouraged to read the rcirc manual and customize rcirc to
your needs. If you have made it this far, do connect to
irc.freenode.net, and feel free to say 'Hi' to me. I am 'mbuf' on
irc.freenode.net. A screenshot of an rcirc session is shown below:

    19:35 <mbuf>    http://ecb.sourceforge.net/screenshots/index.html
    19:35 ***       Arpita QUIT Client Quit
    19:36 <rtnpro>  !
    19:36 <mbuf>    rtnpro, shoot!
    19:37 <rtnpro>  How do we get the emacs code browser?
    19:37 <rtnpro>  <EOF>
    19:37 <mbuf>    rtnpro, 1. Need to install "ecb" from your distro package manager
                    2. you could have searched this on the Internet :)
    19:38 <rtnpro>  It is not in my distro
    19:38 <sumitc>  !
    19:38 <mbuf>    rtnpro, and which distro are you using?
    19:38 <rtnpro>  Its Fedora 9
    19:39 <rtnpro>  I have got emacs but not emacs code browser
    19:39 <mbuf>    rtnpro, you can always install from source
    19:39 <techno_freak>    rtnpro, http://ecb.sourceforge.net/downloads.html
    19:39 ***       khushbu QUIT Ping timeout: 244 seconds
    19:39 <rtnpro>  ok
    19:39 <mbuf>    sumitc, shoot!
    19:39 <sumitc>  what is a tag-file?
    19:40 <rtnpro>  What factors should decide the choice of our editor?
    19:40 <mbuf>    rtnpro, wait!
    19:40 ***       pushkal JOIN
    19:40 <mbuf>    sumitc, the TAGS file contains the details of the reference count,
                    and locations of variables/functions et. al.
    19:41 <sumitc>  So, a tag file is always associated with a specific file?
    19:41 <mbuf>    sumitc, no, it can have information of files in a directory
    19:41 <sumitc>  ok
    19:41 <sumitc>  <eof>
    19:42 <mbuf>    sumitc, think of it as a database that answers all your queries
                   regarding code references

# References

1.  Freenode. <https://freenode.net/>

2.  rcirc manual. <https://www.gnu.org/software/emacs/manual/html_mono/rcirc.html>

3.  IRC Help. <http://www.irchelp.org/>

4.  Fedora project - How to use IRC. <https://fedoraproject.org/wiki/How_to_use_IRC>
