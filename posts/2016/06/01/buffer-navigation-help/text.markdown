---
author: SK
tags: 2016, emacs
timestamp: 17:45:00
title: GNU Emacs - Buffer navigation and Help
---
*[Published in Open Source For You (OSFY) magazine, September 2015 edition.]*

Continuing with our series on Emacs, in this article, let's explore
basic buffer navigation, formatting, and help commands.

# Buffers

You can view a list of buffers in GNU Emacs using the *C-x C-b*
command, and can switch to a buffer using the *C-x b* shortcut. The
default list of buffers as seen in GNU Emacs 24.3.1 on Ubuntu 14.10 is
shown below:

    CRM Buffer                  Size Mode            File
    .   *scratch*               191 Lisp Interaction 
    *   *Messages*              485 Fundamental
    %   *Completions*           264 Completion List
    %   *Help*                 1855 Help

In order to kill (close) a buffer, you can use *C-x k* key
combination. If you have multiple buffers that you would like to
close, you can use *M-x kill-some-buffers* command, which will prompt
you for a confirmation for every buffer. You can also rename a buffer
using the *M-x rename-buffer* command. If you would like to save the
buffer contents to a file, you can use *C-x s*, and it will prompt you
with many options before proceeding to save the file. You can also
save a modified buffer using *C-x C-s*.

# Navigation

It is important to learn how to use the Emacs navigation commands instead
of the arrow keys. When you learn to use macros, you will find it
useful to automate steps. A summary of the basic navigation shortcuts
are given in the following table:

-------------------------------------
Command   Meaning
-------   ---------------------------
C-f       Move forward one character

C-b       Move backward one character

M-f       Move forward one word

M-b       Move back one word

C-p       Move to previous line

C-n       Move to next line

C-a       Move to beginning of line

C-e       Move to end of line
-------------------------------------

You can scroll down a buffer using *C-v*, and scroll up using *M-v*
shortcut. If you would like to move to the beginning of a file, use
*M-<* command. You can move to the end of a file using *M->* shortcut.

In order to move the cursor to a specific line, use *M-x goto-line* or
*M-g g*. If you would like to position the contents where the cursor
is located to the center of the screen, you can use *C-l*. If you want
to repeat a command 'n' ('n' is an integer) number of times, you can
use *M-n* following by the command. You can also use *C-u n*, but, if
you don't specify 'n' here, the next command, by default, will be
executed four times.

# Clipboard

You can delete a character where the cursor is located using *C-d*. To
remove a word, you can use *M-d*. You can remove text from the cursor
position till the end of line using *C-k*. The cut text goes into a
kill ring buffer. You can paste whatever you have removed using
*C-y*. You can cycle through the text that has been removed by
repeatedly using *M-y* after *C-y*. Consider the text:

    When you have a question about Emacs, the Emacs manual is often the
    best starting point.

If you place the cursor before "about" in the above sentence, and
press 'M-d', it will remove " about". You can now see the value of the
kill-ring using *C-h v* (describe variable), and it will prompt you
with the message "Describe variable:", where you can enter
*kill-ring*. You will see an entry for " about". Now, place the cursor
after "question" and press 'M-d' again, and "Emacs" will be
removed. If you again check the kill-ring value, you will see an entry
for " Emacs".

You can now move the cursor to any location in the buffer, and press
'C-y', and it will paste "Emacs". If you then press *M-y*, it will
paste "about" which was the text that was cut before "Emacs". This
way, you can cycle through the kill ring buffer.

To select a region of text, you first need to mark the starting
position with the cursor using *C-space*. You can then use any of the
navigation commands to highlight the text that you want. You can
either cut the marked region using *C-w*, or copy it using *M-w*. This
will get copied to the kill-ring. You can then paste the text
to any buffer. You can also select the entire buffer using *C-x h*.

# Formatting text

It is good to have text within 80 columns and 25 rows (80x25), not
just for readability, but, to avoid making any assumptions about the
users' screen size. If you have a really long sentence or paragraph,
you can reformat it using the *M-q* shortcut from anywhere in the
sentence or paragraph.

You can transpose two characters using *C-t* shortcut. For example, if
you have misspelt "emacs" as "emcas", you can place the cursor on 'a'
and type *C-t* to swap the letters 'a' and 'c' to get "emacs". You can
transpose two words using the *M-t* command. For example, if you have
the phrase "morning good" in a buffer, and you type *M-t* anywhere on
the word "morning", it will swap the words to give you "good morning".
You can also swap lines and sentences using *C-x C-t* and *M-x
transpose-sentences* commands respectively.

If you would like to capitalize the first character of a word in upper
case, you need to place the cursor on it, and key in *M-c* to make the
first letter capital. You can turn all those letters from the cursor
location to the rest of the word into capitals using *M-u* command. For example, if
you have the word "sentence", and you place the cursor on the letter
't', and you key in *M-u* the result will be "senTENCE". To make all
the characters lower case, you can use *M-l*. You can turn the entire
previous word into capital letters using the *M - M-c*
shortcut. Similarily, to make the previous words either upper case and
lower case, you can use *M - M-u* and *M - M-l* commands, respectively.

# Info

You can read manuals written in *Info* format inside GNU Emacs. When
you type *M-x info*, it opens up a menu with the built-in
documentation available from 'Info' files. The Info mode is mostly
used to read hypertext manuals. You can use *Tab* and *Shift-tab* to
navigate through the available links. Since the contents are in a
buffer, all the basic navigation commands are applicable. To exit from
*Info*, you can use *q* (quit).

You can press the *return* key on a link to open its contents. For
example, when you click on "Emacs FAQ" it opens "The GNU Emacs FAQ"
contents. The following are the sections listed in its buffer:

    * Menu
    * FAQ notation
    * General questions
    * Getting help
    * Status of Emacs
    * Common requests
    * Bugs and problems
    * Compiling and installing Emacs
    * Finding Emacs and related packages
    * Key bindings
    * Alternate character sets
    * Mail and news
    * Concept index

Suppose, you hit the *return* key on "FAQ notation", it will open its
contents with the title "1 FAQ notation". If you want to cycle through
the headings at this level, you can press *n* (next topic) and *p*
(previous topic). If you want to move one level up, to the "The GNU
Emacs FAQ", you can use the *u* shortcut.

If you simply want to move to the topmost node ("The GNU Emacs FAQ"),
you can use the *t* shortcut. For example, if you were in the section
"3.1 I'm just starting Emacs; how do I do basic editing?" and you
pressed 't', it will take you to the topmost section - "The GNU Emacs
FAQ".

You can move to the previous node that you visited using the *l*
shortcut. You can also explicitly specify the node you want to move to
using the *g* shortcut. For example, if you are in the "1 FAQ
notation" page, and you pressed *g*, it will then prompt you with the
message "Go to node:". You can then type "Extended" and hit TAB to
auto-complete to "Extended commands", which is one of the links in the
page.

You are encouraged to try the built-in info tutorial available by
pressing *h* in the "info" buffer.

# Help

You can read manual pages inside GNU Emacs using *M-x man* command
followed by the *return* key. Suppose, you try this command from the
*scratch* buffer, it will then prompt you with the message "Manual
entry:". You can then input *ls* (list directory contents), and it
will open a new buffer and window titled "Man ls" with the
contents of the manual entry for the *ls* command.

From the scratch buffer, you can scroll forward the other window
('*Man ls*') using *C-M-v* (Ctrl-Alt-v), and scroll back using
*C-M-shift-v* (Ctrl-Alt-Shift-v).

You can list all the available, active key-bindings using the *C-h b*
command.

The *C-h f* shortcut is used to provide documentation for a function
available in your installed version of GNU Emacs. For example, if you
use *C-h f*, it will prompt you with the message "Describe
function:". If you input *scroll-other-window*, it will open a new
buffer and window titled "Help" with the description and usage of the
function. The output as seen in GNU Emacs 24.3.1 is shown below:

    scroll-other-window is an interactive built-in function in `C source
    code'.
    
    It is bound to <M-next>, C-M-v, ESC <next>.
    
    (scroll-other-window &optional ARG)
    
    Scroll next window upward ARG lines; or near full screen if no ARG.
    A near full screen is `next-screen-context-lines' less than a full screen.
    The next window is the one below the current one; or the one at the top
    if the current one is at the bottom.  Negative ARG means scroll downward.
    If ARG is the atom `-', scroll downward by nearly full screen.
    When calling from a program, supply as argument a number, nil, or `-'.
    
    If `other-window-scroll-buffer' is non-nil, scroll the window
    showing that buffer, popping the buffer up if necessary.
    If in the minibuffer, `minibuffer-scroll-window' if non-nil
    specifies the window to scroll.  This takes precedence over
    `other-window-scroll-buffer'.
    
    [back]

On the other hand, if you know the command, and would like to get help
related to it, you can use the *C-h k* command. This will prompt you
with the message "Describe key (or click or menu item):". You must key
in the actual shortcut, for example, *C-h b*, and it will update the
buffer "Help", and open it in a new window, showing the description
for the shortcut as shown below:

    C-h b runs the command describe-bindings, which is an interactive
    compiled Lisp function.
    
    It is bound to C-h b, <f1> b, <help> b, <menu-bar> <help-menu>
    <describe> <list-keybindings>.
    
    (describe-bindings &optional PREFIX BUFFER)
    
    Show a list of all defined keys, and their definitions.
    We put that list in a buffer, and display the buffer.
    
    The optional argument PREFIX, if non-nil, should be a key sequence;
    then we display only bindings that start with that prefix.
    The optional argument BUFFER specifies which buffer's bindings
    to display (default, the current buffer).  BUFFER can be a buffer
    or a buffer name.
    
    [back]

You are encouraged to try out all the commands so that you understand
what actions they perform. When you use GNU Emacs as your main text
editor, you will be able to regularly practice these commands, and
master them.
