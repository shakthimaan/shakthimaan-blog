---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: hiredis
---

<a href="https://github.com/antirez/hiredis">Hiredis</a>, a
minimalistic C client library for the <a
href="http://redis.io/">Redis</a> database is now available for
Fedora/RHEL. Install it using:

~~~~ {.shell}
  $ sudo yum install hiredis hiredis-devel
~~~~
