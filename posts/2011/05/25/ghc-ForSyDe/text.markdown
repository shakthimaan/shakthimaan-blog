---
author: SK
tags: 2011, fedora, haskell
timestamp: 00:00:00
title: ghc-ForSyDe
---

<a href="http://www.ict.kth.se/forsyde/">Formal System Design</a>
(ForSyDe) from <a href="http://www.kth.se/ict?l=en_UK">KTH Royal
Institute of Technology</a>, Sweden is now available for Fedora. It is
a methodology with the objective to move system design (System on
Chip, Hardware and Software systems) to a higher level of abstraction,
and to bridge the abstraction gap by transformational design
refinement. You can install it using:

~~~~ {.shell}
$ sudo yum install ghc-ForSyDe
~~~~

It is the 100th Haskell package in Fedora.
