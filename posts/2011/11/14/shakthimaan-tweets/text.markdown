---
author: SK
tags: 2011, documentation
timestamp: 00:00:00
title: Shakthimaan's tweets
---

<a href="http://identi.ca/shakthimaan"><img
src="http://shakthimaan.com/downloads/companylogos/identi_ca.jpg"
alt="identica logo"></img></a>

<a href="http://twitter.com/shakthimaan"><img
src="http://shakthimaan.com/downloads/companylogos/twitter.png"
alt="twitter logo"></img></a>

Shakthimaan's tweets are now available for reference at <a
href="http://www.shakthimaan.com/links/tweets.html">http://www.shakthimaan.com/links/tweets.html</a>. I
use identi.ca and twitter for documentation links, book references,
and useful tips. I am now making it available for others as well. You
can also get the sources at:

~~~~ {.shell}
$ git clone git://gitorious.org/shakthimaan-tweets/mainline.git  
~~~~