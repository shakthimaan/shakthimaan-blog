---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: FUDCon Pune
---

I had filed a ticket year-end of 2009 to <a
href="https://fedorahosted.org/fedora-india/">Fedora India</a> to
organize a <a href="http://fedoraproject.org/wiki/FUDCon">FUDCon</a>
in India. We couldn't make it in 2010, but it is happening this year!

<img alt="FUDCon Pune"
src="https://fedoraproject.org/w/uploads/4/40/Button3-going.png"></img>