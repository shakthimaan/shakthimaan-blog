---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: FUDCon Pune badges
---

<img alt="FUDCon Pune"
src="https://fedoraproject.org/w/uploads/c/cf/Button.png"></img>

As part of the <a href="http://fudcon.in/">FUDCon Pune 2011</a>
organizing team, I had volunteered to help with design work. After
attending few training sessions on <a
href="http://inkscape.org/">Inkscape</a> by <a
href="http://fedoraproject.org/wiki/User:Duffy">Máirín Duffy</a>, I
decided to give it a try. <a
href="http://fedoraproject.org/wiki/User:Ianweller">Ian Weller</a> had
written a Python script to create name badges with Inkscape. It would
read list of names from a comma separated file (csv) file, and would
generate pdfs from the design provided in the svg file. I had
customized the script with help from <a
href="http://fedoraproject.org/wiki/DaveMalcolm">Dave Malcolm</a>,
created csv files for speakers, volunteers and registered delegates,
and generated over 400 named badges for the FUDCon event.

<img alt="FUDCon coupons"
src="http://shakthimaan.com/downloads/glv/2011/fudcon-2011/registration-desk-name-badges.jpg"></img>

Thanks to <a href="http://fedoraproject.org/wiki/User:Kushal">Kushal
Das</a> for the <a
href="http://www.flickr.com/photos/kushaldas/sets/72157628097141150">photograph</a>.

Badges printed on green paper would be for volunteers and organizers,
while those printed on blue paper were for speakers. Everyone else got
their names printed on white paper. We had also printed badges with
just the FUDCon Pune logo for people who register at the venue to
write their names on it. We decided not to use QR codes. If we had
data on the delegates such as t-shirt size, food preference,
identi.ca/twitter feed, IRC nick names, we could have printed them on
the badge as well. I did leave enough white space, so people could
write whatever they want. The scripts, the Inkscape svg design, an
example csv and sample pdf generated are available at <a
href="http://shakthimaan.fedorapeople.org/docs/fudcon/">http://shakthimaan.fedorapeople.org/docs/fudcon</a>. I
had also designed the coupons for lunch, day I and II, and for the
FUDPub:

<img alt="FUDCon coupons"
src="http://shakthimaan.com/downloads/glv/2011/fudcon-2011/fudcon-coupons.png"></img>