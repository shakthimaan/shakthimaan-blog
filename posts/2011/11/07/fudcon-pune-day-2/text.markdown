---
author: SK
tags: 2011, fedora
timestamp: 00:00:10
title: FUDCon Pune Day II
---

<img alt="FUDCon Pune"
src="https://fedoraproject.org/w/uploads/c/cf/Button.png"></img>

I arrived early again on day II of <a href="http://fudcon.in/">FUDCon
Pune 2011</a>. The day's proceedings started with a keynote by <a
href="http://fedoraproject.org/wiki/User:HarishPillay">Harish
Pillay</a> on his thoughts on community work, and on his new role as
the lead of <a
href="http://fedoraproject.org/wiki/Category:Community_Architecture">Community
Architecture</a>. I then attended the <a
href="http://fudcon.in/sessions/security-open-source-world">'Security
in the Open Source world!'</a> talk by <a
href="http://fedoraproject.org/wiki/User:Eteo">Eugene Teo</a> and <a
href="http://fedoraproject.org/wiki/User:Huzaifas">Huzaifa
Sidhpurwala</a>. Their talk covered quite a bit on the various
security threats, and on how they are handled.

<img alt="Eugene Teo"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album72/11_eugene_teo.jpg"></img>

Since I attended the GlusterFS overview talk on the first day, I
wanted to follow it up with the <a
href="http://fudcon.in/sessions/glusterfs-hacking-howto">'GlusterFS:
Hacking HOWTO'</a> talk by <a href="https://github.com/amarts">Amar
Tumballi</a>. He suggested newbies to read on translators as a
starting point to work with Gluster, along with few ideas that they
could start with. Lunch was again served at 12 noon. After lunch, I
headed to Seminar Hall 2 for my talk on <a
href="http://fudcon.in/sessions/qucs-qt-love-story">'Quite Universal
Circuit Simulator - A Qt Love Story'</a> (<a
href="http://qucs.sourceforge.net/">QUCS</a>). It is an introduction
to electrical circuit theory using circuit components as "fictional"
men and women. The example circuits were created using
qucs-0.0.15. The examples are available at the <a
href="https://gitorious.org/qucs-a-qt-love-story/qucs-a-qt-love-story_prj">gitorious
repo</a>:

~~~~ {.shell}
$ git clone git://gitorious.org/qucs-a-qt-love-story/qucs-a-qt-love-story_prj.git
~~~~

After my talk, I went to the auditorium to attend the talk by <a
href="http://fedoraproject.org/wiki/User:Amitshah">Amit Shah</a> on <a
href="http://fudcon.in/sites/default/files/slides/Virtualization-with-libvirt.pdf">'Linux
Virtualization'</a> followed by <a
href="http://fedoraproject.org/wiki/User:Kashyapc">Kashyap
Chamarthy's</a> talk on <a
href="http://fudcon.in/sessions/virtualization-libvirt">'Virtualization
with Libvirt'</a>. They had given a good overview of virtualization in
the Linux kernel, and available tools that one could use. I do use <a
href="https://fedorahosted.org/publican/">Publican</a>, and thus
attended Jared Smith's talk on the same. Publican does insert blank
pages to ensure that new chapters start on the right-hand side if the
content were to be printed as a book. For the final talk of the day, I
attended <a href="http://fedoraproject.org/wiki/User:Sundaram">Rahul
Sundaram's</a> session on <a
href="http://fudcon.in/sessions/ask-fedora-community-support-and-knowledge-base">Askbot
for Fedora</a>, and the roadmap and features that he is interested
in. We then travelled to <a
href="http://www.parcestique.com/pune.htm">Hotel Parc Estique</a> for
the FUDPub!

<img alt="FUDPub"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album72/24_dance_floor.jpg"></img>