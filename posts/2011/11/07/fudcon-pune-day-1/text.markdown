---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: FUDCon Pune Day I
---

<img alt="FUDCon Pune"
src="https://fedoraproject.org/w/uploads/c/cf/Button.png"></img>

I arrived early on day I of <a href="http://fudcon.in/">FUDCon Pune
2011</a> to help with the registration desk. We had different counters
for speakers and volunteers, and for delegates. A printer was
available for us to print badges, directions, or posters as required.

<img alt="Registration desk"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album72/3_volunteer_speaker_registration_desk.jpg"></img>

I attended the keynote by <a
href="http://fedoraproject.org/wiki/User:Jsmith">Jared Smith</a>, the
Fedora Project Leader. The illustrations used in his presentation, <a
href="http://fudcon.in/sessions/fedora-state-union-address">'Fedora
"State of the Union" Address'</a> were really good. I then proceeded
to the classrooms to attend Ramakrishna Reddy's talk on <a
href="http://fudcon.in/sessions/developer-survival-manual-impatient-developer-guide-groking-source">'Developer
Survivor Manual'</a>. He addressed essential things that newbie
developers need to know, and demoed various revision control
systems. Fedora banners were placed at various seminar locations on
campus to indicate where the talks and sessions were being held.

<img alt="Registration desk"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album72/6_banner.jpg"></img>

Lunch was served at 12 noon, and then I moved on to attend the <a
href="http://fudcon.in/sessions/fedora-remix-and-community">'Fedora
Remix and the Community'</a> talk by <a
href="http://fedoraproject.org/wiki/User:Snavin">Danishka
Navin</a>. He shared his experience with the <a
href="http://www.hanthana.org/">Hanthana</a> project, which is a
Fedora remix that has support for Sinhalese, and Tamil and has been
deployed at various schools in Sri Lanka. Fedora is one of the first
and largest user of <a
href="https://github.com/sitaramc/gitolite">gitolite</a>, and I was
happy to meet its author, Sitaram Chamarthy, from TCS Innovation Labs,
Hyderabad, India. <a
href="http://fudcon.in/sites/default/files/slides/gitolite-at-fudcon-india-2011.pdf">His
talk</a> was filled with numerous examples from people using
gitolite. The other large users of gitolite are <a
href="http://kde.org/">KDE</a> and <a
href="http://www.kernel.org">kernel.org</a>. I then attended the <a
href="http://fudcon.in/sessions/glusterfs-red-hat-storage-storage-red-hat">'GlusterFS'</a>
talk by Krishna Srinivas from Red Hat, who gave an overview of the
Gluster file system, its architecture, and uses.