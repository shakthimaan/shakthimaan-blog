---
author: SK
tags: 2011, documentation, emacs
timestamp: 00:00:00
title: GNU Emacs erc notify
---

I use the following code in .emacs with Emacs <a
href="http://www.emacswiki.org/emacs/ERC">ERC</a> to get notified
using notify-send whenever anyone pings me on IRC:

~~~~ {.lisp}
(defun erc-global-notify (matched-type nick msg)
  (interactive)
  (when (eq matched-type 'current-nick)
    (shell-command
     (concat "notify-send -t 8000 -c \"im.received\" \""
             (car (split-string nick "!"))
             " mentioned your nick\" \""
             msg
             "\""))))
(add-hook 'erc-text-matched-hook 'erc-global-notify)
~~~~