---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: FUDCon Pune QUCS
---

<img alt="FUDCon Pune"
src="https://fedoraproject.org/w/uploads/c/cf/Button.png"></img>

As a follow-up to my talk on <a
href="http://qucs.sourceforge.net/">QUCS</a> on day II of FUDCon Pune
2011, I wanted to create circuit examples on the final day from a text
book that was being followed for basic electrical engineering course
work. This would be a supplement that a student can use when learning
circuit theory. Anuj More and Payas Awadhutkar joined in, and we
worked on schematics from chapter I of <a
href="http://www.amazon.ca/Fundamentals-Electrical-Engineering-Leonard-Bobrow/dp/0195105095">'Fundamentals
of Electrical Engineering'</a> by Leonard S. Boborow, a.k.a "Babu Rao"
in India. The schematics were created in qucs-0.0.16, and are
available from Payas Awadhutkar gitorious repo:

~~~~ {.shell}
$ git clone git://gitorious.org/qucs-baburao/qucs-baburao.git
~~~~

As a finale to the event, Jared Smith cut the Fedora cake!

<img alt="Jared cutting the cake"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album72/39_jared_smith_cake_cutting.jpg"></img>

I would like to thank all the volunteers from <a
href="http://www.coep.org.in/">College of Engineering, Pune</a> and <a
href="http://www.coep.org.in/index.php?profile=abhijit.comp">Prof. Abhijit
A M</a> who coordinated with us in organizing this conference. Thanks
also goes to the Fedora contributors who helped in getting things
done. Special thanks to <a href="http://www.redhat.com/">Red Hat</a>
for sponsoring the event, and for their wonderful support. The COEP
volunteers:

<img alt="COEP volunteers"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album72/32_coep_volunteers.jpg"></img>

We trained many students over the years as part of the Fedora
project. I was very happy to see them as speakers and present on the
things that they have been working on, and also help others when
required during the conference. This is the best outcome that I take
from the event.

All the photos taken at FUDCon Pune 2011 are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album72">/gallery</a>.