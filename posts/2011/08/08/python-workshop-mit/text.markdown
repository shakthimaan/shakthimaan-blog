---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: Python workshop, MIT, Pune
---

I had conducted a one-day hands-on "Introduction to Python"
programming workshop at <a href="http://mitcoe.edu.in/">Maharashtra
Institute of Technology, College of Engineering</a>, Kothrud, Pune,
India on Saturday, August 6, 2011.

The participants were engineering students who had some programming
experience but were new to Python. I wanted to do a hands-on session
so I could help them along the way. I decided to use the presentation
<a
href="http://www-uxsup.csx.cam.ac.uk/courses/PythonProgIntro/">Python:
Introduction for Programmers</a> by Bruce Beckles and Bob Dowling from
the University Computing Service, University of Cambridge. I had
requested permission to re-use the slides giving credit to the
authors, for which they agreed.

Two software labs (I and II) were made available at the venue. Remote
desktop was setup so the slides were visible on both the lab
projectors. A speaker system was arranged so people could hear me from
either lab. Gedit was used to write simple programs.

<img alt="Software Lab I"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album68/4_lab_one.jpg"></img>

Thanks to Prof. Reena D. Pagare (MIT, College of Engineering) for
working with me during the last few weeks in organizing this workshop.

More photos taken at the venue and during the workshop are available
at my <a
href="http://www.shakthimaan.com/Mambo/gallery/album68">/gallery</a>.