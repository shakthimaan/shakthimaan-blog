---
author: SK
tags: 2011, FOSS
timestamp: 00:00:00
title: PICT Ruby Rails workshop
---

I had conducted a session on test driven, behaviour driven development
on Saturday, August 20, 2011 at <a href="http://www.pict.edu/">Pune
Institute of Computer Technology</a>, Pune, Maharashtra, India using
Ruby, Cucumber, and Rails with examples from <a
href="https://joindiaspora.com/">Diaspora</a>.

Thanks to <a href="http://www.j4v4m4n.in/">Praveen Arimbrathodiyil</a>
for initiating this workshop and <a
href="http://fedoraproject.org/wiki/User:Shreyankg">Shreyank Gupta</a>
for helping the students during the hands-on session.

Participants were new to Ruby, and we started off with <a
href="http://www.ruby-lang.org/en/documentation/quickstart/">Ruby in
Twenty Minutes</a>. Using interactive ruby (irb) students were able to
understand the language syntax and its usage. We then moved on to
writing tests in Ruby, and writing them first before writing code.

<img alt="irc session"
src="http://shakthimaan.com/downloads/glv/2011/pict-aug-20-2011/pict-irb-aug-20-2011.JPG"></img>

User stories were introduced with explanation on understanding how
features, and step definitions are written. <a
href="http://cukes.info">Cucumber</a> was used to run through the
features with simple examples. We then moved on to using cucumber with
Rails illustrating an <a
href="http://asciicasts.com/episodes/155-beginning-with-cucumber">example
of a Rails blog application</a>. I had setup Diaspora on my laptop,
and had then given them a visual demo of how cucumber runs feature
tests with selenium webdriver. <a
href="http://www.flickr.com/photos/shreyankg/sets/72157627481625374/">Photos</a>
taken from Shreyank's camera are available.