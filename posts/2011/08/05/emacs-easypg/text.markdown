---
author: SK
tags: 2011, documentation, emacs
timestamp: 00:00:00
title: GNU Emacs EasyPG
---

<a title="EasyPG" href="http://epg.sourceforge.jp/">EasyPG</a>, a
GnuPG interface is available from Emacs 23 which provides automatic
encryption of .gpg files. You can list all the required encrypted .gpg
files to be used by Emacs in a secrets.el file (for example):

~~~~ {.lisp}
;; secrets.el
(load-library "~/pass.el.gpg")
~~~~

You can then load this file in your Emacs initialization file:

~~~~ {.lisp}
;; .emacs
(load "/path/to/secrets.el")
~~~~

The first time you load the above, you will be prompted to create a
password for the encryption. Remember it. Whenever you start Emacs
thereafter, or when you try to modify the .gpg files, you will be
prompted for the password.