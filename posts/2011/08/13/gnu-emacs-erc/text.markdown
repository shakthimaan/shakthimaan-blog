---
author: SK
tags: 2011, documentation, emacs
timestamp: 00:00:00
title: GNU Emacs ERC
---

<a title="ERC" href="http://www.emacswiki.org/emacs/ERC/">ERC</a> is a
modular, extensible Emacs Internet relay chat client. It is available
as part of GNU Emacs. You can use it to connect to irc.freenode.net
(for example) chat server using:

~~~~ {.lisp}
;; .emacs
(load "/path/to/secrets.el")

;; erc
(defun erc-freenode-connect ()
  (interactive)
  (erc :server "irc.freenode.net" :port 6667 :full-name "<Firstname Lastname>" 
   :nick "<your-nickname-here>")
  (require 'erc)
  (require 'erc-match)
  (setq erc-keywords '("<your-nickname-here>"))
  (setq erc-current-nick-highlight-type 'nick)
  (setq erc-track-exclude-types '("JOIN" "PART" "QUIT" "NICK" "MODE"))
  (setq erc-track-use-faces t)
  (setq erc-track-faces-priority-list
	'(erc-current-nick-face erc-keyword-face))
  (setq erc-track-priority-faces-only 'all)
  (setq erc-input-line-position -2)
  (setq erc-echo-notices-in-minibuffer-flag t)
  (setq erc-autojoin-channels-alist 
	'(("freenode.net" "#fedora-india" "#fedora-devel" "##linux-india" 
            "#edev" "#fedora-arm" "#fedora-haskell" "#fudcon-planning" "#gcc"
  	    ))))

(defun nickname-freenode-after-connect (server nick)
  (when (and (string-match "freenode\\.net" server)
	     (boundp 'irc-freenode-nick-passwd))
    (erc-message "PRIVMSG" (concat "NickServ identify " irc-freenode-nick-passwd))))
(add-hook 'erc-after-connect 'nickname-freenode-after-connect)
~~~~

The secrets.el file loads your encrypted files that contain passwords:

~~~~ {.lisp}
;; secrets.el
(load-library "~/pass.el.gpg")
~~~~

A sample pass.el.gpg file:

~~~~ {.lisp}
;; pass.el.gpg
(set 'irc-freenode-nick-passwd "your-password")
~~~~

The first time you load the above, you will be prompted to create a
password for the encryption. Remember it. Whenever you start Emacs
thereafter, or when you try to modify the .gpg files, you will be
prompted for the password. To initiate connection to irc.freenode.net
within Emacs, you can use:

~~~~ {.lisp}
M-x erc-freenode-connect
~~~~ 

It should connect to the server, join you to the channels, and
identify yourself to NickServ! Each channel is a buffer, and thus
Emacs buffer commands work. Whenever someone sends you a message using
your nick, you will get a notification in the Emacs status bar. For
more ERC commands and options, please refer the <a
href="http://mwolson.org/static/doc/erc.html">ERC user manual</a>.