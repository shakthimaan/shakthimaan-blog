---
author: SK
tags: 2011, FOSS
timestamp: 00:00:00
title: FISAT, GNU Make
---

I had presented GNU Make, <a
href="http://shakthimaan.com/downloads.html#dum-ka-biryani-make-for-each-other">Dum
Ka Biryani, Make for each other</a> at <a
href="http://icefoss.fisat.ac.in/">ICE-FOSS 2011</a>, August 26-27,
2011 at <a href="http://www.fisat.ac.in/">Federal Institute of Science
and Technology</a>, Angamaly, Kerala.

<img alt="FISAT"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album69/5_fisat.jpg"></img>

The conference had talks, hands-on workshops, and project demos. There
were interesting projects displayed at the stalls by the students
varying from Arduino-based hardware projects, to Python mobile
application development to games, and applications developed using
opencv. Pamphlets were made for each project, and given to the
visitors at the stall. I reviewed the projects that were demoed and
gave them feedback. The Institute is in the process of migrating their
servers, so we should (hopefully) see the sources made available
online.

I also had a chance to meet Anvar K Sadath, Executive Director at the
<a href="https://www.itschool.gov.in/index.php">IT@School</a>
project. They have trained nearly 2 lakh teachers on F/OSS over the
years. Their new initiative is <a
href="https://www.itschool.gov.in/animation/">animation training</a>
using free/open source software.

The Institute does have a cluster setup called "Dakshina" which is
used by students, and the faculty. They also do allow other nearby
colleges to use the facility on request.

<img alt="Dakshina cluster" width="320" height="427"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album69/17_dakshina_cluster.sized.jpg"></img>

Thanks to the Management of FISAT, for sponsoring my travel and for
the wonderful hospitality. Photos taken during the trip are available
at my <a
href="http://www.shakthimaan.com/Mambo/gallery/album69">/gallery</a>.