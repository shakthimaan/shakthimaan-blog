---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: Shaastra, IIT Madras
---

The Shaastra team has been trying to get me to speak at their event
for many years. This year I wanted to make it, and as part of the <a
href="http://www.shaastra.org/2011/main/events/Hackfest/">Hackfest at
Shaastra 2011</a>, September 28 to October 2, 2011 at <a
href="http://www.iitm.ac.in/">IIT-Madras</a>, Chennai, India I had
presented the talk on <a
href="http://shakthimaan.com/downloads.html#i-want-2-do-project-tell-me-wat-2-do">i-want-2-do-project. tell-me-wat-2-do</a>.

<img alt="Audience"
src="http://shakthimaan.com/downloads/glv/2011/shaastra-2011/1-audience.JPG"></img>

Most of the students were interested in participating in the Google
Summer of Code program. I was also able to meet and talk with past
Google Summer of Code students, who had already been introduced to
this presentation.

I visited the IITM Research Expo at the K V grounds on campus where
students had presented their research work. There were quite a few
interesting <a href="http://www.ee.iitm.ac.in/mems/">papers on
MEMS</a>. I also attended the Paper and Poster presentation by
students at the <a href="http://respark.iitm.ac.in/">IITM Research
Park</a>. A small memento that I received:

<img alt="momento"
src="http://shakthimaan.com/downloads/glv/2011/shaastra-2011/3-momento.JPG"></img>