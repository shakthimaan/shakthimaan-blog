---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: Fedora Activity Day II, COEP
---

In preparation to <a
href="http://fedoraproject.org/wiki/FUDCon:India_2011">FUDCon, Pune
2011</a>, we had organized our <a
href="http://fedoraproject.org/wiki/FAD_Pune_2011">second Fedora
Activity Day (FAD)</a> at the FOSS Lab, <a
href="http://www.coep.org.in/">College of Engineering, Pune, India</a>
on Saturday, October 8, 2011. Thanks to COEP for hosting the FAD, and
to <a
href="http://www.coep.org.in/index.php?profile=abhijit.comp">Prof. Abhijit</a>
for working with us in organizing the same. <a
href="http://fedoraproject.org/wiki/User:Tuxdna">Saleem Ansari</a> had
setup <a href="http://fudcon.in">http://fudcon.in</a> using <a
href="http://usecod.com/">Conference Organization Distribution</a>,
which we used it for registration at the venue.

<img alt="FOSS Lab audience"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album71/1_foss_lab_audience.jpg"></img>

<a href="http://fedoraproject.org/wiki/User:Pjp">Prasad Pandit</a>
started the day's proceedings with an <a
href="http://pjp.dgplug.org/tools/introduction-python.pdf">introduction
to Python</a>. Basic syntax, semantics of Python were covered, and we
helped the participants in getting started in writing simple
scripts. I then presented an overview of contributing to Fedora using
the <a
href="http://shakthimaan.com/downloads.html#i-want-2-do-project-tell-me-wat-2-do">i-want-2-do-project. tell-me-wat-2-do-fedora</a>
presentation, and the various communication channels that they need to
be familiar with to work with the larger Fedora community.

<img alt="communication channels"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album71/2a_fedora_contribution.jpg"></img>

We had lunch at the <a href="http://coepboatclub.com/">COEP Boat
Club</a> canteen. After lunch, <a
href="http://fedoraproject.org/wiki/User:Kashyapc">Kashyap
Chamarthy</a> presented <a
href="http://kashyapc.fedorapeople.org/Presentations/virtualization-in-fedora.pdf">KVM
virtualization</a> in Fedora illustrating examples using libvirt and
virt tools. <a href="http://fedoraproject.org/wiki/User:Amitshah">Amit
Shah</a> and Kashyap answered queries regarding virtualization. Saleem
Ansari then presented an <a
href="https://github.com/tuxdna/conf_jmilug">introduction to web
development and Django</a>, illustrating the use of model, view and
template design. I concluded the day's sessions with an introduction
to git using the <a
href="http://shakthimaan.com/downloads.html#di-git-ally-managing-love-letters">di-git-ally
managing love letters</a> presentation.

Few photos taken at the event are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album71">/gallery</a>.
