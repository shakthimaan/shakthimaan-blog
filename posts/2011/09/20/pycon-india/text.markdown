---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: PyCon India
---

I had presented <a
href="http://www.shakthimaan.com/downloads.html#from-python-to-silicon">From
Python to Silicon: python-myhdl</a> talk at <a
href="http://in.pycon.org/2011/">PyCon India 2011</a>, September
16-18, 2011 at Symbiosis, Pune, India.

<img alt="PyCon India sponsors"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album70/3_sponsors.jpg"></img>

The first day of the (un)conference was filled with tutorials. I
attended the <a
href="http://in.pycon.org/2011/talks/4-functional-programming-with-python">Functional
Programming with Python</a> talk by Anand Chitipothu. He had
illustrated list comprehensions, recursions, higher-order functions,
iterators and generators in Python with numerous examples.

On the second day, I attended the keynote by Raymond Hettinger on <a
href="http://urtalk.kpoint.in/kapsule/gcc-e7c717db-f77e-43dc-9dc0-255eb47e9dd3">What
Makes Python Awesome</a>. It was a very informative talk illustrating
the key characteristics of Python, and the community that surrounds
it.

I also attended the <a
href="http://in.pycon.org/2011/talks/13-python-on-android">Python on
Android</a> talk by Sajjad Anwar, who gave simple examples using
android-scripting. The <a
href="http://in.pycon.org/2011/talks/7-emacs-as-a-python-ide">Emacs as
a Python IDE</a> talk by Noufal Ibrahim had useful tips and tricks on
using Emacs for development work, and issue tracking using <a
href="http://orgmode.org/">org-mode</a>. I also attended the <a
href="http://in.pycon.org/2011/talks/43-decorators-as-composable-abstractions">Decorators
as Composable Abstractions</a> by Sidhant Godiwala which was an
introduction to using decorators in Python.

On the final day of the event, I attended <a
href="http://in.pycon.org/2011/talks/35-network-programming-with-umit-project">Network
Programming with Umit Project</a> by Narendran Thangaranjan who gave
demos on network protocol implementation, and testing in Python using
the <a href="http://www.umitproject.org/">Umit project</a>. Jivitesh
Singh Dhaliwal gave a demo of using PySerial to control robots in the
<a
href="http://in.pycon.org/2011/talks/30-python-in-the-real-world-from-everyday-applications-to-advanced-robotics">Python
in the Real World: From Blinking LEDs to Advanced Robotics</a> talk.

My presentation slides are <a
href="http://www.shakthimaan.com/downloads.html#from-python-to-silicon">available</a>. Thanks
to Christopher Felton for his valuable feedback.

Few photos taken during the event are available at my <a
href="http://www.shakthimaan.com/Mambo/gallery/album70">/gallery</a>.