---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: Fedora Activity Day I, Red Hat, Pune
---

In preparation to <a
href="http://fedoraproject.org/wiki/FUDCon:India_2011">FUDCon, Pune
2011</a>, we had organized a <a
href="https://fedoraproject.org/wiki/FAD_Pune_2011_Sep_24">Fedora
Activity Day (FAD) I</a> at Red Hat, Pune, India on Saturday,
September 24, 2011.

<img alt="Introductions"
src="http://shakthimaan.com/downloads/glv/2011/fad-1-rh-pune-2011/2-introductions.JPG"></img>

<a href="https://fedoraproject.org/wiki/User:Sundaram">Rahul
Sundaram</a> started the proceedings with a session on how to <a
href="http://sundaram.fedorapeople.org/presentations/fedora-how-to-contribute.pdf">contribute
to Fedora</a>, and how people can get involved with the community. I
showed the various communication channels that people need to use to
connect with the large Fedora community.

<img alt="Understanding Fedora"
src="http://shakthimaan.com/downloads/glv/2011/fad-1-rh-pune-2011/1-fedora-contribute.JPG"></img>

We then proceeded to do a hands-on session on RPM packaging. We used
the <a
href="http://fedoraproject.org/wiki/How_to_create_a_GNU_Hello_RPM_package">GNU
Hello RPM packaging</a> example from the fedoraproject.org wiki. Rahul
and I explained each section of the .spec file, and showed them how to
use rpmbuild. The participants learnt to write the .spec file, and
also built, installed, and tested the hello package.

We then took a break for lunch following which I presented a hands-on
session on git using the <a
href="file:///tmp/news/downloads.html#di-git-ally-managing-love-letters">di-git-ally
managing love letters</a> presentation. <a
href="http://fedoraproject.org/wiki/User:Siddhesh">Siddhesh
Poyarekar</a> then took an introductory hands-on <a
href="http://meetbot.fedoraproject.org/fedora-classroom/2010-05-03/autotools_workshop.2010-05-03-13.29.log.html">session
on autotools</a>.

<img alt="Autotools session"
src="http://shakthimaan.com/downloads/glv/2011/fad-1-rh-pune-2011/3-autotools-siddhesh.JPG"></img>

All the presentations are available in the <a
href="https://fedoraproject.org/wiki/FAD_Pune_2011_Sep_24#Agenda">FAD
wiki</a> page. Thanks to Red Hat for letting us use their facility,
and for sponsoring the pizza! They were able to arrange for ten
laptops with Fedora 15 installed for participants who didn't have
laptops.

Thanks also to <a
href="http://fedoraproject.org/wiki/User:Kashyapc">Kashyap
Chamarthy</a>, <a
href="http://fedoraproject.org/wiki/User:Kushal">Kushal Das</a>, <a
href="http://fedoraproject.org/wiki/User:Siddhesh">Siddhesh
Poyarekar</a> for their help to the participants during the workshop
sessions.