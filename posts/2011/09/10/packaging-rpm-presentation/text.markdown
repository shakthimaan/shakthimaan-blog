---
author: SK
tags: 2011, fedora, documentation
timestamp: 00:00:00
title: Packaging RPM presentation
---

Released <a
href="http://shakthimaan.com/downloads.html#packaging-red-hot-paneer-butter-masala">Packaging
RPM</a> (Packaging Red hot, Paneer (butter) Masala) presentation
1.7. The LaTeX sources are available at <a
href="https://gitorious.org/packaging-red-hot-paneer-butter-masala">gitorious.org</a>.

~~~~ {.shell}
$ git clone git://gitorious.org/packaging-red-hot-paneer-butter-masala/mainline.git
~~~~