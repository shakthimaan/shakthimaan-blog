---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: GNUnify, GNU Make, Pune
---

Presented <a
href="http://shakthimaan.com/downloads.html#dum-ka-biryani-make-for-each-other">Dum
Ka Biryani, Make for each other</a> at <a
href="http://gnunify.in">GNUnify 2011</a>, Symbiosis Institute of
Computer Studies and Research, Pune. The presentation is an
illustrative, introduction on <a
href="http://www.gnu.org/software/make/">GNU Make</a>.

<img
src="http://shakthimaan.com/downloads/glv/2011/gnunify/1-dum-biryani-presentation.jpg"
alt="make presentation picture"></img>

The <a
href="http://shakthimaan.com/downloads/glv/presentations/dum-ka-biryani-make-for-each-other.pdf">pdf</a>
and the LaTeX beamer sources are available under the GNU Free
Documentation License. You can clone the sources using:

~~~~ {.shell}
$ git clone git://gitorious.org/dum-ka-biryani-make-for-each-other/mainline.git
~~~~

I had also conducted an introductory workshop on gcc, Makefiles,
static, shared libraries for newbies at the (un)conference.

<img
src="http://shakthimaan.com/downloads/glv/2011/gnunify/2-c-workshop.jpg"
alt="C workshop picture"></img>