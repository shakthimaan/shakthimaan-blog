---
author: SK
tags: 2011, fedora, haskell
timestamp: 00:00:00
title: ghc-parameterized-data
---

<a
href="http://hackage.haskell.org/package/parameterized-data">parameterized-data</a>
is now available for Fedora. Install it using:

~~~~ {.shell}
$ sudo yum install ghc-parameterized-data
~~~~

A <a
href="http://www.ict.kth.se/forsyde/files/tutorial/tutorial.html#FSVec">tutorial</a>
illustrating vectors parameterized in size using the above is written
by Alfonso Acosta.