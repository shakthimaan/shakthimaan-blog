---
author: SK
tags: 2011, fedora, haskell
timestamp: 00:00:00
title: ghc-ansi-terminal
---

<a
href="http://hackage.haskell.org/package/ansi-terminal-0.5.5">ghc-ansi-terminal</a>,
ANSI terminal support for Haskell is now available for Fedora. It
allows cursor movement, clearing the screen, coloured output, showing
or hiding the cursor, and setting the title.

~~~~ {.shell}
$ sudo yum install ghc-ansi-terminal ghc-ansi-terminal-devel
~~~~

An example code, ansi.hs is given below:

~~~~ {.haskell}
import System.Console.ANSI

main = do
    setCursorPosition 5 0
    setTitle "ANSI Terminal Short Example"

    setSGR [ SetConsoleIntensity BoldIntensity
           , SetColor Foreground Vivid Red
           ]
    putStr "Hello"
    
    setSGR [ SetConsoleIntensity NormalIntensity
           , SetColor Foreground Vivid White
           , SetColor Background Dull Blue
           ]
    putStrLn "World!"
~~~~

A screenshot of the output when the above code is executed:

<img
src="http://shakthimaan.com/downloads/screenshots/ghc-ansi-terminal-screenshot.png"
alt="ghc-ansi-terminal output screenshot"></img>