---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: Electronics For You Expo, New Delhi
---

I had participated in the panel discussion on "Free/Open Source
Hardware - What it means to Design Engineers" at the <a
href="http://www.efyexpo.com/">Electronics For You Expo</a> 2011, <a
href="http://en.wikipedia.org/wiki/Pragati_Maidan">Pragati Maidan</a>,
New Delhi on Saturday, February 19, 2011 representing <a
href="http://fedoraproject.org">Fedora</a>, and <a
href="http://spins.fedoraproject.org/fel/">Fedora Electronic
Lab</a>. <a href="http://www.massimobanzi.com/about/">Massimo
Banzi</a> (Arduino) chaired the session.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album66/4_ajith_kumar.jpg"
alt="panel discussion"></img>

The <a
href="http://shakthimaan.com/downloads.html#free-open-source-hardware-what-it-means-to-design-engineers">presentation</a>
and <a
href="http://www.shakthimaan.com/Mambo/gallery/album66">photos</a> are
available. It was good to meet up with Massimo Banzi, <a
href="http://opencores.org/acc,view,marcus.erlandsson">Marcus
Erlandsson</a> (<a href="http://www.opencores.org">OpenCores.org</a>)
and <a
href="http://www.ifixit.com/User/Contributions/2/Kyle+Wiens">Kyle
Wiens</a> (<a href="http://www.ifixit.com">iFixit</a>). Marcus
Erlandsson gave a demo of the <a href="http://www.orsoc.se/">ORSoC</a>
development board:

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album66/2_orsoc_board.jpg"
alt="orsoc board"></img>

I was also able to catch up with few Arduino hackers from Delhi. We
had a good discussion about open hardware, licensing, community
development, hardware hacking, and of course Fedora Electronic
Lab. Special thanks to Electronics For You for sponsoring the travel.