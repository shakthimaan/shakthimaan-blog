---
author: SK
tags: 2011, photo, travel
timestamp: 00:00:00
title: Wai, Mahabaleshwar trip
---

I had been on a weekend trip to <a
href="http://en.wikipedia.org/wiki/Wai,_Maharashtra">Wai</a> and <a
href="http://en.wikipedia.org/wiki/Mahabaleshwar">Mahabaleshwar</a>,
Maharashtra, India. More photos in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album73">/gallery</a>.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album73/DSC03186_2.jpg"
alt="highway"></img>

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album73/DSC03186_40.jpg"
alt="Sahyadri hills"></img>

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album73/DSC03186_64.jpg"
alt="Dhom Dam"></img>