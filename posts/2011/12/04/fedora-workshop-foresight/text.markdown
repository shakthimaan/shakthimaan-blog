---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: Fedora workshop, Foresight Institute
---

I had conducted a one-day introductory workshop on Fedora at <a
href="http://www.foresightedu.com">Foresight Institute of Management
and Research</a>, affiliated to University of Pune, Maharashtra, India
on Saturday, December 3, 2011.

<img alt="Foresight Institute"
src="http://shakthimaan.com/downloads/glv/2011/foresight-2011/2-foresight.JPG"></img>

I started the day's proceedings with an introduction to free/open
source software. Most of the students who participated were studying
towards their bachelor's programme in computer applications (BCA), and
were familiar with C, C++ and Java development. I gave a demo of the
Fedora desktop, and also showed them the plethora of software that
they can use.

I also introduced them to revision control with examples on using
git. I also addressed the various communication tools that we use, the
basic communication guidelines, and the Fedora sub-projects that they
can participate in.

<img alt="Audience"
src="http://shakthimaan.com/downloads/glv/2011/foresight-2011/1-audience.JPG"></img>

During the post-lunch session, I explained to them about copyright,
trademarks, and licensing, and how to use them. I explained the basic
concepts in installation, and gave them a demo of installation of
Fedora. I have given them CD/DVD images of both Fedora 15 and 16.

Thanks to Antriksh Shah for working with me for a month in organizing
this workshop.