---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: Tux workshop, COEP
---

I had conducted an introductory session on Fedora as part of the <a
href="http://mind-spark.org/tux.php">Tux workshop</a>, MindSpark 2011
at <a href="http://www.coep.org.in/">College of Engineering, Pune</a>,
Maharashtra, India on Tuesday, December 27, 2011.

<img alt="Tux workshop"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album74/2_poster.jpg"></img>

There were hundred participants distributed between the morning and
repeat sessions in the afternoon. I explained about hardware
architecture, system boot sequence, and discussed basic installation
concepts to the participants. I then gave a demo of installing Fedora
using Virtual Machine Manager. I showed them the plethora of F/OSS
software that they can use, and also discussed about Fedora
sub-projects, and basic project/communication guidelines. I have given
them F16 ISO DVD images.

Thanks to Sanket Mehta for working with me for a month in organizing
this workshop. Few photos taken at the event are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album74">/gallery</a>.

