---
author: SK
tags: 2011, fedora, haskell
timestamp: 00:00:00
title: ghc-MonadCatchIO-mtl
---

<a
href="http://hackage.haskell.org/package/MonadCatchIO-mtl">MonadCatchIO-mtl</a>,
a monad-transformer version of the Control.Exception.catch function is
now available for Fedora. Install it using:

~~~~ {.shell}
$ sudo yum install ghc-MonadCatchIO-mtl ghc-MonadCatchIO-mtl-devel
~~~~

A simple example to throw or catch an exception that is an instance of
the Exception class:

~~~~ {.haskell}
{-# LANGUAGE DeriveDataTypeable #-}

import Control.Monad.CatchIO
import Data.Typeable
import Prelude hiding (catch)

data MyException = ThisException | ThatException
     deriving (Show, Typeable)

instance Exception MyException

main = do
     throw ThisException `catch` \e -> putStrLn ("Caught " ++ show (e :: MyException))
~~~~

Thanks to Daniel Gorín for the upstream changes.