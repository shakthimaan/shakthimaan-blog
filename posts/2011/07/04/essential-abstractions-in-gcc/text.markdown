---
author: SK
tags: 2011, FOSS
timestamp: 00:00:00
title: Essential Abstractions in GCC
---

I have been wanting to attend the <a
href="http://www.cse.iitb.ac.in/grc/gcc-workshop-11/index.php">'Essential
Abstractions in GCC'</a> workshop at <a
href="http://www.iitb.ac.in/">IIT Bombay</a>, Powai, Mumbai for many
years, and I was finally able to make it this year (their fourth),
June 30-July 3, 2011. Thanks to <a href="http://www.redhat.com">Red
Hat</a> for sponsoring me.

The first day started off with an introduction to the <a
href="http://www.cse.iitb.ac.in/grc/">GCC Resource Center</a> at IIT,
Bombay, and their primary research interests in program analysis and
optimization, translation validation, retargetable compilation, and
parallelization and vectorization. They proceeded to give an overview
of compiling GCC, and probing techniques used in understanding the
functionality of the <a href="http://gcc.gnu.org/">GNU C
compiler</a>. Lab sessions were held in the afternoon, and assignments
were given to illustrate the concepts discussed. In general, lectures
were scheduled in the morning, and lab sessions in the afternoon.

The second day focussed on introducing the control flow in gcc, adding
passes to gcc, and manipulating <a
href="http://gcc.gnu.org/wiki/GIMPLE">GIMPLE</a> for adding
interprocedural and intraprocedural passes. Using simple GIMPLE API in
gcc-4.6.0 were illustrated, along with adding static/dynamic plugin
passes. The lab sessions were held in the afternoon. Teaching
Assistants (students) were present to assist the participants during
the lab sessions. There were regular tea breaks provided between
breakfast, lunch, and dinner.

The third day began with an introduction to machine
descriptions. Examples of retargetability mechanisms in gcc using <a
href="http://spimsimulator.sourceforge.net">spim</a>, a MIPS processor
simulator were illustrated with examples. The instructions sets were
added incrementally at different machine description levels, beginning
from assignment operations to arithmetic to pointers and function
calls. The issues of retargetability mechanisms in gcc were also
discussed.

The final day started with an introduction on parallelization and
vectorization, theory, and concepts. Their implementation in gcc-4.6.0
was illustrated, specifically for the case of loops with data
dependency diagrams. The use of <a
href="http://gcc.gnu.org/wiki/Graphite">graphite</a>, and <a
href="http://www.cse.ohio-state.edu/~pouchet/software/pocc/">polyhedral
compilation</a> in gcc-4.6.0 were also discussed. We had lab
assignments in the afternoon, and the session concluded with a summary
of the essential concepts required in understanding the internals of
GCC.

The workshop is useful if you have worked with compiler internals. I
only wish they would release the sources of their work under a
Free/Open Source license for everyone to benefit from.

Few photos taken during the trip are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album67">/gallery</a>.