---
author: SK
tags: 2011, documentation, emacs
timestamp: 00:00:00
title: GNU Emacs reference card
---

<a
href="http://www.shakthimaan.com/downloads.html#emacs-a-day-keeps-the-vizing-away">Emacs-a-day-keeps-the-vi-zing-away</a>,
a <a
href="http://shakthimaan.com/downloads/glv/presentations/emacs-a-day-keeps-the-vi-zing-away.pdf">GNU
Emacs reference card</a> is now available under the GNU General Public
License. You can get the LaTeX sources from:

~~~~ {.shell}
$ git clone git://gitorious.org/emacs-a-day-keeps-the-vi-zing-away/mainline.git
~~~~