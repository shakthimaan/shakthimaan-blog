---
author: SK
tags: 2011, fedora
timestamp: 00:00:00
title: freeDiameter
---

<a href="http://www.freediameter.net/trac/">freeDiameter</a>, a
free/open source <a
href="http://en.wikipedia.org/wiki/Diameter_%28protocol%29">Diameter</a>
protocol implementation is now available for Fedora/RHEL. Install it
using:

~~~~ {.shell}
$ sudo yum install freeDiameter freeDiameter-devel
~~~~

It fully supports the Diameter Base Protocol as specified in <a
href="http://tools.ietf.org/html/rfc3588">RFC 3588</a>, Diameter
Extensible Authentication Protocol (EAP) application server from <a
href="http://tools.ietf.org/html/rfc4072">RFC 4072</a>, and Diameter
Session Initiation Protocol (SIP) application from <a
href="http://tools.ietf.org/html/rfc4740">RFC 4740</a>. Thanks to
Sebastien Decugis for accepting the upstream patches.