---
author: SK
tags: 2010, photo
timestamp: 00:00:00
title: Lavasa
---

Visited <a
href="http://www.lavasa.com/high/visiting_lavasa.aspx">Lavasa</a>,
Pune, Maharashtra, India for the <a
href="http://en.wikipedia.org/wiki/Diwali">Deepavali</a> weekend. More
photos are available at my <a
href="http://www.shakthimaan.com/Mambo/gallery/album65">/gallery</a>.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album65/13_lavasa_city.jpg"
alt="Lavasa view from the top"></img>

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album65/22_baji_pasalkar_reservoir.jpg"
alt="Baji Pasalkar reservoir"></img>

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album65/25_dasve_boulevard.jpg"
alt="Dasve boulevard"></img>