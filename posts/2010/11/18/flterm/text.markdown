---
author: SK
tags: 2010, fedora
timestamp: 00:00:00
title: flterm
---

flterm, a serial terminal and firmware download program, part of the
<a href="http://www.milkymist.org/">Milkymist VJ SoC</a> is now
available on Fedora.

~~~~ {.shell}
  $ sudo yum install flterm
~~~~

You can run it using:

~~~~ {.shell}
  $ flterm
  Serial boot program for the Milkymist VJ SoC - v. 1.1
  Copyright (C) 2007, 2008, 2009 Sebastien Bourdeauducq

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, version 3 of the License.

  Usage: flterm --port <port> [--double-rate]
                --kernel <kernel_image> [--kernel-adr <address>]
                [--cmdline <cmdline> [--cmdline-adr <address>]]
                [--initrd <initrd_image> [--initrd-adr <address>]]

  Default load addresses:
    kernel:  0x40000000
    cmdline: 0x41000000
    initrd:  0x41002000
~~~~