---
author: SK
tags: 2010, FOSS
timestamp: 00:00:00
title: Blender workshop
---

Attended May months' <a href="http://ilughyd.org.in/">Hyderabad user
group</a> meeting on Sunday, May 16, 2010 at:

~~~~ {.shell}
Rhythm & Hues Studios India Pvt. Ltd.
The V, Vega Block, 11th Floor,
Left Wing, Plot No - 17, Software Units layout
HITEC City, Madhapur, 
Hyderabad 500 081
India
~~~~

Satish "iloveblender" Goda gave a very informative talk about the
history of the <a href="http://www.blender.org/">Blender</a> project,
<a
href="http://www.blender.org/features-gallery/blender-open-projects/">animation
movies created</a> using the Blender software, pipelines involved in
animation movie production, and on how the blender community functions
in producing open animation movies with the guidance of <a
href="http://www.blender.org/blenderorg/blender-foundation/history/">Ton
Roosendaal</a>.

Their open animation movie projects are code named after names of
fruits. One interesting aspect is that before starting on a new movie
project, one can pay and pre-order the DVD, and your name is added to
the credits when the movie is released. This funding also helps the
team in producing the movie!

Thanks to the Management of Rhythm &amp; Hues Studios India
Pvt. Ltd. for hosting our user group meet at their auditorium. Special
thanks to "cheedhu" and "iloveblender" for initiating the effort. We
hope to have more sessions of blender and other F/OSS sessions at this
venue.

A photo taken at the end of the session:

<img
src="http://shakthimaan.com/downloads/glv/hyd/blender-hyderabad-meet-may-16-2010.png"
alt=""></img>