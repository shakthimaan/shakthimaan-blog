---
author: SK
tags: 2010, fedora
timestamp: 00:00:00
title: perl-Sys-CPU
---

Pushed <a
href="http://search.cpan.org/~mkoderer/Sys-CPU/CPU.pm/">perl-Sys-CPU</a>
to Fedora repository. You can use it like:

~~~~ {.perl}
use Sys::CPU;
  
$number_of_cpus = Sys::CPU::cpu_count();
printf("I have %d CPU's\n", $number_of_cpus);

print "  Speed : ", Sys::CPU::cpu_clock(), "\n";
print "  Type  : ", Sys::CPU::cpu_type(), "\n";
~~~~