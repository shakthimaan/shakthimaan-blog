---
author: SK
tags: 2010, FOSS
timestamp: 00:00:00
title: Sreenidhi-KMRF OLPC visit
---

Visited <a href="http://www.sreenidhiinternational.com/">Sreenidhi
International School</a> and members of the <a
href="http://www.kmrfoundation.org/">KMR Foundation</a> today,
Saturday, May 8, 2010 in Hyderabad. They have <a
href="http://laptop.org/en/">OLPC</a> XOs that they are using for
class(grade) IV students in a school near their premises at Aziznagar,
Ranga Reddy district, Andhra Pradesh, India. They require support for
Telugu language and use of XO software for the school students. Here
is a screenshot of one of the XOs:

<img alt="olpc xo"
src="http://shakthimaan.com/downloads/glv/hyd/olpc/olpc-xo.png"></img>