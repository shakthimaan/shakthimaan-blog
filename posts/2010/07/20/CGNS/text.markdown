---
author: SK
tags: 2010, fedora
timestamp: 00:00:00
title: Computational Fluid Dynamics General Notation System
---

The <a href="http://cgns.sourceforge.net/">Computational Fluid
Dynamics General Notation System</a> (CGNS) library is now available
in Fedora under the zlib license. Install it using:

~~~~ {.shell}
$ yum install cgnslib
~~~~

It provides a general, portable, and extensible standard for the
storage and retrieval of computational fluid dynamics (CFD) analysis
data. It consists of a collection of conventions, and free and open
software implementing those conventions. It is self-descriptive,
machine-independent, <a
href="http://www.grc.nasa.gov/WWW/cgns/user/index.html">well-documented</a>,
and administered by an international <a
href="http://cgns.sourceforge.net/steering.html">Steering
Committee</a>.