---
author: SK
tags: 2010, travel
timestamp: 10:00:00
title: Jyväskylä, Finland
---

<img alt="Office building"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album61/4_jyvaskyla_office_building.jpg"></img><br
/>

<img alt="Dinner with colleagues"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album61/34_work_colleagues_at_restaurant.jpg"></img><br
/>

<img alt="At ski resort"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album61/44_high_speed_blind_ski_turn.jpg"></img><br
/>

More photos available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album61">/gallery</a>.
