---
author: SK
tags: 2010, fedora, haskell
timestamp: 00:00:00
title: ghc-type-level
---

<a href="http://hackage.haskell.org/package/type-level">Type-level</a>
programming library is now available on Fedora testing
repository. Type-level functions are implemented using functional
dependencies of multi parameter type classes. A tutorial on type-level
numerals and their usage to implement numerically-parameterized
vectors is available at <a
href="http://www.ict.kth.se/forsyde/files/tutorial/tutorial.html#FSVec">http://www.ict.kth.se/forsyde/files/tutorial/tutorial.html#FSVec</a>. You
can install it on Fedora using:

~~~~ {.shell}
$ sudo yum --enablerepo=updates-testing update ghc-type-level 
~~~~