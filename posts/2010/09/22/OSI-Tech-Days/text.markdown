---
author: SK
tags: 2010, FOSS
timestamp: 00:00:05
title: OSI Tech Days 2010
---

Attended <a href="http://www.osidays.com">OSI Tech Days</a>, September 19-21, 2010, Chennai to listen to <a href="http://emoglen.law.columbia.edu/">Prof. Eben Moglens'</a> talk on <a href="http://www.softwarefreedom.org/events/2010/freedom-and-web-equality-21st-century/">Freedom and the Web: Equality in the 21st Century</a>. I also manned the <a href="http://www.ilugc.in">ILUGC</a> stall for some time. It was good to catch up with lot of old-timers. 

Later in the evening, I was asked to join the panel discussion on "Building a FOSS Ecosystem in India; the role of different players", and had a chance to meet <a href="http://thelittlesasi.wikidot.com/">Dr. M. Sasikumar</a> (Director, CDAC, Mumbai), <a href="http://www.cse.iitb.ac.in/~siva/">Prof. G. Sivakumar</a> (IIT, Mumbai), V S Raghunathan (Technical Director, <a href="http://www.tn.nic.in/">National Informatics Centre, Tamil Nadu State Centre, Chennai</a>),  <a href="http://www.au-kbc.org/cnkpage.htm">Prof. C N Krishnan</a> et. al. A picture of the panel discussion:

<img src="http://shakthimaan.com/downloads/glv/2010/osi-days-2010/osi-days-2010.jpg" alt="FOSS ecosystem panel discussion"></img>