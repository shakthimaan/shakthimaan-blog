---
author: SK
tags: 2010, FOSS
timestamp: 00:00:00
title: Software Freedom Day, Chennai
---

Attended Software Freedom Day, 2010, at Chennai, on Saturday,
September 18, 2010 at Birla Planetarium, organized by <a
href="http://www.ilugc.in">ILUGC</a>. <a
href="http://www.facebook.com/album.php?aid=38346&amp;id=133359730009404">Photos</a>,
courtesy of <a
href="http://goinggnu.wordpress.com/">T. Shrinivasan</a>. Two
important observations:

* The number of colleges/universities that participated in the Free/Open Source Software (F/OSS) stalls has increased.
* F/OSS awareness programmes are essential, and have their own signficance.