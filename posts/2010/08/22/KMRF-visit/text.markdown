---
author: SK
tags: 2010, FOSS
timestamp: 00:00:00
title: KMRF OLPC visit
---

Visited <a href="http://www.kmrfoundation.org/">KMRF</a> who needed
assistance in installation, troubleshooting, and use of the <a
href="http://wiki.laptop.org/go/Home">XO</a> laptops. I had a chance
to visit the Government school in Aziznagar, Ranga Reddy district,
Andhra Pradesh, India, where they have their XO laptop
deployment. Most of the students can speak only in Telugu. They have a
chart in the classroom for English-Telugu reference.

<img
src="http://shakthimaan.com/downloads/glv/hyd/olpc/2-text-reference.jpg"
alt="olpc text chart"></img>.

<a href="http://www.geekcomix.com/dm/tuxmath/gallery/">Tuxmath</a> was
installed, and they enjoy using it a lot:

<img src="http://shakthimaan.com/downloads/glv/hyd/olpc/1-tuxmath.jpg"
alt="olpc text chart"></img>