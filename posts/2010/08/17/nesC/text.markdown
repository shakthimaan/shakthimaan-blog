---
author: SK
tags: 2010, fedora, emacs
timestamp: 00:00:00
title: nesC
---

<a href="http://nescc.sourceforge.net/">nesC</a> is now available for
Fedora. It is an extension to the C programming language designed for
use with <a href="http://www.tinyos.net/">TinyOS</a>, which is used in
wireless sensor networks. Install it using:

~~~~ {.shell}
$ sudo yum --enablerepo=updates-testing update nesc
~~~~

It could not be shipped with Fedora earlier because it used the now
deprecated <a
href="http://www.opensource.org/licenses/intel-open-source-license.php">Intel
Open Source license</a>. Thanks to <a
href="http://www.barnowl.org/">David Gay</a> and <a
href="http://csl.stanford.edu/~pal/">Philip Levis</a>, it has now been
updated with dual BSD/GPL license. There is also support for GNU Emacs
with the emacs-nesc package.

~~~~ {.shell}
$ sudo yum --enablerepo=updates-testing update emacs-nesc
~~~~

Here is a screenshot of GNU Emacs with syntax highlighting in
nesc-mode: <img
src="http://www.shakthimaan.com/downloads/screenshots/emacs-nesc-screenshot.png"
alt="Emacs nesc screenshot"></img>