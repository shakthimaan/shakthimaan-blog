---
author: SK
tags: 2010, fedora
timestamp: 00:00:00
title: Clojure workshop
---

A hands-on workshop on <a href="http://clojure.org/">Clojure</a> was
organized in <a href="www.ilughyd.org.in">TwinCLinGs'</a> August
months' meet at <a
href="http://www.rhythm.com/india/overview.html">Rhythm &amp; Hues
Studios Pvt. Ltd</a>, Hitech City, Hyderabad, India by <a
href="http://freegeek.in/blog/">Bhaishampayan Ghose</a>, co-founder
and chief geek at <a href="http://infinitelybeta.com/">Infinitely
Beta</a>. A picture of the attendees:

<img
src="http://shakthimaan.com/downloads/glv/hyd/images/clojure-hyderabad-workshop-aug-29-2010.jpg"
alt="clojure meet attendees"></img>

The <a
href="http://www.slideshare.net/zaph0d/pune-clojure-course-outline">slides</a>
are available. You can install clojure on Fedora using:

~~~~ {.shell}
$ sudo yum install clojure
~~~~

A clojure programming book is also available at <a
href="http://en.wikibooks.org/wiki/Clojure">http://en.wikibooks.org/wiki/Clojure</a>
to get started! Thanks to volunteers who sponsored for Bhaishampayan
Ghoses' travel, to and from Pune, India.