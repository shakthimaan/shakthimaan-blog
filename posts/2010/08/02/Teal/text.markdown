---
author: SK
tags: 2010, fedora
timestamp: 00:00:00
title: Teal
---

<a href="http://www.trusster.com/products/teal/">Teal</a>, the popular
verification utility and connection library is now available for
Fedora:

~~~~ {.shell}
$ sudo yum install teal
~~~~

It is a C++ multithreaded library to verify verilog designs. It deals
with simulation logging, error reporting, threading, memory model
management. It basically provides all the low level building blocks
needed to start a verification environment. A simple example:

~~~~ {.cpp}
#include <teal.h>
using namespace teal;
int verification_top ()
{
  vreg clock (“testbench.clk”);
  vout log (“Chapter 4- Example 1”);
  dictionary::start (“simple_clock_test.txt”);
  uint number_of_periods (dictionary::find (“number_of_clocks”,20));
  for (int i(0); i < number_of_periods; ++i)  {
    log << note << “i is “ << i << clock is << clock << endm;
  }
  dictionary::stop ();
  vlog::get (expected) << “test completed” << endl;
}
~~~~

It is released under the Trusster open source license, but, that is
incompatible with the GPL. But, thanks to the founders of
Trusster.com, <a href="http://www.trusster.com/welcome/about/">Mike
Mintz and Robert Ekendahl</a>, who agreed to release the same under
the LGPLv2+ and GPLv2+ license for Fedora!