---
author: SK
tags: 2010, fedora
timestamp: 00:00:00
title: Digital Gate Compiler
---

<a href="http://sourceforge.net/projects/dgc/">Digital Gate Compiler
(DGC)</a> was written by Oliver Kraus in 2003, who is not with <a
href="http://www.uni-erlangen.de/">Universität Erlangen-Nürnberg</a>
anymore. The current maintainer of the project is <a
href="http://www.lzs.eei.uni-erlangen.de/Mitarbeiter/Dichtl">Tobias
Dichtl</a>. His line of scientific research is not the same as that of
DGC, but, he said he will be adding "extended burst mode synthesis
support" this year. Meanwhile, I had updated the sources with
autotools build changes, and released 0.98 for Fedora. The updated
changes are available at <a
href="http://git.fedorahosted.org/git/dgc.git">dgc.git Fedora Hosted
repository</a>.