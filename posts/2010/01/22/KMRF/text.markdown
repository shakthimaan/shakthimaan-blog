---
author: SK
tags: 2010, FOSS
timestamp: 00:00:00
title: KMRF OLPC
---

We will be initiating olpc, Hyderabad for content creation (Telugu)
and to support the students, faculty and staff at <a
href="http://www.kmrfoundation.org/">KMRF</a> who need
assistance. Thanks to my good friend, <a
href="http://verma.sfsu.edu/index.php">Dr. Sameer Verma</a> for
coordinating with me last few months in this regard. <a
href="http://cob.sfsu.edu/cob/directory/faculty_profile.cfm?facid=416">Prof. Humaira
Mahi</a> will be having a field deployment through <a
href="http://www.kmrfoundation.org/">KMR Foundation</a>. Please do
write to me if you are interested in helping them in Hyderabad.