---
author: SK
tags: 2010, fedora
timestamp: 00:00:00
title: Mepco Schlenk IRC training
---

I had scheduled to conduct a one-day Fedora, Free Software workshop at
<a href="http://www.mepcoeng.ac.in/">Mepco Schlenk Engineering
College, Sivaskasi, Tamil Nadu, India</a> on Saturday, October 2,
2010, but due to the <a
href="http://www.thehindu.com/news/national/article800650.ece">Ayodhya
verdict</a>, I had to cancel it in the last moment.

I then tried to get <a
href="http://fedoraproject.org/wiki/Communicate/IRCHowTo">Internet
Relay Chat</a> (IRC) up and running on their college computer labs
that were running Fedora. They were using squid proxy, and I had
requested the system administrator to <a
href="http://fedorasolved.org/Members/realz/squid_IM">configure</a> it
to allow IRC connections. He got it working, and we decided to go
ahead with online IRC training for the following sessions:

* <a href="http://shakthimaan.com/downloads.html#i-want-2-do-project-tell-me-wat-2-do">i-want-2-do-project. tell-me-wat-2-do-fedora</a>
* <a href="http://shakthimaan.com/downloads.html#di-git-ally-managing-love-letters">di-git-ally managing love letters</a>
* Q/A session
* <a href="http://shakthimaan.com/downloads.html#packaging-red-hot-paneer-butter-masala">Packaging RPM</a>

The <a
href="http://shakthimaan.com/downloads/glv/2010/logs/mepco-schlenk-foss-oct-2-2010.log.html">IRC
log</a> of the session is available. They were kind enough to take
some <a
href="http://www.shakthimaan.com/Mambo/gallery/album64">pictures</a>
during the sessions and send it across to me.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album64/3_lab.jpg"
alt="IRC session in progress"></img>

Thanks to Prof. Shenbagaraj for working with me for the past two
months in organizing this workshop. Hopefully, someday, I will meet
them in person.