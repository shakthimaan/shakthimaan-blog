---
author: SK
tags: 2010, FOSS
timestamp: 00:00:00
title: OLPC at Sreenidhi International School
---

I had visited <a
href="http://www.sreenidhiinternational.com/">Sreenidhi International
School</a>, Aziznagar, Moinabad, Andhra Pradesh, India to help them
with the <a href="http://kmrfoundation.org/">KMR Foundation's</a> <a
href="http://wiki.laptop.org/go/The_OLPC_Wiki">OLPC</a> project. They
will soon start using an instance of <a
href="http://wordpress.org/">WordPress</a> for their blogs, where you
can get to know updates on their OLPC deployment. They already have a
blog for their <a href="http://sis.edu.in/blog/">Primary Years
Programme (PYP)</a>. I helped them with few software, hardware
problems that they were facing with their OLPCs (XO-1).

<img
src="http://shakthimaan.com/downloads/glv/hyd/olpc/kmrf-olpc-512.JPG"
alt="Dismantling the OLPC"></img>