---
author: SK
tags: 2010, documentation
timestamp: 00:00:00
title: LaTeX \newcommand
---

Used \\newcommand to abstract the four actor names in <a
href="http://shakthimaan.com/downloads.html#di-git-ally-managing-love-letters">di-git-ally
managing love letters</a> presentation:

~~~~ {.latex}
\newcommand{\oneactor}{pretty-zinta}
\newcommand{\twoactor}{raaani-mukerji}
\newcommand{\threeactor}{nayantaaara}
\newcommand{\fouractor}{aishvarya-ray}
~~~~

You can now get the <a
href="http://gitorious.org/di-git-ally-managing-love-letters">sources
from gitorious</a>, and replace the above names to your liking, and
rebuild the presentation by invoking <em>make</em> from the sources
directory.

~~~~ {.shell}
$ git clone git://gitorious.org/di-git-ally-managing-love-letters/mainline.git
~~~~
