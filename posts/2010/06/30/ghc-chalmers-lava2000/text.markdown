---
author: SK
tags: 2010, fedora, haskell
timestamp: 00:00:00
title: Chalmers Lava2000
---

<a
href="http://hackage.haskell.org/package/chalmers-lava2000">Chalmers
Lava2000</a>, a hardware description library in Haskell is now
available for Fedora. Thanks to <a
href="http://www.cse.chalmers.se/~emax/index.html">Emil Axelsson</a>
and <a href="http://fedoraproject.org/wiki/JensPetersen">Jens
Peterson</a> for their feedback, and package review. You can install
the same using:

~~~~ {.shell}
$ sudo yum install ghc-chalmers-lava2000 ghc-chalmers-lava2000-devel ghc-chalmers-lava2000-doc
~~~~ 

To illustrate a half adder example with the use of the Lava library,
create a Test.hs file:

~~~~ {.haskell}
import Lava

halfAdd (a, b) = (sum, carry)
  where
    sum   = xor2 (a, b)
    carry = and2 (a, b)
~~~~

Load it with ghci (Glasgow Haskell Compiler):

~~~~ {.shell}
$ ghci Test.hs

GHCi, version 6.12.2: http://www.haskell.org/ghc/  :? for help
Loading package ghc-prim ... linking ... done.
Loading package integer-gmp ... linking ... done.
Loading package base ... linking ... done.
Loading package ffi-1.0 ... linking ... done.
[1 of 1] Compiling Main             ( Test.hs, interpreted )
Ok, modules loaded: Main.
~~~~

Test half adder with low, high inputs using:

~~~~ {.haskell}
*Main> simulate halfAdd(low, high)
Loading package syb-0.1.0.2 ... linking ... done.
Loading package base-3.0.3.2 ... linking ... done.
Loading package array-0.3.0.0 ... linking ... done.
Loading package filepath-1.1.0.3 ... linking ... done.
Loading package old-locale-1.0.0.2 ... linking ... done.
Loading package old-time-1.0.0.3 ... linking ... done.
Loading package unix-2.4.0.0 ... linking ... done.
Loading package directory-1.0.1.0 ... linking ... done.
Loading package process-1.0.1.2 ... linking ... done.
Loading package time-1.1.4 ... linking ... done.
Loading package random-1.0.0.2 ... linking ... done.
Loading package haskell98 ... linking ... done.
Loading package chalmers-lava2000-1.1.1 ... linking ... done.
(high,low)
*Main> 
~~~~

Testing half adder with high, high inputs gives:

~~~~ {.haskell}
*Main> simulate halfAdd(high, high)
(low,high)
~~~~

You can also generate vhdl file using:

~~~~ {.haskell}
*Main> writeVhdl "halfAdd" halfAdd

Loading package syb-0.1.0.2 ... linking ... done.
Loading package base-3.0.3.2 ... linking ... done.
Loading package array-0.3.0.0 ... linking ... done.
Loading package filepath-1.1.0.4 ... linking ... done.
Loading package old-locale-1.0.0.2 ... linking ... done.
Loading package old-time-1.0.0.4 ... linking ... done.
Loading package unix-2.4.0.1 ... linking ... done.
Loading package directory-1.0.1.1 ... linking ... done.
Loading package process-1.0.1.2 ... linking ... done.
Loading package time-1.1.4 ... linking ... done.
Loading package random-1.0.0.2 ... linking ... done.
Loading package haskell98 ... linking ... done.
Loading package chalmers-lava2000-1.1.1 ... linking ... done.
Writing to file "halfAdd.vhd" ... Done.
~~~~

Copy /usr/share/chalmers-lava2000-1.1.1/Vhdl/lava.vhd to your project
directory, and you can verify the generated halfAdd.vhd with it using
<a href="http://ghdl.free.fr/">ghdl</a>:

~~~~ {.shell}
$ ghdl -a lava.vhd
$ ghdl -a halfAdd.vhd 
$
~~~~

You are encouraged to read the <a
href="http://www.cse.chalmers.se/edu/course/TDA956/Papers/lava-tutorial.ps">"Slightly
Revised Tutorial on Lava"</a> by <a
href="http://www.chalmers.se/cse/EN/people/claessen-koen">Koen
Claessen</a>, and <a href="http://www.cse.chalmers.se/~ms/">Mary
Sheeran</a> for more detailed documentation on using the library.