---
author: SK
tags: 2010, fedora, laptop
timestamp: 00:00:00
title: Dell Vostro 1310 rawhide
---

Have updated my Dell Vostro 1310 to Fedora rawhide. The relevant <a
href="http://www.shakthimaan.com/installs/dell-vostro-1310.html">configuration
outputs</a> are now made available:

* <a href="http://www.shakthimaan.com/downloads/laptop/dell-vostro-1310/f13/lspci.txt">/sbin/lspci</a>
* <a href="http://www.shakthimaan.com/downloads/laptop/dell-vostro-1310/f13/lsmod.txt">/sbin/lsmod</a>
* <a href="http://www.shakthimaan.com/downloads/laptop/dell-vostro-1310/f13/menu.lst.txt">/boot/grub/menu.lst</a>
* <a href="http://www.shakthimaan.com/downloads/laptop/dell-vostro-1310/f13/dmesg.txt">dmesg</a>

Updating tcl to tcl-8.5.8-2.fc14 solves <a
href="https://bugzilla.redhat.com/show_bug.cgi?id=540296">540296</a>. One
can now do:

~~~~ {.shell}
$ tclsh
% package require Tk
~~~~

Running "mcu8051ide --check-libraries" should now be clean on Fedora:

~~~~ {.shell}
Checking libraries...
	1/9 Checking for library BWidget
		Library present	... YES
		Version 1.7	... YES
	2/9 Checking for library Itcl
		Library present	... YES
		Version 3.4	... YES
	3/9 Checking for library Tcl
		Library present	... YES
		Version 8.2	... YES
	4/9 Checking for library md5
		Library present	... YES
		Version 2.0	... YES
	5/9 Checking for library crc16
		Library present	... YES
		Version 1.1	... YES
	6/9 Checking for library Tk
		Library present	... YES
		Version 8.5	... YES
	7/9 Checking for library img::png
		Library present	... YES
		Version 1.3	... YES
	8/9 Checking for library tdom
		Library present	... YES
		Version 0.8	... YES
	9/9 Checking for library Tclx
		Library present	... YES
		Version 8.0	... YES
RESULTS:
	Number of fails: 0
	Everything seems ok
~~~~

Pushed recent <a href="http://mcu8051ide.sf.net">mcu8051ide</a> 1.3.7
and <a href="http://vrq.sf.net">vrq</a> 1.0.76 to Fedora repository.