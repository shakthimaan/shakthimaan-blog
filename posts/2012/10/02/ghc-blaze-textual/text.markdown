---
author: SK
tags: 2012, fedora, haskell
timestamp: 17:30:00
title: ghc-blaze-textual
---

<a
href="http://hackage.haskell.org/package/blaze-textual">blaze-textual</a>
is a library for rendering Haskell data types to bytestrings. It is
now available in Fedora. Install it using:

~~~~ {.shell}
$ sudo yum install ghc-blaze-textual-devel
~~~~

You can import the Text module (for example) using:

~~~~ {.shell}
ghci> :m + Blaze.Text
~~~~

A simple example of using **digit**, **integral**, and **float**
functions is shown below:

~~~~ {.haskell}
import qualified Data.ByteString.Lazy as L
import Blaze.ByteString.Builder

import Blaze.Text
import Blaze.Text.Int

main = do
     let a = toLazyByteString $ digit 3
         b = toLazyByteString $ integral 3
         c = toLazyByteString $ float 3
     L.putStrLn a
     L.putStrLn b
     L.putStrLn c
~~~~

You can compile it using:

~~~~ {.shell}
$ ghc --make test.hs
[1 of 1] Compiling Main             ( test.hs, test.o )
Linking test ...
~~~~

Running *test* gives:

~~~~ {.shell}
$ ./test
3
3
3.0
~~~~
