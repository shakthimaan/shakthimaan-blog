---
author: SK
tags: 2012, fedora
timestamp: 10:45:00
title: Fedora Activity Day, RVCE, Bengaluru, October 7, 2012
---

A <a href="https://fedoraproject.org/wiki/FAD_Bengaluru_2012">Fedora Activity Day</a> was organized at <a href="http://www.rvce.edu.in/">R. V. College of Engineering</a>, Bengaluru on Sunday, October 7, 2012. The participants were students from the Master of Computer Applications (MCA) department.

I started the day's proceedings on best <a href="http://shakthimaan.com/downloads.html#i-want-2-do-project-tell-me-wat-2-do">communication practices, project guidelines</a> that need to be followed, and introduced the audience to the various Fedora sub-projects that they can get started with. The next session was on version control systems, and their importance with an <a href="http://shakthimaan.com/downloads.html#di-git-ally-managing-love-letters">introduction to Git</a>. We had a hands-on session where the participants practised simple Git commands.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album84/4_git_session.jpg" alt="Git hands-on session"></img>

<a href="https://fedoraproject.org/wiki/User:Kumarpraveen">Praveen Kumar</a> (HP) then spoke about Bugzilla, and how one can file bugs for Fedora. He also explained how to write meaningful bug reports.

Post-lunch, I introduced them to packaging concepts and terminology. Using the <a href="http://shakthimaan.com/downloads.html#packaging-red-hot-paneer-butter-masala">RPM packaging presentation</a>, we taught them how to build, and test RPMs. Praveen Kumar explained the Fedora package review process, and the various tools and infrastructure that we use for the same.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album84/6_pk_bugzilla.jpg" alt="Praveen Kumar on Bugzilla"></img>

The final session of the day was from <a href="https://fedoraproject.org/wiki/User:Charlesrose">Charles Rose</a> (Dell) who introduced the participants to virtualization with KVM on Fedora. He also addressed the basic concepts involved in virtualization with numerous examples. The department lab is planning to move all their backend services to Virtual Machines.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album84/8_qemu_cmd_line.jpg" alt="Charles Rose on Virtualization"></img>

Thanks to Prof. Renuka Prasad for working with us in organizing this workshop. 

More photos are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album84">/gallery</a>.
