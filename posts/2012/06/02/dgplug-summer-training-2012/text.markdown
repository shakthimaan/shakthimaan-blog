---
author: SK
tags: 2012, fedora
timestamp: 15:40:00
title: I Know What You Are Going To Do This Summer 2012
---

We are happy to announce <a
href="http://wiki.dgplug.org/index.php/SummerTraining12">"I know what
You Are Going To Do This Summer 2012"</a>, a free (as in freedom),
online (<a
href="http://fedoraproject.org/wiki/Communicate/IRCHowTo">IRC-based</a>)
training program in Free/Libre/Open Source Software at #dgplug on
irc.freenode.net.

If you are a prospective candidate, or a mentor, who would like to
participate in this year's sessions, please go through the <a
href="http://www.dgplug.org/irclogs/">previous year's IRC logs</a>.

We will have review, and Q&A sessions, on topics addressed in the
previous years before proceeding with new topics for this year.

The session timings are usually after 1900 IST, every day.

To participate, you will need a reliable Internet connection, and any
latest distribution installed (Fedora 16/17 preferable).

The program is open to all. If you are interested in participating,
please confirm the same by sending an e-mail to kushaldas AT gmail DOT
com, or shakthimaan at fedoraproject dot org.

References:

URL: <a href="http://dgplug.org">http://dgplug.org</a>

Planet: <a
href="http://planet.dgplug.org">http://planet.dgplug.org</a>

Wiki: <a href="http://wiki.dgplug.org/">http://wiki.dgplug.org</a>

Mailing list group (for queries, discussions): <a
href="http://lists.dgplug.org/listinfo.cgi/users-dgplug.org">http://lists.dgplug.org/listinfo.cgi/users-dgplug.org</a>

