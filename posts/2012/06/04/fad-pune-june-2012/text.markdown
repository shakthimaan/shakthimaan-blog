---
author: SK
tags: 2012, fedora, haskell
timestamp: 12:45:00
title: Fedora Activity Day, June 2, 2012
---

A <a
href="https://fedoraproject.org/wiki/FAD_Pune_2012_June_02">Fedora
Activity Day</a> (FAD) was organized on Saturday, June 2, 2012 at the
Red Hat office premises in Pune, India.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album81/5_discussion.jpg"
alt="Discussion in progress"></img>

This was an activity based event, where there were no talks. The
various topics suggested during the FAD included development,
packaging, and documentation work.

The newbies worked on <a
href="https://fedoraproject.org/wiki/How_to_create_a_GNU_Hello_RPM_package">GNU
Hello RPM packaging</a>, while others worked on pushing updates to
existing packages, and packaging new software.

I had submitted a new <a
href="http://embed.cs.utah.edu/csmith/">Csmith</a> package release for
review, and packaged and submitted <a
href="http://hackage.haskell.org/package/sized-types-0.3.4.0">sized-types</a>,
and <a
href="http://hackage.haskell.org/package/netlist-to-vhdl-0.3.1">netlist-to-vhdl</a>
Haskell packages for review. Lakshmi Narasimhan completed the package
review for sized-types.

I had requested Fedora Infrastructure to make <a
href="http://www.agilofortrac.com/">Agilo for Trac</a> (Apache
license) available. It was made available for testing during the FAD,
and I tested the same with the <a
href="https://fedorahosted.org/fedora-electronic-lab/">Fedora
Electronic Lab</a> trac instance. All the Agilo plugins are now
enabled in the trac for use.

We also had an F17 release party during the event, where Rahul
Sundaram cut a beautiful, tasty, F17 cake.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album81/1_cake.jpg"
alt="F17 cake"></img>

Thanks to Red Hat for hosting the event, sponsoring lunch, and
providing the facilities for the FAD.

More photos are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album81">/gallery</a>.


