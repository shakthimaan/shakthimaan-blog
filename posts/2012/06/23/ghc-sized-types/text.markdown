---
author: SK
tags: 2012, haskell, fedora
timestamp: 15:55:00
title: ghc-sized-types
---
<a href="http://hackage.haskell.org/package/sized-types">sized-types</a> 
provides indices, matrices, signed and unsigned bit vectors. It is now 
available in Fedora. Install it using:

~~~~ {.shell}
 $ sudo yum install ghc-sized-types-devel
~~~~

An n-bit sized type is represented by X<sub>n</sub>. For example, a
4-bit unsigned number can be represented by Unsigned X4. Few examples:

~~~~ {.haskell}
ghci> [minBound .. maxBound] :: [X4]
[0,1,2,3]

ghci> 100 :: Unsigned X4
4

ghci> 100 + 100 :: Signed X8
-56
~~~~

The signed and unsigned types can also be used in matrix operations:

~~~~ {.haskell}
ghci> let n = matrix [1..12] :: Matrix (X3,X4) Int
ghci> n
[ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 ]

ghci> print $ tranpose n
[ 1, 5, 9, 2, 6, 10, 3, 7, 11, 4, 8, 12 ]

ghci> let m = matrix [3,4,5,6] :: Matrix X4 Int
ghci> m
[ 3, 4, 5, 6]
ghci> m ! 2
5
~~~~

These general purpose types are very useful in specifying hardware
descriptions, especially when you need fixed-width, irregular size,
signed and unsigned numbers.