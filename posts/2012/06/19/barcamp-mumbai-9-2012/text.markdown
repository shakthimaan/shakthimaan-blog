---
author: SK
tags: 2012, fedora
timestamp: 08:20:00
title: Barcamp Mumbai 9, June 17, 2012
---

I attended <a href="http://barcampmumbai.org/index.php/BCM9">Barcamp
Mumbai 9</a> at <a href="http://www.vjti.ac.in/">Veermata Jijabai
Technological Institute (VJTI)</a>, Mumbai, Maharashtra, India on
Sunday, June 17, 2012. After an informal introduction, four parallel
tracks were scheduled. Each session was 20 minutes long, and 10
minutes between talks for people to move between the halls. Although
most of the talks were non-technical, there were quite a few
interesting sessions.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album82/4_vjti_sign_board.jpg"
alt="VJTI sign board"></img>

Raj Desai presented on "The Math of Music". He explained how the
rhythms or beats used in music formed a pattern, and how they often
use prime numbers. For example, a 5-beat sequence is made from a
combination of a 2-3 beat sequence, or, a 7-beat sequence is made from
a 2-2-3 or a 3-2-2 beat sequence. He also played the different beats,
giving numerous examples.

Krishna Patel played the short movie <a
href="http://www.pixar.com/shorts/gg/index.html">"Geri's Game"</a> in
his session on "How To Watch a Movie", and explained the different
aspects in a movie like composition, props, sound, colour etc. He
mentioned that one might have to watch a movie several times focusing
on just one aspect at a time to observe and learn how they have been
used.

<img src="http://www.shakthimaan.com/Mambo/gallery/albums/album82/6_list_of_talks.jpg" alt="List of talks"></img>

The session on "How to memorise a pack of 52 playing cards in under 2
minutes" by Aniceto Pereira taught how to use two simple mnemonic
systems to memorise a pack of cards. For the four suites, we used the
letters C (clubs), S (spades), H (hearts), D (diamonds). For each card
in a suite, a consonant is assigned. For example, Ace was assigned the
letter 't' or 'd', because there is one vertical line in it, referring
to one. The number '2' was assigned the letter 'n' because there were
two downward strokes in it. So, if we had an Ace ('t') of clubs ('c'),
we would combine the letter and the consonant to form an image, say
"cat", and associate it with our environment to remember it. For each
card that we have, we build a series of images to remember them.

<a href="http://web.gnuer.org/blog/index.php">Anurag Patel's</a>
session on "Sh*t people say to a chat bot" was hilarious! He had
created <a href="http://rickfare.com">http://rickfare.com</a> to
compute Mumbai's autorickshaw fare calculation, and later added
support for other cities as well. There were times when people started
to chat with the bot, and he shared quite a few entertaining,
priceless conversations from the server logs.

The talk on "Negotiating with VCs - An Entrepreneur’s Legal Guide" by
Abhyudaya Agarwal was very informative, and detailed. I had presented
<a
href="http://shakthimaan.com/downloads.html#qucs-a-qt-love-story">"Quite
Universal Circuit Simulator - A Qt Love Story"</a>, an introduction to
electrical circuit theory using <a
href="http://qucs.sourceforge.net/">Qucs</a>. You can install Qucs on
Fedora using:

~~~~ {.shell}
  $ sudo yum install qucs
~~~~

I also had a chance to stay at <a
href="http://en.wikipedia.org/wiki/Anushakti_Nagar">Anu Shakti
Nagar</a>, a quiet, serene, beautiful residential township in Mumbai
in this visit. Few pictures taken during the trip are available in my
<a
href="http://www.shakthimaan.com/Mambo/gallery/album82">/gallery</a>.


