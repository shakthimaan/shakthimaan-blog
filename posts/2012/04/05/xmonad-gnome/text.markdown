---
author: SK
tags: 2012, haskell, xmonad, gnome
timestamp: 12:30:00
title: xmonad-gnome
---

I was looking for a tiling window manager to use, and found <a
href="http://xmonad.org/">xmonad</a>. It is written in Haskell, and is
quite extensible. You can install it on Fedora using:

~~~~ {.shell}
$ sudo yum install xmonad-gnome
~~~~

Here is a screenshot of the same (click image):

<a
href="http://www.shakthimaan.com/downloads/screenshots/xmonad-screenshot.png"><img
width="676" height="263" alt="xmonad screenshot"
src="http://www.shakthimaan.com/downloads/screenshots/xmonad-screenshot-thumbnail.png"></img></a>

The left ALT key is used by default as the "mod" key in xmonad. Since
I use it with GNU Emacs, the Windows key has been reconfigured now to
be used as the "mod" key. I also use xmonad with GNOME 3 in fallback
mode, which actually looks like GNOME 2. The configuration file
~/.xmonad/xmonad.hs:

~~~~ {.haskell}
import XMonad
import XMonad.Config.Gnome
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig

main = do
     xmonad $ gnomeConfig {
            workspaces = myWorkspaces
            , modMask = mod4Mask
     } `additionalKeysP` myKeys

myWorkspaces = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

myKeys = [
 
    -- other additional keys
 
    ] ++ -- (++) is needed here because the following list comprehension
         -- is a list, not a single key binding. Simply adding it to the
         -- list of key bindings would result in something like [ b1, b2,
         -- [ b3, b4, b5 ] ] resulting in a type error. (Lists must
         -- contain items all of the same type.)
 
    [ (otherModMasks ++ "M-" ++ [key], action tag)
      | (tag, key)  &lt;- zip myWorkspaces "123456789"
      , (otherModMasks, action) &lt;- [ ("", windows . W.view) -- was W.greedyView
                                      , ("S-", windows . W.shift)]
    ]
~~~~
