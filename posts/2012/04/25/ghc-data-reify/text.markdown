---
author: SK
tags: 2012, haskell
timestamp: 10:10:00
title: ghc-data-reify
---

<a href="http://hackage.haskell.org/package/data-reify">data-reify</a>
provides a way to turn recursive data structures into graphs. It is
now available in Fedora. Install it using:

~~~~ {.shell}
 $ sudo yum install ghc-data-reify-devel
~~~~

A list [1,2,3] can be written using Cons, Nil, and In for recursion
using:

~~~~
In (Cons 1 (In (Cons 2 (In (Cons 3 (In Nil))))))
~~~~

An example when using data-reify for the above is given below:

~~~~ {.haskell}
{-# LANGUAGE TypeFamilies #-}
module Main where

import Control.Applicative hiding (Const)

import Data.Reify
import Control.Monad

data List a b = Nil | Cons a b
  deriving Show

instance MuRef [a] where
  type DeRef [a] = List a 

  mapDeRef f (x:xs) = Cons x <$> f xs
  mapDeRef f []     = pure Nil

main = do
        let g1 = [1, 2, 3]
        reifyGraph g1 >>= print
~~~~

Compile it using:

~~~~ {.shell}
$ ghc --make Test.hs
[1 of 1] Compiling Main             ( Test.hs, Test.o )
Linking Test ...
~~~~

Run it using:

~~~~ {.shell}
$ ./Test
let [(1,Cons 1 2),(2,Cons 2 3),(3,Cons 3 4),(4,Nil)] in 1
~~~~
