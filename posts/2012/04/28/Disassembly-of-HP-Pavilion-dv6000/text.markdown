---
author: SK
tags: 2012, laptop, photo
timestamp: 15:40:00
title: Disassembly of HP Pavilion dv6000
---

Some pictures of a used HP Pavilion dv6000 which had a fried motherboard:

<img alt="Keyboard removed"
src="http://shakthimaan.com/downloads/hardware/hp-dv6000/2-without-keyboard.JPG"></img>

<img alt="Keyboard"
src="http://shakthimaan.com/downloads/hardware/hp-dv6000/3-keyboard.JPG"></img>

<img alt="Metal casing"
src="http://shakthimaan.com/downloads/hardware/hp-dv6000/4-metal-casing.JPG"></img>

<img alt="TFT"
src="http://shakthimaan.com/downloads/hardware/hp-dv6000/6-TFT.JPG"></img>

<img alt="Without the TFT"
src="http://shakthimaan.com/downloads/hardware/hp-dv6000/5-without-TFT.JPG"></img>

<img alt="Motherboard"
src="http://shakthimaan.com/downloads/hardware/hp-dv6000/7-motherboard.JPG"></img>