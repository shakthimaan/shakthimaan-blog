---
author: SK
tags: 2012, haskell
timestamp: 07:40:00
title: ghc-dotgen
---

<a href="http://hackage.haskell.org/package/dotgen">dotgen</a>
provides a a simple interface for generating .dot graph files. It is
now available in Fedora. Install it along with graphviz using:

~~~~ {.shell}
 $ sudo yum install ghc-dotgen-devel graphviz
~~~~

A binary search tree example is shown below:

~~~~ {.haskell}
module Main where

import Text.Dot

box label = node $ [ ("shape","record"),("height",".1"),("label",label) ]

main = putStrLn $ showDot $ do
     c0 &lt;- box "&lt;f0&gt; |&lt;f1&gt; G|&lt;f2&gt; "
     c1 &lt;- box "&lt;f0&gt; |&lt;f1&gt; E|&lt;f2&gt; "
     c2 &lt;- box "&lt;f0&gt; |&lt;f1&gt; B|&lt;f2&gt; "
     c3 &lt;- box "&lt;f0&gt; |&lt;f1&gt; F|&lt;f2&gt; "
     c4 &lt;- box "&lt;f0&gt; |&lt;f1&gt; R|&lt;f2&gt; "
     c5 &lt;- box "&lt;f0&gt; |&lt;f1&gt; H|&lt;f2&gt; "
     c6 &lt;- box "&lt;f0&gt; |&lt;f1&gt; Y|&lt;f2&gt; "
     c7 &lt;- box "&lt;f0&gt; |&lt;f1&gt; A|&lt;f2&gt; "
     c8 &lt;- box "&lt;f0&gt; |&lt;f1&gt; C|&lt;f2&gt; "

     c0 .-&gt;. c4
     c0 .-&gt;. c1
     c1 .-&gt;. c2
     c1 .-&gt;. c3
     c2 .-&gt;. c8
     c2 .-&gt;. c7
     c4 .-&gt;. c6
     c4 .-&gt;. c5

     return ()
~~~~ 

Compile, and run it using:

~~~~ {.shell}
$ ghc --make Test.hs
[1 of 1] Compiling Main             ( Test.hs, Test.o )
Linking Test ...

$ ./Test > test.dot
~~~~

You can convert the generated .dot graph file into .png using:

~~~~ {.shell}
$ dot -Tpng test.dot -o test.png
~~~~

A screenshot of the generated png:

<a
href="http://www.shakthimaan.com/downloads/screenshots/binary-search-tree.png"><img
alt="binary search tree screenshot"
src="http://www.shakthimaan.com/downloads/screenshots/binary-search-tree.png"></img></a>
