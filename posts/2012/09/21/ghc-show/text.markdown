---
author: SK
tags: 2012, fedora, haskell
timestamp: 13:45:00
title: ghc-show
---

<a href="http://hackage.haskell.org/package/show">show</a> package
provides ShowQ, ShowFun and SimpleReflect modules. It is now available
in Fedora. Install it using:

~~~~ {.shell}
 $ sudo yum install ghc-show-devel
~~~~

To import the ShowQ module (for example), you can use:

~~~~ {.shell}
Prelude> :m + ShowQ
~~~~

The ShowFun module provides Typeable instances for IO expressions. The
ShowQ module includes SmallCheck and QuickCheck support. Example uses
of mysmallcheck, myquickcheck and tests functions in ShowQ are shown
below:

~~~~ {.shell}
Prelude ShowQ> mysmallcheck (\s -> length ("Hello") == 5)
Depth 0:
  Completed 1 test(s) without failure.
Depth 1:
  Completed 1 test(s) without failure.
Depth 2:
  Completed 1 test(s) without failure.
Depth 3:
  Completed 1 test(s) without failure.
Depth 4:
  Completed 1 test(s) without failure.
Depth 5:
  Completed 1 test(s) without failure.
Depth 6:
  Completed 1 test(s) without failure.
()

Prelude ShowQ> myquickcheck (\s -> length ("Hello") == 5)
"+++ OK, passed 100 tests.
OK, passed 100 tests."

Prelude ShowQ> tests (\s -> length("Hello") == 5) 5 [["a"], ["b"]]
+++ OK, passed 100 tests.
"OK, passed 100 tests.1%b.\n1%a.\n"
~~~~

The SimpleReflect module expands functions literally, and provides
simple reflection of Haskell expressions containing variables. Few
examples:

~~~~ {.shell}
Prelude SimpleReflect> foldr f x [1..5]
f 1 (f 2 (f 3 (f 4 (f 5 x))))

Prelude SimpleReflect> sum [1..5] :: Expr
0 + 1 + 2 + 3 + 4 + 5

Prelude SimpleReflect> sum $ map (*x) [1..5]
0 + 1 * x + 2 * x + 3 * x + 4 * x + 5 * x
~~~~
