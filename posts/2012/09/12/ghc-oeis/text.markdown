---
author: SK
tags: 2012, haskell, fedora
timestamp: 18:10:00
title: ghc-oeis
---

<a href="http://hackage.haskell.org/package/oeis">oeis</a> package
provides an interface to the Online Encyclopaedia of Integer Sequences
- <a href="http://oeis.org">http://oeis.org</a>. It is now available
in Fedora. Install it using:

~~~~ {.shell}
 $ sudo yum install ghc-oeis-devel
~~~~

To import the OEIS module, use:

~~~~ {.haskell}
ghci> :m + Math.OEIS
~~~~

Few examples are shown below:

~~~~ {.haskell}
Prelude Math.OEIS> getSequenceByID "A000040"
Just [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,
97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,
193,197,199,211,223,227,229,233,239,241,251,257,263,269,271]
~~~~

~~~~ {.haskell}
Prelude Math.OEIS> description `fmap` lookupSequenceByID "A000040"
Just "The prime numbers."
~~~~

~~~~ {.haskell}
Prelude Math.OEIS> lookupOEIS "2,3,5,7"
["The prime numbers.","[2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,
67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,
167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,
269,271]"]
~~~~

~~~~ {.haskell}
Prelude Math.OEIS> extendSequence [2,3,5]
[2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,
101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,
193,197,199,211,223,227,229,233,239,241,251,257,263,269,271]
~~~~


