---
author: SK
tags: 2012, fedora, emacs
timestamp: 15:30:00
title: emacs-identica-mode
---

<a
href="https://savannah.nongnu.org/projects/identica-mode/">identica-mode</a>
is an Emacs mode to receive and submit updates to laconica
microblogging site. It is now available in Fedora. Install it using:

~~~~ {.shell}
 $ sudo yum install emacs-identica-mode
~~~~

To enter into identica-mode in GNU Emacs, you can use:

~~~~ {.lisp}
M-x identica-mode
~~~~

Or, you can also add the following in your ~/.emacs:

~~~~ {.lisp}
(require 'identica-mode)
(setq identica-username "yourusername")
~~~~

To send an update, use C-c C-s within GNU Emacs, or use:

~~~~ {.lisp}
M-x identica-update-status-interactive
~~~~

To view the public timeline, you can use:

~~~~ {.lisp}
C-c C-a
~~~~

To view a user's timeline, you can use:

~~~~ {.lisp}
C-c C-u
~~~~

To send a direct message to a user, you can use:

~~~~ {.lisp}
C-c C-d
~~~~

You can press 'G' anytime to refresh the timeline in the \*identica\*
buffer. The Identica mode manual has more examples and shortcuts to
interact with identi.ca.
