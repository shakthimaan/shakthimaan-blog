---
author: SK
tags: 2012, fedora, haskell
timestamp: 05:45:00
title: ghc-data-inttrie
---

<a
href="http://hackage.haskell.org/package/data-inttrie">data-inttrie</a>
package is a simple lazy, infinite trie for integers. It is now
available in Fedora. Install it using:

~~~~ {.shell}
 $ sudo yum install ghc-data-inttrie-devel
~~~~

To import the data-inttrie module, use:

~~~~ {.haskell}
Prelude> :m + Data.IntTrie
~~~~

The Bits class in Data.Bits module defines bitwise operations on
integral types, and is defined using:

~~~~ {.haskell}
class Num a => Bits a where
~~~~

The complement function, for example, reverses the bits in the argument:

~~~~ {.haskell}
Prelude> :m + Data.bits
Prelude Data.Bits> complement 3
-4
~~~~

The apply function in Data.IntTrie applies a trie to an argument:

~~~~ {.haskell}
apply :: (Ord b, Bits b) => IntTrie a -> b -> a
~~~~

For example:

~~~~ {.haskell}
Prelude Data.IntTrie> let bits f = apply (fmap f identity)
Prelude Data.IntTrie> bits (*3) 4 :: Int
12
~~~~
