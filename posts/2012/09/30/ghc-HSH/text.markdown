---
author: SK
tags: 2012, fedora, haskell
timestamp: 18:00:00
title: ghc-HSH
---

<a href="http://hackage.haskell.org/package/HSH">HSH</a> package
allows you to use shell commands and expressions with Haskell
programs. It is now available in Fedora. Install it using:

~~~~ {.shell}
$ sudo yum install ghc-HSH-devel
~~~~

You can import the HSH module using:

~~~~ {.shell}
ghci> :m + HSH
~~~~

The **runIO** function can execute a shell command:

~~~~ {.shell}
Prelude HSH> runIO "date"
Sun Sep 30 21:29:50 IST 2012
~~~~

You can use pipes with multiple commands by separating them with
"-|-":

~~~~ {.shell}
Prelude HSH> runIO $ "date" -|- "wc"
      1       6      29
~~~~

The **runSL** function takes a command as an argument and returns the
first line of the output:

~~~~ {.shell}
Prelude HSH> runSL "cal"
"   September 2012"
~~~~

The **setenv** function can set an environment variable:

~~~~ {.shell}
Prelude HSH> runIO "echo $TERM"
xterm

Prelude HSH> runIO $ setenv [("TERM", "gnome-terminal")] $ "echo $TERM"
gnome-terminal
~~~~

The HSH package also provides shell equivalent commands. Few examples
are shown below:

~~~~ {.shell}
Prelude HSH> basename "/tmp/tmp/doc"
"doc"

Prelude HSH> dirname "/tmp/tmp/doc"
"/tmp/tmp"

Prelude HSH> pwd
"/tmp"

Prelude HSH> cut 2 ' ' "alpha bravo charlie delta echo"
"charlie"
~~~~

The **wcL** and **wcW** functions count the lines and words
respectively:

~~~~ {.shell}
Prelude HSH> wcL ["hello", "world"]
["2"]

Prelude HSH> wcW ["first", "second", "third fourth"]
["4"]
~~~~
