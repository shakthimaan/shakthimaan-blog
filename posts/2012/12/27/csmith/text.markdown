---
author: SK
tags: 2012, fedora
timestamp: 09:20:00
title: csmith
---

<a href="http://embed.cs.utah.edu/csmith/">csmith</a> is a tool for
testing compilers. It can generate random C programs for the C99
standard. It is now available in Fedora. Install it using:

~~~~ {.shell}
$ sudo yum install csmith
~~~~

The following simple bash script, given by the developers,
demonstrates its usage:

~~~~ {.shell}
set -e
while [ true ]
do
  csmith > test.c;
  gcc-4.0 -I${CSMITH_PATH}/runtime -O -w test.c -o /dev/null;
done
~~~~

There are quite a number of options you can use to tell csmith to
generate the programs that you want. For example, if you don't want
argc to be passed to the *main* function, you can use:

~~~~ {.shell}
$ csmith --no-argc
~~~~

The *main* function in the generated C program will resemble:

~~~~ {.c}
...
int main (void)
{
...
}
~~~~

The maximum number of fields in a struct that csmith will generate is
ten. You can increase it by using the --max-struct-fields option:

~~~~ {.shell}
$ csmith --max-struct-fields 15
~~~~

A structure that was created with the above option is shown below:

~~~~ {.c}
...
struct S1 {
   unsigned f0 : 20;
   const signed f1 : 2;
   volatile signed f2 : 15;
   signed f3 : 23;
   unsigned f4 : 9;
   signed f5 : 1;
   volatile uint8_t  f6;
   const volatile signed f7 : 12;
   const volatile signed f8 : 20;
   signed f9 : 27;
   const unsigned f10 : 11;
   uint16_t  f11;
};
...
~~~~

csmith also produces a brief summary or statistics on the program it
generates. A sample output is shown below:

~~~~ {.c}
/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 278
XXX total union variables: 13

XXX non-zero bitfields defined in structs: 0
XXX zero bitfields defined in structs: 0
XXX const bitfields defined in structs: 0
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 0
breakdown:
XXX full-bitfields structs in the program: 0
breakdown:
XXX times a bitfields struct's address is taken: 0
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 0
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 0

XXX max expression depth: 41
breakdown:
   depth: 1, occurrence: 81
   depth: 2, occurrence: 19
   depth: 3, occurrence: 2
   depth: 4, occurrence: 3
   depth: 5, occurrence: 1
   depth: 10, occurrence: 1
   depth: 11, occurrence: 1
   depth: 12, occurrence: 1
   depth: 13, occurrence: 3
   depth: 14, occurrence: 1
   depth: 15, occurrence: 1
   depth: 16, occurrence: 1
   depth: 18, occurrence: 2
   depth: 21, occurrence: 2
   depth: 28, occurrence: 1
   depth: 32, occurrence: 1
   depth: 40, occurrence: 1
   depth: 41, occurrence: 1

XXX total number of pointers: 191

XXX times a variable address is taken: 88
XXX times a pointer is dereferenced on RHS: 93
breakdown:
   depth: 1, occurrence: 85
   depth: 2, occurrence: 5
   depth: 3, occurrence: 3
XXX times a pointer is dereferenced on LHS: 137
breakdown:
   depth: 1, occurrence: 129
   depth: 2, occurrence: 5
   depth: 3, occurrence: 2
   depth: 4, occurrence: 1
XXX times a pointer is compared with null: 16
XXX times a pointer is compared with address of another variable: 2
XXX times a pointer is compared with another pointer: 5
XXX times a pointer is qualified to be dereferenced: 4443

XXX max dereference level: 5
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 501
   level: 2, occurrence: 29
   level: 3, occurrence: 13
   level: 4, occurrence: 2
   level: 5, occurrence: 1
XXX number of pointers point to pointers: 57
XXX number of pointers point to scalars: 132
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 42.4
XXX average alias set size: 1.73

XXX times a non-volatile is read: 668
XXX times a non-volatile is write: 407
XXX times a volatile is read: 6
XXX    times read thru a pointer: 2
XXX times a volatile is write: 1
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 73
XXX percentage of non-volatile access: 99.4

XXX forward jumps: 0
XXX backward jumps: 1

XXX stmts: 81
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 27
   depth: 1, occurrence: 18
   depth: 2, occurrence: 17
   depth: 3, occurrence: 10
   depth: 4, occurrence: 5
   depth: 5, occurrence: 4

XXX percentage a fresh-made variable is used: 16.2
XXX percentage an existing variable is used: 83.8
********************* end of statistics **********************/
~~~~

If you don't want the statistics, and would like a brief output, you
can use the --concise option with csmith:

~~~~ {.shell}
$ csmith --concise
~~~~

You are encouraged to read the <a
href="http://embed.cs.utah.edu/csmith/using.html">usage guide</a> for
more information on using the tool.