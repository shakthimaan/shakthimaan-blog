---
author: SK
tags: 2012, fedora, haskell
timestamp: 16:00:00
title: ghc-arrows
---

Arrows provide a generalization for monads, which was introduced by
John Hughes. The <a
href="http://hackage.haskell.org/package/arrows">arrows</a> package
provides classes that extend the Arrows class. It is now available in
Fedora. Install it using:

~~~~ {.shell}
$ sudo yum install ghc-arrows-devel
~~~~

Consider the identity function defined using the arrows notation:

~~~~ {.haskell}
{-# LANGUAGE Arrows #-}
 
import Control.Arrow (returnA)
 
idA :: a -> a
idA = proc a -> returnA -< a
~~~~

The idA function returns the given input as shown below:

~~~~ {.shell}
*Main> idA 6
6
*Main> idA True
True
*Main> idA "Eureka!"
"Eureka!"
~~~~

A mulTwo function that multiplies an integer by two can be written as:

~~~~ {.haskell}
{-# LANGUAGE Arrows #-}
 
import Control.Arrow (returnA)

mulTwo :: Int -> Int
mulTwo = proc a -> returnA -< (a * 2)
~~~~

Testing mulTwo with ghci:

~~~~ {.shell}
*Main> mulTwo 4
8
*Main> mulTwo 5
10
~~~~

We can also use the do notation with arrows. For example:

~~~~ {.haskell}
{-# LANGUAGE Arrows #-}
 
import Control.Arrow (returnA)

mulFour :: Int -> Int
mulFour = proc a -> do b <- mulTwo -< a
                       mulTwo -< b
~~~~

Loading mulFour in ghci:

~~~~ {.shell}
*Main> mulFour 2
8
*Main> mulFour 3
12
~~~~

Arrows also supports the use of conditional statements. An example
when used with the if ... then ... else construct is as follows:

~~~~ {.haskell}
{-# LANGUAGE Arrows #-}
 
import Control.Arrow (returnA)

condMul :: Int -> Int
condMul = proc a ->
               if a < 5
               then returnA -< (a * 2)
               else returnA -< (a * 3)                      
~~~~

The condMul function multiplies the input integer by two if it is less than five, and
with three when greater than five:

~~~~ {.shell}
*Main> condMul 2
4
*Main> condMul 6
18
~~~~
