---
author: SK
tags: 2012, fedora
timestamp: 16:20:00
title: OpenStack workshop, Symbiosis, Pune, December 1, 2012
---

A workshop on <a href="http://www.openstack.org/">OpenStack</a> was
conducted on Saturday, December 1, 2012 at the <a
href="http://sicsr.ac.in/">Symbiosis Institute of Computer Studies and
Research (SICSR)</a>, Pune, India. Both theory and lab sessions were
organized for the students. I started the proceedings using Perry
Myers's presentation on "Introduction and Overview of OpenStack for
IaaS (Infrastructure as a Service) Clouds" for the Essex release. The
various building blocks of OpenStack with their functionality was
explained. An overall big picture of the architecture was presented to
them with illustrations.

<img
src="http://www.shakthimaan.com/downloads/glv/2012/openstack-symbiosis-2012/openstack-workshop-lab.JPG"
alt="OpenStack hands-on session"></img>

The <a
href="http://fedorapeople.org/~russellb/openstack-lab-rhsummit-2012/">OpenStack
Lab Guide</a> was then given to the participants to setup their own
OpenStack private cloud. Some of them had brought their own laptops,
while others used the Fedora machines in the labs. We started by
setting up the Keystone service, and adding users for
authentication. The Glance service was then installed, and
configured. A <a
href="http://docs.openstack.org/trunk/openstack-compute/admin/content/starting-images.html">Fedora
17 and cirros image</a> were then imported into Glance. The Nova
service was then setup, and a SSH keypair was created for testing.

The Horizon dashboard user interface was used to start a virtual
machine instance. Using ssh and the created keypair, we were able to
login to the virtual machine and use it.  curl was used to test the
different REST API on the running stack. I also showed them simple
Python examples to demonstrate the OpenStack APIs. As a final
presentation for the day, I gave an introduction on libvirtd, and KVM.

Thanks to Manoj Aswani for working with me in organizing this
workshop. Thanks also to Perry Myers and Mark McLoughlin for
permission to use their presentation.

