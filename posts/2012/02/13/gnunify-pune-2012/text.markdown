---
author: SK
tags: 2012, FOSS
timestamp: 16:10:00
title: GNUnify, 2012
---

I attended <a href="http://gnunify.in/">GNUnify 2012</a> on February
10-11, 2012 at <a href="http://sicsr.ac.in/">Symbiosis Institute of
Computer Studies and Research</a>, Pune, India.

Day I

The first day first session that I attended was on <a
href="http://gnunify.in/2012/KYS/ArunKhan#talk1">"Build your own cloud
computing infrastructure"</a> by <a
href="http://www.gnunify.in/2012/KYS/ArunKhan">Arun Khan</a>. He
addressed the different concepts and tools available for setting up a
private computing infrastructure. After the first session, I moved to
listen to <a href="http://fedoraproject.org/wiki/User:Tuxdna">Saleem
Ansari's</a> talk on <a
href="http://gnunify.in/2012/KYS/SaleemAnsari#talk1">"Torque Box -
Ruby App Server"</a>. TorqueBox is small adaptation layer on top of
JBoss's Java application server. I then attended <a
href="http://gnunify.in/2012/KYS/SrikantPatnaik">Srikant Patnaik's</a>
talk on <a href="http://gnunify.in/2012/KYS/SrikantPatnaik#talk1">"Web
based embedded project design"</a>. He demonstrated a simple example
of using an LED with Arduino and controlling it with a Python CGI
webserver. He also demonstrated the use of Fritzing to build the
demoed circuit. All the tools are available on Fedora:

~~~~ {.shell}
 $ sudo yum install fritzing arduino
~~~~

After lunch, I attended the talk on "Btrfs - The next Generation
Filesystem on Linux" by <a
href="http://wiki.embeddednirvana.org/User:Neependra">Neependra
Khare</a>. He addressed the different problems in the current
filesystems, and how Btrfs tries to solve them. For the last talk of
the day I attended Dr. Abhijat Vichare's (from <a
href="http://www.crlindia.com/">Computational Research
Laboratories</a> Pune) session on "Portability concepts in GCC". It
was a detailed session on the internals of <a
href="http://gcc.gnu.org/">GCC</a>.

Day II

I attended <a
href="http://fedoraproject.org/wiki/User:Suchakra">Suchakra
Sharma's</a> session on <a
href="http://gnunify.in/2012/KYS/SuchakrapaniDattSharma#talk1">"Developing
QT Apps on Android"</a>. I then went to assist with the system
programming contest organized by Neependra Khare. The participants
were given a written test on the first day, and based on the results,
few were selected to compete in the programming contest (sponsored by
<a href="http://stec-inc.com/">STEC</a>). For the contest, few
programming problems were given to the participants to be solved in C
within couple of hours time. The first, second and third prices were
worth INR 5000, 3000 and 2000 respectively. Kiran Divekar (from <a
href="http://www.marvell.com/">Marvell</a>) joined us, and we reviewed
the code and chose the winners. After lunch, we announced the winners
and the organizers and <a href="http://plug.org.in">PLUG</a> members
helped with the prize distribution.

<img alt="Programming contest"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album75/5_programming_contest.jpg"></img>

I then went to attend Arun Khan's session on <a
href="http://gnunify.in/2012/KYS/arunkhan#talk2">"Convert your old
laptop to an useful network device"</a>. He had illustrated with
numerous examples on how he had used old hardware into functional,
useful network devices. It is extremely useful for beginners who are
interested in getting started in working with inexpensive hardware. I
then had a chance to meet and talk with <a
href="http://blog.mozilla.com/gen/">Gen Kanai</a> and <a
href="http://playingwithsid.blogspot.in/">Rakesh "Arky" Ambati</a>
from Mozilla. It was also good to catch up with lot of Wikimedia
folks, with whom I went for dinner at the <a
href="http://www.vaishalihotel.in/main.html">Vaishali
restaurant</a>. Few pictures taken during the event are available in
my <a
href="http://www.shakthimaan.com/Mambo/gallery/album75">/gallery</a>.

<img alt="Wikimedia dinner at Vaishali"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album75/7_vaishali_dinner.jpg"></img>
