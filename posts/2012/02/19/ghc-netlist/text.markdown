---
author: SK
tags: 2012, haskell, verilog, vhdl
timestamp: 02:05:00
title: ghc-netlist
---
<a href="http://hackage.haskell.org/package/netlist">Netlist</a> is a simplified and generic netlist designed to be compatible with Hardware Description Languages (HDLs) like Verilog and VHDL. It is now available in Fedora. Install it using:

~~~~ {.shell}
 $ sudo yum install ghc-netlist-devel
~~~~

An example usage from the sources is given below:

~~~~ {.haskell}
import Language.Netlist.AST
import Language.Netlist.Util

t :: Module
t = Module "foo" (f ins) (f outs) [] ds
  where
    f xs = [ (x, makeRange Down sz) | (x, sz) &lt;- xs ]
    ins = [("clk", 1), ("reset", 1), ("enable", 1), ("x", 16)]
    outs = [("z", 16)]

ds :: [Decl]
ds = [ NetDecl "a" (makeRange Down 16) (Just (ExprVar "x"))
     , NetDecl "b" (makeRange Down 16) (Just (sizedInteger 16 10))
     , MemDecl "c" Nothing (makeRange Down 16) Nothing
     , ProcessDecl (Event (ExprVar "clk") PosEdge)
                   (Just (Event (ExprVar "reset") PosEdge, (Assign (ExprVar "c") (sizedInteger 16 0))))
                   (If (ExprVar "enable")
                         (Assign (ExprVar "c") (ExprVar "x"))
                         Nothing)
     ]
~~~~

Load it using ghci version 7.0.2 gives:

~~~~ {.shell}
$ ghci Test.hs 
GHCi, version 7.0.2: http://www.haskell.org/ghc/  :? for help
Loading package ghc-prim ... linking ... done.
Loading package integer-gmp ... linking ... done.
Loading package base ... linking ... done.
[1 of 1] Compiling Main             ( Test.hs, interpreted )
Ok, modules loaded: Main.
~~~~

You can check the value of 'ds' using:

~~~~ {.haskell}
*Main> ds
Loading package array-0.3.0.2 ... linking ... done.
Loading package bytestring-0.9.1.10 ... linking ... done.
Loading package containers-0.4.0.0 ... linking ... done.
Loading package binary-0.5.0.2 ... linking ... done.
Loading package syb-0.3 ... linking ... done.
Loading package netlist-0.3.1 ... linking ... done.
[NetDecl "a" (Just (Range (ExprLit Nothing (ExprNum 15)) (ExprLit Nothing (ExprNum 0)))) (Just (ExprVar "x")),NetDecl "b" (Just (Range (ExprLit Nothing (ExprNum 15)) (ExprLit Nothing (ExprNum 0)))) (Just (ExprLit (Just 16) (ExprNum 10))),MemDecl "c" Nothing (Just (Range (ExprLit Nothing (ExprNum 15)) (ExprLit Nothing (ExprNum 0)))) Nothing,ProcessDecl (Event (ExprVar "clk") PosEdge) (Just (Event (ExprVar "reset") PosEdge,Assign (ExprVar "c") (ExprLit (Just 16) (ExprNum 0)))) (If (ExprVar "enable") (Assign (ExprVar "c") (ExprVar "x")) Nothing)]
*Main> 
~~~~