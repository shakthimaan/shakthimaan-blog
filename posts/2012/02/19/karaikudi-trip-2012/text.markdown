---
author: SK
tags: 2012, photo
timestamp: 14:10:00
title: Karaikudi trip photos
---

Few photos taken on a trip to <a
href="http://en.wikipedia.org/wiki/Karaikudi">Karaikudi</a>, Tamil
Nadu, India. More photos in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album76">/gallery</a>.

<img alt="Street view"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album76/1_street_view.jpg"></img>

<img alt="Inside courtyard"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album76/4_inside_courtyard.jpg"></img>

<img alt="Chettinad palace"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album76/10_chettinad_palace.jpg"></img>