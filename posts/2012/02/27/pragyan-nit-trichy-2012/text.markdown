---
author: SK
tags: 2012, FOSS
timestamp: 12:20:00
title: Pragyan, NIT, Trichy, India, 2012
---

I had presented the (in)famous <a
href="http://shakthimaan.com/downloads.html#i-want-2-do-project-tell-me-wat-2-do">"i-want-2-do-project.tell-me-wat-2-do-fedora"</a>
presentation as a guest lecturer at <a
href="http://www.pragyan.org">"Pragyan 2012"</a>, <a
href="http://www.nitt.edu/home/">NIT, Trichy</a>, Tamil Nadu, India on
Sunday, February 26, 2012.

<img alt="NIT Trichy"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album77/6_nit_trichy.jpg"></img>

Most of the student participants were familiar with the the *nix
desktop. I explained to them the various project, communication
guidelines, and best practices that they need to follow when working
on free/open source software projects. I had also introduced them to
the various Fedora sub-projects that they can participate and learn
from.

<img alt="Audience"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album77/8_audience.jpg"></img>

Four years ago, I was invited by the <a
href="http://www.pragyan.org/12/home/pengufest/">Pengufest</a> team of
Pragyan, but, I couldn't make it then. Apparently, my presentation has
been circulating among them for quite some time, and they now got to
hear it from the horse's mouth.

On Saturday, as part of the Pengufest track there were sessions held
by <a href="http://www.bell-labs.com/user/poosala/">Dr. Viswanath
Poosala</a> on "Engineering Innovation - A recipe for coming up with
disruptive ideas", and sessions on "How to be a Hacker?", "Functional
Programming and why it matters", and "Lisp - God's own programming
language" by <a
href="http://www.linuxforu.com/author/vivek-shangari/">Vivek
Shangari</a>.

I also attended <a href="http://sunson.livejournal.com/">Suraj
Kumar's</a> talk on "GNU/Linux in large scale Internet based
businesses", where he addressed the different concepts, terminologies
and F/OSS tools that people use for large scale web deployments.

I would like to thank Red Hat for sponsoring my travel, and the
organizing team of Pragyan 2012 for the wonderful hospitality.

<img alt="Twilight"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album77/4_twilight.jpg"></img>

More photos taken during the trip are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album77">/gallery</a>.