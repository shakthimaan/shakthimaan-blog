---
author: SK
tags: 2012, FOSS, vlsi
timestamp: 13:20:00
title: VLSI Conference, Hyderabad, 2012
---

I attended the <a href="http://vlsiconference.com/vlsi2012/">25th
International Conference on VLSI Design and 11th International
Conference on Embedded Systems</a> between January 7-11, 2012 at the
<a href="http://www.hicc.com/">Hyderabad International Convention
Centre</a>, Hyderabad, India. The first two days consisted of tutorial
sessions, and the next three days had the <a
href="http://vlsiconference.com/vlsi2012/conf_schedule.htm">conference
presentations</a>. There were also exhibit stalls from different
companies.

<img alt="25th VLSID 2012"
src="http://shakthimaan.com/downloads/glv/2012/vlsid-jan/vlsid-jan-2012.png"></img>

On the first day I attended the tutorial session on <a
href="http://www.systemc-ams.org/">SystemC AMS extensions</a> by <a
href="https://www.fp7-smartcode.eu/ecws2/biosketch/damm">Markus
Damm</a>, <a
href="https://www.ict.tuwien.ac.at/mitarbeiter/adhikari">Sumit
Adhikari</a>, and <a
href="http://www.lip6.fr/actualite/personnes-fiche.php?ident=P102">François
Pecheux</a>. <a
href="http://fedoraproject.org/wiki/User:Chitlesh">Chitlesh Goorah</a>
had earlier tried to get <a
href="http://www.spinics.net/linux/fedora/fedora-electronic-lab/msg00117.html">SystemC
into Fedora</a> and <a
href="http://spins.fedoraproject.org/fel/">Fedora Electronic Lab</a>,
but, due to licensing issues it could not be included. SystemC-AMS is
now released under Apache license. Open SystemC Initiative and
Accellera Unite have now integrated to become <a
href="http://www.accellera.org/home/">Accellera Systems
Initiative</a>. We hope to work with them to get their sources under a
single free/open source software license. François Pecheux is from <a
href="http://www.lip6.fr/index.php?LANG=en">Laboratoire d'Informatique
de Paris 6</a>, <a href="http://www.upmc.fr/en/index.html">Pierre
&amp; Marie Curie University (UPMC)</a>, Paris, France, and we already
ship their free/open source EDA tools in Fedora.

On day two, I attended the tutorial session by <a
href="http://people.epfl.ch/arvind.sridhar">Sridhar Arvind</a> on <a
href="http://esl.epfl.ch/3d-ice.html">3D-ICE</a>, a free/open source
interlayer cooling emulator from <a href="http://esl.epfl.ch">Embedded
System Laboratory</a>, <a
href="http://www.epfl.ch/index.en.html">Ecole polytechnique fédérale
de Lausanne</a>, Switzerland. I have already been working with
Sridhar, <a href="http://people.epfl.ch/david.atienza">Prof. David
Atienza</a> and <a
href="http://people.epfl.ch/alessandro.vincenzi">Alessandro
Vincenzi</a> on testing 3D-ICE on Fedora. I had built and tested the
dependency <a
href="http://crd-legacy.lbl.gov/~xiaoye/SuperLU/">SuperLU</a> library
and the 3D-ICE package before the tutorial session. Their software has
already been downloaded by over 70 research labs around the world. I
will push our tested changes to them. On the later half of the day, I
attended a session on verification constraint complexity. <a
href="http://www.trusster.com/products/teal/">Teal</a> is a useful
verification utility and connection library that has support for
constraints and parameter control. The authors of the tool had agreed
to release it as free/open source software, and we also ship it in
Fedora.

On the following three days of the conference, I attended various
paper presentations from different tracks from reconfigurable
architectures to methods in AMS optimization. I met <a
href="http://www.isical.ac.in/~ssk/">Prof. Susmita Sur-Kolay</a> from
the <a href="http://www.isical.ac.in/">Indian Statistical
Institute</a>, Kolkata, India where they run Fedora in their
labs. They also wished to use the 3D-ICE tool and GPU tools in their
labs. I also visited the exhibit stalls meeting different people from
the industry and academia. There are quite a few interesting free/open
source tools that users can benefit from, and we will work in making
them available in Fedora. In 2013, the conference will be held in
Pune. Thanks to Red Hat for sponsoring my travel and participation at
the conference.