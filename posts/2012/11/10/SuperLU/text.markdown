---
author: SK
tags: 2012, fedora
timestamp: 04:00:00
title: SuperLU
---

<a href="http://crd-legacy.lbl.gov/~xiaoye/SuperLU/">SuperLU</a> is a
library for solving sparse linear system of equations AX = B. It is
now available in Fedora. Install it using:

~~~~ {.shell}
$ sudo yum install SuperLU-devel
~~~~

The superlu.c 5x5 example in the sources demonstrates the use of the
SuperLU library:

~~~~ {.c}
#include "slu_ddefs.h"

main(int argc, char *argv[])
{
    SuperMatrix A, L, U, B;
    double   *a, *rhs;
    double   s, u, p, e, r, l;
    int      *asub, *xa;
    int      *perm_r; /* row permutations from partial pivoting */
    int      *perm_c; /* column permutation vector */
    int      nrhs, info, i, m, n, nnz, permc_spec;
    superlu_options_t options;
    SuperLUStat_t stat;

    /* Initialize matrix A. */
    m = n = 5;
    nnz = 12;
    if ( !(a = doubleMalloc(nnz)) ) ABORT("Malloc fails for a[].");
    if ( !(asub = intMalloc(nnz)) ) ABORT("Malloc fails for asub[].");
    if ( !(xa = intMalloc(n+1)) ) ABORT("Malloc fails for xa[].");
    s = 19.0; u = 21.0; p = 16.0; e = 5.0; r = 18.0; l = 12.0;
    a[0] = s; a[1] = l; a[2] = l; a[3] = u; a[4] = l; a[5] = l;
    a[6] = u; a[7] = p; a[8] = u; a[9] = e; a[10]= u; a[11]= r;
    asub[0] = 0; asub[1] = 1; asub[2] = 4; asub[3] = 1;
    asub[4] = 2; asub[5] = 4; asub[6] = 0; asub[7] = 2;
    asub[8] = 0; asub[9] = 3; asub[10]= 3; asub[11]= 4;
    xa[0] = 0; xa[1] = 3; xa[2] = 6; xa[3] = 8; xa[4] = 10; xa[5] = 12;

    /* Create matrix A in the format expected by SuperLU. */
    dCreate_CompCol_Matrix(&A, m, n, nnz, a, asub, xa, SLU_NC, SLU_D, SLU_GE);
    
    /* Create right-hand side matrix B. */
    nrhs = 1;
    if ( !(rhs = doubleMalloc(m * nrhs)) ) ABORT("Malloc fails for rhs[].");
    for (i = 0; i < m; ++i) rhs[i] = 1.0;
    dCreate_Dense_Matrix(&B, m, nrhs, rhs, m, SLU_DN, SLU_D, SLU_GE);

    if ( !(perm_r = intMalloc(m)) ) ABORT("Malloc fails for perm_r[].");
    if ( !(perm_c = intMalloc(n)) ) ABORT("Malloc fails for perm_c[].");

    /* Set the default input options. */
    set_default_options(&options);
    options.ColPerm = NATURAL;

    /* Initialize the statistics variables. */
    StatInit(&stat);

    /* Solve the linear system. */
    dgssv(&options, &A, perm_c, perm_r, &L, &U, &B, &stat, &info);
    
    dPrint_CompCol_Matrix("A", &A);
    dPrint_CompCol_Matrix("U", &U);
    dPrint_SuperNode_Matrix("L", &L);
    print_int_vec("\nperm_r", m, perm_r);

    /* De-allocate storage */
    SUPERLU_FREE (rhs);
    SUPERLU_FREE (perm_r);
    SUPERLU_FREE (perm_c);
    Destroy_CompCol_Matrix(&A);
    Destroy_SuperMatrix_Store(&B);
    Destroy_SuperNode_Matrix(&L);
    Destroy_CompCol_Matrix(&U);
    StatFree(&stat);
}
~~~~

Running the program produces the following output:

~~~~ {.shell}
$ ./superlu

CompCol matrix A:
Stype 0, Dtype 1, Mtype 0
nrow 5, ncol 5, nnz 12
nzval: 19.000000  12.000000  12.000000  21.000000  12.000000  12.000000  21.000000  16.000000  21.000000  5.000000  21.000000  18.000000  
rowind: 0  1  4  1  2  4  0  2  0  3  3  4  
colptr: 0  3  6  8  10  12  

CompCol matrix U:
Stype 0, Dtype 1, Mtype 4
nrow 5, ncol 5, nnz 11
nzval: 21.000000  -13.263158  7.578947  21.000000  
rowind: 0  1  2  0  
colptr: 0  0  0  1  4  4  

SuperNode matrix L:
Stype 3, Dtype 1, Mtype 1
nrow 5, ncol 5, nnz 11, nsuper 2
nzval:
0	0	1.900000e+01
1	0	6.315789e-01
4	0	6.315789e-01
1	1	2.100000e+01
2	1	5.714286e-01
4	1	5.714286e-01
1	2	-1.326316e+01
2	2	2.357895e+01
4	2	-2.410714e-01
3	3	5.000000e+00
4	3	-7.714286e-01
3	4	2.100000e+01
4	4	3.420000e+01

nzval_colptr: 0  3  6  9  11  13  
rowind: 0  1  4  1  2  4  3  4  
rowind_colptr: 0  3  6  6  8  8  
col_to_sup: 0  1  1  2  2  
sup_to_col: 0  1  3  5  

perm_r
0	0
1	1
2	2
3	3
4	4
~~~~

The <a
href="http://crd-legacy.lbl.gov/~xiaoye/SuperLU/superlu_ug.pdf">SuperLU
User's Guide</a> provides detailed documentation on the algorithms and
usage of three libraries used to solve sparse linear systems.