---
author: SK
tags: 2012, documentation, FOSS, freedom
timestamp: 19:10:00
title: Publican, Document Freedom Day workshop, ISI, Kolkata
---

I presented <a href="https://fedorahosted.org/publican/">Publican</a>
at the <a
href="http://www.ilug-cal.info/index.php?option=com_content&amp;task&amp;=view&amp;id=185">Document
Freedom Workshop 2012</a> at the <a
href="http://www.isical.ac.in/~cssc/">Computer and Statistical Service
Centre (CSSC)</a>, S N Bose Bhavan, <a
href="http://www.isical.ac.in/">Indian Statistical Institute,
Kolkata</a>, West Bengal, India held between March 28-29, 2012.

<img alt="Poster at registration desk"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album79/3_poster_at_registration_desk.jpg"></img>

Day I

<a href="http://fedoraproject.org/wiki/User:Amani">A Mani</a> started
the day's proceedings with an introduction to Free/Libre/Open Source
Software, and basic shell commands. We had a computer lab with Fedora
installed where the participants used the terminal to try out the
hands-on exercises.

I then introduced Publican to the audience comprising mostly of
students and faculty. <a
href="http://fedoraproject.org/wiki/User:Jsmith">Jared Smith</a> had
presented Publican during <a href="http://fudcon.in/">FUDCon Pune
2011</a>, and, with his permission, I added more content and a lab
section. The participants were able to use Publican to create and
build documents with Bengali content!

<img alt="Lab session"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album79/11_latex_lab_session.jpg"></img>

After lunch, <a href="http://souravsengupta.com/">Sourav Sen Gupta</a>
gave an introduction to <a
href="http://www.latex-project.org/">LaTeX</a> and <a
href="http://en.wikipedia.org/wiki/Beamer_%28LaTeX%29">Beamer</a> with
his "'Golden Ratio' for Typesetting - A quick and random introduction
to LaTeX and Beamer" presentation. He used the <a
href="http://kile.sourceforge.net/">Kile</a> editor to demonstrate
LaTeX markups and for generating PDF files. I helped the students with
the lab session in writing, and troubleshooting warnings and errors
when using Kile with pdflatex.

I also met and spoke with two physicists, <a
href="http://en.wikipedia.org/wiki/John_A._Smolin">John Smolin</a> and
<a href="http://www.infres.enst.fr/~markham/">Damian Markham</a>, who
had come to present at another workshop on <a
href="http://www.isical.ac.in/~coec/workshop/quant2012.html">"Information
and Security in Quantum World"</a>. John Smolin is an avid Fedora
user!

Day II

The first session on the second day was by <a
href="http://www.jitrc.com/">Jit Ray Chowdhury</a> on <a
href="http://moodle.org/">Moodle</a> CMS. He started with the basics
of installing Apache, MySQL and PHP on Fedora, followed by creating a
simple HTML, and PHP page. Participants then learnt how to install
Drupal, Moodle and configure the same.

<a
href="http://www.hbcse.tifr.res.in/people/academic/nagarjuna-g">Prof. Nagarjuna</a>
then started his session on why document freedom is essential, and
gave an introduction to <a href="http://orgmode.org/">Emacs
org-mode</a>, with examples. I also met Krishnakant Mane but couldn't
attend his (post-lunch) session on "LibreOffice and Screen Readers" as
I had to leave early to catch a flight.

Thanks to the organizing committee: <a
href="http://www.isical.ac.in/~mandar/">Prof. Mandar Mitra</a>, A
Mani, Partha Pratim Kundu, <a
href="http://www.isical.ac.in/~malay_r/">Malay Bhattacharya</a>, and
Tanmay Basu for the wonderful hospitality, and to Red Hat for
sponsoring my travel.

The <a href="http://shakthimaan.com/downloads.html#publican">publican
presentation</a> is available. Few photos taken during the event are
available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album79">/gallery</a>.

