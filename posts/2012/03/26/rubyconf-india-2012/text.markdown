---
author: SK
tags: 2012, ruby
timestamp: 17:10:00
title: RubyConf, India, 2012
---

I attended and also gave a lightning talk at <a
href="http://rubyconfindia.org/">RubyConf India 2012</a> at the <a
href="http://pune.regency.hyatt.com/hyatt/hotels/index.jsp?null">Hyatt
Regency</a>, Pune, Maharashtra, India between March 24-25, 2012.

<img alt="RubyConf India 2012" src="http://www.shakthimaan.com/Mambo/gallery/albums/album78/1_banner.jpg"></img>

Day I

The keynote was delivered by <a
href="http://blog.headius.com/">Charles Oliver Nutter</a> on JRuby,
and Ruby virtual machines. This was followed by the recorded video
keynote by <a
href="http://en.wikipedia.org/wiki/Yukihiro_Matsumoto">Yukihiro
Matsumoto</a> on Ruby, its history, and his focus on Ruby for embedded
and scientific computing for the next couple of years. mruby for
embedded systems will be an interesting project to look forward to
this summer.

I then attended the talk on "Using Ruby to Craft and Test Beautiful
Command Line Applications" by Nikhil Mungel and Shishir Das, who gave
useful examples on how to construct meaningful and helpful command
line tools. <a href="http://parolkar.com/">Abhishek Parolkar</a>,
founder of <a href="http://www.bigdata.sg">BigData.sg</a>, spoke on
available Ruby tools, and architectures for processing large volumes
of data in the "Ruby for the soul of Big Data nerds" talk.

Post lunch, I attended the session on "Responsive Design: now 90%
easier with SASS!" by Arpan CJ, who also designed the RubyConf India
2012 website. He explained how <a
href="http://sass-lang.com/">Sass</a> is useful in creating websites
that can be quickly made to fit different display dimensions and
resolutions across phones, tablets, and PCs.

<a href="http://blog.deobald.ca/">Steven Deobald</a> presented on
"Clojure is my favourite ruby". He gave wonderful programming
construct examples for Ruby and Clojure, and why he enjoyed Clojure
more than Ruby. For the last talk of the day, I attended Sandip
Ransing and Shailesh Patil's talk on "VoIP on Rails in India" where
they had developed a call centre Rails front-end application that uses
<a href="http://adhearsion.com/">Adhearsion</a>. They have an internal
LAN setup for call centres: Rails application -> Adhearsion ->
Asterisk -> PRI (Primary Rate Interface) -> PSTN (Public Switched
Telephone Network).

There were quite a number of lightning talks at the end of the
day. One from <a href="http://www.flipkart.com">flipkart.com</a>,
where they mentioned that they use a service oriented architecture
with JSON over HTTP, along with RabbitMQ, Padrino and JRuby.

Day II

The second day began with a keynote by <a
href="http://lindsaar.net/">Mikel Lindsaar</a> on "How to win", where
he emphasized on defining a purpose/reason to do anything and
everything. <a href="http://blog.saush.com/">Chang Sau Sheong</a>
presented on using Ruby and R tools for doing population simulation in
his "Sex, Money and Evolution - Simulation and Data Analysis with Ruby
and R" talk. Niranjan Prabhakar Sarade gave an overview of the
internal data structures present in the Ruby MRI virtual machine
source code in the "What lies beneath the beautiful code?" talk.

After lunch, I attended the "Smells and patterns in test/spec code"
talk by Sidu Ponnappa and Aninda Kundu where they discussed the
different patterns and smells of test driven code, and their
implications. Karunakar presented on "Large scale Ruby project,
challenges and Pitfalls", sharing his experience on managing and
maintaining large scale Ruby projects. For the last talk of the day, I
attended <a href="http://matthewkirk.com/">Matthew Kirk's</a> talk on
"'method_missing' Should be Missing" where he talked about the overuse
of method_missing, "monkey patching", and eval from his experience.

Lightning talks were scheduled for the second day as well, and I was
happy to present <a
href="http://shakthimaan.com/downloads.html#nursery-rhymes-rb">nursery_rhymes.rb</a>. Few
photos taken during the event are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album78">/gallery</a>.