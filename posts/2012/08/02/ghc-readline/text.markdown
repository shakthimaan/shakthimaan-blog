---
author: SK
tags: 2012, haskell, fedora
timestamp: 17:30:00
title: ghc-readline
---

<a href="http://hackage.haskell.org/package/readline">readline</a> is
a Haskell binding to the <a
href="http://cnswww.cns.cwru.edu/php/chet/readline/rltop.html">GNU
Readline</a> library. It is now available in Fedora. Install it using:

~~~~ {.shell}
 $ sudo yum install ghc-readline-devel
~~~~

The Readline library provides functions to for line editing, and
managing history of commands entered interactively through the
command-line. It is Free Software. A simple example of using
ghc-readline is demonstrated:

~~~~ {.haskell}
import System.Console.Readline

readEvalPrintLoop :: IO ()
readEvalPrintLoop = do
   maybeLine <- readline "% "
   case maybeLine of 
    Nothing     -> return () 
    Just "exit" -> return ()
    Just line -> do addHistory line
                    putStrLn $ "The user input: " ++ (show line)
                    readEvalPrintLoop

main = do
     readEvalPrintLoop
~~~~

Compile it using:

~~~~ {.shell}
 $ ghc --make read.hs
Linking read ...
~~~~

Run it and test it:

~~~~ {.shell}
 $ ./read
% 
The user input: ""
% Lorem ipsum
The user input: "Lorem ipsum"
% exit
 $
~~~~

