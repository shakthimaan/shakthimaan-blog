---
author: SK
tags: 2012, haskell, fedora
timestamp: 17:30:00
title: ghc-numbers
---

<a href="http://hackage.haskell.org/package/numbers">numbers</a>
package provides instances of numerical classes for different types of
numbers - (computable) real numbers, precison fixed numbers, floating
point numbers, differentiable numbers, symbolic numbers, and interval
arithmetic. It is now available in Fedora. Install it using:

~~~~ {.shell}
 $ sudo yum install ghc-numbers-devel
~~~~

For interval arithmentic, Interval is defined as a type constructor:

~~~~ {.haskell}
data Interval a = I a a
~~~~

The ival function two arguments of the same type and returns an
Interval a, while, the getIval function takes an interval and returns
a pair.

~~~~ {.shell}
ghci> :m + Data.Number.Interval

ghci> ival 1 2
1..2

ghci> getIval (ival 3 4)
(3,4)
~~~~

The CReal type implements (constructive) real numbers. The showCReal
function takes a number of decimals, a real number, and returns a
string.

~~~~ {.shell}
ghci> :m + Data.Number.CReal

ghci> showCReal 5 pi
"3.14159"
~~~~

The Dif type is a type defined for differentiable numbers. The dCon
function takes a number and constructs a Dif number with the same
value, while the val function does the opposite.

~~~~ {.shell}
ghci> :m + Data.Number.Dif

ghci> dCon 3
3~~

ghci> val (dCon 5)
5
~~~~

The mkDif function takes a value and a Dif value and makes a Dif
number as its derivative.

~~~~ {.shell}
ghci> mkDif 4 (dCon 2)
4~~
~~~~

The deriv function takes a derivative of a function. For example, if
we have an equation f(x)=x^2^, then the first derivative,
f'(x)=2x. This can be defined as:

~~~~ {.shell}
ghci> let f x = x * x
ghci> let f' = deriv f

ghci> f 3
9

ghci> f' 3
6
~~~~
