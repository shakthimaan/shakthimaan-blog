---
author: SK
tags: 2012, fedora
timestamp: 08:00:00
title: Fedora workshop, ICCS, June 30, 2012
---
I had organized a one-day Fedora workshop at <a
href="http://iccs.ac.in/">Indira College of Commerce and Science</a>,
Pune, Maharashtra, India on Saturday, June 30, 2012. The college is
affiliated to the <a href="http://www.unipune.ac.in/">University of
Pune</a>.

The participants were post-graduate students in computer
applications. They were familiar with basic programming in C, C++,
Java and PHP. Their syllabus now mandates the use of *nix. Fedora was
installed on all their lab machines for students to use and learn.

The forenoon session was lecture-based, where I began with an overview
of the system architecture. Installation concepts were discussed,
along with common newbie mistakes that are made during installation. I
then demoed the Fedora desktop, and showed the various software, and
window managers that they can use. The need to use a version control
system was emphasized, and also introduced them to the use of git,
with few examples.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album83/5_lab_session.jpg"
alt="Lab session in progress"></img>

Post-lunch was a lab session, where students worked on shell scripts
and C programming exercises. I gave them numerous examples on how they
can improve their programs by composing functions that do one thing
and do it well. The students were also advised to follow coding
guidelines, and learn and improve by reading code from free/open
source software projects. I have also given them the latest Fedora
.iso images (32-bit and 64-bit).

Thanks to Antriksh Shah for his help in organizing this workshop. Few
photos taken during the event are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album83">/gallery</a>.






