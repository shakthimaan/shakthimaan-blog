---
author: SK
tags: 2012, haskell, fedora
timestamp: 12:00:00
title: unlambda
---
<a href="http://hackage.haskell.org/package/unlambda">unlambda</a>
package contains an interpreter written in Haskell for the <a
href="http://en.wikipedia.org/wiki/Unlambda">Unlambda</a> language. It
is now available in Fedora. Install it using:

~~~~ {.shell}
 $ sudo yum install unlambda
~~~~

Unlambda is a "nearly pure" functional programming language. There are
no named functions in Unlambda. The s and k primitive functions are
part of the core language, and are sufficient to make Unlambda Turing
complete. The backquote(`) is used for function application. There are
no variables in Unlambda. r prints a newline, and i is the identity
function.

~~~~ {.shell}
 $ unlambda
`ri

 $
~~~~

The k combinator takes two arguments (by currying), say X and Y, and
is expressed as \`\`kXY, which evaluates to X (Y is also evaluated). The
s combinator takes three arguments and is applied as \`\`FXYZ, which
evaluates to \`\`XZ\`YZ. The .x functions prints the character x to the
output. So, the r function is an instance of .x function where x
represents the newline character.

~~~~ {.shell}
 $ unlambda
```k.Hii

H $
~~~~

The classic hello world program:

~~~~ {.shell}
$ unlambda
`r```````````.H.e.l.l.o. .w.o.r.l.di
Hello world
~~~~

The following hello.unl program prints "Hello world!" followed by
asterisk symbol, in incremental fashion.

~~~~ {.shell}
```s``sii`ki
 ``s``s`ks	
     ``s``s`ks``s`k`s`kr
               ``s`k`si``s`k`s`k
                               `d````````````.H.e.l.l.o.,. .w.o.r.l.d.!
                        k
	k
  `k``s``s`ksk`k.*
~~~~

~~~~ {.shell}
$ unlambda < hello.unl

Hello, world!
Hello, world!*
Hello, world!**
Hello, world!***
Hello, world!****
Hello, world!*****
Hello, world!******
Hello, world!*******
Hello, world!********
Hello, world!*********
Hello, world!**********
...
~~~~
