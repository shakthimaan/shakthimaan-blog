---
author: SK
tags: 2012, haskell, fedora
timestamp: 04:50:00
title: ghc-lazysmallcheck
---
<a
href="http://hackage.haskell.org/package/lazysmallcheck">lazysmallcheck</a>
is a demand-driven testing library for Haskell programs. It requires
fewer test cases to verify properties for inputs for a depth. It is
now available on Fedora. Install it using:

~~~~ {.shell}
 $ sudo yum install ghc-lazysmallcheck-devel
~~~~

The depthCheck function lists the number of fewer tests required at a
given depth. For example:

~~~~ {.haskell}
import Test.LazySmallCheck
import System

type Set a = [a]

empty :: Set a
empty = []

insert :: Ord a => a -> Set a -> Set a
insert a [] = [a]
insert a (x:xs)
  | a < x = a:x:xs
  | a > x = x:insert a xs
  | a == x = x:xs

set :: Ord a => [a] -> Set a
set = foldr insert Main.empty

ordered [] = True
ordered [x] = True
ordered (x:y:zs) = x <= y && ordered (y:zs)

allDiff [] = True
allDiff (x:xs) = x `notElem` xs && allDiff xs

isSet s = ordered s && allDiff s

-- Properties

infixr 0 -->
False --> _ = True
True --> x = x

prop_insertSet :: (Char, Set Char) -> Bool
prop_insertSet (c, s) = ordered s --> ordered (insert c s)

main = do
        [d] <- getArgs
        depthCheck (read d) prop_insertSet
~~~~

You can compile and run it using:

~~~~ {.shell}
$  ghc --make ListSet.hs 
[1 of 1] Compiling Main             ( ListSet.hs, ListSet.o )
Linking ListSet ...

 $  ./ListSet 2
OK, required 23 tests at depth 2
~~~~