---
author: SK
tags: 2012, haskell
timestamp: 11:50:00
title: Blog using Hakyll
---

I wanted to migrate my blog to a static site generator, and chose <a
href="http://jaspervdj.be/hakyll/">Hakyll</a>, since it is written in
Haskell. You can install it on Fedora using:

~~~~ {.shell}
$ sudo yum install ghc-hakyll-devel
~~~~

I started looking at existing sites generated using Hakyll, and chose
<a href="http://www.skybluetrades.net/">Ian Ross's blog</a> as a
reference. I began customizing the same during the <a
href="http://www.hackfest.in">Hackfest</a> organized by <a
href="http://changer.in/">Changer</a> in Pune, India on Saturday, May
5, 2012.

The 2012 posts now have permanent URLs. Posts are tagged. I have
retained the CSS, current <a
href="http://www.shakthimaan.com/news.xml">RSS feed</a> and <a
href="http://www.shakthimaan.com/news.html">blog URL</a> during this
migration. You can get the sources from gitorious.org:

~~~~ {.shell}
$ git clone git://gitorious.org/shakthimaan-blog/mainline.git
~~~~
