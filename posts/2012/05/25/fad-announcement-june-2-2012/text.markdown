---
author: SK
tags: 2012, fedora
timestamp: 13:30:00
title: Fedora Activity Day, Red Hat, Pune, June 2, 2012
---

A <a
href="http://fedoraproject.org/wiki/Fedora_Activity_Day_-_FAD">Fedora
Activity Day</a> event is scheduled for Saturday, June 2, 2012 at the
Red Hat, Pune, India office premises.

Venue:

~~~~ {.shell}
  Red Hat Software Services Pvt Ltd 
  Tower X, Level-1,
  Cybercity, Magarpatta City,
  Hadapsar, Pune 411 013
  Maharashtra
  India
~~~~

Date : Saturday, June 2, 2012.

Time : 1000 IST onwards.

Entry is free, but, we have limited seats (50). Online registration
closes on Tuesday, May 29, 2012, 2359 IST. If you are interested in
attending, please add your name to <a
href="https://fedoraproject.org/wiki/FAD_Pune_2012_June_02">https://fedoraproject.org/wiki/FAD_Pune_2012_June_02</a>.

There will be no registration on the day of the event.

This is purely an activity based event where you are required to work
on Fedora related sub-projects. There will be no talks.

Lunch will be sponsored by Red Hat. We will also have an F17 release
party!

Please make sure to bring a valid photo identity card (driving
license/voter's id etc.) to enter the premises.

If you would like to suggest projects/task to work on during the FAD,
please feel free to update the wiki page.

You are encouraged to bring your laptop. Please do ensure that you
have all the necessary software installed for your work, or atleast:

~~~~ {.shell}
  # yum install @development-tools fedora-packager
~~~~ 

There will be Internet access available at the facility.
