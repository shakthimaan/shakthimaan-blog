---
author: SK
tags: 2012, travel, photo
timestamp: 11:45:00
title: Fort Jadhav Ghad
---

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album80/3_fort_jadhav_ghad.jpg"
alt="Fort Jadhav Ghad"></img><br />

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album80/7_welcome_note.jpg"
alt="Welcome note"></img><br />

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album80/13_museum_side.jpg"
alt="Museum side"></img><br />

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album80/23_fort_rear_side.jpg"
alt="Fort rear, side"></img><br />

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album80/34_swimming_pool.jpg"
alt="swimming pool"></img><br />

More photos available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album80">/gallery</a>.
