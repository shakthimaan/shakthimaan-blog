---
author: SK
tags: 2015, haskell
timestamp: 16:00:00
title: Introduction to Haskell
---
*[Published in Open Source For You (OSFY) magazine, July 2014 edition.]*

Haskell, a free and open source programming language, is the outcome
of 20 years of research. It has all the advantages of functional
programming and an intuitive syntax based on mathematical
notation. This article flags off a series in which we will explore
Haskell at length.

Haskell is a statically typed, general purpose programming
language. Code written in Haskell can be compiled and also used with
an interpreter. The static typing helps detect plenty of compile time
bugs. The type system in Haskell is very powerful and can
automatically infer types. Functions are treated as first-class
citizens and you can pass them around as arguments. It is a pure
functional language and employs lazy evaluation. It also supports
procedural and strict evaluation similar to other programming
paradigms.

Haskell code is known for its brevity and is very concise. The latest
language standard is Haskell 2010. The language supports many
extensions, and has been gaining wide-spread interest in the industry
due to its capability to run algorithms on multi-core systems. It has
support for concurrency because of the use of software transactional
memory. Haskell allows you to quickly create prototypes with its
platform and tools. Hoogle and Hayoo API search engines are available
to query and browse the list of Haskell packages and libraries. The
entire set of Haskell packages are available in Hackage.

The Haskell Platform contains all the software required to get you
started on it. On GNU/Linux, you can use your distribution package
manager to install the same. On Fedora, for example, you can use the
following command:

~~~~ {.bash}
# yum install haskell-platform
~~~~

On Ubuntu, you can use the following:

~~~~ {.bash}
# apt-get install haskell-platform
~~~~

On Windows, you can download and run
HaskellPlatform-2013.2.0.0-setup.exe from the Haskell platform web
site and follow the instructions for installation.

For Mac OS X, download either the 32-bit or 64-bit _.pkg_ file, and
click on either to proceed with the installation.

The most popular Haskell interpreter is the Glasgow Haskell Compiler
(GHC). To use its interpreter, you can run *ghci* from the command
prompt on your system:

~~~~ {.bash}
$ ghci
GHCi, version 7.6.3: http://www.haskell.org/ghc/  :? for help
Loading package ghc-prim ... linking ... done.
Loading package integer-gmp ... linking ... done.
Loading package base ... linking ... done.
Prelude> 
~~~~

The *Prelude* prompt indicates that the basic Haskell library modules
have been imported for your use.

To exit from GHCi, type _:quit_ in the Prelude prompt:

~~~~ {.bash}
Prelude> :quit
Leaving GHCi.
~~~~

The basic data types used in Haskell are discussed below.

A *Char* data type is for a Unicode character. You can view the type
using the command _:type_ at the GHCi prompt:

~~~~ {.haskell}
Prelude> :type 's'
's' :: Char
~~~~

The '::' symbol is used to separate the expression on the left with
the data type on the right.

A *Bool* data type represents a logical value of either True or False.

~~~~ {.haskell}
Prelude> :type True
True :: Bool
~~~~

Signed numbers with a fixed width are represented by the *Int* data
type. The *Integer* type is used for signed numbers that do not have a
fixed width.

~~~~ {.haskell}
Prelude> 5
5
~~~~

The *Double* and *Float* types are used to represent decimal
values. The Double type has better precision for floating point
numbers:

~~~~ {.haskell}
Prelude> 3.0
3.0
~~~~

The basic data types can be combined to form composite types. There
are two widely used composite types in Haskell, namely, lists and
tuples. A list is a collection of elements of the same data type
enclosed within square parenthesis. A list of characters is shown
below:

~~~~ {.haskell}
Prelude> :type ['a', 'b', 'c']
['a', 'b', 'c'] :: [Char]
~~~~

The static typing in Haskell produces errors during compile or load
time (in GHCi) when you mix data types inside a list. For example:

~~~~ {.haskell}
Prelude> ['a', 1, 2]

<interactive>:7:7:
    No instance for (Num Char) arising from the literal `1'
    Possible fix: add an instance declaration for (Num Char)
    In the expression: 1
    In the expression: ['a', 1, 2]
    In an equation for `it': it = ['a', 1, 2]
~~~~

You can have a list of lists as long as they contain the same data
type:

~~~~{.haskell}
Prelude> :type [['a'], ['b', 'c']]
[['a'], ['b', 'c']] :: [[Char]]
~~~~

A tuple is an ordered list of elements with a fixed size, enclosed
within parenthesis, where each element can be of a different data
type. For example:

~~~~ {.haskell}
Prelude> :type ('t', True)
('t', True) :: (Char, Bool)
~~~~

Note that the tuple with type (Char, Bool) is different from the tuple
with type (Bool, Char).

~~~~ {.haskell}
Prelude> :t (False, 'f')
(False, 'f') :: (Bool, Char)
~~~~

Haskell originates from the theory of Lambda calculus, which was
developed by Alonzo Church to formally study mathematics. In 1958,
John McCarthy created Lisp, that relates programming with Lambda
calculus. Robin Milner created a functional programming language
called ML (meta language) for automated proofs of mathematical
theorems in 1970. During the 1980s, there were a number of lazy
functional programming languages scattered across the research
community. Miranda was a very popular proprietary programming language
released by Research Software Ltd in 1985.

A need arose to unify the different research developments, for which a
committee was formed and the first version of the standard was
released in 1990. It was called Haskell 1.0, after the mathematician
and logician, Haskell Brooks Curry. Subsequently, there were four
revisions made - 1.1, 1.2, 1.3 and 1.4. In 1997, the Haskell 98 report
was released. In 2009, the Haskell 2010 standard was published and is
the latest standard as on date. It has Foreign Function Interface
(FFI) bindings to interface with other programming languages. The Hugs
interpreter is useful for teaching, while the Glasgow Haskell Compiler
(GHC) is very popular. The paper by John Hughes on *"Why Functional
Programming matters?"* in as excellent paper to read. A number of
software companies in the industry have begun to use Haskell in
production systems.

We shall be exploring more features, constructs and use of the
language in future articles.

# References #

[1] Haskell. <a href="http://haskell.org/">http://haskell.org/</a>

[2] Haskell 2010. <a href="http://www.haskell.org/haskellwiki/Haskell_2010">http://www.haskell.org/haskellwiki/Haskell_2010</a>

[3] Hoogle. <a href="http://www.haskell.org/hoogle/">http://www.haskell.org/hoogle/</a>

[4] Hayoo. <a href="http://holumbus.fh-wedel.de/hayoo/hayoo.html">http://holumbus.fh-wedel.de/hayoo/hayoo.html</a>

[5] Hackage. <a href="http://hackage.haskell.org/">http://hackage.haskell.org/</a>

[6] Haskell Platform. <a href="http://www.haskell.org/platform/">http://www.haskell.org/platform/</a>

[7] Glasgow Haskell Compiler. <a href="http://www.haskell.org/ghc/">http://www.haskell.org/ghc/</a>

[8] Alonzo Church. <a href="http://www-groups.dcs.st-and.ac.uk/history/Mathematicians/Church.html">http://www-groups.dcs.st-and.ac.uk/history/Mathematicians/Church.html</a>

[9] John McCarthy. <a href="http://www-formal.stanford.edu/jmc/">http://www-formal.stanford.edu/jmc/</a>

[10] Lisp. <a href="http://en.wikipedia.org/wiki/Lisp_%28programming_language%29">http://en.wikipedia.org/wiki/Lisp_%28programming_language%29</a>

[11] Robin Milner. <a href="http://www.cl.cam.ac.uk/archive/rm135/">http://www.cl.cam.ac.uk/archive/rm135/</a>

[12] Miranda. <a href="http://miranda.org.uk/">http://miranda.org.uk/</a>

[13] Haskell 1.0. <a href="http://www.haskell.org/definition/haskell-report-1.0.ps.gz">http://www.haskell.org/definition/haskell-report-1.0.ps.gz</a>

[14] Haskell Brooks Curry. <a href="http://www-history.mcs.st-andrews.ac.uk/Biographies/Curry.html">http://www-history.mcs.st-andrews.ac.uk/Biographies/Curry.html</a>

[15] Hugs. <a href="http://www.haskell.org/hugs/">http://www.haskell.org/hugs/</a>

[16] "Why Functional Programming matters?" <a href="http://www.cse.chalmers.se/~rjmh/Papers/whyfp.html">http://www.cse.chalmers.se/~rjmh/Papers/whyfp.html</a>

[17] Why functional programming? Why Haskell?. <a href="http://book.realworldhaskell.org/read/why-functional-programming-why-haskell.html">http://book.realworldhaskell.org/read/why-functional-programming-why-haskell.html</a>
