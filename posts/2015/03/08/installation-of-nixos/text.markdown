---
author: SK
tags: 2015, foss, install
timestamp: 16:00:00
title: Installation of NixOS 14.12
---

Boot from LiveCD ( nixos-graphical-14.12.140.0dbc415-x86_64-linux.iso
), with 40 GB virtual disk, and login as root (no password required).

~~~~ {.bash}
nixos login: root

[root@nixos:~]#
~~~~

Start the KDE environment using the following command:

~~~~ {.bash}
[root@nixos:~]# start display-manager
~~~~

You can then add the English Dvorak layout (optional) by selecting
'System Settings' -> 'Input Devices' -> 'Keyboard settings' ->
'Layouts' -> 'Configure layouts' -> 'Add' and use the label (dvo) for
the new layout. Check that networking works as shown below:

~~~~ {.bash}
[root@nixos:~]# ifconfig

[root@nixos:~]# ping -c3 www.google.com
~~~~

You can now partition the disk using 'fdisk /dev/sda' and create two
partitions (39 GB /dev/sda1 and swap on /dev/sda2). Create the
filesystems, and turn on swap using the following commands:

~~~~ {.bash}
# mkfs.ext4 -L nixos /dev/sda1

# mkswap -L swap /dev/sda2
# swapon /dev/sda2
~~~~

Generate a basic system configuration file with nixos-generate-config:

~~~~ {.bash}
# mount /dev/disk/by-label/nixos /mnt

# nixos-generate-config --root /mnt
~~~~

Update /mnt/etc/nixos/configuration.nix with new packages that you
need as illustrated below:

~~~~ {.text}
# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda";

  # networking.hostName = "nixos"; # Define your hostname.
  networking.hostId = "56db3cd3";
  # networking.wireless.enable = true;  # Enables wireless.

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "lat9w-16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    wget emacs24 git python gnuplot notmuch
    haskellPackages.pandoc

    # Installing texlive is slow and incomplete on NixOS
    # (pkgs.texLiveAggregationFun { paths = [ pkgs.texLive pkgs.texLiveExtra pkgs.texLiveBeamer ]; })    
  ];
  # texLive tetex lmodern
  
  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable the X11 windowing system.
  services.xserver.enable = true;
  services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable the KDE Desktop Environment.
  services.xserver.displayManager.kdm.enable = true;
  services.xserver.desktopManager.kde4.enable = true;


  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.extraUsers.apollo = {
    home = "/home/apollo";
    extraGroups = [ "wheel" ];
    useDefaultShell = true;
    isNormalUser = true;
    uid = 1000;
  };

}
~~~~

Install NixOS to hard disk:

~~~~ {.bash}
# nixos-install

setting root password...
Enter new UNIX password: ***
Retype new UNIX password: ***

passwd: password updated successfully
installation finished!
~~~~

You can now reboot into the system:

~~~~ {.bash}
# reboot
~~~~

After you login to the console, set a password for the 'apollo'
user. A screenshot of the desktop is shown below:

![](http://shakthimaan.com/downloads/screenshots/nixos-14-12-screenshot.png)
