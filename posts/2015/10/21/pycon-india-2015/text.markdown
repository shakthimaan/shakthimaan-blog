---
author: SK
tags: 2015, travel, documentation
timestamp: 10:00:00
title: PyCon India 2015, Bengaluru
---

I attended [PyCon India 2015](https://in.pycon.org/2015/) at NIMHANS
Convention Centre, Bengaluru, India between October 2-4, 2015.

<img alt="PyCon India 2015 stage"
src="http://shakthimaan.com/images/pycon-india-2015/pycon-india-2015.JPG"></img>

Day 1

I took an early train from Chennai to reach Bengaluru, and headed
straight to the venue. The workshops were scheduled on the first
day. After completing the registration formalities, I went to observe
the DevSprint. I met a number of [dgplug](http://dgplug.org/) folks
with whom I have been interacting online on #dgplug
(irc.freenode.net).

After lunch, I attended [Prof. Ronojoy Adhikari's](https://github.com/ronojoy) workshop on ["Reasoning under
uncertainity with
Python"](https://in.pycon.org/cfp/pycon-india-2015/proposals/reasoning-under-uncertainty-with-python/). [Dorai
Thodla](http://doraithodla.com/) began the workshop with an
introduction on "Data Science" and its use and applications in the
industry. Prof. Ronojoy then spoke on ["Data Science:
Theory"](https://speakerdeck.com/ronojoy/data-science-theory) and
["Probability Theory for Data
Science"](https://speakerdeck.com/ronojoy/data-science-probability-theory). We
already use Boolean logic in our programming. We also have probability
concepts implemented in Python. He then proceeded to demonstrate
["Probabilistic Programming in
Python"](https://speakerdeck.com/ronojoy/probabilistic-programming-in-python)
using [Lea](https://pypi.python.org/pypi/lea). He had presented a
short preview of this talk at the [Chennaipy](http://chennaipy.org/)
August 2015 meet-up. Prof. Ronojoy showed very interesting Lea
one-liners with IPython Notebook. A sample is provided below:

~~~~ {.python}
In [1]: from lea import *

In [2]: faircoin = Lea.fromVals('Head', 'Tail')

In [3]: faircoin.random(10)

Out[3]: ('Tail',
         'Tail',
         'Tail',
         'Head',
         'Head',
         'Tail',
         'Tail',
         'Head',
         'Tail',
         'Head')

In [4]: die = Lea.fromVals(1, 2, 3, 4, 5, 6)

In [5]: die1 = die.clone()
        die2 = die.clone()

In [6]: dice = die1 + die2

In [7]: dice

OUt[10]: 2 : 1/36
         3 : 2/36
         4 : 3/36
         5 : 4/36
         6 : 5/36
         7 : 6/36
         8 : 5/36
         9 : 4/36
        10 : 3/36
        11 : 2/36
        12 : 1/36
~~~~~

I had installed the [Anaconda
distribution](https://www.continuum.io/downloads) and Lea for the
workshop, but, couldn't get
[Pomegranate](https://github.com/jmschrei/pomegranate) to
compile. Later, I
[worked](https://github.com/jmschrei/pomegranate/issues/35) with
[Jacob Schreiber](http://homes.cs.washington.edu/~jmschr/) to get it
built on Ubuntu 14.10.

Prof. Ronojoy then explained Bayesian networks, and how the
probabilities can be fed to compute the probability for a
causal-effect relationship. He demonstrated [numerous
examples](https://github.com/ValueFromData/reasoning-under-uncertainty)
with live coding. His work is part of the [Value from
Data](http://valuefromdata.net/) initiative. Prof. Ronojoy and Dorai's
["Bayesian Inference and Data Analytics" video
lectures](https://www.youtube.com/playlist?list=PLhkiT_RYTEU1mkJHBUiA2Ze56h6z1uZgj)
are available in YouTube.

Day 2

After collecting my speaker kit, I attended the "KG award"
presentation, which was given to [Vijay Kumar](http://bravegnu.org/), followed by the
key note by [Dr. Ajit
Kumar](https://expeyes.wordpress.com/people/). Vijay Kumar is doing an
excellent job in organizing the monthly Chennaipy meet-ups. It was
also good to meet Dr. Ajit Kumar after many years, having known his
work and involvement with the Phoenix project.

During the tea break, I took the time to test my laptop with the
projector in auditorium-1, where I was scheduled to speak. I had few
demos in my talk, and I needed to set everything up before the
talk. After the tea break, I attended the session by [Pratap
Vardhan](https://github.com/pratapvardhan/) on ["Consuming Government
Data with Python and
D3"](https://in.pycon.org/cfp/pycon-india-2015/proposals/consuming-government-data-with-python-and-d3/). The
insights from the data and the analysis by Gramener were quite
interesting.

I then moved to the "dgplug" 1130 IST staircase meeting, where I met
more #dgplug students, and was able to connect IRC nicknames to
people's faces.  My suggestion to the students was to start to
identify their interests, and to specialize on them. Around noon, I
moved back to auditorium-1 to setup everything that I needed for my
talk - ["Pretty Printing in
Python"](https://in.pycon.org/cfp/pycon-india-2015/proposals/pretty-printing-in-python/). There
is one slot which I never wanted - the one before lunch because I know
that I will be the one standing between the audience and their lunch,
and that is exactly what I got. Nevertheless, the slot had to be taken
by someone, and I was prepared for it. I had requested the organizers
for a whiteboard and markers for my talk, and they had arranged for
the same. I had lot of material to cover in 45 minutes. I did rush
through, but, paused at times to explain the concepts. The audience
were quite interested in 3D printing, and they asked some really
interesting questions at the end of the talk. The video is available
in YouTube:

<iframe width="560" height="315" src="https://www.youtube.com/embed/9OxWgyjlDmQ?list=PL6GW05BfqWIe6rMoFFWmllPegB2gU069m" frameborder="0" allowfullscreen></iframe>

After a quick lunch, I headed for the lightning talks to present
["Nursery Rhymes"](https://github.com/shakthimaan/nursery-rhymes). We
are all familiar with nursery rhymes, and the idea is to teach
programming languages and software development using them. The
audience needed something to wake them after a heavy lunch, and they
thoroughly enjoyed the singing! The video link is provided below:

<iframe width="560" height="315" src="https://www.youtube.com/embed/KZUv5wCq6xE?list=PL6GW05BfqWIe6rMoFFWmllPegB2gU069m" frameborder="0" allowfullscreen></iframe>

I then attended the ["Machine learning techniques for building a large
scale production ready prediction system using
Python"](https://in.pycon.org/cfp/pycon-india-2015/proposals/building-a-large-scale-production-ready-prediction-system-using-python/)
by Arthi Venkataraman. After a short tea break, I attended the talk by
Pooja Salpekar on ["Test Driven Development with
Ansible"](https://in.pycon.org/cfp/pycon-india-2015/proposals/test-driven-development-with-ansible/). She
gave an overview of using [Serverspec](http://serverspec.org/). The
subject to test is still determined by the developer, and testing with
Vagrant is not the same as testing in production deployments. For the
last session of the day, I attended the ["Python and Riak DB, a
perfect couple for huge scale distributed
computing"](https://in.pycon.org/cfp/pycon-india-2015/proposals/python-and-riak-db-a-perfect-couple-for-huge-scale-distributed-computing/)
by Naren Arya, but, it was yet another generic talk.

In the evening, I participated in the discussion on building and
running communities organized by [Satyaakam
Goswami](http://satyaakam.net/) on how Python communities are
organizing themselves in India, and the challenges faced by them. I
shared my experience with the Chennaipy meet-ups, and insights from my
"i want 2 do project. tell me wat 2 do"
[presentation](http://shakthimaan.com/downloads.html#i-want-2-do-project-tell-me-wat-2-do)
and [book](http://shakthimaan.com/what-to-do.html). A video recording
of the discussion is available:

<iframe width="560" height="315" src="https://www.youtube.com/embed/IWHH6c3wZVQ?list=PL6GW05BfqWIe6rMoFFWmllPegB2gU069m" frameborder="0" allowfullscreen></iframe>

Day 3

[Nicholas H. Tollervey](http://ntoll.org/) began the day's proceedings
with his keynote. He shared his experience on Python and
education. The [Micro Python](https://micropython.org) and [BBC
micro:bit](https://www.microbit.co.uk/) projects are interesting
initiatives. The audience did have questions on why they cannot use
Android or any other mobile phone for Python development. But, I think
they missed the point that Open Hardware initiatives are important and
the openness is not the same with present day mobile devices. Nicholas
also covered important points on how to organize Python Dojo meet-ups,
and as a teacher, the experiences he shared were very useful.

After the tea break, I attended the session by [Sumith
Kulal](https://www.cse.iitb.ac.in/~sumith/) on ["SymEngine: The future
fast core of computer algebra
systems"](https://in.pycon.org/cfp/pycon-india-2015/proposals/symengine-the-future-fast-core-of-computer-algebra-systems/). It
was an introductory session on SymEngine, and he shared the roadmap
and plan that they have for the software. I was then networking with
people before attending ["How to build microservices using ZeroMQ and
WSGI"](https://in.pycon.org/cfp/pycon-india-2015/proposals/design-distributed-web-applications-using-zeromq/)
by Srinath G. S. It was yet another generic talk, and I headed for
early lunch.

The previous night I prepared for another lightning talk titled "Yet
Another Lightning Talk", to once again, wake the audience after their
heavy lunch. The talk went as I had planned, and the audience loved
it!  The video recording is available in YouTube:

<iframe width="560" height="315" src="https://www.youtube.com/embed/QmScfOsdxqg?list=PL6GW05BfqWIe6rMoFFWmllPegB2gU069m" frameborder="0" allowfullscreen></iframe>

A number of people approached me after the lightning talk,
appreciating the creativity, and I did make a lot of new friends. The
last talk of the day that I attended was on ["Python load balancer; 0
to 1 million requests per
second"](https://in.pycon.org/cfp/pycon-india-2015/proposals/python-load-balancer-0-to-1-million-requests-per-second/),
and yet again it turned out to be a a very generic talk, without going
into specifics. I left early to catch a flight.

It will be helpful if organizers can identify talks where the speaker
has contributed to the project. When you hire people for free and open
source software (F/OSS) work, you would like to see what their
contribution to the project is, and how they have added value to the
project. I am interested to hear on what the speaker has done. I can
always read the project web site, and find out more information about
the project. I wish all conferences add this criteria in their talk
selection process.

The workshops have been useful. The content of the talks need to be
improved though. I was able to meet fellow F/OSS developers during the
conference, and also made new friends. I am pleased that I was able to
deliver a talk, and two lightning talks.

I would like to thank [Manufacturing System
Insights](http://www.systeminsights.com/) for sponsoring my travel,
and allowing me to 3D print during office hours!
