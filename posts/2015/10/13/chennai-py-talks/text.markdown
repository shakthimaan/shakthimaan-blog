---
author: SK
tags: 2015, FOSS
timestamp: 13:00:00
title: Paho Python client for MQTT and G-code Visualization Talks, Chennaipy
---

I had given a couple of talks at the <a
href="http://chennaipy.org/">Chennaipy</a> meetups at the <a
href="https://www.imsc.res.in/">Institute of Mathematical
Sciences</a>, Chennai leading up to my talk at <a
href="https://in.pycon.org/2015/">PyCon India 2015</a>.

# August 2015 meetup

The first talk was titled "Paho Python client for MQTT" presented on
Saturday, August 22, 2015. The slides and notes for the presentation
are given below:

## MQTT Architecture

<img alt="pub-sub-model.png"
src="https://raw.githubusercontent.com/shakthimaan/paho-python-client-for-mqtt/master/pub-sub-model.png"></img>

<a
href="http://www.embedded.com/electronics-blogs/embedded-cloud-talkers/4397229/Device-to-Cloud--">Source:
Device to Cloud: MQTT and the power of topic notation</a>

## Topics

~~~~ {.shell}
$SYS/broker/load/connections
$SYS/broker/load/messages/received
$SYS/broker/load/messages/sent

home/living-room/temperature
home/kitchen/humidity
home/store-room/brightness
~~~~

## MQTT Protocol

<img alt="connect-flow.png"
src="https://raw.githubusercontent.com/shakthimaan/paho-python-client-for-mqtt/master/connect-flow.png"></img>

<a
href="http://www.hivemq.com/mqtt-essentials-part-3-client-broker-connection-establishment/">Source:
MQTT Essentials Part 3: Client, Broker and Connection
Establishment</a>

<img alt="subscribe_flow.png"
src="https://raw.githubusercontent.com/shakthimaan/paho-python-client-for-mqtt/master/subscribe_flow.png"></img>

<a
href="http://www.hivemq.com/mqtt-essentials-part-4-mqtt-publish-subscribe-unsubscribe/">Source:
MQTT Essentials Part 4: MQTT Publish, Subscribe & Unsubscribe</a>

## Mosquitto MQTT Installation

~~~~ {.shell}
$ sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa

  You are about to add the following PPA to your system:
 
  More info: https://launchpad.net/~mosquitto-dev/+archive/ubuntu/mosquitto-ppa
  Press [ENTER] to continue or ctrl-c to cancel adding it

  gpg: keyring `/tmp/tmpd67wf6/secring.gpg' created
  gpg: keyring `/tmp/tmpd67wf6/pubring.gpg' created
  gpg: requesting key 262C4500 from hkp server keyserver.ubuntu.com
  gpg: /tmp/tmpd67wf6/trustdb.gpg: trustdb created
  gpg: key 262C4500: public key "Launchpad mosquitto" imported
  gpg: Total number processed: 1
  gpg:               imported: 1  (RSA: 1)
  OK

$ apt-get update
$ apt-get install python-pip mosquitto mosquitto-clients
$ pip install paho-mqtt
~~~~

## Connectivity
### Start mosquitto server

~~~~ {.shell}
$ sudo /etc/init.d/mosquitto status
mosquitto stop/waiting

$ sudo /etc/init.d/mosquitto start
mosquitto start/running, process 11346
~~~~

### Verification

~~~~ {.shell}
$ netstat -na | grep :1883
tcp        0      0 0.0.0.0:1883            0.0.0.0:*               LISTEN     
tcp6       0      0 :::1883                 :::*                    LISTEN 

$ lsb_release -a
No LSB modules are available.
Distributor ID:	Ubuntu
Description:	Ubuntu 14.10
Release:	14.10
Codename:	utopic
~~~~

## Pub/Sub
### Subscribe

~~~~ {.shell}
$ mosquitto_sub -d -t hello/world

Client mosqsub/11678-achilles sending CONNECT
Client mosqsub/11678-achilles received CONNACK
Client mosqsub/11678-achilles sending SUBSCRIBE (Mid: 1, Topic: hello/world, QoS: 0)
Client mosqsub/11678-achilles received SUBACK
Subscribed (mid: 1): 0
~~~~

### Publish

~~~~ {.shell}
$ mosquitto_pub -d -t hello/world -m "Vanakkam"
Client mosqpub/11712-achilles sending CONNECT
Client mosqpub/11712-achilles received CONNACK
Client mosqpub/11712-achilles sending PUBLISH (d0, q0, r0, m1, 'hello/world', \
  ... (8 bytes))
Client mosqpub/11712-achilles sending DISCONNECT
~~~~

### Verification

~~~~ {.shell}
Client mosqsub/11678-achilles received PUBLISH (d0, q0, r0, m0, 'hello/world', \
  ... (8 bytes))
Vanakkam
~~~~


## Paho Python connectivity

~~~~ {.python}
  import paho.mqtt.client as mqtt

  def on_connect(client, userdata, flags, rc):
      print("Connected with result code "+str(rc))
      client.subscribe("hello/world")

  def on_message(client, userdata, msg):
      print(msg.topic+" "+str(msg.payload))

  client = mqtt.Client()
  client.on_connect = on_connect
  client.on_message = on_message

  client.connect("localhost", 1883, 60)

  client.loop_forever()
~~~~

~~~~ {.shell}
$ python client.py
~~~~

~~~~ {.shell}
$ mosquitto_pub -d -t hello/world -m "Hello, World"
~~~~

## iot.eclipse.org

~~~~ {.python}
  import paho.mqtt.client as mqtt

  def on_connect(client, userdata, flags, rc):
      print("Connected with result code "+str(rc))
      client.subscribe("$SYS/#")

  def on_message(client, userdata, msg):
      print(msg.topic+" "+str(msg.payload))

  client = mqtt.Client()
  client.on_connect = on_connect
  client.on_message = on_message

  client.connect("iot.eclipse.org", 1883, 60)

  client.loop_forever()
~~~~

## Source Code

~~~~ {.shell}
  paho-mqtt-1.1:
  -rw-rw-r-- 1 shakthi shakthi  2031 Feb  1  2015 about.html
  -rw-rw-r-- 1 shakthi shakthi  3736 Feb  1  2015 CONTRIBUTING.md
  -rw-rw-r-- 1 shakthi shakthi  1569 Feb  1  2015 edl-v10
  -rw-rw-r-- 1 shakthi shakthi 11695 Feb  1  2015 epl-v10
  drwxrwxr-x 2 shakthi shakthi  4096 Feb  1  2015 examples
  -rw-rw-r-- 1 shakthi shakthi   156 Feb  1  2015 LICENSE.txt
  -rw-rw-r-- 1 shakthi shakthi  9230 Feb  1  2015 notice.html
  -rw-rw-r-- 1 shakthi shakthi   814 Feb  1  2015 PKG-INFO
  -rw-rw-r-- 1 shakthi shakthi 30871 Feb  1  2015 README.rst
  -rw-rw-r-- 1 shakthi shakthi   972 Feb  1  2015 setup.py
  drwxrwxr-x 3 shakthi shakthi  4096 Feb  1  2015 src
~~~~

## Embedded

<img
src="https://raw.githubusercontent.com/shakthimaan/paho-python-client-for-mqtt/master/raspberry-small.jpg"
alt="raspberry-small.jpg"></img><img
src="https://raw.githubusercontent.com/shakthimaan/paho-python-client-for-mqtt/master/3g.png"
alt="3g.png"></img><img
src="https://raw.githubusercontent.com/shakthimaan/paho-python-client-for-mqtt/master/rfid.png"
alt="rfid.png"></img>

<img
src="https://raw.githubusercontent.com/shakthimaan/paho-python-client-for-mqtt/master/arduino.jpg"
alt="arduino.jpg"></img><img
src="https://raw.githubusercontent.com/shakthimaan/paho-python-client-for-mqtt/master/radiation.png"
alt="radiation.png"></img><img
src="https://raw.githubusercontent.com/shakthimaan/paho-python-client-for-mqtt/master/bluetooth.png"
alt="bluetooth.png"></img>

<a
href="https://www.cooking-hacks.com/documentation/tutorials/raspberry-pi-to-arduino-shields-connection-bridge">Source:
Raspberry Pi to Arduino</a>

## References

* <a href="mqtt.org">mqtt.org</a>
* <a href="hivemq.com">hivemq.com</a>
* <a href="mosquitto.org">mosquitto.org</a>
* <a href="https://www.arduino.cc/">www.arduino.cc</a>
* <a href="https://www.raspberrypi.org">www.raspberrypi.org</a>
* <a href="cooking-hacks.com">cooking-hacks.com</a>
* <a href="https://pypi.python.org/pypi/paho-mqtt">pypi.python.org/pypi/paho-mqtt</a>
* <a href="http://iot.eclipse.org/">iot.eclipse.org</a>
* <a href="https://github.com/eschulte/epresent">github.com/eschulte/epresent</a>

# September 2015 meetup

The September 2015 talk was scheduled on Saturday, September 26, 2015
and I gave a talk on "G-code Visualization Tools". The talk began with
an introduction and history of G-codes. A Maya pyramid STL file was
then used with Pronterface to generate the following G-codes:

~~~~ {.shell}
G90         ; Set to Absolute Positioning
G21         ; Set Units to Millimeters
M103        ; Turn off extruder
M105        ; Get extruder temperature
M106        ; Fan (cooling) on
M140 S60.0  ; Set Bed temperature
M141 S30.0  ; Set Chamber temperature
M142 S0.0   ; Holding pressure
M113 S1.0   ; Set Extruder PWM
M108 S210.0 ; Set Extruder Speed
M104 S200.0 ; Set Extruder Temperature
G1 X-54.72 Y-53.352 Z0.72 F60.0 ; G1 Move, F Feedrate, mm/rev
M101        ; Turn extruder 1 on
G1 X-54.72 Y53.352 Z0.72 F240.0
G1 X-51.84 Y53.352 Z0.72 F240.0
...
~~~~

I then gave a demo of "Yet Another GCode Viewer" written by Jonathan
Winterflood that uses the Pyglet library to render the G-code
paths. You can also zoom, rotate and scale the image after it has
loaded the input data. The brim that is printed for 3D objects is
clearly visible in the rendition. A screenshot is shown below:

<img src="http://shakthimaan.com/images/chennaipy-2015/yagv.png" alt="YAGV"></img>

Blender is a free and open source software for computer graphics, 3D
modelling and animations. It is written in C, C++ and Python. A
Blender gcode reader plugin written by Simon Kirkby exists to
visualize G-codes. This plugin generates the image from the G-codes,
and converts the change in paths into arcs for a smoother image. The
advantage of this plugin is that you can use all the goodies from
Blender and Python. A screenshot of the pyramid using the Blender
plugin is shown below:

<img
src="http://shakthimaan.com/images/chennaipy-2015/gcode-blender.png"
alt="G-code Blender plugin"></img>

The Visualization Toolkit (VTK) is yet another free and open source
software for image processing and visualization. It requires its input
file in a VTK file format. After using the gcode2vtk utility to
convert the Maya pyramid G-code file to a VTK file format, I was able
to demonstrate the G-code path simulation using ParaView - a data
analysis and visualization application. An illustration of the Maya
pyramid in ParaView is shown below:

<img
src="http://shakthimaan.com/images/chennaipy-2015/gcode-paraview.png"
alt="G-code ParaView"></img>

ParaView is written in C, C++, Fortran and Python. Both Blender and
ParaView allow you to run simulations on the G-codes.

All the visualization tools discussed are free and open source
software, and you can install them on your favourite *nix system.
