---
author: SK
tags: 2015, documentation
timestamp: 16:45:00
title: International edition - i want 2 do project. tell me wat 2 do
---

This year, 2015, marks a decade of completion of my website
shakthimaan.com! Thanks to the grace of God, the Almighty. Thanks also
to my parents, friends and well-wishers for their wonderful support
and encouragement over the years.

I am also happy to announce the launch of the international hard cover
edition of my book, "i want 2 do project. tell me wat 2 do", for
worldwide distribution. You can order the book at:

* Amazon.com [http://www.amazon.com/want-project-tell-wat-do/dp/9351967387](http://www.amazon.com/want-project-tell-wat-do/dp/9351967387)

* Dogears Etc. [http://www.dogearsetc.com/books/I-Want-To-Do-A-Project.-Tell-Me-What-To-Do./38632](http://www.dogearsetc.com/books/I-Want-To-Do-A-Project.-Tell-Me-What-To-Do./38632)

* Infibeam [http://www.infibeam.com/Books/i-want-do-project-tell-me-what-do-shakthi-kannan/9789351967835.html](http://www.infibeam.com/Books/i-want-do-project-tell-me-what-do-shakthi-kannan/9789351967835.html)


![](http://shakthimaan.com/images/books/hard-soft-cover-books.png "Soft and Hard cover editions")

The topics covered in the book include:

* Mailing List Guidelines
* Attention to Detail
* Project Communication
* Project Guidelines
* Development Guidelines
* Methodology of Work
* Tools
* Reading and Writing
* Art of Making Presentations
* Sustenance

The "Mailing List Guidelines" sample chapter is available for download from:

  [http://shakthimaan.com/downloads/book/chapter1.pdf](http://shakthimaan.com/downloads/book/chapter1.pdf)

The home page for the book is at:

  [http://www.shakthimaan.com/what-to-do.html](http://www.shakthimaan.com/what-to-do.html)

Kindly forward this information to your friends who may also benefit
from the same when working with free and open source software.
