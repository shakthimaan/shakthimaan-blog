---
author: SK
tags: 2015, foss, documentation
timestamp: 16:00:00
title: PostgreSQL Quick Reference
---

------------------------------------------------------------------------------------
Command                                                        Functionality
-------                                                        ---------------------
$ sudo /etc/init.d/postgresql start                            Start server (Ubuntu)

$ psql -U postgres                                             Connect

postgres=# \\l                                                 Show databases

postgres=# \\h <command>                                       Help

postgres=# CREATE DATABASE jerry;                              Create database               

postgres=# DROP DATABASE jerry;                                Delete database              

postgres=# SET search_path TO schema;                          Use schema              

$ psql -U postgres -d <database>                               Use database

postgres=# \\c test                                            Change database              

postgres=# \\du                                                List users

postgres=# \\d                                                 List tables

postgres=# CREATE SCHEMA sausalito;                            Create schema

postgres=# \\dn                                                List schema

postgres=# DROP SCHEMA sausalito;                              Drop schema

postgres=# SELECT * FROM sausalito.employees;                  Select rows

postgres=# CREATE TABLE sausalito.employees (id INT);          Create table

postgres=# INSERT INTO sausalito.employees VALUES (1);         Insert record

postgres=# UPDATE sausalito.employees SET id = 4 WHERE id = 2; Update table record

postgres=# DELETE FROM sausalito.employees WHERE id = 3;       Delete record

postgres=# DROP TABLE sausalito.employees;                     Drop table

postgres=# \\q                                                  Quit from session
------------------------------------------------------------------------------------
