---
author: SK
tags: 2006, FOSS
timestamp: 10:00:00
title: FLOSS workshop, Arunai Engineering College, Tamil Nadu
---

The photos (more than 120) taken during the two-day FOSS Exhibit held
on March 25-26, 2006 organized by ILUGC, and NRC-FOSS at <a
href="http://www.arunai.org/">Arunai Engineering College,
Thiruvannamalai, Tamil Nadu</a> are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album26">/gallery</a>.

<img alt="Arunai gateway"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album26/106_arunai_gw.jpg
"></img><br />

I would like to thank the management, faculty of Arunai Engineering
College (AEC) for their wonderful support. My hearty congratulations to the
students for their valiant efforts in this event.

Special thanks to our fellow <a href="http://ilugc.in/">ILUGC</a> and
<a href="http://nrcfoss.org.in">NRC-FOSS</a> folks, and Dr. Srinivasan
for taking the initiative. We had a very good time :)

A brief summary of the events and the fun we had:

# Day I : Saturday, March 25, 2006 #

Team I began on Saturday, March 25, 2006 morning by 0500 IST. I was
picked up first, then Suman (Triplicane), Dr. Srinivasan (Saidapet),
Aanjhan and Parthan (Kathibara junction), Raman and Bhaskar
(Tambaram). Aanjhan came by bus from Bangalore to Chennai, the
previous night, and joined Parthan in the morning.

We did break for breakfast. We got back in the Toyota Qualis after
breakfast and were about to leave when we realized that Suman was not
there. We found him in the hotel.

We arrived at around 0830 IST in the morning. After a formal
inauguration, Raman began the day's proceedings with introduction to
FLOSS, followed by Fedora Core installation. Team 2a arrived and were
introduced to the audience. They moved to the labs to train the
students. After installation demo, I discussed about career
opportunities with FLOSS.  Meanwhile Team 2b, Asokan and Thyagurajan,
had arrived and introduced themselves. Suman gave an introduction on
Qt programming. We had lunch between 1300-1400.

Post-lunch session began with an excellent presentation by Asokan
Pichai on shell scripting and the power of scripting. Thyagurajan gave
a talk on GLAMP after Asokan's talk. Tea break was at 1530 IST. Two
folks from Sun Microsystems, Bangalore had come, Arindam and
Moinak. So we had to give them some time for a session. They gave a
demo of Opensolaris and wanted more developers for the same.

Aanjhan spoke on how students can contribute to FLOSS. At 1700 we
moved to the labs to help the other teams who were training
students. We finally left at 1900 to the hotel. When we reached the
hotel, one of the students told us that we had left someone behind.
Guess who? Suman. Nevertheless, he was dropped to the hotel.

We had good dinner, and in Room #203, Saravanan, Raman, Parthan and
myself had good discussions about FLOSS and ILUGC activities. We hit
the sack only at midnight.

# Day II, Sunday, March 26, 2006 #

Many folks visited the temple in the morning, and then went to the
college to help in the stalls.  Preparations were in full-swing with
students helping in setting up the stalls, putting up banners,
etc. Team III arrived in the morning.

The auditorium session began around 1000. Audience were school
students, college students, and the general public. I introduced them
to FLOSS, showed the *nix desktop environment, software that they can
use with GNU/Linux etc.

The chief guest, M. R. Rajagopalan, Director, C-DAC, Chennai
inaugurated the stalls. They along with the college management and
administration folks then came to the auditorium for a quick
inauguration session. Lunch was at 1330. The stalls were bustling with
activity.

<img alt="Stalls"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album26/95_stalls.jpg
"></img><br />

We resumed sessions in the auditorium at 1500 and Moinak introduced
folks on Bellenix and OpenSolaris.  Arindam then began talking about
core kernel hacking to newbies, and continued on and on and on till
1700, and most of them left for the stalls. I was happy they moved to
the stalls as they were to close by 1700. We didn't have time to play
The.Code.linux or have a Q&A discussion session (interactive, like in
day one).

There was a big thanks-giving at the end of the day to all volunteers,
and we departed at 1800.

Overall, I was overwhelmed by the enthusiasm of the AEC students, and
given a chance, I would go there again.
