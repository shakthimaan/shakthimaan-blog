---
author: SK
tags: 2006, FOSS
timestamp: 10:00:00
title: Free Software workshop, FISAT, Kochi
---

The final day of "Bharatham 2006" on Saturday March 12, 2006 at <a
href="http://www.fisat.ac.in/">Federal Institute of Science and
Technology, Angamaly, Kochi, Kerala</a> was cancelled due to a fatal
accident, the previous night, to students from a nearby college. After
the cultural festival on Friday night, the students had left in their
car at around 2230 IST. They didn't have control of the car at one
turning near a small bridge, skidded across, and drowned in the
river. The doors were locked and had got jammed. Out of five students,
only one managed to escape. A tragedy to all students, faculty,
friends in and around Angamaly.

Since I had come all the way, they just wanted me to conduct an
informal session to the faculty and few students who were there. I
spoke on Free Software and GNU/Linux. As a mark of respect to the loss
of life, I didn't take any photographs.

I spent the remaining time with the students there, and got to know
more about Free Software activities in Kerala, right from Calicut, to
Palghat, Trichur, Angamaly, Kochin, Kollam, Thiruvananthapuram.

<a href="http://pramode.net/">Pramode</a> is the inspiration in
Government Engineering College, Trichur and other places in Kerala.

<a href="http://space-kerala.org/">Space Kerala</a> promote Free
Software and do good work in promoting the same to schools, colleges,
NGOs throughout Kerala.

Thanks to Sumod (final year, ICE) for coordinating with me for the
past three weeks, and for the hospitality offered.

We want to collaborate more with our folks in Kerala and will organize
more workshops in future.
