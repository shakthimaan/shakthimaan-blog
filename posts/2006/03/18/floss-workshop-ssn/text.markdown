---
author: SK
tags: 2006, FOSS
timestamp: 10:00:00
title: FLOSS workshop, SSN College of Engineering
---

The photos taken during the "Introduction to FLOSS" workshop at <a
href="http://www.ssn.edu.in/">SSN College of Engineering, SSN Nagar,
Tamil Nadu, India</a>, today, Saturday, March 18, 2006 are available
in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album25">/gallery</a>.

<img alt="laptops"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album25/6_ibmr_and_my_t41.jpg
"></img><br />

Thanks to the Management, SSN College of Engineering, Dr. Aravindhan
(HOD, CSD), Prof. Balasubramanian (Lecturer, CSD), and to students and
faculty/staff of SSN College of Engineering. Thanks also to Naren who
has been coordinating this event for the past three weeks.

Vijay Kumar began the day's proceedings with an introduction to Free
Software. I then conducted the <a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">"Careers
with GNU/Linux"</a> presentation. Joe Steeve handled installation of
Fedora Core. We had lunch between 1130 and 1230 IST. Ashwin began the
afternoon session with his talk on <a
href="http://www.shakthimaan.com/downloads/glv/presentations/ssn.ashwin.mar.18.2006.pdf">"Squid,
GLAMP, ntop"</a>. Vijay then gave an introduction to GUI programming
with GTK. Sujith, from <a href="http://nrcfoss.org.in/">NRC-FOSS</a>,
followed with his presentation on Scilab and GNU Octave. I then gave a
demo of "Multimedia on GNU/Linux" and played the the demo video of
xgl/opengl.

The audience were from different departments, and we had good
interactive sessions. SSN folks are very keen on *nix environments,
and are interested in many follow-up sessions. Suggestions were
provided to start a user group within campus so students can
participate.

Thanks to Vijay Kumar, Joe Steeve, Naren, Ashwin, Sujith for their
company and participation. I had a great time!
