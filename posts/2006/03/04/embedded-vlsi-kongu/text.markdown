---
author: SK
tags: 2006, FOSS, vhdl, vlsi
timestamp: 10:00:00
title: Embedded/VLSI workshop, Kongu Engineering College, Erode
---

I would like to thank Kongu LUG, management, Prof. Jayapathi, HOD, CSE
department, staff and students of <a
href="http://www.kongu.ac.in/">Kongu Engineering College</a> (KEC) for
the opportunity to conduct a one-day "GNU Embedded/VLSI" workshop at
Kongu Engineering College, Perundurai, Erode on Saturday, March 4,
2006.

<img alt="Embedded demo"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album24/5_embedded_demo.jpg
"></img><br />

The <a href="http://shakthimaan.com/downloads.html#alliance">VHDL code
examples</a> and <a
href="http://shakthimaan.com/downloads.html#embedded-gnu-linux-labs-howto">Embedded
HOWTO</a> presentations are available.

I spent about an hour and a half with the students in the morning in
their RMS laboratory (0900 to 1030 IST). The morning session was on
"Embedded GNU/Linux labs HOWTO" (1030 to 1230 IST). Lunch was between
1230 and 1400 IST. The afternoon session was on "Alliance VLSI CAD
tools" (1400 to 1600 IST).

KEC have started an FM studio (90.4 MHz) for listeners in and around
(10 km radius) Erode. So, they caught me by surprise and conducted an
interview. The questions were on Free Software, higher education,
GNU/Linux community work etc. They were using soundforge as suggested
by the management. I had told them about Audacity and how I use it on
GNU/Linux for sound recording/editing etc. I have asked for a copy of
the same, if possible, so I can upload it. It should be aired on March
13, 2006, sometime between 1630 to 1930 IST in Erode.

<img alt="Interview"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album24/15_interview_in_progress.jpg
"></img><br />

Special thanks to Vijay Anand, final year, CSD for working with me for
the past couple of weeks in coordinating the event.

The photos taken during the presentation can be viewed from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album24">/gallery</a>.
