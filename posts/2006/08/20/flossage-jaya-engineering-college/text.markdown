---
author: SK
tags: 2006, FOSS
timestamp: 10:00:00
title: FLOSSAGE, Jaya Engineering College
---

The photos taken during "FLOSSAGE 2006", between August 19-20, 2006 at
<a href="http://www.jec.ac.in/">Jaya Engineering College,
Thiruninravur, Tamil Nadu, India</a> can be viewed from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album35">/gallery</a>.

<img alt="Audience"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album35/4_audience.jpg"></img><br
/>

I shall make a CD copy of the photos (high resolution) and send it to
Harish (Secretary, Jaya Engineering College GNU/Linux Campus
Club). Thanks to Harish for coordinating and helping us around. I must
congratulate Prof. Kumaran for doing a very good job in organizing the
whole event. Thanks also to the management, faculty, students of Jaya
Engineering college for the wonderful hospitality offered.

Students were very enthusiastic about FLOSS. It was something very new
to them, and they were very eager to learn it and try it. I was
particulary impressed by the 'Blender team' who picked it up so
easily, and were doing cool animations! If we can continue to guide
them, they will definitely make good contributions to FLOSS.

Thanks to the other <a href="http://ilugc.in">ILUGC</a>, <a
href="http://nrcfoss.org.in">NRCFOSS</a> volunteers too. I had a great
time!
