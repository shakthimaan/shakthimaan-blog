---
author: SK
tags: 2006, FOSS
timestamp: 10:00:00
title: FLOSS training, Jaya Engineering College
---

Members of <a href="http://ilugc.in">ILUGC</a>, <a
href="http://nrcfoss.org.in">NRC-FOSS</a> conducted a FLOSS training
session to Jaya Engineering College (Thiruninravur) students, today,
Sunday, August 13, 2006.

<img alt="Lab session in progress"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album34/11_training_in_progress.jpg"></img><br
/>

The photos taken during the session can be viewed from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album34">/gallery</a>.

Please contact Raman P (raamanp at yahoo.co.in), if you are interested
in volunteering for the LDD at Jaya Engineering College, on August
19-20, 2006.
