---
author: SK
tags: 2006, FOSS
timestamp: 10:00:00
title: FLOSS workshop, Karpagam Arts and Science College
---

I conducted a full-day "Introduction to Free/Libre Open Source
Software" workshop on Saturday, August 5, 2006 at <a
href="http://www.karpagamuniv.com">Karpagam Arts and Science College,
Coimbatore, Tamil Nadu, India</a>.

<img alt="Revolution OS movie"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album33/8_rms_rev_os.jpg"></img><br
/>

I covered <a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">career
opportunities with FLOSS</a>, Free/Libre Open Source Software that is
available for students/engineers, the GNU/Linux desktop, localization
with GNU/Linux, and played the movie, <a
href="http://www.revolution-os.com/">"The Revolution OS"</a>.

The students were from the CS and MCA department. Usually, I prefer to
speak in Tamil within Tamil Nadu, but, because the audience comprised
of people from Tamil Nadu and Kerala, English was preferred. The
college has recently received autonomous status. I have given <a
href="http://www.au-kbc.org/">AU-KBC</a> contacts to them to take up
FLOSS projects, and to start taking FLOSS initiatives to serve the
community in and around Coimbatore.

They _will_ be starting a GNU/Linux Campus Club/User Group on campus,
soon. They do require technical support and guidance (initially,
atleast) from <a href="http://ilugc.in">ILUGC</a>, AU-KBC members for
FLOSS work.

I would like to thank Prof. E. Karthikeyan, Senior Lecturer, Computer
Science Department to have arranged for the workshop in one week's
time, and for the wonderful hospitality offered.

You can check out the photos from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album33">/gallery</a>.
