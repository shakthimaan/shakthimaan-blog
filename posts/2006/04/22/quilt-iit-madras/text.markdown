---
author: SK
tags: 2006, FOSS
timestamp: 10:00:00
title: Introduction to Quilt, IIT, Madras
---

I gave an introductory session on <a
href="http://savannah.nongnu.org/projects/quilt">Quilt</a> on
Saturday, April 22, 2006 at 1500 IST at CSD, #320, <a
href="http://www.iitm.ac.in/">IIT, Madras</a>.

Quilt is a tool to manage patches in project code
development/documentation. The tutorial session was filled with
illustrative examples on how to use quilt. The <a
href="http://shakthimaan.com/downloads.html#quilt-tutorial">tutorial</a>
and the code samples that I had written are released under the GNU
Free Documentation License.

Thanks to Ashok Raj (Intel), Thomas Petazzoni (Enix), and Jordan (AMD)
for their feedback regarding the tutorial.
