---
author: SK
tags: 2006, FOSS
timestamp: 10:00:00
title: FLOSS workshop, MNM Jain Engineering College, Thorappakkam
---

I conducted an "Introduction to FLOSS" workshop today, Monday, July
31, 2006 at <a href="http://www.mnmjec.ac.in/">MNM Jain Engineering
College, Thorappakkam, Tamil Nadu, India</a>.

I would like to thank Prashant Mohan (<a
href="http://ilugc.in">ILUGC</a>), Vidhya Shankar (student), Assitant
Professor Muthu Sundar (Assistant Professor, CSE) for taking the
initiative.

I discussed <a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">career
opporunities with FLOSS</a>, introduced the GNU/Linux desktop, and
played the movie, <a href="http://www.revolution-os.com/">"The
Revolution OS"</a>.

IBM has started doing some good projects using GNU/Linux with the
college.

Prof. Muthu Sundar is already in touch with <a
href="http://nrcfoss.org.in">NRCFOSS</a>, and is planning to get more
students to work on FLOSS projects with them.
