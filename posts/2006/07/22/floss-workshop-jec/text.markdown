---
author: SK
tags: 2006, FOSS
timestamp: 10:00:00
title: FLOSS workshop, Jaya Engineering College, Thiruninravur
---

The photos taken during the "Introduction to FLOSS" workshop held
today, Saturday, July 22, 2006 at <a href="http://www.jec.ac.in/">Jaya
Engineering College, Thiruninravur, Tamil Nadu, India</a> can be
viewed from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album32">/gallery</a>.

<img alt="Session in progress"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album32/5_session_in_progress.jpg
"></img><br />

It was an introductory session with talks on <a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">career
opportunities with FLOSS</a>, Free/Libre Open Source Software, and the
GNU/Linux desktop.

I would like to thank Prof. Kumaran, HOD, CSD for taking the
initiative and following it up. Thanks also to Harish for his
correspondence and help.

There were about 200 students during the workshop, and they are very
eager to *learn* using FLOSS. They already have a FLOSS lab setup. The
<a href="http://ilugc.in">ILUGC</a> mailing list has been provided to
them for technical support.

They are planning for an LDD for August 18, 19, 2006 (Friday,
Saturday) - will be confirmed by them. It will be open to the public
and will be primarily useful to the schools, colleges in and around
Thiruninravur, and Chennai.

To those new colleges/schools who haven't had a taste of FLOSS, I
shall provide them with the contact of <a
href="http://nrcfoss.org.in">NRCFOSS</a>, so they can take it further
- with support from ILUGC too.