---
author: SK
tags: 2006, FOSS, vlsi
timestamp: 10:00:00
title: Embedded/VLSI workshop, IIT, Madras
---

The code examples and presentation slides used during the GNU
Embedded/VLSI workshop on Saturday, February 25, 2006 at Central
Lecture Theater, IIT-M are available at <a
href="http://shakthimaan.com/downloads.html#embedded-gnu-linux-labs-howto">Embedded
HOWTO</a>.

<img alt="On stage"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album23/6_myself_intro.jpg
"></img><br />

I would like to thank Dr. Nitin, EE, IIT-M for working
with us for the past one month in planning this event,
and for providing us with the needed support.

Thanks to Kumar Appaiah for coordinating the event. Thanks also to
Aanjhan and Bharathi for their talks and presence.

The photos taken can be viewed from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album23">/gallery</a>.
