---
author: SK
tags: 2006, FOSS
timestamp: 10:00:00
title: Bioinformatics FLOSS workshop, Karpagam Arts and Science College
---

Deepan Chakravarthy and myself conducted a one-day workshop for the
Bioinformatics department, <a
href="http://www.karpagamuniv.com/">Karpagam Arts and Science College,
Coimbatore, Tamil Nadu, India</a> on Saturday, September 9, 2006.

I introduced them to FLOSS, and then Deepan spoke on how FLOSS is
useful in bioinformatics. He showed a demo of <a
href="http://emboss.sourceforge.net/">Emboss</a>, and lot of other
applications. After lunch, I gave a demo of the GNU/Linux desktop
environment, and also explained basic commands that they can use in
the console environment. Deepan then continued with his demo of
bioinformatics applications and discussed about FLOSS projects at <a
href="http://bioinformatics.org">bioinformatics.org</a>.

The students/faculty were really enthusiastic, and wanted follow-up
sessions. I emphasized that they should get started with setting up a
FLOSS lab, and a GNU/Linux user group on-campus. They are ready to
provide facilities, stay, food even if you want to spend a week there,
and give them hands-on training.  There are no distractions until
October 5, 2006, after which they have model exams, followed by
holidays and exams.

I would like to thank Prof. Hemalatha for working with us for the past
couple of weeks in coordinating and organizing this event. Special
thanks to the department faculty and management for the facilities and
hospitality offered.