---
author: SK
tags: 2019, workshop
timestamp: 11:30:00
title: Git workshop, R V College of Engineering, Bengaluru
---

I had organized a <a href="https://www.git-scm.com/">Git</a> workshop for students who are pursuing their Master of Computer Applications (MCA) from in and around Bengaluru at <a href="https://www.rvce.edu.in/">R V College of Engineering</a>. I am sharing their anonymous feedback:

<img alt="Doubts solved" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065147-b4b78b0f.jpg" />

<img alt="Interesting session" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065148-644e6972.jpg">

<img alt="Useful for beginners" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065149-77c3f7fa.jpg">

<img alt="Helpful for final year project" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065150-41f7cb66.jpg">

<img alt="Great session" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065151-5aa73b38.jpg">

<img alt="Good training" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065152-9f79626b.jpg">

<img alt="Very good" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065153-71463050.jpg">

<img alt="Informative session" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065154-9af6af01.jpg">

<img alt="Teaching with patience" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065155-bed9a8e0.jpg">

<img alt="Way of teaching" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065156-217f1b12.jpg">

<img alt="Concepts of Git" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065156-b21dd863.jpg">

<img alt="Debugging errors" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065157-a0c20c2d.jpg">

<img alt="Way of solving doubts" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065158-0c85968e.jpg">

<img alt="Learn about Git" src="https://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/09/29/20190929065159-74eacdec.jpg">

