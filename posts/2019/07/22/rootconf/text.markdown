---
author: SK
tags: 2019, workshop
timestamp: 15:00:00
title: Aerospike Wireshark Lua plugin workshop, Rootconf 2019, Bengaluru
---

[Rootconf 2019](https://rootconf.in/2019/) was held on June 21-22, 2019 at NIMHANS Convention
Centre, in Bengaluru on topics ranging from infrastructure security,
site reliability engineering, DevOps and distributed systems.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/hasgeek/48125351246/in/album-72157709244493203/" title="Rootconf 2019 Day 1"><img src="https://live.staticflickr.com/65535/48125351246_9c93cccc35_z.jpg" width="640" height="427" alt="Rootconf 2019 Day 1"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

# Day I

I had proposed a workshop titled ["Shooting the trouble down to the
Wireshark Lua
Plugin"](https://hasgeek.com/rootconf/2019/proposals/shooting-the-trouble-down-to-the-wireshark-lua-plu-78tKrEaA8wCRiiQorcgtWd)
for the event, and it was selected. I have been working on the
["Aerospike Wireshark Lua
plugin"](https://github.com/aerospike/aerospike-wireshark-plugin) for
dissecting Aerospike protocols, and hence I wanted to share the
insights on the same. The plugin source code is released under the
AGPLv3 license.

["Wireshark"](https://www.wireshark.org/) is a popular Free/Libre and
Open Source Software protocol analyzer for analyzing protocols and
troubleshooting networks. The ["Lua programming
language"](https://www.lua.org/) is useful to extend C projects to
allow developers to do scripting. Since Wireshark is written in C, the
plugin extension is provided by Lua. Aerospike uses the PAXOS family
and custom built protocols for distributed database operations, and
the plugin has been quite useful for packet dissection, and solving
customer issues.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/hasgeek/48125457642/" title="Rootconf 2019 Day 1"><img src="https://live.staticflickr.com/65535/48125457642_b785f3da76_z.jpg" width="640" height="427" alt="Rootconf 2019 Day 1"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

The workshop had both theory and lab exercises. I began with an
overview of Lua, Wireshark GUI, and the essential Wireshark Lua
interfaces. The Aerospike Info protocol was chosen and exercises were
given to dissect the version, type and size fields. I finished the
session with real-world examples, future work and references. Around
50 participants attended the workshop, and those who had laptops were
able to work on the exercises. The workshop presentation and lab
exercises are available in the
[aerospike-wireshark-plugin/docs/workshop](https://github.com/aerospike/aerospike-wireshark-plugin/tree/master/docs/workshop)
GitHub repository.

I had follow-up discussions with the participants before moving to the
main auditorium. ["Using pod security policies to harden your
Kubernetes
cluster"](https://hasgeek.com/rootconf/2019/proposals/using-pod-security-policies-to-harden-your-kuberne-DdDFAC3Yo99YbjjoX6jEAG)
by [Suraj Deshmukh](https://suraj.io/) was an interesting talk on the
level of security that should be employed with containers. After
lunch, I started my role as emcee in the main auditorium.

The keynote of the day was by [Bernd Erk](http://www.gethash.org/),
the CEO at Netways GmbH, who is also the co-founder of the Icinga
project. He gave an excellent talk on ["How convenience is killing
open
standards"](https://hasgeek.com/rootconf/2019/proposals/how-convenience-is-killing-open-standards-rW23MHu8VaVkgiekhAdKJj). He
gave numerous examples on how people are not aware of open standards,
and take proprietary systems for granted. This was followed by flash
talks from the audience. Jaskaran Narula then spoke on ["Securing
infrastructure with OpenScap: the automation
way"](https://hasgeek.com/rootconf/2019/proposals/securing-infrastructure-with-openscap-the-automati-6oAnRM3CZQJV5GUpSLL7Dk),
and also shared a demo of the same.

After the tea break, Shubham Mittal gave a talk on ["OSINT for
Proactive
Defense"](https://hasgeek.com/rootconf/2019/proposals/osint-for-proactive-defense-KnwMGWp8KBuuQ4ZLD95RcJ)
in which he shared the Open Source Intelligence (OSINT) tools,
techniques and procedures to protect the perimeter security for an
organization. The last talk of the day was by Shadab Siddiqui on
["Running a successful bug bounty programme in your
organization"](https://hasgeek.com/rootconf/2019/proposals/devil-lies-in-the-details-running-a-successful-bug-XooB33RJJKQqNrYLpVHCHa).

# Day II

[Anant
Shrivastava](https://anantshri.info/) started the day's proceedings
with a recap on the talks from day one.

The first talk of the day was by Jiten Vaidya, co-founder and CEO at
Planetscale who spoke on ["OLTP or OLAP: why not
both?"](https://hasgeek.com/rootconf/2019/proposals/oltp-or-olap-why-not-both-hScyhPzGitYkbiybShtK3U). He
gave an architectural overview of vitess.io, a Free/Libre and Open
Source sharding middleware for running OLTP workloads. The design
looked like they were implementing the Kubernetes features on a MySQL
cluster. [Ratnadeep Debnath](https://github.com/rtnpro) then spoke on
["Scaling MySQL beyond limits with
ProxySQL"](https://hasgeek.com/rootconf/2019/proposals/scale-mysql-beyond-limits-with-proxysql-aZUmJCNCScSdwYCK4k8j4V).

After the morning break, [Brian
McKenna](https://brianmckenna.org/blog/) gave an excellent talk on
["Functional programming and Nix for reproducible, immutable
infrastructure"](https://hasgeek.com/rootconf/2019/proposals/functional-programming-and-nix-for-reproducible-im-o2UxJAGjfy8evQdGA96uj3). I
have listened to his talks at the Functional Programming conference in
Bengaluru, and they have been using Nix in production. The language
constructs and cases were well demonstrated with examples. This was
followed by yet another excellent talk by Piyush Verma on
["Software/Site Reliability of Distributed
Systems"](https://hasgeek.com/rootconf/2019/proposals/software-site-reliability-of-distributed-systems-jUMKh38AaeCf6Xb2xV8HuN). He
took a very simple request-response example, and incorporated site
reliability features, and showed how complex things are today. All the
major issues, pitfalls, and troubles were clearly explained with
beautiful illustrations.

Aaditya Talwai presented his talk on ["Virtuous Cycles: Enabling SRE
via automated feedback
loops"](https://hasgeek.com/rootconf/2019/proposals/virtuous-cycles-enabling-sre-via-automated-feedbac-qkNJRcuSp9pbRos44VT2tH)
after the lunch break. This was followed by Vivek Sridhar's talk on
["Virtual nodes to auto-scale applications on
Kubernetes"](https://hasgeek.com/rootconf/2019/proposals/virtual-nodes-to-auto-scale-applications-on-kubern-aRmD3838atCTXi7HqY7nRC). Microsoft
has been investing heavily on Free/Libre and Open Source, and have
been hiring a lot of Python developers as well. Satya Nadella has been
bringing in lot of changes, and it will be interesting to see their
long-term progress. After Vivek's talk, we had few slots for flash
talks from the audience, and then Deepak Goyal gave his talk on
["Kafka streams at
scale"](https://hasgeek.com/rootconf/2019/proposals/kafka-streams-at-scale-yik2Kzo4vx3zDeQqnX3F3n).

After the evening beverage break, Øystein Grøvlen, gave an excellent
talk on [PolarDB - A database architecture for the
cloud](https://hasgeek.com/rootconf/2019/proposals/polardb-a-database-architecture-for-the-cloud-zuxWbc3xhgQjwXbdCbRU2H). They
are using it with Alibaba in China to handle petabytes of data. The
computing layer and shared storage layers are distinct, and they use
RDMA protocol for cluster communication. They still use a single
master and multiple read-only replicas. They are exploring parallel
query execution for improving performance of analytical queries.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/hasgeek/48125465926/" title="Rootconf 2019 Day 2"><img src="https://live.staticflickr.com/65535/48125465926_14504a4d03_z.jpg" width="640" height="427" alt="Rootconf 2019 Day 2"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

Overall, the talks and presentations were very good for 2019. Time management is of utmost importance at Rootconf, and we have been very consistent. I was
happy to emcee again for Rootconf!
