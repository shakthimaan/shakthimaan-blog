---
author: SK
tags: 2019, foss
timestamp: 13:30:00
title: "Opportunities in FLOSS" at Computer Society of India, Madras
---

I had given a talk on ["Opportunities in Free (Libre) and Open Source
Software"](http://ieeecs-madras.managedbiz.com/pgms/2019/mm-190302-open-source.pdf)(FLOSS)
on Saturday, March 2, 2019 at the Computer Society of India, Madras
Chapter, jointly organized by the IEEE Computer Society, Madras
Chapter and ACM India Chennai Professional Chapter. The Computer
Society of India, Education Directorate is located opposite to the
Institute of Mathematical Sciences in Taramani, close to the Tidel
Park. Students, IT professionals and professors from IIT Madras, Anna University and
engineering colleges in and around Chennai attended the event.

<img alt="Session in progress" src="http://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/03/08/20190308155249-176a8585.jpg" />

At around 6:00 p.m. people had assembled for networking, snacks and
beverage. I started the ["Building Careers with
FLOSS"](http://www.shakthimaan.com/downloads.html#building-careers-with-floss)
presentation at 6:30
p.m. [Prof. Dr. D. Janakiram](http://dos.iitm.ac.in/djwebsite/), CSE,
IIT Madras also shared his insights on the benefits of learning from
source code available under a FLOSS license. The session was very
interactive and the audience asked a lot of good
questions. Dr. B. Govindarajulu, author of the famous ["IBM PC and
Clones: Hardware, Troubleshooting and
Maintenance"](https://www.amazon.in/IBM-PC-CLONES-Troubleshooting-Maintenance/dp/0070482861)
book then presented his views on creating a Computer History Museum in
Chennai. Dinner was served around 8:00 p.m. at the venue.

<img alt="Group photo" src="http://gallery.shakthimaan.com/uploads/k/d/s/kdszajl69v//2019/03/08/20190308155250-3f3f7229.jpg" />

A [review of my
book](http://ieeecs-madras.managedbiz.com/icnl/19q1/p30-p32.pdf) has
been published in [Volume 14: No. 1, January-March 2019 IEEE India
Council
Newsletter](http://ieeecs-madras.managedbiz.com/icnl/19q1/index.html). The
excerpts of Chapter 4 of my book on ["Project
Guidelines"](http://ieeecs-madras.managedbiz.com/icnl/19q1/p147-p150.pdf)
is also available. I had also written an article on ["Seasons of
Code"](http://ieeecs-madras.managedbiz.com/icnl/19q1/p121-p122.pdf)
which is published in this edition of the IEEE newsletter.

Special thanks to Mr. H. R. Mohan, Editor, IEEE India and Chairman, ACM Professional Chapter, Chennai for organizing the event and for the logistics support.
