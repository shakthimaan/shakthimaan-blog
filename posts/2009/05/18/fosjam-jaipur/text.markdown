---
author: SK
tags: 2009, fedora
timestamp: 00:00:00
title: Fosjam.in, Jaipur
---

I would like to thank the Free/Open Source Software <a
href="http://lugj.in">user group members of Jaipur</a> and the Jaipur
Engineering College and Research Centre Foundation for organizing
http://fosjam.in between May 16-17, 2009 at their college premises,
Jaipur, Rajasthan, India.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album56/7_fosjam_poster.jpg"
alt="fosjam poster"></img>

The event has been organized in 10 days time, with heavy discussions
on #lug-jaipur on irc.freenode.net. I was overwhelmed by the
enthusiasm of the students. The online registration rose to 280+.

We had students coming from outside Jaipur as well, and it was a mad
rush. The organizers didn't have the heart to say "no", but, they
still managed to accommodate 200 people. This, I assume, is the first
time that this kinda workshop has been planned at this place. People,
there is no need to panic! It is not the end of the world, yet, and
this is not the last workshop that we are going to do in Jaipur.

The profiles of the user group members are at
http://fosjam.in/about/about-lug-jaipur.

Special thanks to all of them for their energy, spirit, dynamism in
organizing the event.

I also met students from Malaviya National Institute of Technology,
(MNIT), Swami Keshvanand Institute of Technology (SKIT), Gyan Vihar
Institute of Technology et. al.

It was a pleasure to meet the Director of the Institution, Mr. Arpit
Agarwal, a young, open-minded entrepreneur, who was very happy and
eager to hear students talking about Free/Open Source. His continued
support for this cause, is greatly appreciated. He is happy to help us
in organizing more workshops for the young minds, or even a national
FOSS unconference in Jaipur!

There are about 17+ colleges around Jaipur, and workshops have to be
taken to other places like Kota, Udaipur, Jodhpur, Bikaner
et. al. Most of them are affiliated to Rajasthan Technical University
(RTU).

Some students are fluent in English. Most of them prefer to converse
in Hindi. Of course, technical jargon is in English. So, it is mostly
<a href="http://en.wikipedia.org/wiki/Hinglish">Hinglish</a>.

A separate IRC session was organized to show people how to login and
use IRC.

Lot of them use Fedora or some distribution, and are extremely happy
with it. Those who enjoy it have realized the power of Free/Open
Source Software. The others have started to realize that there is
something important here that they can work with.

My presentations, code, documentation are available at <a
href="http://www.shakthimaan.com/downloads.html">http://www.shakthimaan.com/downloads.html</a>.

Some photos taken during the trip are at <a
href="http://www.shakthimaan.com/Mambo/gallery/album56">http://www.shakthimaan.com/Mambo/gallery/album56</a>

The organizers could possible provide more concrete statistics on the
event. Please bear for some time for people to get back their sleep,
and you will hear more from blogs, event reports, photos.