---
author: SK
tags: 2009, fedora
timestamp: 00:00:00
title: Fedora workshop, NBKR Institute
---

I addressed the following topics for a Free Software workshop at NBKR
Institute of Science and Technology, Vidyanagar, Nellore district,
Andhra Pradesh, India on Saturday, May 2, 2009:

* Career opportunities with Free Software
* i-want-2-do-project. tell-me-wat-2-fedora
* Mailing list guidelines
* Communication guidelines
* Fedora sub-projects
* Embedded Systems concepts

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album55/11_audience.png"
alt="Audience"></img>

I would like to thank Rajesh Sola, Lecturer, for organizing the
workshop, and for coordinating with me for the past couple of months
for this workshop.

The CS labs have Fedora installed. I have given them Fedora 10 DVDs
and stickers (thanks to Rahul Sundaram) which they can distribute to
other engineering departments.

Pointers to fedora mailing lists, IRCs have been given to the
students.

Some photos taken during this trip and at the venue are available at:
<a
href="http://www.shakthimaan.com/Mambo/gallery/album55">http://www.shakthimaan.com/Mambo/gallery/album55</a>.
