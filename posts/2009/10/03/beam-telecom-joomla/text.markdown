---
author: SK
tags: 2009, FOSS
timestamp: 00:00:00
title: Beam Telecom uses Joomla
---

<a href="http://beamcablesystem.in/">Beam Telecom</a> (formerly Beam
Cable), a popular ISP in Madhapur, Hi-tech area at Hyderabad is <a
href="http://shakthimaan.com/downloads/clips/beam-cable-using-joomla.png">using
Joomla</a> for their portal. This screenshot was taken before they
fixed their database migration :)

<img
src="http://shakthimaan.com/downloads/clips/beam-cable-using-joomla.png"
alt="Beam Telecom using Joomla"></img>