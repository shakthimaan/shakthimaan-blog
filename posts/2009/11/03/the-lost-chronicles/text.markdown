---
author: SK
tags: 2009, FOSS
timestamp: 00:00:00
title: The Lost Chronicles
---

We approach technical advisor X and assistant professor Y for a F/OSS
unconference at a premier International Institute, and they never
respond for weeks. Apparently, we knew the Head of the Institute, who
whole-heartedly agrees, having been a user group member during his
college years, and asks X and Y to take things further.

Now, X and Y are upset, because we approached the Head of the
Institute, and permission was granted. Then X and Y show off their
ego, presence, and reciprocate by not communicating at all our
requests. Weeks go by, and we still don't get any prompt response. And
we decide to finally call it off with them because it was not helping
us progress any further.

End result is that the students/faculty of the Institute are at a big
loss for not having the event at their venue for no fault of theirs!

While, I have seen such extreme *ego* cases in India in most of my
attempts to organize Free Software unconferences, I have never shared
it with others. Maybe I should write a book -- The Adventures of
Shakthimaan: The "Lost" Chronicles :)