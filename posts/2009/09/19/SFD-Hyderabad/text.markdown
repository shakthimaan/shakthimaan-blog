---
author: SK
tags: 2009, FOSS
timestamp: 00:00:00
title: Software Freedom Day, Hyderabad
---

On September 19, 2009, Software Freedom Day at Hyderabad, I visited
Devnar Foundation for the Blind, a follow-up to last weekends'
installation.

Most of their systems have been donated. So, there are different
motherboards, and brands. Apparently, the local AMC support guy who
had come today didn't have drivers for different proprietary OSes, and
for different motherboards. I then migrated their office computers to
Fedora 11 :)

Now, they have HCL, Wipro computers running Fedora. I have disabled
many start-up services, to boot the systems faster. I also adjusted
Orca pitch, rate settings for clarity. It should take them some time
to get accustomed to the American accent.

Thanks to Prof. Haasan, Department of Astronomy, Osmania University, I
have deployed Fedora 11 repository (29 GB) at one of their department
systems.

They are interested in creating a Fedora Spin for Astronomy, and
release it, as this is the <a
href="http://www.astronomy2009.org">International Year of
Astronomy</a>. I have provided Rahul Sundarams' e-mail contact to
them.