---
author: SK
tags: 2009, FOSS
timestamp: 00:00:00
title: Devnar Foundation for the Blind
---

Today, Sunday, September 13, 2009, I deployed Fedora 11, and Fedora 11
offline repository at Devnar Foundation for the Blind, Mayurmarg,
Begumpet, Hyderabad, India.

They have been using a proprietary software on a proprietary system,
and wanted to explore better alternatives in this regard.

I was given a 256 MB RAM system, with 40 GB hard-disk. This system did
not have Internet connectivity. The offline repository really
helped. Most of the systems are donated by companies for the school to
use. The school follows the Andhra Pradesh State syllabus, and this
year they have introduced Intermediate (after Board X). They write
their exams on slate or on Braille paper (which I believe, is very
expensive). Some students stay in the school hostel, while others' are
day scholars.

They do have one <a href="http://mountbattenbrailler.com/">MountBatten
Brailler</a> (costing around 1,00,000 INR) which can produce output to
a computer or a printer. It also spells out the input. I didn't have
time to test it with Fedora (serial, parallel ports).

A junior teacher, Miss. Shashi, did try out <a
href="http://live.gnome.org/Orca">Orca</a> with the Gnome shortcut
keys, listening to Orcas' audio output. She was able to navigate
through the Gnome menus, and use Openoffice.org Writer. They did want
spoken American English, which is default in Gnome. It will be good to
have those Orca audio recorded in Telugu or other regional languages
in India.

There were some very interesting questions like:

* Orca loads only after logging in -- which spells out menus, the
  frame window that is selected, keys pressed etc. So, grub which
  loads prior to all these doesn't announce the choice of kernel. So,
  how does one choose the right kernel?

* A sound can be played after gdm loads? Otherwise, they wouldn't know
  if the system booted to gdm or not. If there are multiple user
  accounts, some application need to spell-out the login names?

* By default there is no shutdown sound in Fedora 11? How to set one?

* Some applications pop-up menus didn't support tab feature, so one
  had to remember to use shortcuts like Alt+S to Save, or Alt+C to
  Cancel.

I shall check these requests with the Orca project team, and any other
queries that they might have as follow-ups.

I would like to thank Padma Shri Dr. A. Saibaba Goud for giving me an
opportunity to deploy the solution at their school. Thanks to Rakesh
("arky") Ambati for introducing me to them, who also works for <a
href="http://www.braillewithoutborders.org/ENGLISH/index.html">Braille
Without Borders</a>. Eric Ofori from South Africa will be following it
up with support for the school staff (about 40 in number). If anyone
who is coming to Hyderabad and who would like to help them, please do
contact me offline.

With all due respect to the physically challenged, I did not take any
photos.

