---
author: SK
tags: 2009, fedora
timestamp: 00:00:00
title: gEDA workshop, GEC, Barton Hill
---

I would like to thank Prof. Ranjith Kumar, Department of Mechanical
Engineering, Government Engineering College (GEC), Barton Hill,
Thiruvananthapuram, Kerala, India for coordinating with me for the
past one month for the National Seminar event on Embedded Design with
GNU/Linux that was held on Saturday, March 21, 2009 <a
href="http://fsc.gecbh.ac.in/">http://fsc.gecbh.ac.in</a>.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album54/13_poster.png"
alt="Poster"></img>

I would also like to thank the Directorate of Technical Education, and
Kerala State Council for Science, Technology, and Environment for
organizing the event.

I discussed basic concepts of embedded systems, and gave a demo of
gEDA tools suite (gschem, pcb, qucs, iverilog, gtkwave) on Fedora 10.

Some photos taken during the visit are available at: <a
href="http://www.shakthimaan.com/Mambo/gallery/album54">http://www.shakthimaan.com/Mambo/gallery/album54</a>