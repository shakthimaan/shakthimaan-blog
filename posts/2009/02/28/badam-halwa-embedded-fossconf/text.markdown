---
author: SK
tags: 2009, FOSS
timestamp: 10:00:00
title: Badam Halwa of Embedded Systems, FOSSConf 2009, Madurai
---

I would like to thank the organizers, sponsors of fossconf.in 2009,
students, faculty, ILUGC members and well-wishers for a wonderful
event. It was good to catch up with lot of ILUGC members after a
really long time!

<img alt="stage banner"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album53/6_stage_banner.jpg"></img><br
/>

Please find my presentation slides on "Badam Halwa of Embedded Systems" at: <a
href="http://shakthimaan.com/downloads/glv/presentations/badam.halwa.of.embedded.systems.odp">[odp]</a><a
href="http://shakthimaan.com/downloads/glv/presentations/badam.halwa.of.embedded.systems.pdf">[pdf]</a>

Few photos that I took are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album53">/gallery</a>.
