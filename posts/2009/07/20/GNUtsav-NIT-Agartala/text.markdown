---
author: SK
tags: 2009, fedora
timestamp: 00:00:00
title: GNUtsav, NIT, Agartala
---

I had conducted a Fedora workshop ("GNUtsav") at National Institute of
Technology (NIT), <a href="http://groups.google.co.in/group/nitalug
">Agartala</a>, Tripura, India on July 18-19, 2009.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album57/4_banner.jpg"
alt="GNUtsav poster"></img>

I would like to thank the Fedora project for sponsoring the event.
Special thanks to the student volunteers who worked hard in organizing
the event. Mention must be made for the support of the faculty, Prof.
Swapan Debbarma, Prof. Anupam Jamatia ("ajnr" on freenode), and Prof.
Dwijen Rudrapal.

My presentation sessions included:

* i-want-2-do-project. tell-me-wat-2-do-fedora.
* Badam Halwa of Embedded Systems
* di-git-ally managing love letters
* Fedora Electronic Lab (demo)
* Packaging RPM -- Red hot, Paneer (butter) Masala

The presentations are available from <a
href="http://www.shakthimaan.com/downloads.html">http://www.shakthimaan.com/downloads.html</a>

It has only been two years since this Institution has started. It is
25 km from Agartala city. The Institute has 50% reservation for State
students, and 50% for students coming through an All India Engineering
Entrance Examination. People speak Hindi, English, Bengali, and
Kokborok.

Guwahati, Assam is the main connectivity for this part of the world,
or, flying from Kolkatta, West Bengal is the other option. Subsidized
rate flights are available from major domestic carriers in India.
Helicopter service between North-Eastern States is also available,
but, is expensive. Bus journey from Guwahati, Assam to Agartala,
Tripura is very tedious, and might take atleast 24 hours. When passing
through the dense forests, <a
href="http://en.wikipedia.org/wiki/Central_Reserve_Police_Force">CPRF</a>
escort is required due to insurgency problems in forest areas.

Students have access to distros at the labs, as well as in the hostel.
Power cuts are quite common here. Mobile cell coverage is nil or works
at only few places. Internet connectivity is nil or very slow on
campus. Hence, we have provided them with an offline Fedora 10
repository (around 24 GB) that they can use in their computers or in
the LAN. Special thanks also goes to <a
href="http://dgplug.org/intro/">dgplug user group</a> from Durgapur,
West Bengal for the offline repos. There are few browsing centers in
the city that students can use, but have to travel the distance from
the campus. Work is in progress to provide students with a Fedora 11
repository.

Agartala is a very scenic, lush green, and very peaceful place.
Hopefully when the construction work on campus is completed in the
next six months, they will be better equipped with Internet access for
communicating online, and for VoIP sessions.

As customary, here are few <a
href="http://www.shakthimaan.com/Mambo/gallery/album57 ">photos</a>
that I took during the trip.

