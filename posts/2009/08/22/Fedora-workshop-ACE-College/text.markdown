---
author: SK
tags: 2009, fedora
timestamp: 00:00:00
title: Fedora workshop, ACE College
---

Today, Saturday, August 22, 2009, I conducted a Fedora workshop at <a
href="http://aceec.ac.in/">ACE College of Engineering</a>, Ankushapur,
<a href="http://en.wikipedia.org/wiki/Ghatkesar">Ghatkesar</a> Mandal,
Ranga Reddy District, Andhra Pradesh, India.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album58/3_ace_college.jpg"
alt="ACE college"></img>

I have addressed "i-want-2-do-project. tell-me-wat-2-do-fedora",
"Packaging RPM", "Badam Halwa of Embedded Systems" <a
href="http://shakthimaan.com/downloads.html">presentations</a>. The
attendees were Computer Science and Information Technology Department
students, and faculty. I would like to thank Mrs. K. Jaya Bharathi,
Head of the Computer Science Department, for coordinating with me for
the past one month in organizing this workshop.

They don't yet have a Fedora Lab. I have provided them with Fedora 10,
11 repositories for their offline use (53 GB). I have also given them
Fedora 10, 11 DVDs.

Ghatkesar is 25 km from Hyderabad, and one hour drive from Hyderabad
through <a
href="http://en.wikipedia.org/wiki/National_Highway_7_%28India%29">NH-7</a>.

As customary, here are some <a
href="http://www.shakthimaan.com/Mambo/gallery/album58">pictures</a>
taken during the visit.