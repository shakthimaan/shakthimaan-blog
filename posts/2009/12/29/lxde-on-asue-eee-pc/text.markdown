---
author: SK
tags: 2009, fedora, laptop
timestamp: 00:00:00
title: LXDE on Asus EEE PC
---

Installed livecd-tools, used livecd-iso-to-disk to write F12 LXDE spin
to a USB disk, and used it to boot a Asus EEE PC:

~~~~ {.shell}
# yum install livecd-tools
~~~~

<img
src="http://shakthimaan.com/downloads/clips/asus_eee_pc_f12_lxde.jpg"
alt="Fedora 12 LXDE from USB disk"></img>.