---
author: SK
tags: 2009, fedora
timestamp: 00:00:00
title: FedX
---

Downloaded Fedora 12 (i386) mirror using <a
href="http://gitorious.org/fedx">FedX</a>, written by <a
href="http://ratnadeepdebnath.wordpress.com/">Ratnadeep Debnath</a>
and myself. I have updated it to rsync from a mirror to a local copy
as well as from an external disk to a local directory.

~~~~ {.shell}
  $ git clone git://gitorious.org/fedx/mainline.git
~~~~

* Fedora 11 (i386) with rpmfusion repository = 29 GB
* Fedora 12 (i386) with rpmfusion repository = 23 GB

I assume that this drastic size reduction is due to the switch from
gzip compression to XZ (LZMA format). Please refer <a
href="https://fedoraproject.org/wiki/Features/XZRpmPayloads">XZRpmPayloads</a>
(when the site is back up and running). This Fedora offline repository
is very handy when working offline, or when it is required to install
packages when using slow Internet connectivity speeds.