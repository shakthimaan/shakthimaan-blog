---
author: SK
tags: 2009, fedora
timestamp: 00:00:00
title: Poky scripts
---

Pushed poky-scripts to Fedora repository. <a
href="http://pokylinux.org/">Poky</a> is a platform builder tool
written in Python that allows you to build toolchains, kernels, rootfs
images for different target hardware. Use:

~~~~ {.shell}
# yum install poky-scripts
~~~~

You can test run <a
href="http://pokylinux.org/releases/pinky-3.1/">pre-built images</a>,
for example from Pinky (3.1.x) releases:

~~~~ {.shell}
$ poky-qemu zImage-2.6.23-pinky-3.1-qemuarm.bin poky-image-sdk-qemuarm-pinky-3.1.rootfs.ext2
~~~~

<img
src="http://shakthimaan.com/downloads/clips/poky-pinky-qemu-arm-screenshot.png"
alt="Poky QEMU ARM screenshot"></img>

If you want to build your complete environment, you need to download
the latest Poky stable release 3.2.x 'Purple' (latest) and extract it
to ~/devel. Make sure you have a broadband connection and atleast 3G
of disk space as the scripts will fetch, build everything from
toolchains, kernels, user space applications. Follow the <a
href="http://pokylinux.org/support/">Poky handbook</a> which is very
well documented. Supported reference platforms include (to mention a
few):

* Intel Atom devices
* Compulab cm-x270 and em-x270
* Freescale MX31
* ST Nomadik
* Marvell Zylonite
* QEMU ARM and x86
