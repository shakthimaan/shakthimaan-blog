---
author: SK
tags: 2009, FOSS
timestamp: 00:00:00
title: Sparcy
---

Bought the GDL3204 GPS data logger from Sparc Systems Ltd, Mumbai,
India for INR 7500 (taxes and shipping charges additional). They use
<a href="http://www.gpleda.org/">gEDA (GPL Electronic Design
Automation)</a> and <a href="http://pcb.gpleda.org/index.html">Printed
Circuit Board (PCB)</a> for their work, and I thought I should support
them by buying a unit.

<img src="http://shakthimaan.com/downloads/hardware/GDL3204.jpg"
alt="GLD3204 GPS data logger"></img>

It has a serial interface to connect to a computer, and can store
74000 track points on a 4MB non-volatile memory. It can be used for <a
href="http://www.openstreetmap.org/">openstreetmap</a> work. You can
use the sparcy (command line tool) with it from Fedora.

~~~~ {.shell}
# yum install sparcy
~~~~