---
author: SK
tags: 2009, fedora
timestamp: 00:00:00
title: Fedora workshop, Maharaja Sayajirao University of Baroda
---

I had conducted a one-day Fedora, Free Software workshop at the
Bachelor of Computer Applications department, Maharaja Sayajirao
University of Baroda (Vadodara), Gujarat, India on Saturday, December
26, 2009.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album60/3a_telescope_dome.jpg"
alt="MSU, Baroda"></img>

Thanks to <a href="http://www.vglug.info">VGLUG</a> for coordinating
with me for the past few months in organizing this event. I would like
to thank the Fedora project for sponsoring the travel.

The BCA department lab systems were equipped to run on Fedora 12, and
we used it for couple of the hands-on sessions:

* Use of git: <a href="http://shakthimaan.com/downloads/glv/presentations/di-git-ally-managing-love-letters.pdf">di-git-ally managing love letters</a>.
* <a href="http://shakthimaan.com/downloads/glv/presentations/packaging-red-hot-paneer-butter-masala.pdf">Packaging Fedora RPM</a>.

I had also addressed the following topics:

* <a href="http://www.shakthimaan.com/downloads/glv/presentations/i-want-2-do-project-tell-me-wat-2-do-fedora.pdf">i-want-2-do-project. tell-me-wat-2-do-fedora</a>.
* <a href="http://www.shakthimaan.com/downloads/glv/presentations/badam.halwa.of.embedded.systems.pdf">Badam Halwa of Embedded Systems</a>.

<img
src="http://www.shakthimaan.com/Mambo/gallery/albums/album60/9_git_session_in_progress.jpg"
alt="Git session"></img>

Participants were from the BCA department, and had also come from <a
href="http://wikitravel.org/en/Ahmedabad">Ahmedabad</a>. Vadodara
(Baroda) connects to Ahmedabad via 95 km <a
href="http://en.wikipedia.org/wiki/Indian_National_Expressway_1">National
Expressway 1</a>.

Free/Open Source Software is officially included in the Gujarat state
school syllabus for higher secondary (XI and XII). In the coming years
it is expected to be even introduced from standard VIII.

The Department coordinator and Professor V A Kalamkar is happy to
facilitate work between VGLUG, and the students for Free/Open Source
Software projects. We hope to have more interaction with them in the
future.

VGLUG members are also keen on participating in the Fedora project at
various sub-projects of their interests.

Due to the <a
href="http://beta.thehindu.com/news/national/article69789.ece">political
crisis</a> in Hyderabad, I was uncertain about reaching the Hyderabad
airport, and thus making it to the workshop. The bandh was withdrawn
for December 25th, and when I made it the airport on the 25th, the
scheduled flight was cancelled. So, I had to re-schedule a different
flight, but, the airline officials said they couldn't guarantee if I
will make it to my connecting flight in Mumbai. This re-scheduled
flight got delayed even further. Luckily, my connecting flight also
got delayed, and I finally managed to make it to Vadodara.

As customary, here are some <a
href="http://www.shakthimaan.com/Mambo/gallery/album60">photos</a>
taken during the trip.