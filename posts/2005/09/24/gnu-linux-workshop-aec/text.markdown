---
author: SK
tags: 2005, FOSS
timestamp: 10:00:00
title: GNU/Linux workshop, Adhiparasakthi Engineering College, Melmaruvathur
---

I would like to thank the Computer Science Department, <a
href="http://adhiparasakthi.in/">Adhiparasakthi Engineering College,
Melmaruvathur, Tamil Nadu, India</a> for inviting me, and for helping
me organize a GNU/Linux workshop for CSE students. Special mention
goes to Mr. Ramabadran for working with me for the past three weeks in
coordinating this event.

The workshop was held today, September 24, 2005 at the college
premises. The topics covered: <a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">career
opportunities with GNU/Linux</a> and <a
href="http://shakthimaan.com/downloads.html#free-software-for-engineers">Free
Software for engineers</a>.

The engineering college is interested in working towards bringing more
FLOSS industry exposure to their campus, and for conducting more
workshops. If anybody is interested, I can put you on to the concerned
individuals.
