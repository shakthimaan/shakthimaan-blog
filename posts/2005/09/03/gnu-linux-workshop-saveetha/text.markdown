---
author: SK
tags: 2005, FOSS
timestamp: 10:00:00
title: GNU/Linux workshop, Saveetha Engineering College, Chennai
---

I would like to thank Srinivasan, from <a
href="http://www.saveetha.com/engineering/">Saveetha Engineering
College, Chennai, Tamil Nadu, India</a> for taking the initiative for
a GNU/Linux workshop, the faculty (CS and IT departments), and
management of Saveetha Engineering College for making the necessary
arrangements for the workshop.

<img alt="Audience"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album16/6_audience_house_full.jpg
"></img><br />

The workshop was conducted, today, Saturday, September 3, 2005 at the
college premises. I addressed the following: <a
href="http://shakthimaan.com/downloads.html#free-software-for-engineers">Introduction
to Free Software</a>, <a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">Careers
with GNU/Linux</a>, and the GNU/Linux desktop.

More photos can be viewed from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album16">/gallery</a>.
