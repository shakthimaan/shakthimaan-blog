---
author: SK
tags: 2005, FOSS
timestamp: 10:00:00
title: GNU/Linux workshop, Stella Maris College, Chennai
---

I organized a GNU/Linux workshop at <a
href="http://www.stellamariscollege.org/">Stella Maris College,
Chennai, Tamil Nadu, India</a>, today, September 10, 2005.

<img alt="Audience"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album17/3_audience.jpg
"></img><br />

I discussed the various <a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">career
opportunities related to GNU/Linux</a>, gave a demo of the GNU/Linux
desktop, and gave an introduction on <a
href="http://shakthimaan.com/downloads.html#free-software-for-engineers">Free
Software for engineers</a>.

I would like to thank the faculty, CS department, and management of
Stella Maris College for the opportunity.

Few photos taken during the workshop are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album17">/gallery</a>.