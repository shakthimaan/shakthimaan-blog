---
author: SK
tags: 2005, FOSS
timestamp: 10:00:00
title: GNU/Linux workshop for NGOs, Ma Foi Academy, Chennai
---

<a href="http://ilugc.in">ILUGC</a> in collaboration with <a
href="http://www.ciosa.org.in">CIOSA</a> organized a GNU/Linux
workshop for NGOs on Sunday, October 16, 2005 at Ma Foi Academy,
Chennai.

<img alt="Audience"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album19/16_gnu_linux_desktop.jpg
"></img><br />

It was an introductory session on Free Software, OpenOffice, desktop
applications, Internet applications, multimedia on GNU/Linux. Thanks
to Prasanna (CIOSA) for taking the initiative, Asokan Pichai (Ma Foi)
for providing us with the facilites at Ma Foi, and Vamsee Kanakala (Ma
Foi) for coordinating the events.

Thanks also goes to ILUGC volunteers Bharathi, Raman, Hariram,
Sudharshan, Ramanathan and others. You can check out the photos taken
during the workshop from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album19">/gallery</a>.
