---
author: SK
tags: 2005, vlsi, vhdl, FOSS
timestamp: 10:00:00
title: Embedded/VLSI workshop, Crescent Engineering College, Chennai
---

I would like to thank Prof. Raja Prabhu, Prof. Bhaskar, Krishnaswamy
(EEE), Prasanna (EEE) from <a
href="http://www.crescentcollege.org/">Crescent Engineering College,
Chennai, Tamil Nadu, India</a> for the opportunity to conduct a
GNU/Linux workshop for their IEEE Chapter on Saturday, October 22,
2005 at their college premises.

<img alt="Embedded Labs HOWTO"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album20/6_embedded_labs.jpg
"></img><br />

The workshop had two sessions. The <a
href="http://shakthimaan.com/downloads.html#alliance">Alliance VLSI
CAD tools</a> with GNU/Linux session was meant to get students started
on VHDL, and to introduce them to Alliance VLSI CAD tools. The <a
href="http://shakthimaan.com/downloads.html#embedded-gnu-linux-labs-howto">Embedded
GNU/Linux Labs HOWTO</a> presentation was on how to get started on
setting up GNU/Linux labs for embedded, hardware and VLSI.

Few photos taken during the workshop are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album20">/gallery</a>.