---
author: SK
tags: 2005, FOSS
timestamp: 10:00:00
title: GNU/Linux workshop, Alagappa University, Karaikudi
---

I would like to thank the Correspondent, Dr. (Tmt.)  Umayal
Ramanathan, the Registrar, Mr. Dhandapani, CSE HOD, Dr. S. Sakthivel,
<a href="http://www.alagappauniversity.ac.in/">Alagappa University,
Karaikudi, Tamil Nadu, India</a> for providing me with an opportunity
to conduct a GNU/Linux workshop for the MCA students on Saturday, July
23, 2005.

<img alt="Audience"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album13/floss_4.jpg
"></img><br />

The labs already run Red Hat, but, it is used at a basic programming
(bash scripting, desktop usage) level. I conducted the following
sessions: <a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">career
opportunities with GNU/Linux</a>, and on <a
href="http://shakthimaan.com/downloads.html#free-software-for-engineers">Free
Software for engineers</a>.

The download speeds are not that high at the Institute. I gave them
Knoppix 3.6, Fedora Core 3, The.Code.Linux.avi, C-DAC tamil fonts, and
the presentation slides in CDs. BSNL provides Internet connection
facilities for the University. Sify has netcafe centers in the city,
which have much better download speeds. Most of them have a computer
at home.  I have told them to send e-mails if they require any CDs,
and we can simply send the CDs to them through courier.

The students were very much interested in GNU/Linux. You should have
seen their eyes, the eagerness and willingness to really learn, and to
understand the fundamental concepts.

Few photos taken during the trip can be viewed from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album13">/gallery</a>.
