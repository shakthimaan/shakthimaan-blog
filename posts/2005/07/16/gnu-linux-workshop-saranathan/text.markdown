---
author: SK
tags: 2005, FOSS
timestamp: 10:00:00
title: GNU/Linux workshop, Saranathan College of Engineering, Tiruchirapalli
---

I would like to thank the Tiruchirapalli MySQL group and CSE
department faculty, students, and the management of <a
href="http://www.saranathan.ac.in/">Saranathan College of Engineering,
Tiruchirapalli, Tamil Nadu, India</a> for giving me an opportunity to
conduct a one-day GNU/Linux workshop. The workshop was announced in
the newspapers, "The Indian Express" and "The Hindu".

<img alt="Staff"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album12/before_workshop_2.jpg
"></img><br />

The following topics were addressed: <a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">Careers
with GNU/Linux</a>, <a
href="http://shakthimaan.com/downloads.html#free-software-for-engineers">Free
Software for Engineers</a>, and a demo of the GNU/Linux Desktop.

The MYSQL group is working on using LAMP on GNU/Linux for the
Pulivazham village cooperative society milk center farmers. They
require to keep track of accounts, volume of milk stored in the
facilities, and general book-keeping activities. Good luck to the
Tiruchirapalli MySQL team for this project. Hopefully next time when
we visit, we will see Pulivazham farmers using GNU/Linux for their
day-to-day activities.

Thanks to S. Srinivasan, T. Shivakumar, Arul Jeeva, and
Lakshminarayanan of Tiruchirapalli MySQL group, <a
href="http://harshainfotech.com/">Harsha Infotech</a>, and <a
href="http://www.rajaramsystems.com/">Rajaram Systems</a>.

Special thanks to Ramkumar L who has been working with me in planning
this event for the past two weeks. Thanks to Prof. Venkatasubramanian,
HOD, CSE Department, Saranathan College of Engineering for
coordinating the sessions.

The photos of the workshop (and the trip) can be viewed from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album12">/gallery</a>.
