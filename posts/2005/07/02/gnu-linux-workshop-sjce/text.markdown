---
author: SK
tags: 2005, FOSS
timestamp: 10:00:00
title: GNU/Linux workshop, St. Joseph's College of Engineering
---

The photots taken during the GNU/Linux workshop on July 2, 2005, for
MCA and M.Sc. IT students at <a
href="http://stjosephs.ac.in/">St. Joseph's College of Engineering,
Chennai, Tamil Nadu, India</a> are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album10">/gallery</a>.

<img alt="audience"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album10/presentation_4.jpg
"></img><br />

I conducted the following sessions: <a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">careers
with GNU/Linux</a>, and <a
href="http://shakthimaan.com/downloads.html#free-software-for-engineers">Free
Software for Engineers</a>. The.Code.Linux movie was also shown to the
participants.

I would like to thank Parvathavarthini Mam (HOD, MCA), Faculty (MCA,
M.Sc. IT departments), Anandhi Subramanian Mam, and the Management,
St. Joseph's College of Engineering, Chennai for providing the
Audio-Visual hall for the GNU/Linux workshop.
