---
author: SK
tags: 2005, FOSS
timestamp: 10:00:00
title: GNU/Linux workshop, Kongu Engineering College, Erode
---

I would like to thank the Computer Science Department - the students,
professors, HOD and Dean, <a href="http://www.kongu.ac.in/">Kongu
Engineering College, Erode, Tamil Nadu, India</a> for giving me an
opportunity to conduct a one-day workshop/seminar on "GNU/Linux/OSS"
on January 17, 2005 (Monday).

Special thanks to Arvind Kalyan (final year, CS) who has been working
with me for the past three weeks in meticulously planning the seminar,
and answering my questions. Special thanks goes to the Management for
the good hospitality and stay provided at the guest house. The
infrastructure at your college is _state-of-the-art_. Thanks for
providing all the required facilities for the seminar.

The following is a summary of the workshop events:

Session I: <a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">"Careers
with GNU/Linux/OSS"</a> (morning)

Session II: <a
href="http://shakthimaan.com/downloads.html#linux-device-driver-programming-code-examples">Introduction
to device driver programming</a> (morning)

Session III: Knoppix demo and GNU/Linux installation (afternoon)

Session IV: Watched the movie, The.Code.Linux.avi (evening)

We shall organize more events in future as a collaboration between <a
href="http://ilugc.in">ILUGC</a> and Kongu Linux User's Group.
