---
author: SK
tags: 2005, FOSS
timestamp: 10:00:00
title: Tuxedo, Sri Jayachamarajendra College of Engineering, Mysore
---

I would like to thank the team of "Tuxedo 2005", <a
href="http://sjcemysore.org/index.aspx">Sri Jayachamarajendra College
of Engineering, Mysore, Karnataka, India</a> for the opportunity and
hospitality offered during the two-day workshop held between November
12-13, 2005.

<img alt="Poster"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album21/32_tuxedo_05.jpg
"></img><br />

Thanks to Joy, Chairman, IEEE-SJCE Student Branch for working with me
for the past one month (answering all my queries :), and the student
volunteers for taking good care of us, and doing the needful during
the occasion. Special thanks to Dr. C. R. Venugopal (IITB), who has
taken so much initiative, and continues to motivate SJCE students into
FLOSS. Thanks to Aanjhan, Ramanraj and Bharathi for their talks and
company.

SJCE, has Dr. C. R. Venugopal (CRV), Coordinator IIPC, Staff Advisor,
IEEE, who has guided students in the past and continues to guide them
with FLOSS. Under his guidance they have done projects on RTOS, minix
and GNU/Linux.

HP, Bangalore selects students from SJCE to do their Free Software
projects. This is only for final year students. As usual, I insisted
working on Free Software projects during their course. CRV also works
closely with Texas Instruments, Bangalore in getting projects for
their students so they can get hands-on experience.

Bharathi began day one (Saturday) with a session on "GNU, FSF". In the
afternoon, Ramanraj spoke about "CALPP and AI". The quiz prelims and
finals were conducted the same day.

<img alt="Ramanraj session"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album21/16_audience.jpg
"></img><br />

Aanjhan spoke about "Embedded GNU/Linux" on day two (Sunday). They had
a "virtual treasure hunt" contest for students on Sunday. I spoke on
<a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">"Careers
with GNU/Linux"</a> and <a
href="http://shakthimaan.com/downloads.html#free-software-for-engineers">"Free
Software for Engineers"</a> in the afternoon. We screened the movie <a
href="http://www.revolution-os.com/">Revolution OS</a> in the evening.

We have decided to have more collaboration between Chennai and Mysore
LUGS, and probably conduct lot of contests for students to make them
do *practical* stuff and get hands-on experience. We can plan install
fests, hack fests, coding contests etc.

The photos taken during Tuxedo 2005 can be viewed from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album21">/gallery</a>.
