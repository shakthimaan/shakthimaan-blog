---
author: SK
tags: 2005, FOSS
timestamp: 10:00:00
title: GNU/Linux workshop, Thiagarajar College of Engineering, Madurai
---

I would like to thank Senthil Kumaran, Arpit Sud, Praveen, Joe Steeve,
Prof. Shalini (HOD, CSE), and other volunteers for working with me for
the past one month in planning, and coordinating the GNU/Linux
workshop that was held on Saturday, August 20, 2005 at <a
href="http://www.tce.edu/">Thiagarajar College of Engineering,
Madurai, Tamil Nadu, India</a>.

<img alt="Audience"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album15/audience_5.jpg
"></img><br />

The following topics were addressed: <a
href="http://shakthimaan.com/downloads.html#careers-with-gnu-linux">careers
with GNU/Linux</a>, <a
href="http://shakthimaan.com/downloads.html#free-software-for-engineers">Free
Software for Engineers</a>, and the GNU/Linux desktop. An interactive
Q&A session was also organized. We also played "The.Code.Linux" movie.

You can check out more photos from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album15">/gallery</a>.