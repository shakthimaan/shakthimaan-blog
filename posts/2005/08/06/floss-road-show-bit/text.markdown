---
author: SK
tags: 2005, FOSS
timestamp: 10:00:00
title: FOSS Road Show, Bannari Amman Institute of Technology, Sathyamangalam
---

I would like to thank Hariram Aatreya for the opportunity given to me
to accompany Murali, Chandrasekhar, and Venkatesh from the <a
href="http://www.au-kbc.org/">AU-KBC</a> center, Madras Institute of
Technology for the FLOSS road show to <a
href="http://www.bitsathy.ac.in/">Bannari Amman Institute of
Technology, Sathyamangalam, Tamil Nadu, India</a> on August 6, 2005.

<img alt="Audience"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album14/invocation2_13.jpg
"></img><br />

The faculty, management and students were very much interested in
GNU/Linux, and are planning to start a GLUG and increase their
activities with GNU/Linux. Representatives from seven different
colleges in and around Sathyamangalam had also come for the workshop.

On the way back we checked out the FLOSS lab at <a
href="http://www.kongu.ac.in/">Kongu Engineering College, Erode</a>,
and the projects they had been working on. I must thank them for
staying back till eight in the night as we finished late in the
evening at Bannari Amman Institute of Technology.

Kongu Engineering College LUG started with six members in 2003, and it
has now grown to 170 members from all the departments. They meet every
week. A former student (just graduated), Arvind Kalyan, was
responsible for taking the initiative in 2003.  Professors (like
Prof. Jayapathi, HOD, CSE department) and the management provided the
support for the FOSS activities, and continue to do so. The juniors
are now continuing the tradition :). They have also agreed to
collaborate with Bannari Amman Institute of Technology for FOSS
projects.

You can view the photos of the trip and the workshop from my <a
href="http://www.shakthimaan.com/Mambo/gallery/album14">/gallery</a>.
