---
author: SK
tags: 2013, fedora
timestamp: 04:10:00
title: GCC workshop, GNUnify 2013
---

<a href="http://gnunify.in/">GNUnify 2013</a> was held at <a
href="http://sicsr.ac.in/">Symbiosis Institute of Computer Studies and
Research (SICSR)</a>, Pune, Maharashtra, India between February 15 to
17, 2013. I attended day one of the unconference.

<img alt="poster"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album87/poster.jpg"></img>

The first talk that I listened to was by Oyunbileg Baatar on
"Screencasting Demos and HowTos". He mentioned the various free/open
source, desktop recording software available. He also gave a demo of
<a
href="http://recordmydesktop.sourceforge.net/about.php">recordMyDesktop</a>,
and video editing using <a href="http://www.pitivi.org/">PiTiVi</a>.

After a short break, and a formal introduction, I began my session for
the day - <a
href="http://shakthimaan.com/downloads.html#introduction-to-gcc">"Introduction
to GCC"</a>. Fedora 17 was installed in the labs for the participants
to use. I started with a simple hello world example and the use of
header files. I also explained the concepts of compilation and
linking, and briefed them on the syntax of Makefiles. Examples on
creating and using static and shared libraries were illustrated. We
also discussed the different warning and error messages emitted by
GCC. The platform-specific and optimization options were shown with
examples. Students were not familiar with touch typing, and I had to
demonstrate the use of <a
href="http://klavaro.sourceforge.net/en/index.html">Klavaro</a> typing
tutor.

<img alt="GCC workshop"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album87/gcc_workshop.jpg"></img>

The preliminary round for the programming contest was held in the afternoon. Thirty questions on C and systems programming were given to the participants to be answered in thirty minutes. I helped evaluate the answers. The practical test was to be conducted the following day. Thanks to <a href="http://neependra.net/">Neependra Khare</a> and Kiran Divarkar for organizing the programming contest. 

I also attended the OpenStack mini-conf session in the evening where a demo of OpenStack was given by Kiran Murari. This was followed by a session on "OpenStack High Availability" by Syed Armani. Aditya Godbole's closing session for the day on an "Introduction to Ruby" was informative. Few photos that were taken are available in my <a href="http://www.shakthimaan.com/Mambo/gallery/album87">/gallery</a>.