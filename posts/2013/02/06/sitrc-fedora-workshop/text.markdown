---
author: SK
tags: 2013, fedora
timestamp: 05:10:00
title: Fedora workshop, SITRC, Nashik
---

A Fedora workshop was organized at <a
href="http://sitrc.sandipfoundation.org/">Sandip Institute of
Technology and Research Center (SITRC)</a>, <a
href="http://en.wikipedia.org/wiki/Nashik">Nashik</a>, Maharashtra,
India from February 2 to 3, 2013.

<img alt="SITRC, Nashik"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album86/8_amphitheatre.jpg"></img>

Day I

I began the day's proceedings with the <a
href="http://shakthimaan.com/downloads.html#i-want-2-do-project-tell-me-wat-2-do">"i-want-2-do-project. tell-me-wat-2-do-fedora"</a>
presentation in the seminar hall at SITRC. The participants were
introduced to mailing list, communication and effective project
guidelines when working with free/open source software. This was
followed by an introduction on window managers, and demo of the Fedora
desktop, GNOME, Fluxbox, and console environments.

After lunch, I gave an introduction on system architecture, and
installation concepts. Basics of compilation and cross-compilation
topics were discussed. An introduction on git was given using the <a
href="http://shakthimaan.com/downloads.html#di-git-ally-managing-love-letters">"di-git-ally
managing love letters"</a> presentation. After a short tea break, we
moved to the labs for a hands-on session on GCC. This is a <a
href="http://shakthimaan.com/downloads.html#introduction-to-gcc">presentation</a>
based on the book by Brian Gough, <a
href="http://www.network-theory.co.uk/gcc/intro/">"An introduction to
GCC"</a>. Practical lab exercises were given to teach students
compilation and linking methods using GCC. I also briefed them on the
use of Makefiles. C Language standards, platform-specific and
optimization options with GCC were illustrated.

<img alt="GCC lab session"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album86/10_lab_session_in_progress.jpg"></img>

Day II

Lab exercises from the GCC presentation were practised on the second
day, along with the creation and use of static and shared
libraries. The different warning options supported by GCC were
elaborated. A common list of error messages that newbies face were
also discussed. After the lab session, I introduced them to cloud
computing and <a
href="http://fedorapeople.org/~russellb/openstack-lab-rhsummit-2012/index.html">OpenStack</a>,
giving them an overview of the various components, interfaces, and
specifications. I also gave them a demo of the OpenStack Essex release
running on Fedora 17 (x86_64) with the Horizon web interface.

The college was affiliated to University of Pune, and had deployed
GNU/Linux labs for their coursework. Now they are autonomous, and want
to explore and expand their activities. They have a local user group
called <a href="http://snashlug.wordpress.com/">SnashLUG</a>. The
college is 15 km away from the city of Nashik, which is around 200 km
from Pune. The bus journey from Pune to Nashik takes six hours, and
you can book tickets online through <a
href="http://www.msrtc.gov.in/">Maharashtra State Road Transport
Corporation (MSRTC)</a>. There is frequent bus service between Pune
and Nashik.

Thanks to Rahul Mahale for working with me for the past three months
in planning and organizing this workshop. Thanks also to the
Management, and Faculty of SITRC for the wonderful hospitality, and
their support for the workshop.

Few photos taken during the workshop are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album86">/gallery</a>.
