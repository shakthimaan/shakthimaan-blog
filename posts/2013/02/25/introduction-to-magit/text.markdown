---
author: SK
tags: 2013, fedora, emacs
timestamp: 04:30:00
title: Emacs + Magit = Git Magic
---

I had presented an introduction to <a
href="https://github.com/magit/magit">Magit</a>, "Emacs + Magit = Git
Magic", at the <a href="http://www.meetup.com/the-peg">Pune Emacs
Users' group meetup</a> on Friday, February 22, 2013. Magit is an
Emacs mode that interfaces with <a
href="http://git-scm.com/">Git</a>. Magit doesn't provide all the
interfaces of Git, but the frequently used commands. The <a
href="http://magit.github.com/magit/magit.html">user manual</a> was
used as a reference. Magit is available in Fedora. You can install it
using:

~~~~ {.shell}
$ sudo yum install emacs-magit
~~~~

The talk was centered around the notion of writing a poem on Emacs in
Emacs, and using magit to revision it. I started an Emacs session,
created a directory with <a
href="http://www.gnu.org/software/emacs/manual/html_node/emacs/Dired.html">Dired</a>
mode, and used magit (M-x magit-status) to initialize a git
repo. After adding a stanza in the poem, I used the magit commands to
stage (s) and commit the same (c, C-c C-c) from the
magit-buffer. Another stanza and a README file were then added, and
the different untracked, and tracked section visibility options (TAB,
S-TAB, {1-4}, M-{1-4}) were illustrated. After adding the third
stanza, and committing the same, the short (l l) and long (l L)
history formatted outputs were shown. The return (RET) key on a commit
in the magit-log history buffer opens a new magit-commit buffer,
displaying the changes made in the commit. The sha1 can be copied
using the (C-w) shortcut.

<img alt="session-in-progress"
src="http://photos1.meetupstatic.com/photos/event/e/1/b/6/600_207537782.jpeg"></img>

The reflogs are visible with the (l h) option from the
magit-buffer. The (d) command was used to show the difference between
the master and a revision, and (D) for the diff between any two
revisions. Annotated tags (t a) and lightweight tags (t t) can be
created in magit. Resetting the working tree and discarding the
current changes is possible with (X). Stashing (z z) the present
changes, applying a stash (a), and killing the stash (k) were
demonstrated. An org branch was then created (b n) to write a stanza
on org-mode, and then merged (m m) with the master branch. An example
of rebasing (R) was also illustrated. The magit-buffer can be
refreshed (g) to check the current status of the git repo. Anytime,
the magit buffers can be closed with the (q) command. A git command
can be invoked directly using (:), and the corresponding output can be
viewed with ($), which is shown in a magit-process buffer.

A summary of the various shortcuts are available in the <a
href="http://shakthimaan.com/downloads.html#emacs-magit-git-magic">presentation</a>. The
poem that I wrote on Emacs, and used in the talk:

> Emacs is, an operating system \
> Which unlike many others, is truly, a gem \
> Its goodies can be installed, using RPM \
> Or you can use ELPA, which has already packaged them \

> You can customize it, to your needs \
> You can also check EmacsWiki, for more leads \
> Your changes work, as long as reload succeeds \
> And helps you with, your daily deeds \

> People say, it lacks a decent editor \
> But after using its features, they might want to differ \
> Using Magit's shortcuts, you might infer \
> That it is something, you definitely prefer \

> Plan your life, with org-mode \
> You don't necessarily need, to write code \
> TODO lists and agenda views, can easily be showed \
> Reading the documentation, can help you come aboard \

> Emacs is, a double-edged sword \
> Its powerful features, can never be ignored \
> Customization is possible, because of Free Software code \
> And this is, my simple ode. \
