---
author: SK
tags: 2013, FOSS, vlsi
timestamp: 10:50:00
title: VLSI Design Conference, Pune, 2013
---

The <a href="http://www.vlsidesignconference.org/">26th International
Conference on VLSI Design 2013 and the 12th International Conference
on Embedded Systems</a> was held at the <a
href="http://pune.regency.hyatt.com/">Hyatt Regency, Pune</a>, India
between January 5-10, 2013. The first two days were tutorial sessions,
while the main conference began on Monday, January 7, 2013.

<img alt="26th VLSID 2013"
src="http://shakthimaan.com/downloads/glv/2013/vlsid-2013/poster.JPG"></img>

Day 1: Tutorial

On the first day, I attended the tutorial on "Concept to Product -
Design, Verification & Test: A Tutorial" by <a
href="http://www.ece.wisc.edu/~saluja/">Prof. Kewal Saluja</a>, and <a
href="http://www.ee.iitb.ac.in/~viren/">Prof. Virendra
Singh</a>. Prof. Saluja started the tutorial with an introduction and
history of VLSI. An overview of the VLSI realization process was given
with an emphasis on synthesis.  The theme of the conference was
"green" technology, and hence the concepts of low power design were
introduced. The challenges of multi-core and high performance design
including cache coherence were elaborated. Prof. Singh explained the
verification methodologies with an example of implementing a DVD
player. Simulation and formal verification techniques were compared,
with an overview on model checking. Prof. Saluja explained the basics
of VLSI testing, differences between verification and testing, and the
various testing techniques used. The challenges in VLSI testing were
also discussed.

Day 2: Tutorial

On the second day, I attended the tutorial on "Formal Techniques for
Hardware/Software Co-Verification" by <a
href="http://www.kroening.com/">Prof. Daniel Kroening</a>, and <a
href="http://www.cmi.ac.in/people/fac-profile.php?id=mksrivas">Prof. Mandayam
Srinivas</a>. Prof. Kroening began the tutorial with the motivation for
formal methods. Examples on SAT solvers, boundary model checking for
hardware, and bounded program analysis for C programs were
explained. Satisfiability modulo theories for bit-vectors, arrays and
functions were illustrated with numerous examples. In the afternoon,
Prof. Srinivas demoed formal verification for both Verilog and C. He
shared the results of verification done for both a DSP and a
microprocessor. The <a href"http://www.cprover.org/">CProver</a> tool
has been released under a CMBC license. After discussion with Fedora
Legal, and Prof. Kroening, it <a
href="http://lists.fedoraproject.org/pipermail/legal/2013-January/002067.html">has
been updated to a BSD license for inclusion in Fedora</a>. The <a
href="http://www.cprover.org/VLSI2013/">presentation slides</a> used in
the tutorial are available.

Day 3: Main conference

The first day of the main conference began with the keynote by <a
href="http://www.lsi.com/about/ourstory/pages/management.aspx#Abhi">Mr. Abhi
Talwalker</a>, CEO of LSI, on "Intelligent Silicon in the Data-centric
Era". He addressed the challenges in bridging the data deluge gap,
latency issues in data centers, and energy efficient buildings. The
second keynote of the day was given by <a
href="http://en.wikipedia.org/wiki/Ruchir_Puri">Dr. Ruchir Puri</a>,
IBM Fellow, on "Opportunities and Challenges for High Performance
Microprocessor Designs and Design Automation". Dr. Ruchir spoke about
the various IBM multi-core processors, and the challenges facing
multi-core designs - software parallelism, socket bandwidth, power, and
technology complexity. He also said that more EDA innovation needs to
come at the system level.

<img alt="Dias"
src="http://shakthimaan.com/downloads/glv/2013/vlsid-2013/dias.JPG"></img>

After the keynote, I attended the "C1. Embedded Architecture" track
sessions. <a href="http://www.cse.unsw.edu.au/~liangt/">Liang Tang</a>
presented his paper on "Processor for Reconfigurable Baseband
Modulation Mapping". <a
href"http://www.pdn.ac.lk/eng/pages/departmentHome/CE/otherpages/staff/Dr.Swarnalatha%20Radhakrishnan.html">Dr. Swarnalatha
Radhakrishnan</a> then presented her paper on "A Study on
Instruction-set Selection Using Multi-application Based Application
Specific Instruction-Set Processors". She explained about ASIPs
(Application Specific Instruction Set Processor), and shared test
results on choosing specific instruction sets based on the application
domain. The final paper for the session was presented by <a
href="http://www.princeton.edu/~jha/">Prof. Niraj K. Jha</a> on
"Localized Heating for Building Energy Efficiency". He and his team at
Princeton have used ultrasonic sensors to implement localized
heating. A similar approach is planned for lighting as well.

Post-lunch, I attended the sessions for the track "B2. Test Cost
Reduction and Safety". The honourable chief minister of Maharashtra,
<a
href="http://en.wikipedia.org/wiki/Prithviraj_Chavan">Shri. Prithviraj
Chavan</a>, arrived in the afternoon to formally inaugurate the
conference. He is an engineer who graduated from the University of
California, Berkeley, and said that he was committed to put Pune on
the semiconductor map. The afternoon keynote was given by <a
href="http://investor.marvell.com/phoenix.zhtml?c=120802&p=irol-govBio&ID=198477">Mr. Kishore
Manghnani</a> from Marvell, on "Semiconductors in Smart Energy
Products". He primarily discussed about LEDs, and their
applications. This was followed by a panel discussion on "Low power
design". There was an emphasis to create system level, software
architecture techniques to increase leverage in low power design. For
the last track of the day, I attended the sessions on "C3. Design and
Synthesis of Reversible Logic". The <a
href="http://keccak.noekeon.org/">Keccak</a> sponge function family
has been chosen to become the SHA-3 standard.

Day 4: Main conference

The second day of the main conference began with a recorded keynote by
<a
href="http://investor.appliedmicro.com/phoenix.zhtml?c=78121&p=irol-govBio&ID=192748">Dr. Paramesh
Gopi</a>, AppliedMicro, on "Cloud computing needs at less power and
low cost" followed by a talk by <a
href="http://phx.corporate-ir.net/phoenix.zhtml?c=78121&p=irol-govManage">Mr. Amal
Bommireddy</a>, AppliedMicro, on "Challenges of First pass
Silicon". Mr. Bommireddy discussed the factors affecting first pass
success - RTL verification, IP verification, physical design, routing
strategies, package design, and validation board design. The second
keynote of the day was by <a
href="http://www.hhmi.org/research/fellows/scheffer_bio.html">Dr. Louis
Scheffer</a> from the Howard Hughes Medical Institute, on "Deciphering
the brain, cousin to the chip". It was a brilliant talk on applying
chip debugging techniques to inspect and analyse how the brain works.

After the keynote, I visited the exhibition hall where companies had
their products displayed in their respective stalls. AppliedMicro had
a demo of their <a
href="http://www.apm.com/products/x-gene">X-gene</a> ARM64 platform
running Ubuntu. They did mention to me that Fedora runs on their
platform. Marvell had demonstrated their embedded and control
solutions running on Fedora. ARM had their <a
href="http://mbed.org">mbed.org</a> and <a
href="http://embeddedacademic.com">embeddedacademic.com</a> kits on
display for students. Post-lunch, was an excellent keynote by <a
href="http://www.intel.com/jobs/virtualevent/bio/singh.htm">Dr. Vivek
Singh</a>, Intel Fellow, titled "Duniyaa Maange Moore!". He started
with what people need - access, connectivity, education, and
healthcare, and went to discuss the next in line for Intel's
manufacturing process. The 14nm technology is scheduled to be
operational by end of 2013, while 10nm is planned for 2015. They have
also started work on 7nm manufacturing processes. This was followed by
a panel discussion on "Expectations of Manufacturing Sector from
Semiconductor and Embedded System Companies" where the need to bridge
the knowledge gap between mechanical and VLSI/embedded engineers was
emphasized.

Day 5: Main conference

The final day of the main conference began with the keynote by <a
href="http://www.cse.psu.edu/~vijay/">Dr. Vijaykrishnan Narayanan</a>
on "Embedded Vision Systems", where he showed the current research in
intelligent cameras, augmented reality, and interactive systems. I
attended the sessions for the track "C7. Advances in Functional
Verification", and "C8. Logic Synthesis and Design". Post-lunch, <a
href="http://www.linkedin.com/pub/ken-chang/4/279/2b4">Dr. Ken
Chang</a> gave his keynote on "Advancing High Performance
System-on-Package via Heterogeneous 3-D Integration". He said that
Intel's 22nm Ivy Bridge which uses FinFETs took nearly 15 years to
productize, but look promising for the future. Co(CoS) Chip on Chip on
Substrate, and (CoW)oS Chip on Wafer on Substrate technologies were
illustrated. Many hardware design houses use 15 FPGAs on a board for
testing. The Xilinx Virtex-7HT FPGA has analog, memory, and ARM
microprocessor integrated on a single chip giving a throughput of 2.8
Terabits/second. He also mentioned that Known Good Die (KGD)
methodologies are still emerging in the market. For the last track of
the conference, I attended the sessions on "C9. Advances in Circuit
Simulation, Analysis and Design".

Thanks to Red Hat for sponsoring me to attend the conference.
