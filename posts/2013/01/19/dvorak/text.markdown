---
author: SK
tags: 2013, documentation, gnome
timestamp: 11:20:00
title: Dvorak keyboard layout
---

> "The arrangement (QWERTY) of the letters on a typewriter is an example
>  of the success of the least deserving method." ~ Nassim Nicholas Taleb

> "With early typewriters the mechanical arms would jam if two
>    letters were hit in too rapid a sequence. So the classic QWERTY
>    keyboard was designed to 'slow down' typing." ~ Edward de Bono

The continuous use of the QWERTY keyboard causes pain, and I am forced
to rest my fingers. While it is good to take a break, it shouldn't be
done for the wrong reason. I started to look for alternate keyboard
layouts to use, and a typing tutor to practise with. Dr. August Dvorak
and Dr. William Dealey completed the Dvorak simplified keyboard layout
in 1932.

<img alt="Dvorak keyboard layout"
src="http://upload.wikimedia.org/wikipedia/commons/2/25/KB_United_States_Dvorak.svg"></img>

To add the the Dvorak keyboard layout to Gnome, select Applications ->
System Tools -> System Settings. Choose "Region and Language". Under
the "Layout" tab, add Dvorak (English). Klavaro is a typing tutor that
is available for Fedora. You can install it using:

~~~~ {.shell}
$ sudo yum install klavaro
~~~~

There are five levels in Klavaro - introduction, basic course,
adaptability, speed, and fluidity. After adding the Dvorak keyboard
layout on Fedora, I started the exercises in Klavaro. At home I used
Dvorak, while at work I used QWERTY. I was able to quickly reach 30
words per minute (wpm) with Dvorak. When I tried to go beyond 40 wpm,
I was unconsciously still thinking, and using the QWERTY keyboard. To
break that barrier, I switched full-time to use Dvorak, even at
work. Speed was slow, initially, but after a month of practise I
passed all the exercises in Klavaro.

<img alt="Klavaro progress chart"
src="http://shakthimaan.com/downloads/screenshots/klavaro-progress.png"></img>

If you are not familiar with touch typing, it is best to start with
Dvorak. I can now type continuously for hours, and my fingers don't
hurt. I also do take breaks occasionally.

Give it a try!

