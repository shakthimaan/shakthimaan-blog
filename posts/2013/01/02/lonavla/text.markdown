---
author: SK
tags: 2013, travel
timestamp: 06:50:00
title: Lonavla
---

<img alt="Pune-Mumbai expressway at Khandala"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album85/39_pune_mumbai_expressway_at_khandala.jpg"></img><br />

<img alt="Karla caves"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album85/13_karla_caves.jpg"></img><br />

<img alt="Rabindranath Tagore wax model"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album85/22_tagore_wax.jpg"></img><br />

More photos available in my <a href="http://www.shakthimaan.com/Mambo/gallery/album85">/gallery</a>.