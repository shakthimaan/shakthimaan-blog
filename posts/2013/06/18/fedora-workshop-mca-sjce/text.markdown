---
author: SK
tags: 2013, fedora
timestamp: 11:20:00
title: Fedora Workshop, SJCE, Chennai
---

A Fedora workshop was organised at <a
href="http://stjosephs.ac.in/">St. Joseph's College of
Engineering</a>, Chennai, Tamil Nadu, India on Friday, June 14,
2013. The participants were students from the Master of Computer
Applications (MCA) department.

The forenoon session began with an introduction to Free/Open Source
Software (F/OSS) and Fedora. I explained the various <a
href="http://shakthimaan.com/downloads.html#i-want-2-do-project-tell-me-wat-2-do
">project and communication guidelines</a> that students need to
follow, and mentioned the various <a
href="http://fedoraproject.org/wiki/Projects">Fedora sub-projects</a>
that they can contribute to. System architecture, and compilation
concepts were also discussed.  The need to use free and open standards
was emphasized. Copyright, and licensing were briefly addressed.

<img alt="MCA lab"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album93/6_lab_session.jpg"></img><br
/>

After lunch, a programming lab session was held to see how students
solve problems. Their code was reviewed, and suggestions for
improvement were given. <a
href="http://klavaro.sourceforge.net/en/">Klavaro</a> was shown to
students to learn touch typing. I also gave an overview of GCC using
the <a
href="http://shakthimaan.com/downloads.html#introduction-to-gcc">"Introduction
to GCC"</a> presentation. The concept of using revision control
systems was illustrated. A demo of Fedora 18 (x86_64) was shown, and
the same was installed on the lab desktops.

Thanks to Prof. Parvathavarthini Mam for working with me in organizing
this workshop. Thanks also to Prof. Shirley for managing the
logistics.

Few photos taken during the trip are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album93">/gallery</a>.
