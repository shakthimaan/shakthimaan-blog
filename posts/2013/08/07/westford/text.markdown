---
author: SK
tags: 2013, travel
timestamp: 00:40:00
title: Westford, Massachusetts
---

<img alt="Red Hat, Westford"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album94/8_rh_westford_office.jpg"></img><br />

<img alt="Residence Inn"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album94/1_ri_entrance.jpg"></img><br />

<img alt="Boeing Lufthansa"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album94/27_boeing_lufthansa.jpg"></img><br />

More photos available in my <a href="http://www.shakthimaan.com/Mambo/gallery/album94">/gallery</a>.
