---
author: SK
tags: 2013, fedora
timestamp: 08:40:00
title: Fedora Activity Day (FAD), Mysore
---

A <a href="https://fedoraproject.org/wiki/FAD_Mysore_2013">Fedora
Activity Day</a> was held at <a href="http://www.sjce.ac.in/">Sri
Jayachamarajendra College Of Engineering</a>, Mysore, Karnataka, India
on Saturday, April 20, 2013.

<img alt="SJCE"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album92/6_sjce.jpg"></img><br />

The agenda included talks in the morning, and practical sessions in
the afternoon. I started the day's proceedings on best practices to be
followed when working with free/open source software projects, giving
examples on <a
href="http://shakthimaan.com/downloads.html#i-want-2-do-project-tell-me-wat-2-do">effective
project, and communication guidelines</a>. The various <a
href="http://fedoraproject.org/wiki/Projects">Fedora sub-projects</a>
that students can contribute to were mentioned. This was followed by
an introductory session on Python by <a
href="http://aravindavk.in/">Aravinda V K</a>. The <a
href="http://shakthimaan.com/downloads.html#python-introduction-for-programmers">"Python:
Introduction for Programmers" presentation</a> was given to the
students. <a href="https://github.com/vijaykumar-koppad">Vijaykumar
Koppad</a> then gave an overview, and a demo of the <a
href="http://www.gluster.org/">Gluster file system</a>.

After lunch, we had a Q&A session with the participants. Questions on
working with free/open source software projects, differences between
file systems, GNU/Linux distributions, and programming languages were
answered. Basic installation and troubleshooting techniques were
discussed. I addressed system architecture design concepts,
compilation, cross-compilation, and revision control systems, and
briefed them on copyright, and licensing. Students had brought their
laptops to work on Python scripting, and GlusterFS. I also worked on
few bug fixes, package builds for ARM, and package updates:

* Bug 928059 - perl-Sys-CPU 0.54 tests fail on ARM
* Bug 926079 - linsmith: Does not support aarch64 in f19 and rawhide
* Bug 925483 - gputils: Does not support aarch64 in f19 and rawhide
* Bug 922397 - flterm-debuginfo-1.2-1 is empty
* Bug 925202 - csmith: Does not support aarch64 in f19 and rawhide
* Bug 925247 - dgc: Does not support aarch64 in f19 and rawhide
* Bug 925208 - CUnit: Does not support aarch64 in f19 and rawhide
* Bug 901632 - ghc-smallcheck-1.0.2 is available
* Bug 926213 - nesc: Does not support aarch64 in f19 and rawhide
* Bug 953775 - ghc-data-inttrie-0.1.0 is available

Thanks to <a href="http://vbellur.wordpress.com/">Vijay Bellur</a> and
Vijaykumar Koppad for working with me in organizing this
workshop. Thanks also to the Fedora project for sponsoring my travel
and accommodation.

Few photos taken during the trip are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album92">/gallery</a>.