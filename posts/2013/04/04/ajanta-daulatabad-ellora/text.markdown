---
author: SK
tags: 2013, travel
timestamp: 01:40:00
title: Ajanta-Daulatabad-Ellora
---

<img alt="Ajanta caves"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album89/25_entrance_to_cave_19.jpg"></img><br />

<img alt="Daulatabad fort"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album90/2_daulatabad_fort.jpg"></img><br />

<img alt="Ellora caves"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album91/42_pillar.jpg"></img><br />

More photos available in the respective albums - <a
href="http://www.shakthimaan.com/Mambo/gallery/album89">Ajanta
caves</a>, <a
href="http://www.shakthimaan.com/Mambo/gallery/album90">Daulatabad
fort</a>, and <a
href="http://www.shakthimaan.com/Mambo/gallery/album91">Ellora caves</a>.