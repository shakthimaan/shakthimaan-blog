---
author: SK
tags: 2013, fedora
timestamp: 16:30:00
title: Fedora OpenStack workshop, P.E.S. College of Engineering, Aurangabad
---

I had organized a <a
href="http://fedoraproject.org/wiki/OpenStack">Fedora and
OpenStack</a> workshop at <a
href="http://www.pescoe.ac.in/">P.E.S. College of Engineering, Nagsen
Vana, Aurangabad</a>, Maharashtra on Saturday, March 2, 2013.

<img alt="P.E.S. College of Engineering"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album88/7_college_facade.jpg"></img>

After a formal inauguration at 1000 IST, I introduced the students to
communication guidelines, mailing list etiquette, and project
guidelines using the <a
href="http://shakthimaan.com/downloads.html#i-want-2-do-project-tell-me-wat-2-do">"i-want-2-do-project. tell-me-wat-2-do"</a>
presentation. The different <a
href="http://fedoraproject.org/wiki/Projects">Fedora sub-projects</a>
to which they can contribute to were mentioned.  I showed the various
free/open source software tools available for them to learn and
use. The career options with free/open source software were also
discussed. I had asked them to write down any questions they had on
the forenoon session, so I could answer them in the afternoon
session. Few of their questions:

* If I do a project in Java, what are my career options?
* What is the difference between open source and Microsoft?
* Is Linux popular only because of security, or are there other reasons too?
* I am interested in mainframes. How should I learn?
* I am interested in a career in animation. What free/open source
software can I use?
* What are the steps to become a good software engineer?
* Can I patent a software product?

Post-lunch, I answered their queries in the Q&A session, to the best
of my knowledge. I also gave them an introduction on copyright,
trademark and patents, and mentioned that IANAL. I then introduced
them to the architecture of OpenStack, explaining the individual
components, and their functionality. The <a
href="http://fedorapeople.org/~russellb/openstack-lab-rhsummit-2012/index.html">OpenStack
Lab Guide</a> was provided to them to setup their own OpenStack
cloud. Some of them had brought their laptops to try it hands-on. I
demonstrated the Horizon web interface after starting the required
services.

<img alt="Fedora Labs"
src="http://www.shakthimaan.com/Mambo/gallery/albums/album88/9_fedora_lab.jpg"></img>

All their computer labs have been migrated to Fedora 17. Thanks to
Prof. Nitin Ujgare for working with me in organizing this workshop,
and for maintaining the Fedora labs at the Institute. Aurangabad is
around 230 km from Pune, and takes around 4 1/2 hours by road. There
are frequent bus services between Pune and Aurangabad. You can book
bus tickets at <a
href="http://www.msrtc.gov.in/msrtc_live/index.html">Maharashtra State
Road Transport Corporation (MSRTC)</a> web site. There are a number of
historic places to visit in and around Aurangabad. Few photos taken
during the trip are available in my <a
href="http://www.shakthimaan.com/Mambo/gallery/album88">/gallery</a>.






 