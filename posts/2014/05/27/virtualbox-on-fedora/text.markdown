---
author: SK
tags: 2014, fedora
timestamp: 17:26:00
title: Installation of VirtualBox on Fedora 20
---

Steps to install VirtualBox 4.3.10 on Fedora 20. You need to 
first open a terminal and become root user:

~~~~ {.bash}
$ su -
Password:
#
~~~~

Install dkms package:

~~~~ {.bash}
# yum install dkms
~~~~

If Virtual Machine Manager is running, stop the same, 
and uninstall it.

~~~~ {.bash}
# yum remove virt-manager
~~~~

Remove the KVM modules if already loaded:

~~~~ {.bash}
# rmmod kvm_intel
~~~~

Download and install rpmfusion-free repo from rpmfusion.org:

~~~~ {.bash}
# yum install rpmfusion-free-release-20.noarch.rpm
~~~~

Install VirtualBox:

~~~~ {.bash}
# yum install VirtualBox
~~~~ 

Install the required VirtualBox kernel module for your running
kernel. For example, on Fedora 20 with kernel 3.11.10-301, you can
run:

~~~~ {.bash}
# yum install kmod-VirtualBox-3.11.10-301.fc20.x86_64
~~~~

Load the *vboxdrv* driver:

~~~~ {.bash}
# modprobe vboxdrv
~~~~

You can now start VirtualBox and use it. To convert Virt-manager 
images to VirtualBox, you can use:

~~~~ {.bash}
$ VBoxManage convertdd ubuntu1204-lts.img ubuntu1204-lts.vdi
~~~~

