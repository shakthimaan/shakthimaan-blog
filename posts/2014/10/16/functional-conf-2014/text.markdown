---
author: SK
tags: 2014, travel, documentation
timestamp: 10:00:00
title: Functional Conf 2014, Bengaluru
---

I attended [Functional Conf 2014](http://functionalconf.com/) between
October 9-11, 2014 at Hotel Chancery Pavilion, Bengaluru.

Day I

<img alt="Stage" src="http://shakthimaan.com/images/functionalconf-2014/fc2014-stage.JPG"></img>

The first day began with the keynote by Dr. Venkat Subramaniam on the
["The Joy of Functional
Programming"](http://confengine.com/functional-conf-2014/proposal/420/keynote-the-joy-of-functional-programming). The
talk was centered around what is 'mainstream', and why something that
is mainstream is not necessarily the ideal approach. He gave examples
on writing functional programs in Java, and how these code expressions
are easy to read and test.

I then attended the talk on ["Functional Reactive UIs with
Elm"](http://confengine.com/functional-conf-2014/proposal/329/functional-reactive-uis-with-elm)
by Shashi Gowda. He gave an introduction to the [Elm functional
programming language](http://elm-lang.org/) with UI reactive
examples. The syntax of Elm is similar to that of Haskell. The idea is
to write less code for creating interactive applications, and Elm
generates HTML, CSS and Javascript. There are also [d3.js bindings for
Elm](https://github.com/seliopou/elm-d3).

The talk on ["Applying functional programming principles to large
scale data
processing"](http://confengine.com/functional-conf-2014/proposal/354/applying-functional-programming-principles-to-large-scale-data-processing)
by Kishore Nallan from Indix introduced the "Lambda
Architecture". They scrap product details from web pages worldwide,
and receive 4 TB of data every day. The architecture uses an
append-only database, and has multiple readers and views for the
data. You can check their
[engineering.indix.com/blog](http://engineering.indix.com/blog/) for
more information on their attempts to process large
data. [Scalding](https://github.com/twitter/scalding),
[Cascading](http://www.cascading.org/), [Apache
Spark](https://spark.apache.org/), and
[Storm](https://github.com/apache/storm) are tools that they are
experimenting with.
  
Thomas Gazagnaire talk on ["Compile your own cloud with Mirage OS
v2.0"](http://confengine.com/functional-conf-2014/proposal/476/compile-your-own-cloud-with-mirage-os-v20)
was very interesting on how they stripped down the entire OS and
applications, and re-wrote them in OCaml for use in production
environments. The Mirage OS is a unikernel and targets the Xen
hypervisor. It is type safe, and faster to deploy and use. It uses
light-weight threads. [OPAM](https://opam.ocaml.org/) is the OCaml
Package Manager. [IRMIN](https://github.com/mirage/irmin) is an
example of a distributed database implemented in OCaml. The TLS
protocol was implemented in pure OCaml and deployed as a service. The
demo server is available at
[tls.openmirage.org](https://tls.openmirage.org/).

["Property based testing for functional domain
models"](http://confengine.com/functional-conf-2014/proposal/386/property-based-testing-for-functional-domain-models)
by Debasish Ghosh introduced the concept of generative data for tests
in Scala. The idea is from the QuickCheck library and property-based
testing in Haskell. This allows you to focus on executable domain
rules. We can get some properties for free in statically typed
languages. He mentioned
[shapeless](https://github.com/milessabin/shapeless), a type class and
dependent type generic programming library for Scala, and also
[Algebird](https://github.com/twitter/algebird), which provides
Abstract Algebra for Scala.

Vagmi Mudumbai wrote a simple TODO MVC web application using
[ClojureScript](http://clojure.org/clojurescript) and
[Om](https://github.com/swannodette/om) in his ["ClojureScript and Om
- Pragmatic functional programming in the Javascript
Land"](http://confengine.com/functional-conf-2014/proposal/326/clojurescript-and-om-pragmatic-functional-programming-in-the-javascript-land)
talk. Clojure and ClojureScript can help you write concise code for
the problem you want to solve. The immutability of Clojure and the DOM
manipulation mutability of
[React.js](http://facebook.github.io/react/) complement each other
well in implementing performance-savvy web sites.

["Learning (from) Haskell - An experience
report"](http://www.confengine.com/functional-conf-2014/proposal/320/learning-from-haskell-an-experience-report)
by Aditya Godbole was an attempt to teach functional problem solving
using
[Gofer](http://en.wikipedia.org/wiki/Gofer_%28programming_language%29)
in an organization, and the lessons learnt.

At the end of the day, a [Fish
Bowl](http://en.wikipedia.org/wiki/Fishbowl_(conversation)) was
organized where people discussed the choice of functional programming
language for development, and the also shared their experiences on how
they solved problems in the industry using functional programming.

After dinner, there was a
[BoF](http://en.wikipedia.org/wiki/Birds_of_a_feather_(computing))
session on Clojure, but, it ended with mostly discussions on different
programming paradigms, and functional programming languages.

Day II

The first keynote on the second day was by Bruce Tate on ["Fear: The
Role of Fear in Language
Adoption"](http://confengine.com/functional-conf-2014/proposal/711/fear-the-role-of-fear-in-language-adoption). He
classified the challenges in moving to functional programming into two
categories - paralyzing fears and motivational fears. The paralyzing
fears are on adoption, cost and getting developers. These can be
overcome by building communities, having object-oriented languages
implement functional programming features, better deployment options,
and with cleaner interfaces. The motivating fears can be overcome by
the need for handling code complexity, software to run for multi-core
and large distributed systems, and for solving complex problems. He
also mentioned that he sees three large programming language
communities today - a hybrid, only functional programming, and the
JavaScript land.

<img alt="Morten" src="http://shakthimaan.com/images/functionalconf-2014/fc2014-morten.JPG"></img>

Morten Kromberg introduced APL (A Programming Language) and Dyalog in
his ["Pragmatic Functional Programming using
Dyalog"](http://confengine.com/functional-conf-2014/proposal/436/pragmatic-functional-programming-using-dyalog)
talk. APL was invented by Kenneth E. Iverson, an ACM Turing award
recipient. [Conventions Governing Order of
Evaluation](http://jsoftware.com/papers/EvalOrder.htm) by Kenneth
explains the context and need for APL. You can try the language using
the online REPL at [tryapl.org](http://tryapl.org). There are no
reserved keywords in this language. Morten also gave a demo of
MiServer which is a free and open source web server written in APL. A
number of libraries are also available at
[tools.dyalog.com/library/](http://tools.dyalog.com/library/).

Tejas Dinkar talked on ["Monads you already use (without knowing
it)"](http://confengine.com/functional-conf-2014/proposal/437/monads-you-already-use-without-knowing-it)
where he tried to mimic the functionality of Haskell Monads in Ruby,
but, there are differences in their actual implementation.

["Purely functional data structures
demystified"](http://confengine.com/functional-conf-2014/proposal/325/purely-functional-data-structures-demystified)
by Mohit Thatte was an excellent talk that illustrates the thesis on
[Purely Functional Data
Structures](https://www.cs.cmu.edu/~rwh/theses/okasaki.pdf) by Chris
Okasaki. Mohit explained how data structures can be built on existing
lists, and structural decomposition is an effective way to model
complex data. An abstract data type (ADT) can thus be structurally
decomposed using list data structures. Every abstract data type can be
defined by its operations and invariants. For example, the stack has
both push and pop operations, and the invariant is the property of a
stack to follow Last In First Out (LIFO). Most programming languages
don't have an expressive power to specify the invariants. He explained
functional data structures built with Clojure in simple words, and
gave plenty of examples to illustrate the concepts.

I had a chance to meet Ramakrishnan Muthukrishnan, who has been a
Debian contributor since 2001. Ramakrishnan's talk was ["An
introduction to Continuation Passing Style
(CPS)"](http://confengine.com/functional-conf-2014/proposal/359/-an-introduction-to-continuation-passing-style-cps)
using the Scheme programming language. A continuation is what is
needed to complete the rest of the computation. It provides an
alternative model for the use of stacks between function calls. He
gave plenty of examples on how to convert an existing piece of code
into CPS. Will Byrd's Google Hangout talk on [Introduction to
Continuations, call/cc, and
CPS](http://www.youtube.com/watch?v=2GfFlfToBCo) was recommended.

["Elixir Today: a round-up on state of Elixir and it's
ecosystem"](http://confengine.com/functional-conf-2014/proposal/452/elixir-today-a-round-up-on-state-of-elixir-and-its-ecosystem)
talk by Akash Manohar gave an introduction to the Elixir programming
language. The language is built on the Erlang VM, and the community
has taken lot of goodies from the Ruby
world. [Mix](http://elixir-lang.org/docs/stable/mix/) is the tool used
to create, build and test Elixir projects, and [Hex](http://hex.pm/)
is the package manager used for Erlang. Elixir code can be deployed to
Heroku using a buildpack. A number of useful software are already
available - [Poison](https://github.com/devinus/poison) is a JSON
parser, [Hound](https://github.com/HashNuke/hound) for browser
automation and integration tests,
[Ecto](https://github.com/elixir-lang/ecto) is a DSL for communicating
with databases, [Phoenix](https://github.com/phoenixframework/phoenix)
is a web frawework for real-time, fault-tolerant applications, and
[Calliope](https://github.com/nurugger07/calliope) is an Elixir HAML
parser.

The final keynote of the conference was a brilliant talk by Daniel
Steinberg on ["Methodologies, Mathematics, and the Metalinguistic
Implications of
Swift"](http://confengine.com/functional-conf-2014/proposal/712/methodologies-mathematics-and-the-metalinguistic-implications-of-swift). He
began on how people learn mathematics, and why we should reconsider
the way we teach geometry. He emphasized that we always try to learn
from someone in school (games, for example). Instead of presenting a
programming language grammar, the rules can be presented in a playful
way. Individuals and interactions are very important in problem
solving. Math has a soul and it is beautiful. After providing proofs
in geometry with beautiful illustrations, he goes on to say that there
are things in mathematics that we can prove, and things that we cannot
prove, and we have to accept that, giving examples from the Swift
programming language. This was the best talk in the conference.

Day III

<img alt="Banner" src="http://shakthimaan.com/images/functionalconf-2014/fc2014-banner.JPG"></img>

I attended the "Clojure Deep-dive" workshop by Baishampayan Ghose
(BG). He started with the basics of Clojure and an introduction to
functional style of programming. Clojure is opinionated, and the
emphasis is on simplicity and fast problem solving. It involves
programming at the speed of thought, and aims to make you more
productive.

We used the Clojure REPL to practice writing simple functions. You
need to determine the data structures that you want to use first, and
then work on writing functions. In Clojure, data is of greater
importance than code. The [Emacs Clojure Starter
Kit](https://github.com/technomancy/emacs-starter-kit) can be used to
get the required environment to work with the
REPL. [Clojuredocs](http://clojuredocs.org/) is a useful quick
reference.

We then worked on solving a real problem of counting the most
frequently used words from the 'Alice in Wonderland' book from
[Project Gutenberg](http://www.gutenberg.org/). BG then explained the
use and power of macros, multimethods, and concurrency capabilities in
Clojure. Macros allow DSLs to be represented in the Clojure language
itself. There is a [core.typed](https://github.com/clojure/core.typed)
library for static
typing. [core.async](https://github.com/clojure/core.async) can be
used for asynchronous
programming. [Enlive](https://github.com/cgrand/enlive) is a selector
based (CSS) templating library, and
[Ring](https://github.com/ring-clojure/ring) is a Clojure web
applications library. You can find more goodies from the [The Clojure
Toolbox](http://www.clojure-toolbox.com/).

A couple of online resources to get started with Clojure are [Clojure
for the Brave and True](http://www.braveclojure.com/) and [Clojure
from the Ground Up](http://aphyr.com/tags/Clojure-from-the-ground-up).

The video recordings of the talks should be made available in YouTube.

I would like to thank [Manufacturing System
Insights](http://systeminsights.com/) for sponsoring my trip to the
conference.
