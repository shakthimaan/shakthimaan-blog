---
author: SK
tags: 2014, foss, vlsi
timestamp: 15:30:00
title: Eqntott: Truth Table Generator from Boolean Equations
---
*[Published in Electronics For You (EFY) magazine, March 2013 edition.]* [Source](http://electronicsforu.com/electronicsforu/circuitarchives/view_article.asp?sno=2163&id=13594)

This tutorial demonstrates the use of eqntott software with some code
examples.

eqntott (short for 'equation to truth table') is a software tool that
can generate truth tables from Boolean equations, which can be used
for programmable logic array (PLA) programming. It was initially
written at Berkeley, and ported to work on GNU/Linux. It is released
under the new BSD license.

Consider a simple AND example:

~~~~ {.shell}
NAME = and;
INORDER = a b;
OUTORDER = c;

c = a & b;
~~~~

Here NAME refers to the name of the PLA, and is called 'and'. INORDER
lists the input order of elements, while OUTORDER has the list of
outputs in the truth table. Copy this example in a text editor and
save it as 'and.eqn.'

To run the above example, use 'cd' command in order to go to the
directory where the example is saved. Now run the following command:

~~~~ {.shell}
$ eqntott and.eqn
~~~~

You will get the output as:

~~~~ {.shell}
.i 2
.o 1
.p 1
11  1 
.e
~~~~

'.i' in the output refers to the number of inputs defined, which
refers to 'a' and 'b' in the above example. '.o' corresponds to the
number of output variables, which is 'c'. '.p' is the number of
product terms. The truth table generated shows that the output is '1'
when bothe inputs are '1'.

You can use '-l' option with eqntott to output the PLA name, input,
and output elements.

~~~~ {.shell}
$ eqntott -l and.eqn
~~~~

The output will be:

~~~~ {.shell}
.i 2
.o 1
.na and
.ilb  a b
.ob  c
.p 1
11  1 
.e
~~~~

The name of the PLA is mentioned next to '.na' in the output. The
order of inputs and outputs in the truth table is also listed.

The following expressions are allowed in the equations for eqntott:

----------- --------------------------------
Expression  Meaning
----------- ---------------------------------
&           Logical AND operation

|           Logical OR operation

!           Logical NOT operation

ZERO or 0   False or the value zero

ONE or 1    True or the value one

()          To enclose any expression 

?           Don't care condition

*name*      Any input/output in the expression
----------- ----------------------------------

The half adder circuit adds two binary digits, and produces a sum and
a carry. It can be implemented as follows:

~~~~ {.shell}
NAME = half_adder;
INORDER = a b;
OUTORDER = s c;
c = a & b;
s = (!a & b) | (a & !b);
~~~~

When the above equation is run with eqntott:

~~~~ {.shell}
$ eqntott -l half_adder.eqn
~~~~

The output will be:

~~~~ {.shell}
.i 2
.o 2
.na half_adder
.ilb  a b
.ob  s c
.p 3
01  1 0 
10  1 0 
11  0 1 
.e
~~~~

The sum is represented by 's' and carry with 'c.' When either the sum
or carry is '1', the sum is '1' and carry is '0'. When both the sum
and carry are '1', then the sum is '0' and carry is '1'.

The output of eqntott can be customized using the '-.key'
argument. The default option is the string 'iopte'. A few key code
options with their output meaning are given below:

--------- -----------------------------
Character Output
--------- -----------------------------
e         .e

f         .f output-number input-number

h         Human readable format

i         .i number of inputs

l         Truth table with PLA name, inputs and outputs

o         .o number of outputs

p         .p number-of-product-terms

v         eqntott version number

S         PLA connectivity summary
--------- -----------------------------

If the half adder example is run with the following key options:

~~~~ {.shell}
$ eqntott -.lhptv half_adder.eqn
~~~~

The output will be:

~~~~ {.shell}
.na half_adder
.ilb  a b
.ob  s c
2 inputs, 2 outputs, 3 product terms.
s	!a	b
s	a	!b
c	a	b
.p 3
01  1 0 
10  1 0 
11  0 1 
eqntott 9.0
~~~~

The PLA connectivity summary can be displayed using the '-.S' key code
option. When used with 'and.eqn' example as:

~~~~ {.shell}
$ eqntott -.S and.eqn
~~~~

The output will be:

~~~~ {.shell}
PLA Connectivity Summary
#pterms	input
1	b
1	a
#pterms	output
1	c
#out	#in	product term expression
1	2	a & b
~~~~

'-s' option allows you to use an output variable in another
expression. Consider, for example:

~~~~ {.shell}
NAME = s;
INORDER = a b;
OUTORDER = d;

c = a | b;
d = !c;
~~~~

When you run the above example with '-s' option as:

~~~~ {.shell}
$ eqntott -l -s s.eqn
~~~~

The output will be:

~~~~ {.shell}
.i 3
.o 2
.na s
.ilb  a b c
.ob  d c
.p 3
00-  1 0 
-1-  0 1 
1--  0 1 
.e
~~~~

When both the inputs 'a' and 'b' are zero, then the output 'c' is zero
and 'd' is '1'. If either 'a' and 'b' is '1', 'c' is '1' and 'd' is
'0'.

'-f' option allows an input to also be present in the output, and used
as though the value was observed at different times. Consider, for
example:

~~~~ {.shell}
NAME = f;
INORDER = a;
OUTORDER = a;

a = !a;
~~~~

When you execute the above example as:

~~~~ {.shell}
$ eqntott -l -f f.eqn
~~~~

The output will be:

~~~~ {.shell}
.i 1
.o 1
.na f
.ilb  a
.ob  a
.p 1
.f 1 1
0  1 
.e
~~~~

The truth table output is '1' only when the input is '0'. The '-s' and
'-f' options are mutually exclusive.

eqntott can attempt to minimize the logic terms in the equations. '-r'
option will try to reduce the minterms in the output. Consider the
example:

~~~~ {.shell}
NAME = reduce;
INORDER = x y;
OUTORDER = z;

z = x & y | x;
~~~~

When you use the above example with '-l' option as:

~~~~ {.shell}
$ eqntott -l reduce.eqn
~~~~

The output will be:

~~~~ {.shell}
.i 2
.o 1
.na reduce
.ilb  x y
.ob  z
.p 2
1-  1 
11  1 
.e
~~~~

Using '-r' option, eqntott will try to minimize the truth table
output:

~~~~ {.shell}
$ eqntott -l -r reduce.eqn
~~~~

The output will be:

~~~~ {.shell}
.i 2
.o 1
.na reduce
.ilb  x y
.ob  z
.p 1
1-  1 
.e
~~~~

The output 'z' is '1' whenever 'x' is '1'.

You can define expressions using 'define' keyword. The XOR logic takes
two binary inputs, and returns '1' only when either input is '1' but
not both. It can be expressed as:

~~~~ {.shell}
#define xor(a,b) (a&!b | !a&b)

NAME = xor;
INORDER = a b;
OUTORDER = c;

c = xor (a,b);
~~~~

On running the example as:

~~~~ {.shell}
$ eqntott -l xor.eqn
~~~~

The output will be:

~~~~ {.shell}
.i 2
.o 1
.na xor
.ilb  a b
.ob  c
.p 2
01  1 
10  1 
.e
~~~~

As a final example, consider generating an odd parity bit for three
inputs. The parity bit is set to '1' when the number of '1's' in the
input is even, so as to make the total number of '1's' odd. We can
define the logic using:

~~~~ {.shell}
#define xor(a,b) (a&!b | !a&b)

NAME = parity;
INORDER = x y z;
OUTORDER = p;

p = !( xor (xor(x,y), z) );
~~~~

When you run the above example with '-l' and '-s' option as:

~~~~ {.shell}
$ eqntott -l -s parity.eqn
~~~~

The output will be:

~~~~ {.shell}
.i 3
.o 1
.na parity
.ilb  x y z
.ob  p
.p 4
000  1 
011  1 
101  1 
110  1 
.e
~~~~

You can refer the manual page of eqntott for more
documentation. Sources are available from the eqntott webpage.
