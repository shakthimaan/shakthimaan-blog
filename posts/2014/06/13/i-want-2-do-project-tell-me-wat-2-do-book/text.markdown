---
author: SK
tags: 2014, foss
timestamp: 10:30:00
title: Book: i want 2 do project. tell me wat 2 do.
---

I am happy to announce my first self-published book on working with free and open source software projects, titled, "i want 2 do project.
tell me wat 2 do."

<img alt="Shaks with book" src="http://www.shakthimaan.com/images/books/shaks-with-book-min.png"></img>

Topics covered in the book:

1. Mailing List Guidelines
2. Attention to Detail
3. Project Communication
4. Project Guidelines
5. Development Guidelines
6. Methodology of Work
7. Tools
8. Reading and Writing
9. Art of Making Presentations
10. Sustenance

The product details are as follows:

* Price: ₹ 399
* Pages: 135 pages
* Publisher: Self-published (June 2014)
* Language: English
* ISBN-13: 978-93-5174-187-9
* Size: 6 x 9 inches
* Binding: Paperback (Perfect Binding)
* Availability: In Stock (Indian edition)

You can order the book at pothi.com:

<a href="http://pothi.com/pothi/book/shakthi-kannan-i-want-2-do-project-tell-me-wat-2-do">http://pothi.com/pothi/book/shakthi-kannan-i-want-2-do-project-tell-me-wat-2-do</a>

The home page for the book is at:

<a href="http://shakthimaan.com/what-to-do.html">http://shakthimaan.com/what-to-do.html</a>

If you have any comments or queries, please feel free to write to me at author@shakthimaan.com.
