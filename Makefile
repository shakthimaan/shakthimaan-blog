TOPLEVEL=.

all: blog

blog: build
	cd $(TOPLEVEL); \
		./bin/dist/build/blog/blog build

build:
	cd $(TOPLEVEL)/bin; \
		cabal configure; \
		cabal build

# upload:
# From _site directory, do
# ncftpput -u username -R ftp.domain public_html/cn/tmp .

clean:
	rm -rf _site _cache ./bin/dist
	find . -name *~ -exec rm -f {} \;
	cd $(TOPLEVEL)/bin; \
		rm -f *.o *.hi *~ blog
